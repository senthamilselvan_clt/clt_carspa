package com.clt.newcarspa.a4print;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.app.AlertDialog;


import com.clt.newcarspa.R;
import com.clt.newcarspa.model.JobCardDetailsModel;
import com.clt.newcarspa.model.JobCardItemsDetailsModel;
import com.clt.newcarspa.model.PaymentList;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Image;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import pl.allegro.finance.tradukisto.ValueConverters;

/**
 * Created by user on 10/1/2017.
 */

public class A4Printer {


    private Activity mActivity;
    private JobCardDetailsModel mCartForm;
    private List<PaymentList> mPaymentLists;

    Uri data_uri = null;
    String data_type = "application/pdf";
    private static final String printerPlugin = "com.hp.android.print";
    public static final String DEST_JOB_CARD_DETAIL = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString() + "/jobcarddetail.pdf";
    public static final String DEST_INVOICE = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString() + "/invoice.pdf";


    public A4Printer(Activity activity, JobCardDetailsModel cartForm, List<PaymentList> paymentLists) {
        mActivity = activity;
        mCartForm = cartForm;
        mPaymentLists = paymentLists;
    }


    public void printJobCardDetail() {
        try{
            File file = new File(DEST_JOB_CARD_DETAIL);
            file.getParentFile().mkdirs();
            data_uri = Uri.fromFile(file);
            generatePdfJobCardDetail();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void printInvoice() {
        try{
            File file = new File(DEST_INVOICE);
            file.getParentFile().mkdirs();
            data_uri = Uri.fromFile(file);
            generatePdfInvoice();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void generatePdfJobCardDetail() {
        try {
            File file = new File(DEST_JOB_CARD_DETAIL);
            file.getParentFile().mkdirs();
            data_uri = Uri.fromFile(file);
            createJobCardDetail(DEST_JOB_CARD_DETAIL);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }


    public void createJobCardDetail(String file) throws IOException, DocumentException {
        // step 1
        Document document = new Document();
        // step 2
        PdfWriter.getInstance(document, new FileOutputStream(file));

        // step 3
        document.open();

        //Add title
        JobCardDetailPdfUtil.addTitle(document, "JOB CARD DETAILS");

        Drawable d = mActivity.getResources ().getDrawable (R.drawable.carspa_logo);
        Bitmap bitmap = ((BitmapDrawable)d).getBitmap();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        byte[] bitmapData = stream.toByteArray();
        Image logo = Image.getInstance(bitmapData);
        //Image logo = Image.getInstance(IMAGE);
        logo.setBorder(Rectangle.NO_BORDER);
        logo.setBackgroundColor(BaseColor.WHITE);
        logo.setWidthPercentage(20f);
        logo.setScaleToFitHeight(false);




        JobCardDetailPdfUtil.addJobDetail(document, logo, mCartForm);

        //Receipt table creation
        PdfPTable receiptTable = new PdfPTable(5);
        receiptTable.setWidthPercentage(100);
        receiptTable.setWidths(new float[]{1, 5, 1.5f, 1, 1.5f});


        //Add table header
        JobCardDetailPdfUtil.addTableHeader(receiptTable);

        //Add table items
        //Log.d("A4Printer", "mCartForm.getServiceList()" + mCartForm.getServiceList());

        if (mCartForm.getItems() != null) {
            List<Item> itemList = new ArrayList<>();
            Item  item = null;

            String jsonStr=mCartForm.getItems();
            List<JobCardItemsDetailsModel> modelList =  new Gson().fromJson(jsonStr, new TypeToken<List<JobCardItemsDetailsModel>>() {
            }.getType());
            for (JobCardItemsDetailsModel service : modelList) {
                String name = service.getName();
                String price = String.valueOf(service.getPrice());
                item = new Item();
                item.setParticulars(name);
                item.setRate(getDefaultValue(price));
                item.setQty(1);
                item.setAmount(getDefaultValue(price));
                itemList.add(item);

            }
            //Add table items
            JobCardDetailPdfUtil.addTableContentItems(receiptTable, itemList);
        }
        //Add total contents
        JobCardDetailPdfUtil.addCalculationContentToTable(receiptTable, "Total", getDefaultValue(mCartForm.getTotal_amount()), Element.ALIGN_RIGHT);
        /*JobCardDetailPdfUtil.addCalculationContentToTable(receiptTable, "VAT ("+mCartForm.getVat()+"%)", getDefaultValue(mCartForm.getVat_total()), Element.ALIGN_RIGHT);
        JobCardDetailPdfUtil.addCalculationContentToTable(receiptTable, "Discount", getDefaultValue(mCartForm.getDiscount()), Element.ALIGN_RIGHT);
//        JobCardDetailPdfUtil.addCalculationContentToTable(receiptTable, "Grand Total", getDefaultValue(mCartForm.getGrand_total()), Element.ALIGN_RIGHT);
        JobCardDetailPdfUtil.addCalculationContentToTable(receiptTable, "Net Total", getDefaultValue(mCartForm.getNet_total()), Element.ALIGN_RIGHT);
        JobCardDetailPdfUtil.addCalculationContentToTable(receiptTable, "Advance Paid", getDefaultValue(mCartForm.getAdvanced()), Element.ALIGN_RIGHT);*/
        /*if (mPaymentLists !=null && mPaymentLists.size() >0 ) {
            int length = mPaymentLists.size();
            PaymentList paymentList = null;
            for (int count = 0; count < length; count++) {
                paymentList = mPaymentLists.get(count);
                if (paymentList.getAdvanced() == 0){
                    JobCardDetailPdfUtil.addCalculationContentToTable(receiptTable,
                            getFormattedDate(paymentList.getDate_added()), getDefaultValue(paymentList.getAmount()), Element.ALIGN_RIGHT);
                }
            }
        }
        JobCardDetailPdfUtil.addCalculationContentToTable(receiptTable, "Balance Due", getDefaultValue(mCartForm.getBalance()), Element.ALIGN_RIGHT, true);

        JobCardDetailPdfUtil.addAmountInWord(receiptTable, getAmountInWord((int) getDefaultValue(mCartForm.getNet_total())) + " Only");
*/
        //Add receipt table to document
        document.add(receiptTable);


        //change by kavin
        //JobCardDetailPdfUtil.addChuckLines(document, 1);

//        JobCardDetailPdfUtil.addTermsAndConditions(document);

        //PdfUtil.addTermsAndConditionsArabic(document);
//        JobCardDetailPdfUtil.addTermsAndConditionsArabic(document);



        JobCardDetailPdfUtil.addChuckLines(document, 6);

        JobCardDetailPdfUtil.addSignatures(document);



        // step 5
        document.close();


        checkIfHpEprintInstalled();
    }


    private void generatePdfInvoice() {
        try {
            File file = new File(DEST_INVOICE);
            file.getParentFile().mkdirs();
            data_uri = Uri.fromFile(file);
            createInvoice(DEST_INVOICE);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }

    private void createInvoice(String file) throws IOException, DocumentException {
        // step 1
        Document document = new Document();
        // step 2
        PdfWriter.getInstance(document, new FileOutputStream(file));

        // step 3
        document.open();

        //Add title
        InvoicePdfUtil.addTitle(document, "INVOICE");


        Drawable d = mActivity.getResources ().getDrawable (R.drawable.carspa_logo);
        Bitmap bitmap = ((BitmapDrawable)d).getBitmap();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        byte[] bitmapData = stream.toByteArray();
        Image logo = Image.getInstance(bitmapData);
        //Image logo = Image.getInstance(IMAGE);
        logo.setBorder(Rectangle.NO_BORDER);
        logo.setBackgroundColor(BaseColor.WHITE);
        logo.setWidthPercentage(20f);
        logo.setScaleToFitHeight(false);




        InvoicePdfUtil.addJobDetail(document, logo, mCartForm);

        //Receipt table creation
        PdfPTable receiptTable = new PdfPTable(5);
        receiptTable.setWidthPercentage(100);
        receiptTable.setWidths(new float[]{1, 5, 1.5f, 1, 1.5f});


        //Add table header
        InvoicePdfUtil.addTableHeader(receiptTable);

        if (mCartForm.getItems() != null) {
            List<Item> itemList = new ArrayList<>();
            Item  item = null;
            String[] serviceDetail = mCartForm.getItems().split("\n");
            for (String service : serviceDetail) {
                String name = service.substring(0,service.lastIndexOf("-"));
                String price = service.substring(service.lastIndexOf("-")+1, service.length()).trim();
                item = new Item();
                item.setParticulars(name);
                item.setRate(getDefaultValue(price));
                item.setQty(1);
                item.setAmount(getDefaultValue(price));
                itemList.add(item);

            }
            //Add table items
            InvoicePdfUtil.addTableContentItems(receiptTable, itemList);
        }

        /*Log.d("A4Printer", "mCartForm.getServiceList()" + mCartForm.getServiceList());


        if (mCartForm.getServiceList() != null) {
            List<Item> itemList = new ArrayList<>();
            Item  item = null;
            for (Service service : mCartForm.getServiceList()) {
                item = new Item();
                item.setParticulars(service.getName());
                item.setRate(getDefaultValue(service.getPrice()));
                item.setQty(1);
                item.setAmount(getDefaultValue(service.getPrice()));
                itemList.add(item);

            }
            //Add table items
            InvoicePdfUtil.addTableContentItems(receiptTable, itemList);
        }*/



        //Add total contents
        InvoicePdfUtil.addCalculationContentToTable(receiptTable, "Total", getDefaultValue(mCartForm.getTotal_amount()), Element.ALIGN_RIGHT);
        /*InvoicePdfUtil.addCalculationContentToTable(receiptTable, "VAT ("+mCartForm.getVat()+"%)", getDefaultValue(mCartForm.getVat_total()), Element.ALIGN_RIGHT);
        InvoicePdfUtil.addCalculationContentToTable(receiptTable, "Discount", getDefaultValue(mCartForm.getDiscount()), Element.ALIGN_RIGHT);
//        InvoicePdfUtil.addCalculationContentToTable(receiptTable, "Grand Total", getDefaultValue(mCartForm.getGrand_total()), Element.ALIGN_RIGHT);
        InvoicePdfUtil.addCalculationContentToTable(receiptTable, "Net Total", getDefaultValue(mCartForm.getNet_total()), Element.ALIGN_RIGHT);
        InvoicePdfUtil.addCalculationContentToTable(receiptTable, "Advance Paid", getDefaultValue(mCartForm.getAdvanced()), Element.ALIGN_RIGHT);
        if (mPaymentLists !=null && mPaymentLists.size() >0 ) {
            int length = mPaymentLists.size();
            PaymentList paymentList = null;
            for (int count = 0; count < length; count++) {
                paymentList = mPaymentLists.get(count);
                if (paymentList.getAdvanced() == 0){
                    InvoicePdfUtil.addCalculationContentToTable(receiptTable,
                            getFormattedDate(paymentList.getDate_added()), getDefaultValue(paymentList.getAmount()), Element.ALIGN_RIGHT);
                }
            }
        }
        InvoicePdfUtil.addCalculationContentToTable(receiptTable, "Balance Due", getDefaultValue(mCartForm.getBalance()), Element.ALIGN_RIGHT, true);

        InvoicePdfUtil.addAmountInWord(receiptTable, getAmountInWord((int) getDefaultValue(mCartForm.getNet_total())) + " Only");
*/
        //Add receipt table to document
        document.add(receiptTable);


        InvoicePdfUtil.addChuckLines(document, 1);

        InvoicePdfUtil.addTermsAndConditions(document);


        InvoicePdfUtil.addChuckLines(document, 6);

        InvoicePdfUtil.addSignatures(document);

        //PdfUtil.addTermsAndConditionsArabic(document);


        // step 5
        document.close();


        checkIfHpEprintInstalled();
    }


    private void callHpPrint() {
        Intent theIntent = new Intent("org.androidprinting.intent.action.PRINT");
        theIntent.setDataAndType(data_uri, data_type);
        mActivity.startActivityForResult(theIntent, 1);
    }


    private void checkIfHpEprintInstalled() {
        if (isPrinterPackageExisted(printerPlugin)) {
            callHpPrint();
        } else {
            AlertDialog alert = new AlertDialog.Builder(mActivity)
                    .setTitle("Printer Plugin")
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            final String appPackageName = printerPlugin; // getPackageName() from Context or Activity object
                            try {
                                mActivity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                            } catch (android.content.ActivityNotFoundException anfe) {
                                mActivity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
                            }
                        }
                    })
                    .setMessage("Please install the printer plugins")
                    .create();
            alert.show();

        }
    }


    private boolean isPrinterPackageExisted(String targetPackage) {
        List<ApplicationInfo> packages;
        PackageManager pm;

        pm = mActivity.getPackageManager();
        packages = pm.getInstalledApplications(0);
        for (ApplicationInfo packageInfo : packages) {
            if (packageInfo.packageName.equals(targetPackage))
                return true;
        }
        return false;
    }

    private float getDefaultValue(String value){
        float floatValue = 0f;
        try{
            floatValue = Float.parseFloat(value);
        } catch (Exception e) {
        }
        return floatValue;
    }

    private String getAmountInWord(int value) {
        ValueConverters converter = ValueConverters.ENGLISH_INTEGER;
        String valueAsWords = converter.asWords(value);
        return valueAsWords;
    }

    private String getFormattedDate(String date){
        SimpleDateFormat output = new SimpleDateFormat("dd-MM-yyyy");
//        SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd ");
        SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date getDate_addedInput= null;
        try {
            getDate_addedInput = input.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return  output.format(getDate_addedInput);
    }
}
