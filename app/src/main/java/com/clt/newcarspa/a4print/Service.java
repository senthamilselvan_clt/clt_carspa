package com.clt.newcarspa.a4print;

/**
 * Created by user on 10/1/2017.
 */

public class Service {

    private String id;
    private String job_card_id;
    private String service_id;
    private String name;
    private String price;
    private String date_added;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getJob_card_id() {
        return job_card_id;
    }

    public void setJob_card_id(String job_card_id) {
        this.job_card_id = job_card_id;
    }

    public String getService_id() {
        return service_id;
    }

    public void setService_id(String service_id) {
        this.service_id = service_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDate_added() {
        return date_added;
    }

    public void setDate_added(String date_added) {
        this.date_added = date_added;
    }
}
