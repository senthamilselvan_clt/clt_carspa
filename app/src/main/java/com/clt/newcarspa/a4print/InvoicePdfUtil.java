package com.clt.newcarspa.a4print;


import com.clt.newcarspa.model.JobCardDetailsModel;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;

import java.text.DecimalFormat;
import java.util.List;

/**
 * Created by pandiarajan on 29/9/17.
 */

public class InvoicePdfUtil {


    public static final String TERMS_AND_CONDITIONS_1 = "Management is not responsible for valuable things left in the car. The above mentioned vehicle has been received in good condition and the services have been satisfactorily carried out.";
    public static final String TERMS_AND_CONDITIONS_2 = "Carspa center is not responsible for any type of claim related to electrical, technical & mechanical faults/ defects after the service.";
    public static final String TERMS_AND_CONDITIONS_3 = "Leaving the vehicle at the site will be at the customers own risk & the vehicle should be collected on job completion date given.";
    public static final String TERMS_AND_CONDITIONS_4 = "Carspa center will not deliver vehicle after working hours 9 am to 11 pm.";
    public static final String TERMS_AND_CONDITIONS_5 = "Carspa Center employees are granted herewith the permission to operate the vehicle on roads for purpose of testing it. In addition, I declare that carspa center shall not held responsible for any loss or damage occurring while the vehicle under service.";
    public static final String TERMS_AND_CONDITIONS_6 = "Other Conditions apply";


    public static final String TERMS_AND_CONDITIONS_ARABIC_1 = "كارسبا ليست مسئولة عن فقدان أو تلف أي اغراض ثمينة داخل المركبة.";
    public static final String TERMS_AND_CONDITIONS_ARABIC_2 = "لن تكون كارسبا مسئولة عن أي نوع من المطالبات المتعلقة بالاعطال الكهربائية, التقنية أو المكيانيكية بعد الخدمة.";
    public static final String TERMS_AND_CONDITIONS_ARABIC_3 = "ترك المركبة في الموقع يكون على مسئولية العميل, ويجب استلام المركبة في التاريخ المحدد لانجاز العمل.";
    public static final String TERMS_AND_CONDITIONS_ARABIC_4 = "لا تقوم كارسبا بتسليم المركبة بعد ساعات الدوام الرسمي من 9 صباحا الى 11 مساء.";
    public static final String TERMS_AND_CONDITIONS_ARABIC_5 = "موظفو كارسبا مخولون بقيادة المركبة على الشارع بغرض فحصها. لذا, فانني اقر بان كارسبا لن تكون مسئوله عن اية خسارة او عطل ينتج اثناء فترة الخدمة.";
    public static final String TERMS_AND_CONDITIONS_ARABIC_6 = "تطبق البنود والشروط الأخرى.";


    public static final Font regularFont = new Font(Font.FontFamily.HELVETICA, 11, Font.NORMAL);
    public static final Font contentFont = new Font(Font.FontFamily.HELVETICA, 9, Font.NORMAL);
    public static final Font regularBoldFont = new Font(Font.FontFamily.HELVETICA, 11, Font.BOLD);
    public static final Font termsAndConditionsFont = new Font(Font.FontFamily.HELVETICA, 7, Font.NORMAL);
    public static final Font pageTitleFont = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);

    public static void addTitle(Document document, String titleString) throws DocumentException {
        Paragraph title = new Paragraph(new Phrase(titleString, pageTitleFont));
        title.setAlignment(Element.ALIGN_CENTER);
        document.add(title);
    }

    public static void addChuckLines(Document document, int lineCount) throws DocumentException {
        for (int count = 0; count < lineCount; count++) {
            addChuckLines(document);
        }
    }

    public static void addChuckLines(Document document) throws DocumentException {
        document.add(Chunk.NEWLINE);
    }

    public static void addTableHeader(PdfPTable pdfPTable) {
        PdfPCell slNoHeader = new PdfPCell();
        Paragraph slNo = new Paragraph(new Phrase("SI.NO.", regularFont));
        slNo.setAlignment(Element.ALIGN_CENTER);
        slNoHeader.addElement(slNo);
        slNoHeader.setVerticalAlignment(Element.ALIGN_MIDDLE);
        slNoHeader.setHorizontalAlignment(Element.ALIGN_MIDDLE);
        slNoHeader.setMinimumHeight(50);

        PdfPCell particularsHeader = new PdfPCell();
        Paragraph particulars = new Paragraph(new Phrase("Particulars", regularFont));
        particulars.setAlignment(Element.ALIGN_CENTER);
        particularsHeader.addElement(particulars);
        particularsHeader.setVerticalAlignment(Element.ALIGN_MIDDLE);
        particularsHeader.setHorizontalAlignment(Element.ALIGN_MIDDLE);
        particularsHeader.setMinimumHeight(50);

        PdfPCell rateHeader = new PdfPCell();
        Paragraph rate = new Paragraph(new Phrase("Rate", regularFont));
        rate.setAlignment(Element.ALIGN_CENTER);
        rateHeader.addElement(rate);
        rateHeader.setVerticalAlignment(Element.ALIGN_MIDDLE);
        rateHeader.setHorizontalAlignment(Element.ALIGN_MIDDLE);
        rateHeader.setMinimumHeight(50);

        PdfPCell qtyHeader = new PdfPCell();
        Paragraph qty = new Paragraph(new Phrase("Qty", regularFont));
        qty.setAlignment(Element.ALIGN_CENTER);
        qtyHeader.addElement(qty);
        qtyHeader.setVerticalAlignment(Element.ALIGN_MIDDLE);
        qtyHeader.setHorizontalAlignment(Element.ALIGN_MIDDLE);
        qtyHeader.setMinimumHeight(50);

        PdfPCell amountHeader = new PdfPCell();
        Paragraph amount = new Paragraph(new Phrase("Amount", regularBoldFont));
        amount.setAlignment(Element.ALIGN_CENTER);
        amountHeader.addElement(amount);
        amountHeader.setVerticalAlignment(Element.ALIGN_MIDDLE);
        amountHeader.setMinimumHeight(50);
        amountHeader.setHorizontalAlignment(Element.ALIGN_MIDDLE);


        pdfPTable.addCell(slNoHeader);
        pdfPTable.addCell(particularsHeader);
        pdfPTable.addCell(rateHeader);
        pdfPTable.addCell(qtyHeader);
        pdfPTable.addCell(amountHeader);
    }

    public static void addTableContentItems(PdfPTable pdfPTable, List<Item> itemList) {
        int length = itemList.size();
        Item item = null;
        for (int count = 0; count < length; count++) {
            item = itemList.get(count);

            PdfPCell slNoValueCell = new PdfPCell();
            Paragraph slNoValue = new Paragraph(new Phrase((count + 1) + "", contentFont));
            slNoValue.setAlignment(Element.ALIGN_CENTER);
            slNoValueCell.addElement(slNoValue);
            slNoValueCell.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
            slNoValueCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            slNoValueCell.setHorizontalAlignment(Element.ALIGN_MIDDLE);


            PdfPCell particularValueCell = new PdfPCell();
            Paragraph particularValue = new Paragraph(new Phrase(item.getParticulars(), contentFont));
            particularValue.setAlignment(Element.ALIGN_CENTER);
            particularValueCell.addElement(particularValue);
            particularValueCell.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
            particularValueCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            particularValueCell.setHorizontalAlignment(Element.ALIGN_MIDDLE);


            PdfPCell rateValueCell = new PdfPCell();
            Paragraph rateValue = new Paragraph(new Phrase(getFormattedFloatValue(item.getRate()), contentFont));
            rateValue.setAlignment(Element.ALIGN_CENTER);
            rateValueCell.addElement(rateValue);
            rateValueCell.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
            rateValueCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            rateValueCell.setHorizontalAlignment(Element.ALIGN_MIDDLE);

            PdfPCell qtyValueCell = new PdfPCell();
            Paragraph qtyValue = new Paragraph(new Phrase(item.getQty() + "", contentFont));
            qtyValue.setAlignment(Element.ALIGN_RIGHT);
            qtyValueCell.setPaddingRight(10);
            qtyValueCell.addElement(qtyValue);
            qtyValueCell.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
            qtyValueCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            qtyValueCell.setHorizontalAlignment(Element.ALIGN_MIDDLE);


            PdfPCell amountValueCell = new PdfPCell();
            Paragraph amountValue = new Paragraph(new Phrase(getFormattedFloatValue(item.getAmount()), contentFont));
            amountValue.setAlignment(Element.ALIGN_RIGHT);
            amountValueCell.setPaddingRight(10);
            amountValueCell.addElement(amountValue);
            amountValueCell.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
            amountValueCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            amountValueCell.setHorizontalAlignment(Element.ALIGN_MIDDLE);


            if (count == length - 1) {
                slNoValueCell.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.BOTTOM);
                particularValueCell.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.BOTTOM);
                rateValueCell.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.BOTTOM);
                qtyValueCell.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.BOTTOM);
                amountValueCell.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.BOTTOM);

                slNoValueCell.setPaddingBottom(10);
                particularValueCell.setPaddingBottom(10);
                rateValueCell.setPaddingBottom(10);
                qtyValueCell.setPaddingBottom(10);
                amountValueCell.setPaddingBottom(10);
            }

            pdfPTable.addCell(slNoValueCell);
            pdfPTable.addCell(particularValueCell);
            pdfPTable.addCell(rateValueCell);
            pdfPTable.addCell(qtyValueCell);
            pdfPTable.addCell(amountValueCell);

        }
    }

    public static void addCalculationContentToTable(PdfPTable pdfPTable, String key, float value, int alignment) {
        addCalculationContentToTable(pdfPTable, key, value, alignment, false);
    }


    public static void addCalculationContentToTable(PdfPTable pdfPTable, String key, float value, int alignment, boolean isLast) {
        PdfPCell emptyCell = new PdfPCell();
        Paragraph emptyCellValue = new Paragraph(new Phrase(" ", contentFont));
        emptyCellValue.setAlignment(alignment);
        emptyCell.addElement(emptyCellValue);
        emptyCell.setBorder(Rectangle.LEFT | Rectangle.RIGHT);
        if (isLast == true) {
            emptyCell.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.BOTTOM);
        }

        emptyCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        emptyCell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
        emptyCell.setColspan(2);
        emptyCell.setPaddingBottom(10);


        PdfPCell keyCell = new PdfPCell();
        Paragraph keyCellValue = new Paragraph(new Phrase(key, contentFont));
        keyCellValue.setAlignment(alignment);
        keyCell.addElement(keyCellValue);
        keyCell.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.BOTTOM);
        keyCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        keyCell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
        keyCell.setColspan(2);
        keyCell.setPaddingBottom(10);
        keyCell.setPaddingRight(10);

        PdfPCell valueCell = new PdfPCell();
        Paragraph valueCellValue = new Paragraph(new Phrase(getFormattedFloatValue(value), contentFont));
        valueCellValue.setAlignment(alignment);
        valueCell.addElement(valueCellValue);
        valueCell.setBorder(Rectangle.LEFT | Rectangle.RIGHT | Rectangle.BOTTOM);
        valueCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        valueCell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
        valueCell.setPaddingBottom(10);
        valueCell.setPaddingRight(10);

        pdfPTable.addCell(emptyCell);
        pdfPTable.addCell(keyCell);
        pdfPTable.addCell(valueCell);

    }


    public static void addAmountInWord(PdfPTable pdfPTable, String amountInString) {
        PdfPCell amountCell = new PdfPCell();
        Paragraph amountCellValue = new Paragraph(new Phrase("Amount:  " + amountInString, contentFont));
        amountCellValue.setIndentationLeft(20);
        amountCellValue.setAlignment(Element.ALIGN_LEFT);
        amountCell.addElement(amountCellValue);

        amountCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        amountCell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
        amountCell.setColspan(5);
        amountCell.setPaddingBottom(10);

        pdfPTable.addCell(amountCell);
    }

    public static void addTermsAndConditions(Document document) throws DocumentException {
        Paragraph title = new Paragraph(new Phrase(TERMS_AND_CONDITIONS_1, termsAndConditionsFont));
        //title.setIndentationLeft(10);


        /*Paragraph terms1 = new Paragraph(new Phrase("1. " + TERMS_AND_CONDITIONS_1, termsAndConditionsFont));
        terms1.setIndentationLeft(30);

        Paragraph terms2 = new Paragraph(new Phrase("2. " + TERMS_AND_CONDITIONS_2, termsAndConditionsFont));
        terms2.setIndentationLeft(30);

        Paragraph terms3 = new Paragraph(new Phrase("3. " + TERMS_AND_CONDITIONS_3, termsAndConditionsFont));
        terms3.setIndentationLeft(30);

        Paragraph terms4 = new Paragraph(new Phrase("4. " + TERMS_AND_CONDITIONS_4, termsAndConditionsFont));
        terms4.setIndentationLeft(30);

        Paragraph terms5 = new Paragraph(new Phrase("5. " + TERMS_AND_CONDITIONS_5, termsAndConditionsFont));
        terms5.setIndentationLeft(30);

        Paragraph terms6 = new Paragraph(new Phrase("6. " + TERMS_AND_CONDITIONS_6, termsAndConditionsFont));
        terms6.setIndentationLeft(30);*/


        document.add(title);
        /*document.add(terms1);
        document.add(terms2);
        document.add(terms3);
        document.add(terms4);
        document.add(terms5);
        document.add(terms6);*/


    }


    public static void addTermsAndConditionsArabic(Document document) throws DocumentException {
        Paragraph title = new Paragraph(new Phrase("الشروط والبنود:", termsAndConditionsFont));
        title.setAlignment(Element.ALIGN_RIGHT);
        title.setIndentationRight(10);


        Paragraph terms1 = new Paragraph(new Phrase(TERMS_AND_CONDITIONS_ARABIC_1 + " .1", termsAndConditionsFont));
        terms1.setAlignment(Element.ALIGN_RIGHT);
        terms1.setIndentationRight(30);

        Paragraph terms2 = new Paragraph(new Phrase(TERMS_AND_CONDITIONS_ARABIC_2 + " .2", termsAndConditionsFont));
        terms2.setAlignment(Element.ALIGN_RIGHT);
        terms2.setIndentationRight(30);

        Paragraph terms3 = new Paragraph(new Phrase(TERMS_AND_CONDITIONS_ARABIC_3 + " .3", termsAndConditionsFont));
        terms3.setAlignment(Element.ALIGN_RIGHT);
        terms3.setIndentationRight(30);

        Paragraph terms4 = new Paragraph(new Phrase(TERMS_AND_CONDITIONS_ARABIC_4 + " .4", termsAndConditionsFont));
        terms4.setAlignment(Element.ALIGN_RIGHT);
        terms4.setIndentationRight(30);

        Paragraph terms5 = new Paragraph(new Phrase(TERMS_AND_CONDITIONS_ARABIC_5 + " .5", termsAndConditionsFont));
        terms5.setAlignment(Element.ALIGN_RIGHT);
        terms5.setIndentationRight(30);

        Paragraph terms6 = new Paragraph(new Phrase(TERMS_AND_CONDITIONS_ARABIC_6 + " .6", termsAndConditionsFont));
        terms6.setAlignment(Element.ALIGN_RIGHT);
        terms6.setIndentationRight(30);


        document.add(title);
        document.add(terms1);
        document.add(terms2);
        document.add(terms3);
        document.add(terms4);
        document.add(terms5);
        document.add(terms6);


    }


    public static String getFormattedFloatValue(float sourceValue) {
        DecimalFormat df = new DecimalFormat();
        df.setMaximumFractionDigits(2);
        df.setMinimumFractionDigits(2);
        return df.format(sourceValue);
    }


    public static void addSignatures(Document document) throws DocumentException {

        PdfPTable pdfPTable = new PdfPTable(new float[]{2.6f, 2, 2, 2, 2.6f});
        pdfPTable.setWidthPercentage(100f);


        PdfPCell emptyCell = new PdfPCell();
        Paragraph emptyCellValue = new Paragraph(new Phrase(" ", contentFont));
        emptyCellValue.setAlignment(Element.ALIGN_LEFT);
        emptyCell.addElement(emptyCellValue);
        emptyCell.setBorder(Rectangle.NO_BORDER);
        emptyCell.setColspan(3);


        PdfPCell customerSignatureCell = new PdfPCell();
        Paragraph customerSignatureValue = new Paragraph(new Phrase("Signature of the Customer", contentFont));
        customerSignatureValue.setAlignment(Element.ALIGN_LEFT);
        customerSignatureCell.addElement(customerSignatureValue);
        customerSignatureCell.setBorder(Rectangle.TOP);


        PdfPCell foremanSignatureCell = new PdfPCell();
        Paragraph foremanSignatureValue = new Paragraph(new Phrase("Signature of the Foreman", contentFont));
        foremanSignatureValue.setAlignment(Element.ALIGN_RIGHT);
        foremanSignatureCell.addElement(foremanSignatureValue);
        foremanSignatureCell.setBorder(Rectangle.TOP);


        pdfPTable.addCell(customerSignatureCell);
        pdfPTable.addCell(emptyCell);
        pdfPTable.addCell(foremanSignatureCell);

        document.add(pdfPTable);
    }


    public static void addJobDetail(Document document, Image logo, JobCardDetailsModel mCartForm) throws DocumentException {
        PdfPTable pdfPTable = new PdfPTable(2);
        pdfPTable.setWidthPercentage(100f);

        PdfPCell leftCell = new PdfPCell();
        //leftCell.addElement();
        leftCell.setBorder(Rectangle.NO_BORDER);
        leftCell.setVerticalAlignment(Element.ALIGN_CENTER);
        leftCell.setHorizontalAlignment(Element.ALIGN_CENTER);




        String cname,cwebsite,cemail,cphone,caddress,trn;


            //OvseseePOS demo
            cname="OvseseePOS";
            cwebsite="www.overseepos.com";
            cemail="info@connectivelinkstechnology.com";
            cphone="+971 4 238 0815 / +971 55 107 7843";
            caddress="#701(06), Al Mezan Tower,Dubai, United Arab Emirates\n";
            trn="TRN 100330906700003";



        Paragraph companyName = new Paragraph();
        companyName.add(new Phrase(cname, regularFont));
        companyName.setAlignment(Element.ALIGN_LEFT);


        Paragraph companyWebsite = new Paragraph();
        companyWebsite.add(new Phrase(cwebsite, regularFont));
        companyWebsite.setAlignment(Element.ALIGN_LEFT);



        Paragraph companyEmail = new Paragraph();
        companyEmail.add(new Phrase(cemail, regularFont));
        companyEmail.setAlignment(Element.ALIGN_LEFT);


        Paragraph companyPhone = new Paragraph();
        companyPhone.add(new Phrase(cphone, regularFont));
        companyPhone.setAlignment(Element.ALIGN_LEFT);


        Paragraph companyAddress = new Paragraph();
        companyAddress.add(new Phrase(caddress, regularFont));
        companyAddress.setAlignment(Element.ALIGN_LEFT);

        Paragraph companyTRN = new Paragraph();
        companyTRN.add(new Phrase(trn, regularFont));
        companyTRN.setAlignment(Element.ALIGN_LEFT);




        PdfPCell rightCell = new PdfPCell();
        rightCell.setBorder(Rectangle.NO_BORDER);
        rightCell.setVerticalAlignment(PdfPCell.ALIGN_RIGHT);
        rightCell.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);


        PdfPTable rightPdfTable = new PdfPTable(2);
        rightPdfTable.setWidthPercentage(100f);


        Paragraph invoice = new Paragraph();
        invoice.add(new Phrase(getRequiredWhiteSpaces("Invoice #"), regularFont));
        invoice.setAlignment(Element.ALIGN_LEFT);
        PdfPCell invoiceCell = new PdfPCell();
        invoiceCell.setBorder(Rectangle.NO_BORDER);
        invoiceCell.setVerticalAlignment(PdfPCell.ALIGN_RIGHT);
        invoiceCell.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
        invoiceCell.addElement(invoice);


        Paragraph invoiceValue = new Paragraph();
        invoiceValue.add(new Phrase(mCartForm.getInvoice_no(), regularFont));
        invoiceValue.setAlignment(Element.ALIGN_LEFT);
        PdfPCell invoiceValueCell = new PdfPCell();
        invoiceValueCell.setBorder(Rectangle.NO_BORDER);
        invoiceValueCell.setVerticalAlignment(PdfPCell.ALIGN_RIGHT);
        invoiceValueCell.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
        invoiceValueCell.addElement(invoiceValue);

        Paragraph invoiceDate = new Paragraph();
        invoiceDate.add(new Phrase(getRequiredWhiteSpaces("Invoice Date"), regularFont));
        invoiceDate.setAlignment(Element.ALIGN_LEFT);
        PdfPCell invoiceDateCell = new PdfPCell();
        invoiceDateCell.setBorder(Rectangle.NO_BORDER);
        invoiceDateCell.setVerticalAlignment(PdfPCell.ALIGN_RIGHT);
        invoiceDateCell.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
        invoiceDateCell.addElement(invoiceDate);

        Paragraph invoiceDateValue = new Paragraph();
        invoiceDateValue.add(new Phrase(String.valueOf(mCartForm.getCreated_date()), regularFont));
        invoiceDateValue.setAlignment(Element.ALIGN_LEFT);
        PdfPCell invoiceDateValueCell = new PdfPCell();
        invoiceDateValueCell.setBorder(Rectangle.NO_BORDER);
        invoiceDateValueCell.setVerticalAlignment(PdfPCell.ALIGN_RIGHT);
        invoiceDateValueCell.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
        invoiceDateValueCell.addElement(invoiceDateValue);

        Paragraph jobCard = new Paragraph();
        jobCard.add(new Phrase(getRequiredWhiteSpaces("JobCard #"), regularFont));
        jobCard.setAlignment(Element.ALIGN_LEFT);
        PdfPCell jobCardCell = new PdfPCell();
        jobCardCell.setBorder(Rectangle.NO_BORDER);
        jobCardCell.setVerticalAlignment(PdfPCell.ALIGN_RIGHT);
        jobCardCell.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
        jobCardCell.addElement(jobCard);


        Paragraph jobCardValue = new Paragraph();
        jobCardValue.add(new Phrase(String.valueOf(mCartForm.getId()), regularFont));
        jobCardValue.setAlignment(Element.ALIGN_LEFT);
        PdfPCell jobCardValueCell = new PdfPCell();
        jobCardValueCell.setBorder(Rectangle.NO_BORDER);
        jobCardValueCell.setVerticalAlignment(PdfPCell.ALIGN_RIGHT);
        jobCardValueCell.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
        jobCardValueCell.addElement(jobCardValue);







        Paragraph customerType = new Paragraph();
        customerType.add(new Phrase(getRequiredWhiteSpaces("Customer Type"), regularFont));
        customerType.setAlignment(Element.ALIGN_LEFT);
        PdfPCell customerTypeCell = new PdfPCell();
        customerTypeCell.setBorder(Rectangle.NO_BORDER);
        customerTypeCell.setVerticalAlignment(PdfPCell.ALIGN_RIGHT);
        customerTypeCell.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
        customerTypeCell.addElement(customerType);


        Paragraph customerTypeValue = new Paragraph();
        customerTypeValue.add(new Phrase(mCartForm.getName(), regularFont));
        customerTypeValue.setAlignment(Element.ALIGN_LEFT);
        PdfPCell customerTypeValueCell = new PdfPCell();
        customerTypeValueCell.setBorder(Rectangle.NO_BORDER);
        customerTypeValueCell.setVerticalAlignment(PdfPCell.ALIGN_RIGHT);
        customerTypeValueCell.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
        customerTypeValueCell.addElement(customerTypeValue);




        Paragraph customerName = new Paragraph();
        customerName.add(new Phrase(getRequiredWhiteSpaces("Customer Name"), regularFont));
        customerName.setAlignment(Element.ALIGN_LEFT);
        PdfPCell customerNameCell = new PdfPCell();
        customerNameCell.setBorder(Rectangle.NO_BORDER);
        customerNameCell.setVerticalAlignment(PdfPCell.ALIGN_RIGHT);
        customerNameCell.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
        customerNameCell.addElement(customerName);


        Paragraph customerNameValue = new Paragraph();
        customerNameValue.add(new Phrase(mCartForm.getName(), regularFont));
        customerNameValue.setAlignment(Element.ALIGN_LEFT);
        PdfPCell customerNameValueCell = new PdfPCell();
        customerNameValueCell.setBorder(Rectangle.NO_BORDER);
        customerNameValueCell.setVerticalAlignment(PdfPCell.ALIGN_RIGHT);
        customerNameValueCell.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
        customerNameValueCell.addElement(customerNameValue);





        Paragraph date = new Paragraph();
        date.add(new Phrase(getRequiredWhiteSpaces("Date"), regularFont));
        date.setAlignment(Element.ALIGN_LEFT);
        PdfPCell dateCell = new PdfPCell();
        dateCell.setBorder(Rectangle.NO_BORDER);
        dateCell.setVerticalAlignment(PdfPCell.ALIGN_RIGHT);
        dateCell.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
        dateCell.addElement(date);


        Paragraph dateValue = new Paragraph();
        dateValue.add(new Phrase( mCartForm.getAdvisor_date() + " " + mCartForm.getAdvisor_time(), regularFont));
        date.setAlignment(Element.ALIGN_LEFT);
        PdfPCell dateValueCell = new PdfPCell();
        dateValueCell.setBorder(Rectangle.NO_BORDER);
        dateValueCell.setVerticalAlignment(PdfPCell.ALIGN_RIGHT);
        dateValueCell.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
        dateValueCell.addElement(dateValue);



        Paragraph mobile = new Paragraph();
        mobile.add(new Phrase(getRequiredWhiteSpaces("Mobile"), regularFont));
        mobile.setAlignment(Element.ALIGN_LEFT);
        PdfPCell mobileCell = new PdfPCell();
        mobileCell.setBorder(Rectangle.NO_BORDER);
        mobileCell.setVerticalAlignment(PdfPCell.ALIGN_RIGHT);
        mobileCell.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
        mobileCell.addElement(mobile);

        Paragraph mobileValue = new Paragraph();
        mobileValue.add(new Phrase(mCartForm.getMobile_no(), regularFont));
        mobileValue.setAlignment(Element.ALIGN_LEFT);
        PdfPCell mobileValueCell = new PdfPCell();
        mobileValueCell.setBorder(Rectangle.NO_BORDER);
        mobileValueCell.setVerticalAlignment(PdfPCell.ALIGN_RIGHT);
        mobileValueCell.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
        mobileValueCell.addElement(mobileValue);



        Paragraph model = new Paragraph();
        model.add(new Phrase(getRequiredWhiteSpaces("Make/Model"), regularFont));
        model.setAlignment(Element.ALIGN_LEFT);
        PdfPCell modelCell = new PdfPCell();
        modelCell.setBorder(Rectangle.NO_BORDER);
        modelCell.setVerticalAlignment(PdfPCell.ALIGN_RIGHT);
        modelCell.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
        modelCell.addElement(model);


        Paragraph modelValue = new Paragraph();
        modelValue.add(new Phrase(mCartForm.getModel(), regularFont));
        modelValue.setAlignment(Element.ALIGN_LEFT);
        PdfPCell modelValueCell = new PdfPCell();
        modelValueCell.setBorder(Rectangle.NO_BORDER);
        modelValueCell.setVerticalAlignment(PdfPCell.ALIGN_RIGHT);
        modelValueCell.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
        modelValueCell.addElement(modelValue);



        Paragraph regNo = new Paragraph();
        regNo.add(new Phrase(getRequiredWhiteSpaces("Reg.No."), regularFont));
        regNo.setAlignment(Element.ALIGN_LEFT);
        PdfPCell regNoCell = new PdfPCell();
        regNoCell.setBorder(Rectangle.NO_BORDER);
        regNoCell.setVerticalAlignment(PdfPCell.ALIGN_RIGHT);
        regNoCell.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
        regNoCell.addElement(regNo);

        Paragraph regNoValue = new Paragraph();
        regNoValue.add(new Phrase(mCartForm.getReg_no(), regularFont));
        regNoValue.setAlignment(Element.ALIGN_LEFT);
        PdfPCell regNoValueCell = new PdfPCell();
        regNoValueCell.setBorder(Rectangle.NO_BORDER);
        regNoValueCell.setVerticalAlignment(PdfPCell.ALIGN_RIGHT);
        regNoValueCell.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
        regNoValueCell.addElement(regNoValue);


        /*Paragraph vehicleMileage = new Paragraph();
        vehicleMileage.add(new Phrase(getRequiredWhiteSpaces("Vehicle Mileage"), regularFont));
        vehicleMileage.setAlignment(Element.ALIGN_LEFT);
        PdfPCell vehicleMileageCell = new PdfPCell();
        vehicleMileageCell.setBorder(Rectangle.NO_BORDER);
        vehicleMileageCell.setVerticalAlignment(PdfPCell.ALIGN_RIGHT);
        vehicleMileageCell.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
        vehicleMileageCell.addElement(vehicleMileage);

        Paragraph vehicleMileageValue = new Paragraph();
        vehicleMileageValue.add(new Phrase(mCartForm.getVehicle_mileage(), regularFont));
        vehicleMileageValue.setAlignment(Element.ALIGN_LEFT);
        PdfPCell vehicleMileageValueCell = new PdfPCell();
        vehicleMileageValueCell.setBorder(Rectangle.NO_BORDER);
        vehicleMileageValueCell.setVerticalAlignment(PdfPCell.ALIGN_RIGHT);
        vehicleMileageValueCell.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
        vehicleMileageValueCell.addElement(vehicleMileageValue);*/


        Paragraph chassisNo = new Paragraph();
        chassisNo.add(new Phrase(getRequiredWhiteSpaces("Chassis No"), regularFont));
        chassisNo.setAlignment(Element.ALIGN_LEFT);
        PdfPCell chassisNoCell = new PdfPCell();
        chassisNoCell.setBorder(Rectangle.NO_BORDER);
        chassisNoCell.setVerticalAlignment(PdfPCell.ALIGN_RIGHT);
        chassisNoCell.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
        chassisNoCell.addElement(chassisNo);

        Paragraph chassisNoValue = new Paragraph();
        chassisNoValue.add(new Phrase(mCartForm.getChassis_no(), regularFont));
        chassisNoValue.setAlignment(Element.ALIGN_LEFT);
        PdfPCell chassisNoValueCell = new PdfPCell();
        chassisNoValueCell.setBorder(Rectangle.NO_BORDER);
        chassisNoValueCell.setVerticalAlignment(PdfPCell.ALIGN_RIGHT);
        chassisNoValueCell.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
        chassisNoValueCell.addElement(chassisNoValue);

        Paragraph vehicleColor = new Paragraph();
        vehicleColor.add(new Phrase(getRequiredWhiteSpaces("vehicle Color"), regularFont));
        vehicleColor.setAlignment(Element.ALIGN_LEFT);
        PdfPCell vehicleColorCell = new PdfPCell();
        vehicleColorCell.setBorder(Rectangle.NO_BORDER);
        vehicleColorCell.setVerticalAlignment(PdfPCell.ALIGN_RIGHT);
        vehicleColorCell.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
        vehicleColorCell.addElement(vehicleColor);

        Paragraph vehicleColorValue = new Paragraph();
        vehicleColorValue.add(new Phrase(mCartForm.getColours(), regularFont));
        vehicleColorValue.setAlignment(Element.ALIGN_LEFT);
        PdfPCell vehicleColorValueCell = new PdfPCell();
        vehicleColorValueCell.setBorder(Rectangle.NO_BORDER);
        vehicleColorValueCell.setVerticalAlignment(PdfPCell.ALIGN_RIGHT);
        vehicleColorValueCell.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
        vehicleColorValueCell.addElement(vehicleColorValue);


        /*Paragraph paymentType = new Paragraph();
        paymentType.add(new Phrase(getRequiredWhiteSpaces("Payment Type"), regularFont));
        paymentType.setAlignment(Element.ALIGN_LEFT);
        PdfPCell paymentTypeCell = new PdfPCell();
        paymentTypeCell.setBorder(Rectangle.NO_BORDER);
        paymentTypeCell.setVerticalAlignment(PdfPCell.ALIGN_RIGHT);
        paymentTypeCell.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
        paymentTypeCell.addElement(paymentType);

        Paragraph paymentTypeValue = new Paragraph();
        paymentTypeValue.add(new Phrase(mCartForm.getPayment_type(), regularFont));
        paymentTypeValue.setAlignment(Element.ALIGN_LEFT);
        PdfPCell paymentTypeValueCell = new PdfPCell();
        paymentTypeValueCell.setBorder(Rectangle.NO_BORDER);
        paymentTypeValueCell.setVerticalAlignment(PdfPCell.ALIGN_RIGHT);
        paymentTypeValueCell.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
        paymentTypeValueCell.addElement(paymentTypeValue);*/









        rightPdfTable.addCell(invoiceCell);
        rightPdfTable.addCell(invoiceValueCell);

        rightPdfTable.addCell(invoiceDateCell);
        rightPdfTable.addCell(invoiceDateValueCell);

        rightPdfTable.addCell(jobCardCell);
        rightPdfTable.addCell(jobCardValueCell);

        rightPdfTable.addCell(customerTypeCell);
        rightPdfTable.addCell(customerTypeValueCell);

        rightPdfTable.addCell(customerNameCell);
        rightPdfTable.addCell(customerNameValueCell);

        rightPdfTable.addCell(dateCell);
        rightPdfTable.addCell(dateValueCell);

        rightPdfTable.addCell(mobileCell);
        rightPdfTable.addCell(mobileValueCell);

        rightPdfTable.addCell(modelCell);
        rightPdfTable.addCell(modelValueCell);

        rightPdfTable.addCell(regNoCell);
        rightPdfTable.addCell(regNoValueCell);

//        rightPdfTable.addCell(vehicleMileageCell);
//        rightPdfTable.addCell(vehicleMileageValueCell);

        rightPdfTable.addCell(chassisNoCell);
        rightPdfTable.addCell(chassisNoValueCell);

        rightPdfTable.addCell(vehicleColorCell);
        rightPdfTable.addCell(vehicleColorValueCell);

//        rightPdfTable.addCell(paymentTypeCell);
//        rightPdfTable.addCell(paymentTypeValueCell);
















        /*rightCell.addElement(jobCard);
        rightCell.addElement(customerType);
        rightCell.addElement(customerName);
        rightCell.addElement(date);
        rightCell.addElement(mobile);
        rightCell.addElement(model);
        rightCell.addElement(regNo);
        rightCell.addElement(vehicleMileage);*/

        /*rightCell.setPaddingRight(20);
        rightCell.setPaddingLeft(40);*/

        rightCell.addElement(rightPdfTable);
        rightCell.setPaddingBottom(20);


        leftCell.addElement(logo);
        leftCell.addElement(companyName);
        leftCell.addElement(companyWebsite);
        leftCell.addElement(companyEmail);
        leftCell.addElement(companyPhone);
        leftCell.addElement(companyAddress);
        leftCell.addElement(companyTRN);


        pdfPTable.addCell(leftCell);
        pdfPTable.addCell(rightCell);

        pdfPTable.setPaddingTop(10);
        document.add(pdfPTable);
    }

    public static String getRequiredWhiteSpaces(String value) {
        for (int count = value.length(); count < 28; count ++) {
            value = value;
        }
        return value;
    }


}

