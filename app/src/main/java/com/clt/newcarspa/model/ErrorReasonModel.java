package com.clt.newcarspa.model;

/**
 * Created by Karthik Thumathy on 18-Aug-18.
 */
public class ErrorReasonModel {

    private int status;
    private String message;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
