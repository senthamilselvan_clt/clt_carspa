package com.clt.newcarspa.model;

import java.util.List;

/**
 * Created by Karthik Thumathy on 04-Nov-18.
 */
public class SyncJobCardModel {

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    private String action;

    private int job_card_count;

    public String getJob_card_id() {
        return job_card_id;
    }

    public void setJob_card_id(String job_card_id) {
        this.job_card_id = job_card_id;
    }

    private String job_card_id;
    private List<JobCardDetailsRealmDBModel> data;

    public int getJob_card_count() {
        return job_card_count;
    }

    public void setJob_card_count(int job_card_count) {
        this.job_card_count = job_card_count;
    }

    public List<JobCardDetailsRealmDBModel> getData() {
        return data;
    }

    public void setData(List<JobCardDetailsRealmDBModel> data) {
        this.data = data;
    }

    public static class JobCardDetailsRealmDBModel {
        private String id;
        private String user_id;
        private String shop_id;
        private String invoice_no;
        private String customer_code;
        private String vat_reg_no;
        private String name;
        private String po_box;
        private String city;
        private String tel_no;
        private String mobile_no;
        private String fax_no;
        private String email;
        private String reg_no;
        private String model;
        private String chassis_no;
        private String colours;
        private String fuel;
        private String oil;
        private String kilometer;
        private String advisor_id;
        private String advisor_name;
        private String advisor_date;
        private String advisor_time;
        private String delivery_date;
        private String job_type;
        private String status;
        private String total_amount;
        private String created_date_str;
        private String date_added;
        private String date_updated;
        private int item_count;
        private int image_count;
        private int audio_count;
        private List<JobCardItemsDetailsModel> items;
        private List<ImageUploadModel> images;
        private List<AudioUploadModel> audios;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getShop_id() {
            return shop_id;
        }

        public void setShop_id(String shop_id) {
            this.shop_id = shop_id;
        }

        public String getInvoice_no() {
            return invoice_no;
        }

        public void setInvoice_no(String invoice_no) {
            this.invoice_no = invoice_no;
        }

        public String getCustomer_code() {
            return customer_code;
        }

        public void setCustomer_code(String customer_code) {
            this.customer_code = customer_code;
        }

        public String getVat_reg_no() {
            return vat_reg_no;
        }

        public void setVat_reg_no(String vat_reg_no) {
            this.vat_reg_no = vat_reg_no;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPo_box() {
            return po_box;
        }

        public void setPo_box(String po_box) {
            this.po_box = po_box;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getTel_no() {
            return tel_no;
        }

        public void setTel_no(String tel_no) {
            this.tel_no = tel_no;
        }

        public String getMobile_no() {
            return mobile_no;
        }

        public void setMobile_no(String mobile_no) {
            this.mobile_no = mobile_no;
        }

        public String getFax_no() {
            return fax_no;
        }

        public void setFax_no(String fax_no) {
            this.fax_no = fax_no;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getReg_no() {
            return reg_no;
        }

        public void setReg_no(String reg_no) {
            this.reg_no = reg_no;
        }

        public String getModel() {
            return model;
        }

        public void setModel(String model) {
            this.model = model;
        }

        public String getChassis_no() {
            return chassis_no;
        }

        public void setChassis_no(String chassis_no) {
            this.chassis_no = chassis_no;
        }

        public String getColours() {
            return colours;
        }

        public void setColours(String colours) {
            this.colours = colours;
        }

        public String getFuel() {
            return fuel;
        }

        public void setFuel(String fuel) {
            this.fuel = fuel;
        }

        public String getOil() {
            return oil;
        }

        public void setOil(String oil) {
            this.oil = oil;
        }

        public String getKilometer() {
            return kilometer;
        }

        public void setKilometer(String kilometer) {
            this.kilometer = kilometer;
        }

        public String getAdvisor_id() {
            return advisor_id;
        }

        public void setAdvisor_id(String advisor_id) {
            this.advisor_id = advisor_id;
        }

        public String getAdvisor_name() {
            return advisor_name;
        }

        public void setAdvisor_name(String advisor_name) {
            this.advisor_name = advisor_name;
        }

        public String getAdvisor_date() {
            return advisor_date;
        }

        public void setAdvisor_date(String advisor_date) {
            this.advisor_date = advisor_date;
        }

        public String getAdvisor_time() {
            return advisor_time;
        }

        public void setAdvisor_time(String advisor_time) {
            this.advisor_time = advisor_time;
        }

        public String getDelivery_date() {
            return delivery_date;
        }

        public void setDelivery_date(String delivery_date) {
            this.delivery_date = delivery_date;
        }

        public String getJob_type() {
            return job_type;
        }

        public void setJob_type(String job_type) {
            this.job_type = job_type;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getTotal_amount() {
            return total_amount;
        }

        public void setTotal_amount(String total_amount) {
            this.total_amount = total_amount;
        }

        public String getCreated_date_str() {
            return created_date_str;
        }

        public void setCreated_date_str(String created_date_str) {
            this.created_date_str = created_date_str;
        }

        public String getDate_added() {
            return date_added;
        }

        public void setDate_added(String date_added) {
            this.date_added = date_added;
        }

        public String getDate_updated() {
            return date_updated;
        }

        public void setDate_updated(String date_updated) {
            this.date_updated = date_updated;
        }

        public int getItem_count() {
            return item_count;
        }

        public void setItem_count(int item_count) {
            this.item_count = item_count;
        }

        public int getImage_count() {
            return image_count;
        }

        public void setImage_count(int image_count) {
            this.image_count = image_count;
        }

        public int getAudio_count() {
            return audio_count;
        }

        public void setAudio_count(int audio_count) {
            this.audio_count = audio_count;
        }

        public List<JobCardItemsDetailsModel> getItems() {
            return items;
        }

        public void setItems(List<JobCardItemsDetailsModel> items) {
            this.items = items;
        }

        public List<ImageUploadModel> getImages() {
            return images;
        }

        public void setImages(List<ImageUploadModel> images) {
            this.images = images;
        }

        public List<AudioUploadModel> getAudios() {
            return audios;
        }

        public void setAudios(List<AudioUploadModel> audios) {
            this.audios = audios;
        }

        public static class JobCardItemsDetailsModel {

            private String id;
            private String job_card_id;
            private String service_id;
            private String name;
            private String other_name;
            private String price;
            private String date_added;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getJob_card_id() {
                return job_card_id;
            }

            public void setJob_card_id(String job_card_id) {
                this.job_card_id = job_card_id;
            }

            public String getService_id() {
                return service_id;
            }

            public void setService_id(String service_id) {
                this.service_id = service_id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getOther_name() {
                return other_name;
            }

            public void setOther_name(String other_name) {
                this.other_name = other_name;
            }

            public String getPrice() {
                return price;
            }

            public void setPrice(String price) {
                this.price = price;
            }

            public String getDate_added() {
                return date_added;
            }

            public void setDate_added(String date_added) {
                this.date_added = date_added;
            }
        }
    }
}
