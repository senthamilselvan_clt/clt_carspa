package com.clt.newcarspa.model;

import java.util.List;

/**
 * Created by Karthik Thumathy on 31-Dec-18.
 */
public class NewJobCardViewListModel {
    public Integer getJob_card_count() {
        return job_card_count;
    }

    public void setJob_card_count(Integer job_card_count) {
        this.job_card_count = job_card_count;
    }

    public List<JobCardDetailsRealmDBModel> getData() {
        return data;
    }

    public void setData(List<JobCardDetailsRealmDBModel> data) {
        this.data = data;
    }

    private Integer job_card_count;
    private List<JobCardDetailsRealmDBModel> data;

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public Integer getShop_id() {
        return shop_id;
    }

    public void setShop_id(Integer shop_id) {
        this.shop_id = shop_id;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    private Integer user_id;
    private Integer shop_id;
    private String action;
    private String status;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    private String id;
    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    private String search;
}
