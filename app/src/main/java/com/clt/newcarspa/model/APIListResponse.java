package com.clt.newcarspa.model;

import java.util.List;

/**
 * Created by Siva Kumar on 20-04-2016.
 */
public class APIListResponse<T> {
    private String status;
    private List<T> result;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<T> getResult() {
        return result;
    }

    public void setResult(List<T> result)   {
        this.result = result;
    }
}
