package com.clt.newcarspa.model;

/**
 * Created by user on 8/8/2017.
 */

public class PaymentList {

    String id,	job_card_id,payment_type,amount,balance,date_added,name,mob,action,credit_paid,credit_total,credit_balance,invoice_no,net_total,grand_total,vat_total,vat;
    int advanced;

    public String getInvoice_no() {
        return invoice_no;
    }

    public void setInvoice_no(String invoice_no) {
        this.invoice_no = invoice_no;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getJob_card_id() {
        return job_card_id;
    }

    public void setJob_card_id(String job_card_id) {
        this.job_card_id = job_card_id;
    }

    public String getPayment_type() {
        return payment_type;
    }

    public void setPayment_type(String payment_type) {
        this.payment_type = payment_type;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getDate_added() {
        return date_added;
    }

    public void setDate_added(String date_added) {
        this.date_added = date_added;
    }

    public String getCredit_paid() {
        return credit_paid;
    }

    public void setCredit_paid(String credit_paid) {
        this.credit_paid = credit_paid;
    }

    public String getCredit_total() {
        return credit_total;
    }

    public void setCredit_total(String credit_total) {
        this.credit_total = credit_total;
    }

    public String getCredit_balance() {
        return credit_balance;
    }

    public void setCredit_balance(String credit_balance) {
        this.credit_balance = credit_balance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMob() {
        return mob;
    }

    public void setMob(String mob) {
        this.mob = mob;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public int getAdvanced() {
        return advanced;
    }

    public void setAdvanced(int advanced) {
        this.advanced = advanced;
    }

    public String getNet_total() {
        return net_total;
    }

    public void setNet_total(String net_total) {
        this.net_total = net_total;
    }

    public String getGrand_total() {
        return grand_total;
    }

    public void setGrand_total(String grand_total) {
        this.grand_total = grand_total;
    }

    public String getVat_total() {
        return vat_total;
    }

    public void setVat_total(String vat_total) {
        this.vat_total = vat_total;
    }

    public String getVat() {
        return vat;
    }

    public void setVat(String vat) {
        this.vat = vat;
    }
}
