package com.clt.newcarspa.model;

import io.realm.RealmObject;

/**
 * Created by Karthik Thumathy on 17-Oct-18.
 */
public class ImageUploadModel extends RealmObject {

    public String getLocaldbid() {
        return localdbid;
    }

    public void setLocaldbid(String localdbid) {
        this.localdbid = localdbid;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public Integer getShop_id() {
        return shop_id;
    }

    public void setShop_id(Integer shop_id) {
        this.shop_id = shop_id;
    }

    public String getImage_encode_str() {
        return image_encode_str;
    }

    public void setImage_encode_str(String image_encode_str) {
        this.image_encode_str = image_encode_str;
    }

    public String getImage_filename() {
        return image_filename;
    }

    public void setImage_filename(String image_filename) {
        this.image_filename = image_filename;
    }

    public String getLocal_insert() {
        return local_insert;
    }

    public void setLocal_insert(String local_insert) {
        this.local_insert = local_insert;
    }

    public String getSync_status() {
        return sync_status;
    }

    public void setSync_status(String sync_status) {
        this.sync_status = sync_status;
    }

    public Integer getRow_deleted() {
        return row_deleted;
    }

    public void setRow_deleted(Integer row_deleted) {
        this.row_deleted = row_deleted;
    }

    public String getImage_uniqueid() {
        return image_uniqueid;
    }

    public void setImage_uniqueid(String image_uniqueid) {
        this.image_uniqueid = image_uniqueid;
    }

    private String localdbid;
    private Integer id; // this id is reference (job_card_id) for class(JobCardItemsDetailsModel.class)
    private Integer user_id;
    private Integer shop_id;
    private String image_encode_str;
    private String image_filename;
    private String image_carpartname;
    private String image_uniqueid;
    private String local_insert, sync_status;
    private Integer row_deleted; // 0 means row not deleted 1 means row deleted
    private Integer fromServerRecord;
    private String action;
    private Integer car_parts_id;

    public Integer getCar_parts_id() {
        return car_parts_id;
    }

    public void setCar_parts_id(Integer car_parts_id) {
        this.car_parts_id = car_parts_id;
    }
    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Integer getFromServerRecord() {
        return fromServerRecord;
    }

    public void setFromServerRecord(Integer fromServerRecord) {
        this.fromServerRecord = fromServerRecord;
    }

    public String getImage_carpartname() {
        return image_carpartname;
    }

    public void setImage_carpartname(String image_carpartname) {
        this.image_carpartname = image_carpartname;
    }

}
