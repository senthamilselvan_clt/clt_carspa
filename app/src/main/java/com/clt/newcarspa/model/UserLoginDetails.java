package com.clt.newcarspa.model;

import io.realm.RealmObject;

/**
 * Created by Karthik Thumathy on 24-Jul-18.
 */
public class UserLoginDetails extends RealmObject{

        /**
         * id : 1
         * user_name : admin
         * user_pass : 21232f297a57a5a743894a0e4a801fc3
         * role_id : 1
         * manufacturing_unit_id : 1
         * shop_id : 1
         * first_name : P
         * last_name : admin
         * email : admin@mail.com
         * phone : 1234567890
         * is_active : 1
         * status : 1
         * user_action : view_services,edit_services
         * created_at : 2016-03-30 10:13:18
         * updated_at : 2016-03-30 10:13:18
         * fcm_id :
         * role : admin
         */

        private String id;
        private String user_name;
        private String user_pass;
        private String role_id;
        private String manufacturing_unit_id;
        private String shop_id;
        private String first_name;
        private String last_name;
        private String email;
        private String phone;
        private String is_active;
        private String status;
        private String user_action;
        private String created_at;
        private String updated_at;
        private String fcm_id;
        private String role;
        private String action;
        private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUser_name() {
            return user_name;
        }

        public void setUser_name(String user_name) {
            this.user_name = user_name;
        }

        public String getUser_pass() {
            return user_pass;
        }

        public void setUser_pass(String user_pass) {
            this.user_pass = user_pass;
        }

        public String getRole_id() {
            return role_id;
        }

        public void setRole_id(String role_id) {
            this.role_id = role_id;
        }

        public String getManufacturing_unit_id() {
            return manufacturing_unit_id;
        }

        public void setManufacturing_unit_id(String manufacturing_unit_id) {
            this.manufacturing_unit_id = manufacturing_unit_id;
        }

        public String getShop_id() {
            return shop_id;
        }

        public void setShop_id(String shop_id) {
            this.shop_id = shop_id;
        }

        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public String getLast_name() {
            return last_name;
        }

        public void setLast_name(String last_name) {
            this.last_name = last_name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getIs_active() {
            return is_active;
        }

        public void setIs_active(String is_active) {
            this.is_active = is_active;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getUser_action() {
            return user_action;
        }

        public void setUser_action(String user_action) {
            this.user_action = user_action;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public String getFcm_id() {
            return fcm_id;
        }

        public void setFcm_id(String fcm_id) {
            this.fcm_id = fcm_id;
        }

        public String getRole() {
            return role;
        }

        public void setRole(String role) {
            this.role = role;
        }

        public String getAction() {
            return action;
        }

        public void setAction(String action) {
            this.action = action;
        }
    }

