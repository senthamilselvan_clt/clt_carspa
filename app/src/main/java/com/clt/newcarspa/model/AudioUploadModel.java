package com.clt.newcarspa.model;

import io.realm.RealmObject;

/**
 * Created by Karthik Thumathy on 19-Oct-18.
 */
public class AudioUploadModel extends RealmObject {

    private String localdbid;
    private Integer id; // this id is reference (job_card_id) for class(JobCardItemsDetailsModel.class)
    private Integer user_id;
    private Integer shop_id;
    private Integer audio_uniqueid;
    private String audio_encode_str;
    private String audio_filename;
    private String audio_filepath;
    private String image_uniqueid;
    private String local_insert, sync_status;
    private Integer row_deleted; // 0 means row not deleted 1 means row deleted
    private Integer fromServerRecord;
    private String action;

    public Integer getAudio_uniqueid() {
        return audio_uniqueid;
    }

    public void setAudio_uniqueid(Integer audio_uniqueid) {
        this.audio_uniqueid = audio_uniqueid;
    }

    public Integer getAudio_status() {
        return audio_status;
    }

    public void setAudio_status(Integer audio_status) {
        this.audio_status = audio_status;
    }

    private Integer audio_status;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Integer getFromServerRecord() {
        return fromServerRecord;
    }

    public void setFromServerRecord(Integer fromServerRecord) {
        this.fromServerRecord = fromServerRecord;
    }

    public String getImage_uniqueid() {
        return image_uniqueid;
    }

    public void setImage_uniqueid(String image_uniqueid) {
        this.image_uniqueid = image_uniqueid;
    }

    public String getAudio_filepath() {
        return audio_filepath;
    }

    public void setAudio_filepath(String audio_filepath) {
        this.audio_filepath = audio_filepath;
    }

    public String getLocaldbid() {
        return localdbid;
    }

    public void setLocaldbid(String localdbid) {
        this.localdbid = localdbid;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public Integer getShop_id() {
        return shop_id;
    }

    public void setShop_id(Integer shop_id) {
        this.shop_id = shop_id;
    }

    public String getAudio_encode_str() {
        return audio_encode_str;
    }

    public void setAudio_encode_str(String audio_encode_str) {
        this.audio_encode_str = audio_encode_str;
    }

    public String getAudio_filename() {
        return audio_filename;
    }

    public void setAudio_filename(String audio_filename) {
        this.audio_filename = audio_filename;
    }

    public String getLocal_insert() {
        return local_insert;
    }

    public void setLocal_insert(String local_insert) {
        this.local_insert = local_insert;
    }

    public String getSync_status() {
        return sync_status;
    }

    public void setSync_status(String sync_status) {
        this.sync_status = sync_status;
    }

    public Integer getRow_deleted() {
        return row_deleted;
    }

    public void setRow_deleted(Integer row_deleted) {
        this.row_deleted = row_deleted;
    }

}
