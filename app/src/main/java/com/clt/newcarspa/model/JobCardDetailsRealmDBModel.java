package com.clt.newcarspa.model;

import java.util.Date;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by Karthik Thumathy on 27-Jul-18.
 */
public class JobCardDetailsRealmDBModel extends RealmObject {

    private String localdbid;
    private Integer id; // this id is reference (job_card_id) for class(JobCardItemsDetailsModel.class)
    private Integer user_id;
    private Integer shop_id;
    private String invoice_no;
    private String customer_code;
    private String vat_reg_no;
    private String name;
    private String po_box;
    private String city;
    private String tel_no;
    private String mobile_no;
    private String fax_no;
    private String email;
    private String advisor_date;
    private String advisor_time;
    private String reg_no;
    private String model;
    private String chassis_no;
    private String colours;
    private String fuel;
    private String oil;
    private String kilometer;
    private Integer advisor_id;
    private String advisor_name;
    private String delivery_date;
    private String job_type;
    private String total_amount;
    private String date_added; //currentdatetime
    private String date_update; //once update the row only update the currentdate
    private Date created_date;//currentdatetime
    private String created_date_str;//currentdatetime
    private String action;
    private String status;
    private String items;
    private RealmList<JobCardItemsDetailsModel> items_new;
    private String local_insert, sync_status;
    private Integer row_deleted; // 0 means row not deleted 1 means row deleted
    private String old_id;
    private String new_id;
    private String from_date;
    private String to_date;
    private String item_images;
    private String item_audios;

    public RealmList<JobCardItemsDetailsModel> getItems_new() {
        return items_new;
    }

    public void setItems_new(RealmList<JobCardItemsDetailsModel> items_new) {
        this.items_new = items_new;
    }

     public RealmList<ImageUploadModel> getImages() {
         return images;
     }

     public void setImages(RealmList<ImageUploadModel> images) {
         this.images = images;
     }

     public RealmList<AudioUploadModel> getAudios() {
         return audios;
     }

     public void setAudios(RealmList<AudioUploadModel> audios) {
         this.audios = audios;
     }

     private RealmList<ImageUploadModel> images;
     private RealmList<AudioUploadModel> audios;
    private Integer fromServerRecord;


    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    private String search;

    public Integer getFromServerRecord() {
        return fromServerRecord;
    }

    public void setFromServerRecord(Integer fromServerRecord) {
        this.fromServerRecord = fromServerRecord;
    }

    public String getFuel() {
        return fuel;
    }

    public void setFuel(String fuel) {
        this.fuel = fuel;
    }

    public String getOil() {
        return oil;
    }

    public void setOil(String oil) {
        this.oil = oil;
    }

    public String getKilometer() {
        return kilometer;
    }

    public void setKilometer(String kilometer) {
        this.kilometer = kilometer;
    }

    public String getItem_images() {
        return item_images;
    }

    public void setItem_images(String item_images) {
        this.item_images = item_images;
    }

    public String getItem_audios() {
        return item_audios;
    }

    public void setItem_audios(String item_audios) {
        this.item_audios = item_audios;
    }

    public String getFrom_date() {
        return from_date;
    }

    public void setFrom_date(String from_date) {
        this.from_date = from_date;
    }

    public String getTo_date() {
        return to_date;
    }

    public void setTo_date(String to_date) {
        this.to_date = to_date;
    }

    private RealmList<JobCardDetailsRealmDBModel> job_card_pushstr;

    public RealmList<JobCardDetailsRealmDBModel> getJob_card_pushstr() {
        return job_card_pushstr;
    }

    public void setJob_card_pushstr(RealmList<JobCardDetailsRealmDBModel> job_card_pushstr) {
        this.job_card_pushstr = job_card_pushstr;
    }


    public String getLocal_insert() {
        return local_insert;
    }

    public String getSync_status() {
        return sync_status;
    }

    public String getOld_id() {
        return old_id;
    }

    public void setOld_id(String old_id) {
        this.old_id = old_id;
    }

    public String getNew_id() {
        return new_id;
    }

    public void setNew_id(String new_id) {
        this.new_id = new_id;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public Integer getShop_id() {
        return shop_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public void setShop_id(Integer shop_id) {
        this.shop_id = shop_id;
    }

    public Integer getAdvisor_id() {
        return advisor_id;
    }

    public void setAdvisor_id(Integer advisor_id) {
        this.advisor_id = advisor_id;
    }

    public Date getCreated_date() {
        return created_date;
    }

    public void setCreated_date(Date created_date) {
        this.created_date = created_date;
    }

    public String getLocaldbid() {
        return localdbid;
    }

    public void setLocaldbid(String localdbid) {
        this.localdbid = localdbid;
    }

    public String isLocal_insert() {
        return local_insert;
    }

    public void setLocal_insert(String local_insert) {
        this.local_insert = local_insert;
    }

    public String isSync_status() {
        return sync_status;
    }

    public void setSync_status(String sync_status) {
        this.sync_status = sync_status;
    }

    public Integer getRow_deleted() {
        return row_deleted;
    }

    public void setRow_deleted(Integer row_deleted) {
        this.row_deleted = row_deleted;
    }

    public String getItems() {
        return items;
    }

    public void setItems(String items) {
        this.items = items;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getInvoice_no() {
        return invoice_no;
    }

    public void setInvoice_no(String invoice_no) {
        this.invoice_no = invoice_no;
    }

    public String getCustomer_code() {
        return customer_code;
    }

    public void setCustomer_code(String customer_code) {
        this.customer_code = customer_code;
    }

    public String getVat_reg_no() {
        return vat_reg_no;
    }

    public void setVat_reg_no(String vat_reg_no) {
        this.vat_reg_no = vat_reg_no;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPo_box() {
        return po_box;
    }

    public void setPo_box(String po_box) {
        this.po_box = po_box;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getTel_no() {
        return tel_no;
    }

    public void setTel_no(String tel_no) {
        this.tel_no = tel_no;
    }

    public String getMobile_no() {
        return mobile_no;
    }

    public void setMobile_no(String mobile_no) {
        this.mobile_no = mobile_no;
    }

    public String getFax_no() {
        return fax_no;
    }

    public void setFax_no(String fax_no) {
        this.fax_no = fax_no;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getReg_no() {
        return reg_no;
    }

    public void setReg_no(String reg_no) {
        this.reg_no = reg_no;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getChassis_no() {
        return chassis_no;
    }

    public void setChassis_no(String chassis_no) {
        this.chassis_no = chassis_no;
    }

    public String getColours() {
        return colours;
    }

    public void setColours(String colours) {
        this.colours = colours;
    }

    public String getAdvisor_name() {
        return advisor_name;
    }

    public void setAdvisor_name(String advisor_name) {
        this.advisor_name = advisor_name;
    }

    public String getAdvisor_date() {
        return advisor_date;
    }

    public void setAdvisor_date(String advisor_date) {
        this.advisor_date = advisor_date;
    }

    public String getAdvisor_time() {
        return advisor_time;
    }

    public void setAdvisor_time(String advisor_time) {
        this.advisor_time = advisor_time;
    }

    public String getDelivery_date() {
        return delivery_date;
    }

    public void setDelivery_date(String delivery_date) {
        this.delivery_date = delivery_date;
    }

    public String getJob_type() {
        return job_type;
    }

    public void setJob_type(String job_type) {
        this.job_type = job_type;
    }

    public String getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(String total_amount) {
        this.total_amount = total_amount;
    }

    public String getDate_added() {
        return date_added;
    }

    public void setDate_added(String date_added) {
        this.date_added = date_added;
    }

    public String getDate_update() {
        return date_update;
    }

    public void setDate_update(String date_update) {
        this.date_update = date_update;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreated_date_str() {
        return created_date_str;
    }

    public void setCreated_date_str(String created_date_str) {
        this.created_date_str = created_date_str;
    }
}
