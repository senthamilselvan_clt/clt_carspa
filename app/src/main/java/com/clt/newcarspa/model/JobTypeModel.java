package com.clt.newcarspa.model;

import java.util.List;

import io.realm.RealmObject;

public class JobTypeModel extends RealmObject {


    /**
     * action : get_services_list
     * id : 1
     * name : PDI/POLISH
     * other_name :
     * price : 10
     * active : 1
     * date_added : 2018-07-24 12:56:22
     * date_updated : 2018-07-24 13:12:21
     */

    private String action;
    private String id;
    private String name;
    private String other_name;
    private String price;
    private String active;
    private String date_added;
    private String date_updated;
    private boolean ischecked;
    private Integer selectedPosition;

    public double getCheckedPrice(List<JobTypeModel> jobType){
        double price = 0;

        for (int i = 0; i < jobType.size(); i++) {
            if (jobType.get(i).isIschecked()) {
                price += Double.parseDouble(jobType.get(i).getPrice());
            }
        }
        return price;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOther_name() {
        return other_name;
    }

    public void setOther_name(String other_name) {
        this.other_name = other_name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getDate_added() {
        return date_added;
    }

    public void setDate_added(String date_added) {
        this.date_added = date_added;
    }

    public String getDate_updated() {
        return date_updated;
    }

    public void setDate_updated(String date_updated) {
        this.date_updated = date_updated;
    }

    public boolean isIschecked() {
        return ischecked;
    }

    public void setIschecked(boolean ischecked) {
        this.ischecked = ischecked;
    }

    public Integer getSelectedPosition() {
        return selectedPosition;
    }

    public void setSelectedPosition(Integer selectedPosition) {
        this.selectedPosition = selectedPosition;
    }


}
