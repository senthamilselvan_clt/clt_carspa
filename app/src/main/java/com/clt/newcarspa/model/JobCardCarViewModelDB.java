package com.clt.newcarspa.model;

import io.realm.RealmObject;

/**
 * Created by Karthik Thumathy on 01-Aug-18.
 */
public class JobCardCarViewModelDB extends RealmObject {

    private String localdbid;
    private Integer job_card_id; // this id is reference (job_card_id) for class(JobCardItemsDetailsModel.class)
    private String img_back_bumper;
    private String imgRightTailLight;
    private String imgLeftTailLight;
    private String imgTailGate;
    private String imgRightBackWheel;
    private String imgLeftBackWheel;
    private String imgRightBackDoor;
    private String imgLeftBackDoor;
    private String imgRightFrontDoor;
    private String imgLeftFrontDoor;
    private String imgSteering;
    private String imgFrontWindScreen;
    private String imgRightFrontWheel;
    private String imgLeftFrontWheel;
    private String imgBonnet;
    private String imgRightFrontLight;
    private String imgLeftFrontLight;
    private String imgRightFrontFender;
    private String imgLeftFrontFender;
    private String imgFrontBumper;
    private Integer row_deleted;

    public String getLocaldbid() {
        return localdbid;
    }

    public void setLocaldbid(String localdbid) {
        this.localdbid = localdbid;
    }

    public Integer getJob_card_id() {
        return job_card_id;
    }

    public void setJob_card_id(Integer job_card_id) {
        this.job_card_id = job_card_id;
    }

    public String getImg_back_bumper() {
        return img_back_bumper;
    }

    public void setImg_back_bumper(String img_back_bumper) {
        this.img_back_bumper = img_back_bumper;
    }

    public String getImgRightTailLight() {
        return imgRightTailLight;
    }

    public void setImgRightTailLight(String imgRightTailLight) {
        this.imgRightTailLight = imgRightTailLight;
    }

    public String getImgLeftTailLight() {
        return imgLeftTailLight;
    }

    public void setImgLeftTailLight(String imgLeftTailLight) {
        this.imgLeftTailLight = imgLeftTailLight;
    }

    public String getImgTailGate() {
        return imgTailGate;
    }

    public void setImgTailGate(String imgTailGate) {
        this.imgTailGate = imgTailGate;
    }

    public String getImgRightBackWheel() {
        return imgRightBackWheel;
    }

    public void setImgRightBackWheel(String imgRightBackWheel) {
        this.imgRightBackWheel = imgRightBackWheel;
    }

    public String getImgLeftBackWheel() {
        return imgLeftBackWheel;
    }

    public void setImgLeftBackWheel(String imgLeftBackWheel) {
        this.imgLeftBackWheel = imgLeftBackWheel;
    }

    public String getImgRightBackDoor() {
        return imgRightBackDoor;
    }

    public void setImgRightBackDoor(String imgRightBackDoor) {
        this.imgRightBackDoor = imgRightBackDoor;
    }

    public String getImgLeftBackDoor() {
        return imgLeftBackDoor;
    }

    public void setImgLeftBackDoor(String imgLeftBackDoor) {
        this.imgLeftBackDoor = imgLeftBackDoor;
    }

    public String getImgRightFrontDoor() {
        return imgRightFrontDoor;
    }

    public void setImgRightFrontDoor(String imgRightFrontDoor) {
        this.imgRightFrontDoor = imgRightFrontDoor;
    }

    public String getImgLeftFrontDoor() {
        return imgLeftFrontDoor;
    }

    public void setImgLeftFrontDoor(String imgLeftFrontDoor) {
        this.imgLeftFrontDoor = imgLeftFrontDoor;
    }

    public String getImgSteering() {
        return imgSteering;
    }

    public void setImgSteering(String imgSteering) {
        this.imgSteering = imgSteering;
    }

    public String getImgFrontWindScreen() {
        return imgFrontWindScreen;
    }

    public void setImgFrontWindScreen(String imgFrontWindScreen) {
        this.imgFrontWindScreen = imgFrontWindScreen;
    }

    public String getImgRightFrontWheel() {
        return imgRightFrontWheel;
    }

    public void setImgRightFrontWheel(String imgRightFrontWheel) {
        this.imgRightFrontWheel = imgRightFrontWheel;
    }

    public String getImgLeftFrontWheel() {
        return imgLeftFrontWheel;
    }

    public void setImgLeftFrontWheel(String imgLeftFrontWheel) {
        this.imgLeftFrontWheel = imgLeftFrontWheel;
    }

    public String getImgBonnet() {
        return imgBonnet;
    }

    public void setImgBonnet(String imgBonnet) {
        this.imgBonnet = imgBonnet;
    }

    public String getImgRightFrontLight() {
        return imgRightFrontLight;
    }

    public void setImgRightFrontLight(String imgRightFrontLight) {
        this.imgRightFrontLight = imgRightFrontLight;
    }

    public String getImgLeftFrontLight() {
        return imgLeftFrontLight;
    }

    public void setImgLeftFrontLight(String imgLeftFrontLight) {
        this.imgLeftFrontLight = imgLeftFrontLight;
    }

    public String getImgRightFrontFender() {
        return imgRightFrontFender;
    }

    public void setImgRightFrontFender(String imgRightFrontFender) {
        this.imgRightFrontFender = imgRightFrontFender;
    }

    public String getImgLeftFrontFender() {
        return imgLeftFrontFender;
    }

    public void setImgLeftFrontFender(String imgLeftFrontFender) {
        this.imgLeftFrontFender = imgLeftFrontFender;
    }

    public String getImgFrontBumper() {
        return imgFrontBumper;
    }

    public void setImgFrontBumper(String imgFrontBumper) {
        this.imgFrontBumper = imgFrontBumper;
    }

    public Integer getRow_deleted() {
        return row_deleted;
    }

    public void setRow_deleted(Integer row_deleted) {
        this.row_deleted = row_deleted;
    }
}
