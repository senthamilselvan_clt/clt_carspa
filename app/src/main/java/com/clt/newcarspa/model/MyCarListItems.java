package com.clt.newcarspa.model;

import io.realm.RealmObject;

/**
 * Created by Karthik Thumathy on 23-Jul-18.
 */
public class MyCarListItems extends RealmObject {
    private String id;
    private String itemName;
    private int itemImage;

    public int getIsDisplay() {
        return isDisplay;
    }

    public void setIsDisplay(int isDisplay) {
        this.isDisplay = isDisplay;
    }

    private int isDisplay;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public int getItemImage() {
        return itemImage;
    }

    public void setItemImage(int itemImage) {
        this.itemImage = itemImage;
    }
}
