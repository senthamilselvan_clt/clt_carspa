package com.clt.newcarspa.model;

import java.util.Date;

import io.realm.RealmObject;

/**
 * Created by Karthik Thumathy on 27-Jul-18.
 */
public class JobCardItemsDetailsModel extends RealmObject {
    String id;
    Integer job_card_id; //this id is reference (id) for class(JobCardDetailsRealmDBModel.class)
    String service_id;
    String name;
    String other_name;
    Double price;
    Date date_added;
    Integer row_deleted;
    private Integer fromServerRecord;

    public Integer getFromServerRecord() {
        return fromServerRecord;
    }

    public void setFromServerRecord(Integer fromServerRecord) {
        this.fromServerRecord = fromServerRecord;
    }


    public Integer getRow_deleted() {
        return row_deleted;
    }

    public void setRow_deleted(Integer row_deleted) {
        this.row_deleted = row_deleted;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getJob_card_id() {
        return job_card_id;
    }

    public void setJob_card_id(Integer job_card_id) {
        this.job_card_id = job_card_id;
    }

    public String getService_id() {
        return service_id;
    }

    public void setService_id(String service_id) {
        this.service_id = service_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOther_name() {
        return other_name;
    }

    public void setOther_name(String other_name) {
        this.other_name = other_name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Date getDate_added() {
        return date_added;
    }

    public void setDate_added(Date date_added) {
        this.date_added = date_added;
    }

}
