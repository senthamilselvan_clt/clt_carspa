package com.clt.newcarspa.model;

import java.util.List;

/**
 * Created by Karthik Thumathy on 27-Jul-18.
 */
public class ReturnRealmModel {

    private String loginId;
    private String userName;
    private String shopId;
    private Integer afterDeleteImgSize;
    private Integer afterDeleteAudSize;
    private Integer afterDeleteImgAudSize;
    private Integer jobCardDetailSize;
    private Integer jobCardItemDetailSize;
    private Integer AddedExistingJobCardFromServerStatus;
    private List<JobCardDetailsRealmDBModel> jobCard;
    private List<JobCardItemsDetailsModel> jobCardItems;
    private List<ImageUploadModel> imageItems;
    private List<AudioUploadModel> audioItems;
    private Integer imageUploadStatus;
    private Integer audioUploadStatus;
    private boolean status;
    private Integer nextId;
    private Integer addedCarPartsStatus;
    private Integer imagedeletedStatus;
    private Integer audiodeletedStatus;
    private Integer imgWithAudiodeletedStatus;
    private Integer imageCarPartUpdateStatus;
    private List<GalleryUploadModel> galleryList;

    public List<GalleryUploadModel> getGalleryList() {
        return galleryList;
    }

    public void setGalleryList(List<GalleryUploadModel> galleryList) {
        this.galleryList = galleryList;
    }

    public Integer getGalleryListSize() {
        return galleryListSize;
    }

    public void setGalleryListSize(Integer galleryListSize) {
        this.galleryListSize = galleryListSize;
    }

    private Integer galleryListSize;

    public Integer getImgWithAudiodeletedStatus() {
        return imgWithAudiodeletedStatus;
    }

    public void setImgWithAudiodeletedStatus(Integer imgWithAudiodeletedStatus) {
        this.imgWithAudiodeletedStatus = imgWithAudiodeletedStatus;
    }

    public Integer getAfterDeleteImgAudSize() {
        return afterDeleteImgAudSize;
    }

    public void setAfterDeleteImgAudSize(Integer afterDeleteImgAudSize) {
        this.afterDeleteImgAudSize = afterDeleteImgAudSize;
    }


    public Integer getImageCarPartUpdateStatus() {
        return imageCarPartUpdateStatus;
    }

    public void setImageCarPartUpdateStatus(Integer imageCarPartUpdateStatus) {
        this.imageCarPartUpdateStatus = imageCarPartUpdateStatus;
    }

    public Integer getAddedCarPartsStatus() {
        return addedCarPartsStatus;
    }

    public void setAddedCarPartsStatus(Integer addedCarPartsStatus) {
        this.addedCarPartsStatus = addedCarPartsStatus;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public Integer getImageUploadStatus() {
        return imageUploadStatus;
    }

    public void setImageUploadStatus(Integer imageUploadStatus) {
        this.imageUploadStatus = imageUploadStatus;
    }

    public Integer getAudioUploadStatus() {
        return audioUploadStatus;
    }

    public void setAudioUploadStatus(Integer audioUploadStatus) {
        this.audioUploadStatus = audioUploadStatus;
    }

    public Integer getImagedeletedStatus() {
        return imagedeletedStatus;
    }

    public void setImagedeletedStatus(Integer imagedeletedStatus) {
        this.imagedeletedStatus = imagedeletedStatus;
    }

    public Integer getAudiodeletedStatus() {
        return audiodeletedStatus;
    }

    public void setAudiodeletedStatus(Integer audiodeletedStatus) {
        this.audiodeletedStatus = audiodeletedStatus;
    }

    public Integer getAfterDeleteImgSize() {
        return afterDeleteImgSize;
    }

    public void setAfterDeleteImgSize(Integer afterDeleteImgSize) {
        this.afterDeleteImgSize = afterDeleteImgSize;
    }

    public Integer getAfterDeleteAudSize() {
        return afterDeleteAudSize;
    }

    public void setAfterDeleteAudSize(Integer afterDeleteAudSize) {
        this.afterDeleteAudSize = afterDeleteAudSize;
    }

    public Integer getAddedExistingJobCardFromServerStatus() {
        return AddedExistingJobCardFromServerStatus;
    }

    public void setAddedExistingJobCardFromServerStatus(Integer addedExistingJobCardFromServerStatus) {
        AddedExistingJobCardFromServerStatus = addedExistingJobCardFromServerStatus;
    }

    public List<ImageUploadModel> getImageItems() {
        return imageItems;
    }

    public void setImageItems(List<ImageUploadModel> imageItems) {
        this.imageItems = imageItems;
    }

    public List<AudioUploadModel> getAudioItems() {
        return audioItems;
    }

    public void setAudioItems(List<AudioUploadModel> audioItems) {
        this.audioItems = audioItems;
    }

    public List<JobCardDetailsRealmDBModel> getJobCard() {
        return jobCard;
    }

    public void setJobCard(List<JobCardDetailsRealmDBModel> jobCard) {
        this.jobCard = jobCard;
    }

    public List<JobCardItemsDetailsModel> getJobCardItems() {
        return jobCardItems;
    }

    public void setJobCardItems(List<JobCardItemsDetailsModel> jobCardItems) {
        this.jobCardItems = jobCardItems;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Integer getNextId() {
        return nextId;
    }

    public void setNextId(Integer nextId) {
        this.nextId = nextId;
    }

    public Integer getJobCardDetailSize() {
        return jobCardDetailSize;
    }

    public void setJobCardDetailSize(Integer jobCardDetailSize) {
        this.jobCardDetailSize = jobCardDetailSize;
    }

    public Integer getJobCardItemDetailSize() {
        return jobCardItemDetailSize;
    }

    public void setJobCardItemDetailSize(Integer jobCardItemDetailSize) {
        this.jobCardItemDetailSize = jobCardItemDetailSize;
    }
}
