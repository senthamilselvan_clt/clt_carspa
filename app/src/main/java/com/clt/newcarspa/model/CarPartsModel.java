package com.clt.newcarspa.model;

import io.realm.RealmObject;

/**
 * Created by Karthik Thumathy on 24-Oct-18.
 */
public class CarPartsModel extends RealmObject {
    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCar_parts() {
        return car_parts;
    }

    public void setCar_parts(String car_parts) {
        this.car_parts = car_parts;
    }

    private String action;
    private Integer id;
    private Integer car_parts_id;
    private String car_parts;

    public Integer getCar_parts_id() {
        return car_parts_id;
    }

    public void setCar_parts_id(Integer car_parts_id) {
        this.car_parts_id = car_parts_id;
    }

}
