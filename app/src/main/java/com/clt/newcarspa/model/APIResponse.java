package com.clt.newcarspa.model;

/**
 * Created by Siva Kumar on 14-04-2016.
 */
public class APIResponse<T> {
    private String status;
    private T result;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public T getResult() {
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }
}
