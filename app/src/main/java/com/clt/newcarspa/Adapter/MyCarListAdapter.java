package com.clt.newcarspa.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.clt.newcarspa.R;
import com.clt.newcarspa.Utils.Constant;
import com.clt.newcarspa.cltinterfaces.OnItemClickListener;
import com.clt.newcarspa.model.MyCarListItems;

import java.util.List;
import java.util.Locale;

/**
 * Created by Karthik Thumathy on 23-Jul-18.
 */
public class MyCarListAdapter extends RecyclerView.Adapter<MyCarListAdapter.MyCarListHolder> {

    private static final String TAG = MyCarListAdapter.class.getCanonicalName();
    private static final int ITEM = 0;
    private static final int LOADING = 1;
    private Context context;
    private Locale current;
    private String current_language;
    private ViewGroup groupParent;
    OnItemClickListener mItemClickListener;
    public MyCarListHolder rest_holder;
    private List<MyCarListItems> carListItems;
    private boolean isLoadingAdded = false;
    private Integer[] mMyCarListsImages = {
            R.drawable.ic_idcard,
            R.drawable.file, R.drawable.ic_news,
            R.drawable.today, R.drawable.ic_news,
            R.drawable.today, R.drawable.ic_idcard,
            R.drawable.credit, R.drawable.ic_settings,
            R.drawable.ic_logoutnew
    };

    public MyCarListAdapter(Context con, List<MyCarListItems> data) {
        this.context = con;
        current = con.getResources().getConfiguration().locale;
        current_language = current.toString();
        if (data != null) {
            this.carListItems = data;
        }
    }

    @Override
    public MyCarListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        MyCarListHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case ITEM:
                viewHolder = getViewHolder(parent, inflater);
                break;
        }
        return viewHolder;
    }

    @NonNull
    private MyCarListHolder getViewHolder(ViewGroup parent, LayoutInflater inflater) {
        MyCarListHolder viewHolder;
        View v1 = inflater.inflate(R.layout.activity_mycaritemlist, parent, false);
        viewHolder = new MyCarListHolder(v1);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(MyCarListHolder holder, final int position) {

        MyCarListItems carlist = carListItems.get(position);

        switch (getItemViewType(position)) {
            case ITEM:
                Locale current = context.getResources().getConfiguration().locale;
                String current_language = current.toString();
                if(carlist.getItemName().contains(Constant.NEWJOBCARD)) {
                    holder.cardView.setBackgroundResource(R.drawable.carbg8);
//                    holder.imgItem.setImageResource(R.drawable.carbg8);
                }else if(carlist.getItemName().contains(Constant.VIEWJOBCARDLIST)){
                    holder.cardView.setBackgroundResource(R.drawable.carbg6);
//                    holder.imgItem.setImageResource(R.drawable.carbg6);
                }else if(carlist.getItemName().contains(Constant.REPORT)){
                    holder.cardView.setBackgroundResource(R.drawable.carbg13);
//                    holder.imgItem.setImageResource(R.drawable.carbg13);
                }else if(carlist.getItemName().contains(Constant.TODAYDELIVERY)){
                    holder.cardView.setBackgroundResource(R.drawable.carbg9);
//                    holder.imgItem.setImageResource(R.drawable.carbg9);
                }else if(carlist.getItemName().contains(Constant.CREATEINVOICE)){
                    holder.cardView.setBackgroundResource(R.drawable.carbg2);
//                    holder.imgItem.setImageResource(R.drawable.carbg2);
                }else if(carlist.getItemName().contains(Constant.FINDINVOICE)){
                    holder.cardView.setBackgroundResource(R.drawable.carbg7);
//                    holder.imgItem.setImageResource(R.drawable.carbg7);
                }else if(carlist.getItemName().contains(Constant.DRAFT)){
                    holder.cardView.setBackgroundResource(R.drawable.carbg10);
//                    holder.imgItem.setImageResource(R.drawable.carbg10);
                }else if(carlist.getItemName().contains(Constant.CREDITINVOICE)){
                    holder.cardView.setBackgroundResource(R.drawable.draft_primarycolor_roundcorners);
//                    holder.imgItem.setImageResource(R.drawable.draft_primarycolor_roundcorners);
                }else if(carlist.getItemName().contains(Constant.SETTINGS)){
                    holder.cardView.setBackgroundResource(R.drawable.carbg2);
//                    holder.imgItem.setImageResource(R.drawable.carbg2);
                }else if(carlist.getItemName().contains(Constant.GALLERY)){
                    holder.cardView.setBackgroundResource(R.drawable.carbg7);
//                    holder.imgItem.setImageResource(R.drawable.carbg2);
                }else if(carlist.getItemName().contains(Constant.PUSH)){
                    holder.cardView.setBackgroundResource(R.drawable.carbg12);
//                    holder.imgItem.setImageResource(R.drawable.carbg12);
                }else if(carlist.getItemName().contains(Constant.LOGOUT)){
                    holder.cardView.setBackgroundResource(R.drawable.carbg1);
//                    holder.imgItem.setImageResource(R.drawable.carbg1);
                }
                holder.tvItemName.setText(carlist.getItemName());
                holder.tvItemName.setSelected(true);
                holder.tvItemName.setSingleLine(true);

                break;
        }

    }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return carListItems == null ? 0 : carListItems.size();
    }

    @Override
    public int getItemViewType(int position) {
        return (position == carListItems.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
    }

    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new MyCarListItems());
    }

    public void addAll(List<MyCarListItems> results) {
        for (MyCarListItems result : results) {
            add(result);
        }
    }

    public void add(MyCarListItems r) {
        carListItems.add(r);
        notifyItemInserted(carListItems.size() - 1);
    }

    public void clear() {
        isLoadingAdded = false;
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }

    public void remove(MyCarListItems r) {
        int position = carListItems.indexOf(r);
        if (position > -1) {
            carListItems.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void removeLoadingFooter() {
        if (isLoadingAdded) {
            isLoadingAdded = false;

            int position = carListItems.size() - 1;
            MyCarListItems result = getItem(position);

            if (result != null) {
                carListItems.remove(position);
                notifyItemRemoved(position);
            }
        }
    }

    public MyCarListItems getItem(int position) {
        try {
            return carListItems.get(position);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public class MyCarListHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private CardView cardView;
        private TextView tvItemName;
        private ImageView imgItem;

        private MyCarListHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            cardView = itemView.findViewById(R.id.card_view);
//            imgItem = itemView.findViewById(R.id.img_item);
            tvItemName = itemView.findViewById(R.id.tv_itemname);
        }

        @Override
        public void onClick(View v) {
            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(v, getAdapterPosition());
            }
        }
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    private class LoadingVH extends MyCarListHolder {

        private LoadingVH(View itemView) {
            super(itemView);
        }
    }

}
