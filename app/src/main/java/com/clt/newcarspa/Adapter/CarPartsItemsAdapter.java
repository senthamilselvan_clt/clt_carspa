package com.clt.newcarspa.Adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.clt.newcarspa.R;
import com.clt.newcarspa.cltinterfaces.OnItemClickListener;
import com.clt.newcarspa.model.CarPartsModel;

import java.util.List;
import java.util.Locale;

/**
 * Created by Karthik_Thumathy on 26/10/17.
 */

public class CarPartsItemsAdapter extends RecyclerView.Adapter<CarPartsItemsAdapter.CarPartHolder> {

    private static final String TAG = CarPartsItemsAdapter.class.getCanonicalName();
    private Context context;
    private Locale current;
    private String current_language;
    private ViewGroup groupParent;
    OnItemClickListener mItemClickListener;
    public CarPartHolder rest_holder;
    private List<CarPartsModel> carPartsModelList;
    private ButtonAdapterCallback butttoncallback;
    private android.support.v7.app.AlertDialog carPartDialog;
    private String selectedImgUniqueId;

    public CarPartsItemsAdapter(Context con, List<CarPartsModel> carPartsModels, android.support.v7.app.AlertDialog carPartDialog, String selectedImgUniqueId) {
        this.context = con;
        current = con.getResources().getConfiguration().locale;
        current_language = current.toString();
        this.carPartsModelList = carPartsModels;
        this.carPartDialog = carPartDialog;
        this.selectedImgUniqueId = selectedImgUniqueId;
    }

    @Override
    public CarPartHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View jobTypelayout = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.layout_carpartsitems, parent, false);
        CarPartHolder typeHolder = new CarPartHolder(jobTypelayout);
        groupParent = parent;
        return typeHolder;
    }

    @Override
    public void onBindViewHolder(final CarPartHolder holder, final int position) {
        final CarPartsModel carpart = carPartsModelList.get(position);
        Locale current = context.getResources().getConfiguration().locale;
        String current_language = current.toString();

        if (carpart.getCar_parts() != null) {
            holder.tvCarPartName.setText(carpart.getCar_parts());
        }else{
            holder.tvCarPartName.setText("");
        }
        holder.cvCarPartName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (butttoncallback != null) {
                    butttoncallback.getSelectedCarPart(position, carpart.getId(), carpart.getCar_parts(), carPartDialog, selectedImgUniqueId);
                }
            }
        });
    }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return carPartsModelList == null ? 0 : carPartsModelList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return carPartsModelList.size();
    }


    public class CarPartHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private CardView cvCarPartName;
        private TextView tvCarPartName;

        private CarPartHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            cvCarPartName = itemView.findViewById(R.id.cv_carpartname);
            tvCarPartName = itemView.findViewById(R.id.tv_carpartname);
        }

        @Override
        public void onClick(View v) {
            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(v, getAdapterPosition());
            }
        }
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public void setCallback(ButtonAdapterCallback callback) {

        this.butttoncallback = callback;
    }

    public interface ButtonAdapterCallback {

        public void getSelectedCarPart(int position, Integer carPartId, String carPartName, android.support.v7.app.AlertDialog carPartDialog, String selectedImgUniqueId);

    }
}
