package com.clt.newcarspa.Adapter;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.clt.newcarspa.R;
import com.clt.newcarspa.cltinterfaces.OnItemClickListener;
import com.clt.newcarspa.model.AudioUploadModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Karthik_Thumathy on 31/08/18.
 */

public class AddedImgAudioListAdapter extends RecyclerView.Adapter<AddedImgAudioListAdapter.ImgAudioListHolder> {

    private static final String TAG = AddedImgAudioListAdapter.class.getCanonicalName();
    private static final int ITEM = 0;
    private Context context;
    private Locale current;
    private String current_language;
    private ViewGroup groupParent;
    private OnItemClickListener mItemClickListener;
    private ImgAudioListHolder rest_holder;
    private ButtonAdapterCallback butttoncallback;
    private List<AudioUploadModel> imagelist = new ArrayList<>();
    private String audiopath;
    private Integer itemId;

    public AddedImgAudioListAdapter(Context con, List<AudioUploadModel> audlist) {
        this.context = con;
        current = con.getResources().getConfiguration().locale;
        current_language = current.toString();
        this.itemId = itemId;
        if (audlist != null) {
            this.imagelist = audlist;
        }
    }

    @Override
    public ImgAudioListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View dealsListLayout = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.audios_listitems, parent, false);
        ImgAudioListHolder FoodCartHolder_viewholder = new ImgAudioListHolder(dealsListLayout);
        groupParent = parent;
        return FoodCartHolder_viewholder;
    }

    @Override
    public void onBindViewHolder(final ImgAudioListHolder holder, final int position) {

        rest_holder = (ImgAudioListHolder) holder;
        final AudioUploadModel results = imagelist.get(position);

        Locale current = context.getResources().getConfiguration().locale;
        String current_language = current.toString();
        int pos = position + 1;
        rest_holder.tvTitle.setText(""+pos);
        showProductPicture(audiopath +results, position);
        rest_holder.audCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (butttoncallback != null) {
                    butttoncallback.audioItem(position, results.getAudio_encode_str(), results.getAudio_filename(), results.getAudio_filepath());
                }
            }
        });
        rest_holder.audDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (butttoncallback != null) {
                    butttoncallback.audiodelete(position, results.getAudio_filename(), results.getImage_uniqueid(), results.getAudio_uniqueid());
                }
            }
        });
        rest_holder.singleCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    private void showProductPicture(String fileKey, int position) {
        try {
            Glide.with(context)
                    .load(R.drawable.ic_action_audios)
                    .into(rest_holder.audCart);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return imagelist == null ? 0 : imagelist.size();
    }

    @Override
    public int getItemViewType(int position) {
        return ITEM;
    }


    public class ImgAudioListHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ConstraintLayout singleCardView;
        private ImageView audCart, audDelete;
        private TextView tvTitle;

        private ImgAudioListHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            tvTitle = itemView.findViewById(R.id.tv_title);
            audCart = itemView.findViewById(R.id.aud_cart);
            singleCardView = itemView.findViewById(R.id.cl_constraint);
            audDelete = itemView.findViewById(R.id.aud_delete);
        }

        @Override
        public void onClick(View v) {
            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(v, getAdapterPosition());
            }
        }
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public void setCallback(ButtonAdapterCallback callback) {

        this.butttoncallback = callback;
    }

    public interface ButtonAdapterCallback {

        public void audiodelete(int position, String audName, String imageUniqueId, Integer audioUniqueId);
        public void audioItem(int position, String encodestring, String audioName, String filePath);
    }

}
