package com.clt.newcarspa.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;

import com.clt.newcarspa.R;
import com.clt.newcarspa.cltinterfaces.OnItemClickListener;
import com.clt.newcarspa.model.JobCardDetailsModel;
import com.clt.newcarspa.model.JobCardDetailsRealmDBModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class TodayDeliveryJobCardAdapter extends RecyclerView.Adapter<TodayDeliveryJobCardAdapter.JobCardHolder> {

    JobCardDetailsModel cartForm;
    Context context;
    List<JobCardDetailsRealmDBModel> listCart;
    LayoutInflater inflater;
    String appendSelectedList = "";
    OnItemClickListener mItemClickListener;
    ArrayList<String> userArr = new ArrayList<>();
    private ViewGroup groupParent;
    private ButtonAdapterCallback butttoncallback;
    private ArrayAdapter ad;
    private String status;


    public TodayDeliveryJobCardAdapter(Context context, List<JobCardDetailsRealmDBModel> listCart) {
        this.context = context;
        this.listCart = listCart;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public JobCardHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View jobCardlayout = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.fragment_jobcard_listitems, parent, false);
        JobCardHolder typeHolder = new JobCardHolder(jobCardlayout);
        groupParent = parent;
        return typeHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final JobCardHolder holder, int position) {
        final JobCardDetailsRealmDBModel jobType = listCart.get(position);
        holder.spinnerstatus.setVisibility(View.GONE);
        holder.tvStatus.setVisibility(View.VISIBLE);
        holder.tvJobCardId.setText(Html.fromHtml("<B>Id: </B>" + jobType.getId()));
        holder.tvName.setText(Html.fromHtml("<B>Name: </B>" + jobType.getName()));
        holder.tvSpinnerStatusTxt.setText(Html.fromHtml("<B>" + context.getResources().getString(R.string.text_jobcardstatus) + ":" + " </B>"));
//        holder.tvTextCustomerCode.setText(Html.fromHtml("<B>" + context.getResources().getString(R.string.text_jc_customercode) + ":" + " </B>"));
        holder.tvCustomerCode.setText(Html.fromHtml("<B>" + context.getResources().getString(R.string.text_jc_customercode) + ":" + " </B>"+jobType.getCustomer_code()));
        holder.tvItemVatReg.setText(jobType.getInvoice_no());

        SimpleDateFormat output = new SimpleDateFormat("dd-MM-yyyy");
        SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd");
        Date getDateInput = null;
        try {
            getDateInput = input.parse(jobType.getDate_added());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.tvDate.setText(Html.fromHtml("<B>Date: </B>" + output.format(getDateInput)));
        holder.tvGrandTotal.setText(Html.fromHtml("<B>Total: </B>" + jobType.getTotal_amount() + context.getResources().getString(R.string.text_currency)));
        holder.tvStatus.setText(jobType.getStatus());
//        holder.spinnerstatus.setText(jobType.getStatus());
//        holder.spinnerstatus.setEnabled(false);


//        holder.array = holder.spinnerstatus.getResources().getStringArray(R.array.service_status);
//        ArrayAdapter ad = new ArrayAdapter(context, R.layout.layout_spinner, R.id.tvSpinnerText, holder.array);
//        holder.spinnerstatus.setAdapter(ad);
    }

    @Override
    public long getItemId(int i) {
        return listCart.size();
    }

    @Override
    public int getItemCount() {
        return listCart.size();
    }


    public class JobCardHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private String[] array;
        private AutoCompleteTextView spinnerstatus;
        private TextView tvDate, tvGrandTotal, tvTextCustomerCode, tvItemVatReg, tvCustomerCode, tvJobCardId, tvName, tvSpinnerStatusTxt, tvStatus;

        private JobCardHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            tvJobCardId = itemView.findViewById(R.id.tv_itemid);
            tvName = itemView.findViewById(R.id.tv_itemname);
            tvTextCustomerCode = itemView.findViewById(R.id.tv_itemcustomercodetxt);
            tvCustomerCode = itemView.findViewById(R.id.tv_itemcustomercode);
            tvItemVatReg = itemView.findViewById(R.id.tv_itemvatreg);
            tvDate = itemView.findViewById(R.id.tv_date);
            tvGrandTotal = itemView.findViewById(R.id.tv_total);
            spinnerstatus = itemView.findViewById(R.id.spinnerstatus);
            tvStatus = itemView.findViewById(R.id.tv_status);
            tvSpinnerStatusTxt = itemView.findViewById(R.id.tv_spinnerstatustxt);
        }

        @Override
        public void onClick(View v) {
            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(v, getAdapterPosition());
                mItemClickListener.onItemClickWithId(v, getAdapterPosition(), listCart.get(getAdapterPosition()).getId());
            }
        }

    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public void setCallback(TodayDeliveryJobCardAdapter.ButtonAdapterCallback callback) {

        this.butttoncallback = callback;
    }

    public interface ButtonAdapterCallback {
//        public void callUpdateStatusService(JobCardDetailsRealmDBModel model, Integer pos);
    }
}
