package com.clt.newcarspa.Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.clt.newcarspa.R;
import com.clt.newcarspa.cltinterfaces.OnItemClickListener;
import com.clt.newcarspa.database.RealmDBHelper;
import com.clt.newcarspa.model.AudioUploadModel;
import com.clt.newcarspa.model.ImageUploadModel;
import com.joaquimley.faboptions.FabOptions;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import io.realm.Realm;

/**
 * Created by Karthik_Thumathy on 31/08/18.
 */

public class UpdateImageListAdapter extends RecyclerView.Adapter<UpdateImageListAdapter.AddImageListHolder> {

    private Realm mRealm;
    private RealmDBHelper realmDBHelper;
    private static final String TAG = UpdateImageListAdapter.class.getCanonicalName();
    private static final int ITEM = 0;
    private Context context;
    private Locale current;
    private String current_language;
    private ViewGroup groupParent;
    private OnItemClickListener mItemClickListener;
    private AddImageListHolder rest_holder;
    private ButtonAdapterCallback butttoncallback;
    private List<ImageUploadModel> imagelist = new ArrayList<>();
    private String imagepath;
    private List<AudioUploadModel> audioList;

    public UpdateImageListAdapter(Context con, List<ImageUploadModel> imglist) {
        this.context = con;
        current = con.getResources().getConfiguration().locale;
        current_language = current.toString();
        if (imglist != null) {
            this.imagelist = imglist;
        }
    }

    @Override
    public AddImageListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View dealsListLayout = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.images_listitems, parent, false);
        AddImageListHolder FoodCartHolder_viewholder = new AddImageListHolder(dealsListLayout);
        groupParent = parent;
        return FoodCartHolder_viewholder;
    }

    @Override
    public void onBindViewHolder(final AddImageListHolder holder, final int position) {
        rest_holder = (AddImageListHolder) holder;
        mRealm = Realm.getDefaultInstance();
        realmDBHelper = new RealmDBHelper(context);
        final ImageUploadModel results = imagelist.get(position);
        Locale current = context.getResources().getConfiguration().locale;
        String current_language = current.toString();
        decodeBase64AndSetImage(results.getImage_encode_str(), rest_holder.imgCart);
        final int pos = position + 1;
        rest_holder.tvTitle.setText("" + pos);
//        rest_holder.fabOptions.setButtonsMenu(R.menu.fab_menu);
        if (results.getImage_carpartname() != null) {
            String carPartName = !results.getImage_carpartname().isEmpty() ? results.getImage_carpartname() : "";
            rest_holder.tvCarPart.setText(carPartName);
        }else{
            rest_holder.tvCarPart.setText("None");
        }
        rest_holder.imgCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (butttoncallback != null) {
                    butttoncallback.imageItem(position, results.getId(), results.getImage_uniqueid(), results.getImage_encode_str(), "");
                }
            }
        });
        rest_holder.imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (butttoncallback != null) {
                    butttoncallback.imagedelete(position, results.getImage_encode_str());
                }
            }
        });

        rest_holder.tvCarPart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (butttoncallback != null) {
                    butttoncallback.addCarPart(position, context, results.getImage_uniqueid());
                }
            }
        });
        rest_holder.imgPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (butttoncallback != null) {
                    butttoncallback.addAudio(position, results.getImage_uniqueid());
//                            alertDialog.dismiss();
                }
            }
        });
        if (results.getId() != null && results.getImage_uniqueid() != null) {
            audioList = new ArrayList<>();
            audioList = realmDBHelper.getAudiosFromSelectedIdAndImgUniqueId(mRealm, results.getId(), results.getImage_uniqueid());
            if (audioList != null && audioList.size() > 0) {
                rest_holder.tv_notification.setVisibility(View.VISIBLE);
                rest_holder.tv_notification.setText(" "+audioList.size()+" ");
            }
        }
    }

    private void decodeBase64AndSetImage(String completeImageData, ImageView imageView) {
        // Incase you're storing into aws or other places where we have extension stored in the starting.
        String imageDataBytes = completeImageData.substring(completeImageData.indexOf(",") + 1);
        InputStream stream = new ByteArrayInputStream(Base64.decode(imageDataBytes.getBytes(), Base64.DEFAULT));
        Bitmap bitmap = BitmapFactory.decodeStream(stream);
        imageView.setImageBitmap(bitmap);
    }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return imagelist == null ? 0 : imagelist.size();
    }

    @Override
    public int getItemViewType(int position) {
        return ITEM;
    }


    public class AddImageListHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ConstraintLayout singleCardView;
        private ImageView imgCart, imgDelete, imgPlus;
        private TextView tvTitle, tvCarPart, tv_notification;
        private FabOptions fabOptions;

        private AddImageListHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            tvTitle = itemView.findViewById(R.id.tv_title);
            imgCart = itemView.findViewById(R.id.aud_cart);
            imgPlus = itemView.findViewById(R.id.imgplus);
//            fabOptions = (FabOptions) itemView.findViewById(R.id.faboption);
            tvCarPart = itemView.findViewById(R.id.tv_carpart);
            tv_notification = itemView.findViewById(R.id.tv_notification);
            singleCardView = itemView.findViewById(R.id.cl_imageconstraint);
            imgDelete = itemView.findViewById(R.id.aud_delete);
        }

        @Override
        public void onClick(View v) {
            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(v, getAdapterPosition());
            }
        }
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public void setCallback(ButtonAdapterCallback callback) {

        this.butttoncallback = callback;
    }

    public interface ButtonAdapterCallback {

        public void imagedelete(int position, String imgName);

        public void imageItem(int position, Integer id, String imgUniqueId, String imgName, String imagePath);

        public void addAudio(int position, String imgUniqueId);

        public void addCarPart(int position, Context context, String imgUniqueId);
    }

}
