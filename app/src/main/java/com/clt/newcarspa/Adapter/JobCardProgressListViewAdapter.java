package com.clt.newcarspa.Adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;

import com.clt.newcarspa.R;
import com.clt.newcarspa.cltinterfaces.OnItemClickListener;
import com.clt.newcarspa.model.JobCardDetailsModel;
import com.clt.newcarspa.model.JobCardDetailsRealmDBModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class JobCardProgressListViewAdapter extends RecyclerView.Adapter<JobCardProgressListViewAdapter.JobCardHolder> {

    JobCardDetailsModel cartForm;
    Context context;
    List<JobCardDetailsRealmDBModel> listCart;
    LayoutInflater inflater;
    String appendSelectedList = "";
    OnItemClickListener mItemClickListener;
    ArrayList<String> userArr = new ArrayList<>();
    private ViewGroup groupParent;
    private ButtonAdapterCallback butttoncallback;
    private ArrayAdapter ad;
    private boolean isLoadingAdded = false;
    private static final int ITEM = 0;
    private static final int LOADING = 1;
    private String status;


    public JobCardProgressListViewAdapter(Context context, List<JobCardDetailsRealmDBModel> listCart) {
        this.context = context;
        this.listCart = listCart;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public JobCardHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View jobCardlayout = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.fragment_jobcard_listitems, parent, false);
        JobCardHolder typeHolder = new JobCardHolder(jobCardlayout);
        groupParent = parent;
        return typeHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final JobCardHolder holder, int position) {
        final JobCardDetailsRealmDBModel jobType = listCart.get(position);

        holder.tvJobCardId.setText(Html.fromHtml("<B>Id: </B>" + jobType.getId()));
        holder.tvName.setText(Html.fromHtml("<B>Name: </B>" + jobType.getName()));
        holder.tvSpinnerStatusTxt.setText(Html.fromHtml("<B>" + context.getResources().getString(R.string.text_jobcardstatus) + ":" + " </B>"));
        holder.tvCustomerCode.setText(Html.fromHtml("<B>" + context.getResources().getString(R.string.text_jc_customercode) + ":" + " </B>"+jobType.getCustomer_code()+" ("+jobType.getMobile_no()+") "));
        holder.tvItemVatReg.setText(jobType.getInvoice_no());

        SimpleDateFormat output = new SimpleDateFormat("dd-MM-yyyy");
        SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd");
        Date getDateInput = null;
        try {
            if (jobType.getDate_added() != null) {
                getDateInput = input.parse(jobType.getDate_added());
                holder.tvDate.setText(Html.fromHtml("<B>Date: </B>" + output.format(getDateInput)));
            }else{

            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.tvGrandTotal.setText(Html.fromHtml("<B>Total: </B>" + jobType.getTotal_amount() + context.getResources().getString(R.string.text_currency)));
        if (jobType.getStatus() != null) {
            holder.spinnerstatus.setText(jobType.getStatus());
            if (jobType.getStatus().equals("Delivered")) {
                holder.spinnerstatus.setSelection(3);
                holder.spinnerstatus.setEnabled(false);
            }
        }
        holder.spinnerstatus.setFocusable(false);
        holder.spinnerstatus.setThreshold(50);
        holder.spinnerstatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.spinnerstatus.showDropDown();
            }
        });
        holder.spinnerstatus.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, final int pos, long l) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                alertDialog.setTitle(context.getResources().getString(R.string.text_alert));
                alertDialog.setMessage(context.getResources().getString(R.string.text_update_status));
                alertDialog.setPositiveButton(context.getResources().getString(R.string.msg_yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        JobCardDetailsRealmDBModel model = new JobCardDetailsRealmDBModel();
                        model.setAction("set_job_card_update_status");
                        model.setId(jobType.getId());
                        model.setUser_id(jobType.getUser_id());
                        model.setShop_id(jobType.getShop_id());
                        model.setStatus(holder.array[pos]);
                        if (butttoncallback != null) {
                            butttoncallback.callUpdateStatusService(model, pos);
                        }
                    }
                });
                alertDialog.setNegativeButton(context.getResources().getString(R.string.msg_no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                alertDialog.show();
            }
        });

        holder.array = holder.spinnerstatus.getResources().getStringArray(R.array.service_status);
        ArrayAdapter ad = new ArrayAdapter(context, R.layout.layout_spinner, R.id.tvSpinnerText, holder.array);
        holder.spinnerstatus.setAdapter(ad);
    }

    private void onChangeStatus(final int pos, final String[] array) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle(context.getResources().getString(R.string.text_alert));
        alertDialog.setMessage(context.getResources().getString(R.string.text_update_status));
        alertDialog.setPositiveButton(context.getResources().getString(R.string.msg_yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                JobCardDetailsRealmDBModel model = new JobCardDetailsRealmDBModel();
                model.setAction("set_job_card_update_status");
                model.setId(listCart.get(pos).getId());
                model.setUser_id(listCart.get(pos).getUser_id());
                model.setShop_id(listCart.get(pos).getShop_id());
                model.setStatus(array[pos]);
                if (butttoncallback != null) {
                    butttoncallback.callUpdateStatusService(model, pos);
                }

            }
        });
        alertDialog.setNegativeButton(context.getResources().getString(R.string.msg_no), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertDialog.show();
    }

    @Override
    public long getItemId(int i) {
        return listCart.size();
    }

    @Override
    public int getItemCount() {
        return listCart == null ? 0 : listCart.size();
    }

    @Override
    public int getItemViewType(int position) {
        return (position == listCart.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
    }


    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new JobCardDetailsRealmDBModel());
    }

    public void addAll(List<JobCardDetailsRealmDBModel> results) {
        for (JobCardDetailsRealmDBModel result : results) {
            add(result);
        }
    }

    public void add(JobCardDetailsRealmDBModel r) {
        listCart.add(r);
        notifyItemInserted(listCart.size() - 1);
    }

    public void clear() {
        isLoadingAdded = false;
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }

    public void remove(JobCardDetailsRealmDBModel r) {
        int position = listCart.indexOf(r);
        if (position > -1) {
            listCart.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void removeLoadingFooter() {
        if (isLoadingAdded) {
            isLoadingAdded = false;

            int position = listCart.size() - 1;
            JobCardDetailsRealmDBModel result = getItem(position);

            if (result != null) {
                listCart.remove(position);
                notifyItemRemoved(position);
            }
        }
    }

    public JobCardDetailsRealmDBModel getItem(int position) {
        try {
            return listCart.get(position);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public class JobCardHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private String[] array;
        private AutoCompleteTextView spinnerstatus;
        private TextView tvDate, tvGrandTotal, tvTextCustomerCode, tvItemVatReg, tvCustomerCode, tvJobCardId, tvName, tvSpinnerStatusTxt;

        private JobCardHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            tvJobCardId = itemView.findViewById(R.id.tv_itemid);
            tvName = itemView.findViewById(R.id.tv_itemname);
            tvTextCustomerCode = itemView.findViewById(R.id.tv_itemcustomercodetxt);
            tvCustomerCode = itemView.findViewById(R.id.tv_itemcustomercode);
            tvItemVatReg = itemView.findViewById(R.id.tv_itemvatreg);
            tvDate = itemView.findViewById(R.id.tv_date);
            tvGrandTotal = itemView.findViewById(R.id.tv_total);
            spinnerstatus = itemView.findViewById(R.id.spinnerstatus);
            tvSpinnerStatusTxt = itemView.findViewById(R.id.tv_spinnerstatustxt);
        }

        @Override
        public void onClick(View v) {
            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(v, getAdapterPosition());
                mItemClickListener.onItemClickWithId(v, getAdapterPosition(), listCart.get(getAdapterPosition()).getId());
            }
        }

    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public void setCallback(JobCardProgressListViewAdapter.ButtonAdapterCallback callback) {

        this.butttoncallback = callback;
    }

    public interface ButtonAdapterCallback {
        public void callUpdateStatusService(JobCardDetailsRealmDBModel model, Integer pos);
    }
}
