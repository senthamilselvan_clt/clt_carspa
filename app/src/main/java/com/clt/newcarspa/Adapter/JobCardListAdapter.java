package com.clt.newcarspa.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.clt.newcarspa.Fragment.JobCardCompletedFragment;
import com.clt.newcarspa.Fragment.JobCardPendingFragment;
import com.clt.newcarspa.Fragment.JobCardProgressFragment;
import com.clt.newcarspa.model.JobCardDetailsRealmDBModel;

import java.util.List;

public class JobCardListAdapter extends FragmentStatePagerAdapter {

    private int tabCount;
    private List<JobCardDetailsRealmDBModel> questionLists;
    private boolean liveOrLocal;
    private String loginUserId, loginShopId;

    public JobCardListAdapter(FragmentManager fm, int tabCount, boolean liveOrLocal, String loginUserId, String loginShopId) {
        super(fm);
        this.tabCount = tabCount;
        this.liveOrLocal = liveOrLocal;
        this.loginUserId = loginUserId;
        this.loginShopId = loginShopId;
    }

    @Override
    public Fragment getItem(int position) {
        JobCardPendingFragment pendingFragment = new JobCardPendingFragment();
        switch (position) {
            case 0:
                return setFragmentOne(position, liveOrLocal, loginUserId, loginShopId);
            case 1:
                return setFragmentTwo(position, liveOrLocal, loginUserId, loginShopId);
            case 2:
                return setFragmentThree(position, liveOrLocal, loginUserId, loginShopId);
            default:
                return setFragmentOne(position, liveOrLocal, loginUserId, loginShopId);
        }
    }

    private JobCardPendingFragment setFragmentOne(Integer position, boolean liveOrLocal, String loginUserId, String loginShopId) {
       /* TestPageFragment fragment = new TestPageFragment();
        Bundle bundle = new Bundle();
//        bundle.putString("allTabNames", tab_names);
        fragment.setArguments(bundle);*/
        return JobCardPendingFragment.newInstance(position, liveOrLocal, loginUserId, loginShopId);
    }

    private JobCardProgressFragment setFragmentTwo(Integer position, boolean liveOrLocal, String loginUserId, String loginShopId) {
       /* TestPageFragment fragment = new TestPageFragment();
        Bundle bundle = new Bundle();
//        bundle.putString("allTabNames", tab_names);
        fragment.setArguments(bundle);*/
        return JobCardProgressFragment.newInstance(position, liveOrLocal, loginUserId, loginShopId);
    }

    private JobCardCompletedFragment setFragmentThree(Integer position, boolean liveOrLocal, String loginUserId, String loginShopId) {
       /* TestPageFragment fragment = new TestPageFragment();
        Bundle bundle = new Bundle();
//        bundle.putString("allTabNames", tab_names);
        fragment.setArguments(bundle);*/
        return JobCardCompletedFragment.newInstance(position, liveOrLocal, loginUserId, loginShopId);
    }

    @Override
    public int getCount() {
        return tabCount;
    }
}