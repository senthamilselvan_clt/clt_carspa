package com.clt.newcarspa.Adapter.autoimageslider;

/**
 * Created by Karthik_Thumathy on 2/11/17.
 */

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.clt.newcarspa.R;
import com.clt.newcarspa.model.GalleryUploadModel;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;


public class SlidingImage_Adapter extends PagerAdapter {

    private List<GalleryUploadModel> imgmodelslist = new ArrayList<>();
    private LayoutInflater inflater;
    private Context context;
    private LinearLayout llHeader;
    private ImageView imageView;
    private TextView tvTitle, tvDescription, tvDate;
    private ButtonAdapterCallback butttoncallback;

    public SlidingImage_Adapter(Context context, List<GalleryUploadModel> imgmodelslist) {
        this.context = context;
        this.imgmodelslist = imgmodelslist;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return imgmodelslist.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, final int position) {
        View imageLayout = inflater.inflate(R.layout.slidingimages_layout,
                view, false);

        assert imageLayout != null;
        llHeader = imageLayout.findViewById(R.id.ll_header);
        imageView = imageLayout.findViewById(R.id.image);
        tvTitle = imageLayout.findViewById(R.id.tv_title);

        try {
            if (imgmodelslist.get(position).getImage_encode_str() != null && !imgmodelslist.get(position).getImage_encode_str().equalsIgnoreCase("")) {
                decodeBase64AndSetImage(imgmodelslist.get(position).getImage_encode_str(), imageView);
            }else {
                Picasso.with(context)
                        .load(R.mipmap.ic_launcher)
                        .placeholder(R.mipmap.ic_launcher)
                        .error(R.mipmap.ic_launcher)
                        .into(imageView);
            }
        } catch (Exception e) {
            Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(),
                    R.mipmap.ic_launcher);
            imageView.setImageBitmap(bitmap);
        }
        view.addView(imageLayout, 0);

        return imageLayout;
    }

    private void decodeBase64AndSetImage(String completeImageData, ImageView imageView) {
        // Incase you're storing into aws or other places where we have extension stored in the starting.
        String imageDataBytes = completeImageData.substring(completeImageData.indexOf(",") + 1);
        InputStream stream = new ByteArrayInputStream(Base64.decode(imageDataBytes.getBytes(), Base64.DEFAULT));
        Bitmap bitmap = BitmapFactory.decodeStream(stream);
        imageView.setImageBitmap(bitmap);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }

    public void setCallback(ButtonAdapterCallback callback) {

        this.butttoncallback = callback;
    }

    public interface ButtonAdapterCallback {
        public void onItemClicked(int position);
    }
}
