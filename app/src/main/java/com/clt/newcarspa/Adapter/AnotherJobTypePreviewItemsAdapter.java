package com.clt.newcarspa.Adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckedTextView;

import com.clt.newcarspa.BuildConfig;
import com.clt.newcarspa.R;
import com.clt.newcarspa.Utils.Utils;
import com.clt.newcarspa.cltinterfaces.OnItemClickListener;
import com.clt.newcarspa.model.JobCardItemsDetailsModel;
import com.clt.newcarspa.model.JobTypeModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Karthik_Thumathy on 26/10/17.
 */

public class AnotherJobTypePreviewItemsAdapter extends RecyclerView.Adapter<AnotherJobTypePreviewItemsAdapter.JobTypeHolder> {

    private static final String TAG = AnotherJobTypePreviewItemsAdapter.class.getCanonicalName();
    private Context context;
    private Locale current;
    private String current_language;
    private ViewGroup groupParent;
    OnItemClickListener mItemClickListener;
    public AnotherJobTypePreviewItemsAdapter.JobTypeHolder rest_holder;
    private List<JobCardItemsDetailsModel> jobTypeModelList;
    private ButtonAdapterCallback butttoncallback;
    private List<JobTypeModel> jobSelectedItems = new ArrayList<>();

    public AnotherJobTypePreviewItemsAdapter(Context con, List<JobCardItemsDetailsModel> jobTypeList) {
        this.context = con;
        current = con.getResources().getConfiguration().locale;
        current_language = current.toString();
        this.jobTypeModelList = jobTypeList;
    }

    @Override
    public JobTypeHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View jobTypelayout = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.layout_jobtypeitems, parent, false);
        JobTypeHolder typeHolder = new JobTypeHolder(jobTypelayout);
        groupParent = parent;
        return typeHolder;
    }

    @Override
    public void onBindViewHolder(final JobTypeHolder holder, final int position) {
        final JobCardItemsDetailsModel jobType = jobTypeModelList.get(position);
        Locale current = context.getResources().getConfiguration().locale;
        String current_language = current.toString();
//            if (holder.jobTypeCheckedText.isChecked()) {
        if (BuildConfig.FLAVOR.equalsIgnoreCase("dev")) {
            Utils.showToast(context, ""+jobType.getName()+"--"+jobType.getOther_name());
        }
        if (jobType.getOther_name().isEmpty() || jobType.getOther_name().equalsIgnoreCase("") || jobType.getOther_name().equals(null)) {
            holder.jobTypeCheckedText.setText(Html.fromHtml(jobType.getName()));
        }else {
            holder.jobTypeCheckedText.setText(Html.fromHtml(jobType.getName() + "<br>" + jobType.getOther_name()));
        }
        holder.jobTypeCheckedText.setCheckMarkDrawable(R.drawable.ic_checked_icon);
//            }
    }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return jobTypeModelList == null ? 0 : jobTypeModelList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return jobTypeModelList.size();
    }


    public class JobTypeHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private CardView jobTypeCard;
        private CheckedTextView jobTypeCheckedText;

        private JobTypeHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            jobTypeCard = itemView.findViewById(R.id.cv_carpartname);
            jobTypeCheckedText = itemView.findViewById(R.id.ctv_jobtypename);
        }

        @Override
        public void onClick(View v) {
            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(v, getAdapterPosition());
            }
        }
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public void setCallback(ButtonAdapterCallback callback) {

        this.butttoncallback = callback;
    }

    public interface ButtonAdapterCallback {

        public void getJobTypeCheckedItems(List<JobTypeModel> typeSelectedItems);

    }
}
