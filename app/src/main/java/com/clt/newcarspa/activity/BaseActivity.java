package com.clt.newcarspa.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.widget.TextView;

import com.clt.newcarspa.BuildConfig;
import com.clt.newcarspa.R;
import com.clt.newcarspa.Utils.Constant;
import com.clt.newcarspa.Utils.ProgressBarHandler;
import com.clt.newcarspa.Utils.Utils;
import com.clt.newcarspa.cltinterfaces.RestAdapter;
import com.clt.newcarspa.database.RealmDBHelper;
import com.clt.newcarspa.model.APIListResponse;
import com.clt.newcarspa.model.AudioUploadModel;
import com.clt.newcarspa.model.ImageUploadModel;
import com.clt.newcarspa.model.JobCardDetailsRealmDBModel;
import com.clt.newcarspa.model.JobCardItemsDetailsModel;
import com.clt.newcarspa.model.JobTypeModel;
import com.clt.newcarspa.model.MyCarListItems;
import com.clt.newcarspa.model.ReturnRealmModel;
import com.clt.newcarspa.model.UserLoginDetails;
import com.google.gson.Gson;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import io.realm.Realm;
import io.realm.RealmList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

/**
 * Created by Karthik Thumathy on 23-Jul-18.
 */
public class BaseActivity extends AppCompatActivity {

    private static final String TAG = BaseActivity.class.getCanonicalName();
    private static final int PERMISSION_REQUEST_CODE = 1;
    private boolean mResponseApiStatus = false;
    private String mResponseApiErrorMsg;

    private RealmDBHelper realmDBHelper;
    private ProgressBarHandler mProgressBarHandler;
    private Realm mRealm;
    private List<ImageUploadModel> imageCardItemList = new ArrayList<>();
    private List<AudioUploadModel> audioCardItemList = new ArrayList<>();
    private List<JobCardDetailsRealmDBModel> CardLists = new ArrayList<>();
    private List<JobCardItemsDetailsModel> itemsDetailsList = new ArrayList<>();
    private List<JobCardDetailsRealmDBModel> pushedCardLists = new ArrayList<>();

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mRealm.close();
//        realmDBHelper.realmDbClose();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mProgressBarHandler = new ProgressBarHandler(BaseActivity.this);
        realmDBHelper = new RealmDBHelper(BaseActivity.this);
        mRealm = Realm.getDefaultInstance();
    }

    public void ask_permission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ActivityCompat.requestPermissions(this, new String[]{WRITE_EXTERNAL_STORAGE, READ_EXTERNAL_STORAGE, CAMERA}, PERMISSION_REQUEST_CODE); //, ACCESS_FINE_LOCATION, READ_SMS, CALL_PHONE
        }
    }

    public void hideToolbarAppName() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);//for hiding the app name
        }
    }

    /**
     * Overrides the pending Activity transition by performing the "Enter" animation.
     */
    public void overridePendingTransitionEnter() {
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }

    /**
     * Overrides the pending Activity transition by performing the "Exit" animation.
     */
    public void overridePendingTransitionExit() {
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }

    public void showAlertDialog(String message) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(Html.fromHtml(message))
                .setCancelable(true)
                .setTitle(getResources().getString(R.string.app_name))
                .setPositiveButton(getResources().getString(R.string.msg_ok), new DialogInterface.OnClickListener() {
                    public void onClick(
                            final DialogInterface dialog,
                            final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    public void checkInternet() {
        if (Utils.getInstance(getApplicationContext()).isOnline()) {

        } else {
            Utils.showToast(getApplicationContext(), getResources().getString(R.string.msg_networkcheck));
        }
    }

    public void closeAlert(Context context, String screenName) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle(getResources().getString(R.string.app_name));
        alertDialog.setMessage(Html.fromHtml(getResources().getString(R.string.msg_closeit) + "<br>" + "<B>" + screenName + "</B>" + "?"));
        alertDialog.setPositiveButton(getResources().getString(R.string.msg_yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                overridePendingTransitionExit();
                finish();
            }
        });
        alertDialog.setNegativeButton(getResources().getString(R.string.msg_no), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertDialog.show();
    }

    public void saveAndCloseAlert(Context context, String screenName) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle(getResources().getString(R.string.app_name));
        alertDialog.setMessage(Html.fromHtml(getResources().getString(R.string.msg_savendcloseit)));
        alertDialog.setPositiveButton(getResources().getString(R.string.msg_ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                overridePendingTransitionExit();
                finish();
            }
        });
//        alertDialog.setNegativeButton(getResources().getString(R.string.msg_no), new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int which) {
//                dialog.cancel();
//            }
//        });
        alertDialog.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

//        final int PERMISSIONS_REQUEST_CALL_PHONE = 124;
        final int PERMISSIONS_REQUEST_CAMERA = 125;
        final int PERMISSIONS_REQUEST_RECORD_AUDIO = 126;

        switch (requestCode) {

           /* case PERMISSIONS_REQUEST_CALL_PHONE:

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + "9944524355")); //getResources().getString(R.string.call_menu_help_no)
                    try {
                        startActivity(intent);
                    } catch (ActivityNotFoundException e) {
                        e.printStackTrace();
                    } catch (ArrayIndexOutOfBoundsException e) {
                        e.printStackTrace();
                    }
                } else {

                    // if (shouldShowRequestPermissionRationale(CALL_PHONE)) {
                    showMessageOKCancel("Kindly allow access for Phone Call",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                        requestPermissions(new String[]{CALL_PHONE},
                                                PERMISSION_REQUEST_CODE); // call phone
                                    }
                                }
                            });
                    return;
                    // }
                }
                break;*/
            case PERMISSIONS_REQUEST_CAMERA:

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + getResources().getString(R.string.call_menu_help_no)));
//                    try {
//                        startActivity(intent);
//                    } catch (ActivityNotFoundException e) {
//                        e.printStackTrace();
//                    } catch (ArrayIndexOutOfBoundsException e) {
//                        e.printStackTrace();
//                    }
                } else {

                    // if (shouldShowRequestPermissionRationale(CALL_PHONE)) {
                    showMessageOKCancel("Kindly allow access for Camera",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                        requestPermissions(new String[]{CAMERA},
                                                PERMISSION_REQUEST_CODE); // call phone
                                    }
                                }
                            });
                    return;
                    // }
                }
                break;
            case PERMISSIONS_REQUEST_RECORD_AUDIO:

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + getResources().getString(R.string.call_menu_help_no)));
//                    try {
//                        startActivity(intent);
//                    } catch (ActivityNotFoundException e) {
//                        e.printStackTrace();
//                    } catch (ArrayIndexOutOfBoundsException e) {
//                        e.printStackTrace();
//                    }
                } else {

                    // if (shouldShowRequestPermissionRationale(CALL_PHONE)) {
                    showMessageOKCancel("Kindly allow access for Record Audio",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                        requestPermissions(new String[]{RECORD_AUDIO},
                                                PERMISSION_REQUEST_CODE); // call phone
                                    }
                                }
                            });
                    return;
                    // }
                }
                break;
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {
                    try {
                        boolean writeexternalstorageAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                        boolean readexternalstorageAccepted = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                        boolean cameraAccepted = grantResults[2] == PackageManager.PERMISSION_GRANTED;
                        boolean locationAccepted = grantResults[3] == PackageManager.PERMISSION_GRANTED;
                        boolean readsmsAccepted = grantResults[4] == PackageManager.PERMISSION_GRANTED;
                        // boolean callAccepted = grantResults[3] == PackageManager.PERMISSION_GRANTED;


                        if (writeexternalstorageAccepted && readexternalstorageAccepted && cameraAccepted) { // && callAccepted && locationAccepted && readsmsAccepted
                            // Toast.makeText(getApplicationContext(), "Permission Granted, Now you can access location data and call.", //Toast.LENGTH_LONG).show();
                        } else {

                            // Toast.makeText(getApplicationContext(), "Permission Denied, You cannot access location data and call.", //Toast.LENGTH_LONG).show();

                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                if (shouldShowRequestPermissionRationale(WRITE_EXTERNAL_STORAGE)) {
                                    showMessageOKCancel("Kindly allow access to External Storage",
                                            new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                        requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE, READ_EXTERNAL_STORAGE, CAMERA},
                                                                PERMISSION_REQUEST_CODE); // write external storage
                                                    }
                                                }
                                            });
                                    return;
                                }
                                if (shouldShowRequestPermissionRationale(READ_EXTERNAL_STORAGE)) {
                                    showMessageOKCancel("Kindly allow access to Read External Storage",
                                            new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                        requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE, READ_EXTERNAL_STORAGE, CAMERA},
                                                                PERMISSION_REQUEST_CODE); // write external storage
                                                    }
                                                }
                                            });
                                    return;
                                }
                                if (shouldShowRequestPermissionRationale(CAMERA)) {
                                    showMessageOKCancel("Kindly allow access for Camera",
                                            new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                        requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE, READ_EXTERNAL_STORAGE, CAMERA},
                                                                PERMISSION_REQUEST_CODE); // camera
                                                    }
                                                }
                                            });
                                    return;
                                }
                                /*if (shouldShowRequestPermissionRationale(ACCESS_FINE_LOCATION)) {
                                    showMessageOKCancel("Kindly allow access to your Location",
                                            new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                        requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE, ACCESS_FINE_LOCATION, READ_SMS, CAMERA},
                                                                PERMISSION_REQUEST_CODE); //Location
                                                    }
                                                }
                                            });
                                    return;
                                }
                                //
                                if (shouldShowRequestPermissionRationale(READ_SMS)) {
                                    showMessageOKCancel("Kindly allow access for Phone Call",
                                            new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                        requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE, ACCESS_FINE_LOCATION, READ_SMS, CAMERA},
                                                                PERMISSION_REQUEST_CODE); // call phone
                                                    }
                                                }
                                            });
                                    return;
                                }
                                 //
                            if (shouldShowRequestPermissionRationale(READ_SMS)) {
                                showMessageOKCancel("Kindly allow access for Read Sms",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                    requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE, ACCESS_FINE_LOCATION, CALL_PHONE, READ_SMS, CAMERA},
                                                            PERMISSION_REQUEST_CODE); // call phone
                                                }
                                            }
                                        });
                                return;
                            }*/
                                //
                            }

                        }
                    } catch (ArrayIndexOutOfBoundsException e) {
                        e.printStackTrace();
                    }
                }
                break;

        }
    }


    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(BaseActivity.this)
                .setMessage(message)
                .setTitle(getResources().getString(R.string.msg_need_permission))
                .setCancelable(false)
                .setPositiveButton(getResources().getString(R.string.msg_ok), okListener)
                .setNegativeButton(getResources().getString(R.string.msg_close), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                })
                .create()
                .show();
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public boolean checkCallPermission() {
        final int PERMISSIONS_REQUEST_CALL_PHONE = 124;
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= android.os.Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, PERMISSIONS_REQUEST_CALL_PHONE);
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    //Ask permission
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public boolean storageCheckPermission() {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public boolean checkCameraPermission() {
        final int PERMISSIONS_REQUEST_CAMERA = 125;
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= android.os.Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, PERMISSIONS_REQUEST_CAMERA);
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    public List<String> onSetHomeItems() {
        List<String> listitems = new ArrayList<>();
        listitems.add("New Job Card");
        listitems.add("View Job Card List");
        listitems.add("Report");
        listitems.add("Today Delivery");
//        listitems.add("Create Invoice");
//        listitems.add("Find Invoice");
//        listitems.add("Draft");
//        listitems.add("Credit Invoice");
        listitems.add("Settings");
//        listitems.add("Push");
        listitems.add("Gallery");
        listitems.add("Logout");
        return listitems;
    }

    public List<MyCarListItems> onSetHomeListItems() {
        List<String> stringList = onSetHomeItems();
        List<MyCarListItems> homeList = new ArrayList<>();
        MyCarListItems listitems;
        for (int i = 0; i < stringList.size(); i++) {
            listitems = new MyCarListItems();
            listitems.setId(UUID.randomUUID().toString());
            listitems.setItemName(stringList.get(i));
            homeList.add(listitems);
        }
        return homeList;
    }

   /* public void onAddHomePageItems(List<String> mHomeItems){
        realmDBHelper = new RealmDBHelper(BaseActivity.this);
        Integer deleted = realmDBHelper.deleteHomePageItems();
        Integer count = realmDBHelper.addHomePageItems(UUID.randomUUID().toString(),mHomeItems);
        Utils.showLogD("deletedHomePageItems--"+deleted.toString());
        Utils.showLogD("addHomePageItemscount--"+count.toString());
    }*/

    public Integer onAddUserLoginDetails(Realm realm, UserLoginDetails userdetails) {
        realmDBHelper = new RealmDBHelper(BaseActivity.this);
        Integer count = realmDBHelper.addLoginDetails(realm, userdetails);
        Utils.showLogD("addUserLoginDetails--" + count.toString());
        return count;
    }

    public Integer onAddJobTypeDetails(Realm realm, List<JobTypeModel> jobTypeDetails) {
        realmDBHelper = new RealmDBHelper(BaseActivity.this);
        Integer count = realmDBHelper.addJobTypeDetails(realm, jobTypeDetails);
        Utils.showLogD("addUserLoginDetails--" + count.toString());
        return count;
    }

    public void callHelpNo() {
      /*  if (checkCallPermission()) {
            try {
                Intent intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:" + getResources().getString(R.string.call_menu_help_no)));
                startActivity(intent);
            } catch (SecurityException e) {
                Log.d("MainActivity--", "call Help No");
                e.printStackTrace();
            }
        }*/
    }

    public boolean onCheckAPIStatus(int status) {
        if (status == 200) {
            mResponseApiStatus = true;
        } else {
            mResponseApiStatus = false;
        }
        return mResponseApiStatus;
    }

    public String onCheckErrorStatus(Context context, int status) {
        if (status == 200) {
            mResponseApiErrorMsg = "Success";
        } else if (status == 605) {
            mResponseApiErrorMsg = "No records found";
        } else if (status == 400) {
            mResponseApiErrorMsg = "Your Session is Expired. Please Login";
        } else if (status == 401) {
            mResponseApiErrorMsg = "Invalid Username / Password";
        } else if (status == 600) {
            mResponseApiErrorMsg = "Invalid access / Empty request";
        } else if (status == 601) {
            mResponseApiErrorMsg = "Incorrect order Id";
        } else if (status == 602) {
            mResponseApiErrorMsg = "File upload failure";
        } else if (status == 603) {
            mResponseApiErrorMsg = "Mismatched data";
        } else if (status == 606) {
            mResponseApiErrorMsg = "File not found in Server";
        } else if (status == 604) {
            mResponseApiErrorMsg = "File not exist";
        } else if (status == 607) {
            mResponseApiErrorMsg = "Incorrect amount information";
        } else if (status == 500) {
            mResponseApiErrorMsg = String.valueOf(status);
        } else {
            mResponseApiErrorMsg = String.valueOf(status);
        }

        return mResponseApiErrorMsg;
    }

    public List<ImageUploadModel> getRealmImageListToList(ReturnRealmModel realmmodel) {
        List<ImageUploadModel> imgItemsList = new ArrayList<>();
        ImageUploadModel imgItemModel;
        if (realmmodel.getImageItems() != null || realmmodel.getImageItems().size() > 0) {
            for (int k = 0; k < realmmodel.getImageItems().size(); k++) {
                imgItemModel = new ImageUploadModel();
                ImageUploadModel imgItem = realmmodel.getImageItems().get(k);
                imgItemModel.setLocaldbid(imgItem.getLocaldbid());
                imgItemModel.setId(imgItem.getId());
                imgItemModel.setUser_id(imgItem.getUser_id());
                imgItemModel.setShop_id(imgItem.getShop_id());
                imgItemModel.setImage_encode_str(imgItem.getImage_encode_str());
                imgItemModel.setImage_filename(imgItem.getImage_filename());
                imgItemModel.setImage_uniqueid(imgItem.getImage_uniqueid());
                imgItemModel.setLocal_insert(imgItem.getLocal_insert());
                imgItemModel.setSync_status(imgItem.getSync_status());
                imgItemModel.setRow_deleted(imgItem.getRow_deleted());
                imgItemsList.add(imgItemModel);
            }
        }
        return imgItemsList;
    }

    public List<AudioUploadModel> getRealmAudioListToList(ReturnRealmModel realmmodel) {
        List<AudioUploadModel> audItemsList = new ArrayList<>();
        AudioUploadModel audItemModel;
        if (realmmodel.getAudioItems() != null || realmmodel.getAudioItems().size() > 0) {
            for (int k = 0; k < realmmodel.getAudioItems().size(); k++) {
                audItemModel = new AudioUploadModel();
                AudioUploadModel audItem = realmmodel.getAudioItems().get(k);
                audItemModel.setLocaldbid(audItem.getLocaldbid());
                audItemModel.setId(audItem.getId());
                audItemModel.setUser_id(audItem.getUser_id());
                audItemModel.setShop_id(audItem.getShop_id());
                audItemModel.setAudio_encode_str(audItem.getAudio_encode_str());
                audItemModel.setAudio_filename(audItem.getAudio_filename());
                audItemModel.setAudio_filepath(audItem.getAudio_filepath());
                audItemModel.setImage_uniqueid(audItem.getImage_uniqueid());
                audItemModel.setLocal_insert(audItem.getLocal_insert());
                audItemModel.setSync_status(audItem.getSync_status());
                audItemModel.setRow_deleted(audItem.getRow_deleted());
                audItemsList.add(audItemModel);
            }
        }
        return audItemsList;
    }

    public List<JobCardItemsDetailsModel> getRealmListToList(ReturnRealmModel realmmodel) {
        List<JobCardItemsDetailsModel> itemsDetailsList = new ArrayList<>();
        JobCardItemsDetailsModel itemsDetailsModel1;
        if (realmmodel.getJobCardItems() != null || realmmodel.getJobCardItems().size() > 0) {
            for (int k = 0; k < realmmodel.getJobCardItems().size(); k++) {
                itemsDetailsModel1 = new JobCardItemsDetailsModel();
                JobCardItemsDetailsModel item = realmmodel.getJobCardItems().get(k);
//            itemsDetailsModel1.setId(realmmodel.getJobCardItems().get(k).getService_id());
                itemsDetailsModel1.setName(item.getName());
                itemsDetailsModel1.setOther_name(item.getOther_name());
                itemsDetailsModel1.setPrice(item.getPrice());
                itemsDetailsModel1.setJob_card_id(item.getJob_card_id());
                itemsDetailsModel1.setService_id(item.getService_id());
                itemsDetailsList.add(itemsDetailsModel1);
            }
            for (int i = 0; i < itemsDetailsList.size(); i++) {
                Utils.showLogD("1009---" + i + "--" + itemsDetailsList.get(i).getId() + "--" + itemsDetailsList.get(i).getName() + "--" + itemsDetailsList.get(i).getJob_card_id() + "--" +
                        itemsDetailsList.get(i).getService_id());
            }
        }
        return itemsDetailsList;
    }

    public List<JobCardDetailsRealmDBModel> getRealmToCardList(ReturnRealmModel realmmodel) {
        List<JobCardDetailsRealmDBModel> cdList = new ArrayList<>();
        JobCardDetailsRealmDBModel cards;
        if (realmmodel.getJobCard() != null || realmmodel.getJobCard().size() > 0) {
            for (int l = 0; l < realmmodel.getJobCard().size(); l++) {
                cards = new JobCardDetailsRealmDBModel();
                JobCardDetailsRealmDBModel cardDetail = realmmodel.getJobCard().get(l);
                cards.setLocaldbid(cardDetail.getLocaldbid());
                cards.setId(cardDetail.getId());
                cards.setUser_id(cardDetail.getUser_id());
                cards.setShop_id(cardDetail.getShop_id());
                cards.setInvoice_no(cardDetail.getInvoice_no());
                cards.setCustomer_code(cardDetail.getCustomer_code());
                cards.setVat_reg_no(cardDetail.getVat_reg_no());
                cards.setName(cardDetail.getName());
                cards.setPo_box(cardDetail.getPo_box());
                cards.setCity(cardDetail.getCity());
                cards.setTel_no(cardDetail.getTel_no());
                cards.setMobile_no(cardDetail.getMobile_no());
                cards.setFax_no(cardDetail.getFax_no());
                cards.setEmail(cardDetail.getEmail());
                cards.setAdvisor_date(cardDetail.getAdvisor_date());
                cards.setAdvisor_time(cardDetail.getAdvisor_time());
                cards.setReg_no(cardDetail.getReg_no());
                cards.setModel(cardDetail.getModel());
                cards.setChassis_no(cardDetail.getChassis_no());
                cards.setColours(cardDetail.getColours());
                cards.setFuel(cardDetail.getFuel());
                cards.setOil(cardDetail.getOil());
                cards.setKilometer(cardDetail.getKilometer());
                cards.setAdvisor_id(cardDetail.getAdvisor_id());
                cards.setAdvisor_name(cardDetail.getAdvisor_name());
                cards.setDelivery_date(cardDetail.getDelivery_date());
                cards.setJob_type(cardDetail.getJob_type());
                cards.setTotal_amount(cardDetail.getTotal_amount());
                cards.setDate_added(cardDetail.getDate_added());
                cards.setDate_update(cardDetail.getDate_update());
                cards.setCreated_date(cardDetail.getCreated_date());
                cards.setCreated_date_str(cardDetail.getCreated_date_str());
                cards.setAction(cardDetail.getAction());
                cards.setStatus(cardDetail.getStatus());
                cards.setItems(cardDetail.getItems());
                cards.setLocal_insert(cardDetail.getLocal_insert());
                cards.setSync_status(cardDetail.getSync_status());
                cards.setRow_deleted(cardDetail.getRow_deleted());
                cards.setOld_id(cardDetail.getOld_id());
                cards.setNew_id(cardDetail.getNew_id());
                cards.setJob_card_pushstr(cardDetail.getJob_card_pushstr());
                cdList.add(cards);
            }
        }
        for (int i = 0; i < cdList.size(); i++) {
            Utils.showLogD("1009--" + cdList.get(i).getId() + "--" + cdList.get(i).getName() + "--" + cdList.get(i).getCustomer_code());
        }
        return cdList;
    }

    public List<JobCardDetailsRealmDBModel> getViewCardList(ReturnRealmModel realmmodel) {
        List<JobCardDetailsRealmDBModel> cdList = new ArrayList<>();
        JobCardDetailsRealmDBModel cards;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String strDate;
        if (realmmodel.getJobCard() != null || realmmodel.getJobCard().size() > 0) {
            for (int l = 0; l < realmmodel.getJobCard().size(); l++) {
                cards = new JobCardDetailsRealmDBModel();
                JobCardDetailsRealmDBModel cardDetail = realmmodel.getJobCard().get(l);
                cards.setLocaldbid(cardDetail.getLocaldbid());
                cards.setId(cardDetail.getId());
                cards.setUser_id(cardDetail.getUser_id());
                cards.setShop_id(cardDetail.getShop_id());
                cards.setInvoice_no(cardDetail.getInvoice_no());
                cards.setCustomer_code(cardDetail.getCustomer_code());
                cards.setVat_reg_no(cardDetail.getVat_reg_no());
                cards.setName(cardDetail.getName());
                cards.setPo_box(cardDetail.getPo_box());
                cards.setCity(cardDetail.getCity());
                cards.setTel_no(cardDetail.getTel_no());
                cards.setMobile_no(cardDetail.getMobile_no());
                cards.setFax_no(cardDetail.getFax_no());
                cards.setEmail(cardDetail.getEmail());
                cards.setAdvisor_date(cardDetail.getAdvisor_date());
                cards.setAdvisor_time(cardDetail.getAdvisor_time());
                cards.setReg_no(cardDetail.getReg_no());
                cards.setModel(cardDetail.getModel());
                cards.setChassis_no(cardDetail.getChassis_no());
                cards.setColours(cardDetail.getColours());
                cards.setAdvisor_id(cardDetail.getAdvisor_id());
                cards.setAdvisor_name(cardDetail.getAdvisor_name());
                cards.setDelivery_date(cardDetail.getDelivery_date());
                cards.setJob_type(cardDetail.getJob_type());
                cards.setTotal_amount(cardDetail.getTotal_amount());
                cards.setDate_added(cardDetail.getDate_added());
                cards.setDate_update(cardDetail.getDate_update());
                cards.setCreated_date(stringToDate(sdf.format(cardDetail.getCreated_date())));//cardDetail.getCreated_date()));
                cards.setCreated_date_str(cardDetail.getCreated_date_str());//cardDetail.getCreated_date()));
                cards.setAction(cardDetail.getAction());
                cards.setStatus(cardDetail.getStatus());
                cards.setItems(cardDetail.getItems());
                cards.setLocal_insert(cardDetail.getLocal_insert());
                cards.setSync_status(cardDetail.getSync_status());
                cards.setRow_deleted(cardDetail.getRow_deleted());
                cards.setOld_id(cardDetail.getOld_id());
                cards.setNew_id(cardDetail.getNew_id());
                cards.setJob_card_pushstr(cardDetail.getJob_card_pushstr());
                cdList.add(cards);
            }
        }
        for (int i = 0; i < cdList.size(); i++) {
            Utils.showLogD("1009--" + cdList.get(i).getId() + "--" + cdList.get(i).getName() + "--" + cdList.get(i).getCustomer_code());
        }
        return cdList;
    }

    public void getFormattedDate(int year, int monthOfYear, int dayOfMonth, TextView txtDate) {
        int month = monthOfYear + 1;
        String formattedMonth = "" + month;
        String formattedDayOfMonth = "" + dayOfMonth;
        if (month < 10) {
            formattedMonth = "0" + month;
        }
        if (dayOfMonth < 10) {
            formattedDayOfMonth = "0" + dayOfMonth;
        }
        txtDate.setText(formattedDayOfMonth + "-" + formattedMonth + "-" + year);
    }

    public String getFormattedDateReverse(int year, int monthOfYear, int dayOfMonth, TextView txtDate) {
        int month = monthOfYear + 1;
        String date;
        String formattedMonth = "" + month;
        String formattedDayOfMonth = "" + dayOfMonth;
        if (month < 10) {
            formattedMonth = "0" + month;
        }
        if (dayOfMonth < 10) {
            formattedDayOfMonth = "0" + dayOfMonth;
        }
        date = year + "-" + formattedMonth + "-" + formattedDayOfMonth;
//        txtDate.setText( year+ "-" + formattedMonth + "-" + formattedDayOfMonth);
        return date;
    }

    public Date stringToDate(String aDate) {

        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "dd-MM-yyyy");
        Date myDate = null;
        try {
            myDate = dateFormat.parse(aDate);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat timeFormat = new SimpleDateFormat("dd-MM-yyyy");
        String finalDate = timeFormat.format(myDate);
        System.out.println(finalDate);
        return myDate;
    }

    public String stringToString(String aDate) {

        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "dd-MM-yyyy");
        Date myDate = null;
        try {
            myDate = dateFormat.parse(aDate);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat timeFormat = new SimpleDateFormat("dd-MM-yyyy");
        String finalDate = timeFormat.format(myDate);
        System.out.println(finalDate);
        return finalDate;
    }

    public interface CommunicationListener{
        void onResponse(String status);
    }

    public void pushToServer(CommunicationListener listener) {
        String mPushStr = null;
        RealmList<JobCardDetailsRealmDBModel> pushCardList = new RealmList<>();
        RealmList<JobCardItemsDetailsModel> pushCardItemsList;
        List<ImageUploadModel> imageItemsList;
        List<AudioUploadModel> audioItemsList;
        ReturnRealmModel realmmodel = realmDBHelper.getPushAllJobCardDetail(mRealm);
        if (realmmodel != null) {
            if (realmmodel.getJobCard() != null || realmmodel.getJobCard().size() > 0) {
                JobCardDetailsRealmDBModel mJobCardList;
                JobCardItemsDetailsModel mJobCardItemsList;
                ImageUploadModel mImageObj;
                AudioUploadModel mAudioObj;

                imageCardItemList = getRealmImageListToList(realmmodel);
                audioCardItemList = getRealmAudioListToList(realmmodel);
                CardLists = getRealmToCardList(realmmodel);
                itemsDetailsList = getRealmListToList(realmmodel);
                for (int i = 0; i < CardLists.size(); i++) {
                    pushCardItemsList = new RealmList<>();
                    mJobCardList = CardLists.get(i);
                    if (realmmodel.getJobCardItems() != null && realmmodel.getJobCardItems().size() > 0) {
                        for (int j = 0; j < itemsDetailsList.size(); j++) {
                            mJobCardItemsList = itemsDetailsList.get(j);
                            if (mJobCardList.getId().equals(mJobCardItemsList.getJob_card_id())) {
                                pushCardItemsList.add(mJobCardItemsList);
                                Utils.showLogD("20001----" + new Gson().toJson(mJobCardItemsList));
                            }
                        }
                        if (pushCardItemsList.size() > 0) {
                            String items = new Gson().toJson(pushCardItemsList);
                            mJobCardList.setItems(items);
                            mJobCardList.setItems_new(pushCardItemsList);
                        }
                    }
                    imageItemsList = new ArrayList<>();
                    if (imageCardItemList != null && imageCardItemList.size() > 0) {
                        for (int p = 0; p < imageCardItemList.size(); p++) {
                            mImageObj = imageCardItemList.get(p);
                            if (mJobCardList.getId().equals(mImageObj.getId())) {

                                imageItemsList.add(mImageObj);
                            }
                        }
                        if (imageItemsList.size() > 0) {
                            String imageItems = new Gson().toJson(imageItemsList);
                            mJobCardList.setItem_images(imageItems);
                        }
                    }

                    audioItemsList = new ArrayList<>();
                    if (audioCardItemList != null && audioCardItemList.size() > 0) {
                        for (int p = 0; p < audioCardItemList.size(); p++) {
                            mAudioObj = audioCardItemList.get(p);
                            if (mJobCardList.getId().equals(mAudioObj.getId())) {
                                audioItemsList.add(mAudioObj);
                            }
                        }
                        if (audioItemsList.size() > 0) {
                            String audioItems = new Gson().toJson(audioItemsList);
                            mJobCardList.setItem_audios(audioItems);
                        }
                    }

                    pushCardList.add(mJobCardList);
                }
                if (pushCardList.size() > 0) {
                    mPushStr = new Gson().toJson(pushCardList);
                }

                if (realmmodel.getJobCard().size() > 0) {
                    callPushJobCardService(pushCardList, listener);
                } else {
                    listener.onResponse(getResources().getString(R.string.text_norecordfound));
                    Utils.showToast(getApplicationContext(), getResources().getString(R.string.text_norecordfound));
                }
            } else {
                listener.onResponse(getResources().getString(R.string.text_norecordfound));
                Utils.showToast(getApplicationContext(), getResources().getString(R.string.text_norecordfound));
            }
        }
    }

    public void callPushJobCardService(RealmList<JobCardDetailsRealmDBModel> pushStr, final CommunicationListener listener) {
//            , final NewJobCardActivity.CommunicationListener listener) {
        mProgressBarHandler.show();
        JobCardDetailsRealmDBModel carddetail = new JobCardDetailsRealmDBModel();
        carddetail.setAction(Constant.PUSHJOBCARDLIST);
        carddetail.setJob_card_pushstr(pushStr);
        Call<APIListResponse<JobCardDetailsRealmDBModel>> call = RestAdapter.getInstance().getServiceApi().pushJobCardList(carddetail);
        call.enqueue(new Callback<APIListResponse<JobCardDetailsRealmDBModel>>() {
            @Override
            public void onResponse(@NonNull Call<APIListResponse<JobCardDetailsRealmDBModel>> call,
                                   @NonNull Response<APIListResponse<JobCardDetailsRealmDBModel>> response) {
                String status;
                if (response.isSuccessful()) {
                    mProgressBarHandler.hide();
                    if (response.body() != null) {
                        status = response.body().getStatus();
                        if (status.equals(Constant.RESPONSESUCCESS)) {
                            pushedCardLists = response.body().getResult();
                            Utils.showToast(getApplicationContext(), "push successfully");
                            realmDBHelper.updatePushedJobCardStatus(mRealm, pushedCardLists);
                            listener.onResponse("push successfully");
//                            listener.onJobCardSuccessResponse(nextid, status, response.body().getResult().get(0));
                        }
                    }
                } else {
                    mProgressBarHandler.hide();
                }
                realmDBHelper.getAllJobCardDetail(mRealm);
            }

            @Override
            public void onFailure(Call<APIListResponse<JobCardDetailsRealmDBModel>> call, Throwable t) {
                mProgressBarHandler.hide();
                if (getApplicationContext() != null) {
                    String failure = t.getMessage().toString();
                    Log.d("onFailure Response", "" + failure);
                    if (BuildConfig.FLAVOR.equals("dev")) {
                        Utils.showToast(getApplicationContext(), getResources().getString(R.string.msg_servererror) + "--" + failure);
                    } else {
                        Utils.showToast(getApplicationContext(), getResources().getString(R.string.msg_servererror));
                    }
                }
            }
        });
    }
}