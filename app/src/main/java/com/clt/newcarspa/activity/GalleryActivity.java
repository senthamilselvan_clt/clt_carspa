package com.clt.newcarspa.activity;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.clt.newcarspa.Adapter.AddGalleryAdapter;
import com.clt.newcarspa.BuildConfig;
import com.clt.newcarspa.Helper.AlbumStorageDirFactory;
import com.clt.newcarspa.Helper.BaseAlbumDirFactory;
import com.clt.newcarspa.Helper.DimenUtil;
import com.clt.newcarspa.Helper.FileUtils;
import com.clt.newcarspa.Helper.FroyoAlbumDirFactory;
import com.clt.newcarspa.R;
import com.clt.newcarspa.Utils.ProgressBarHandler;
import com.clt.newcarspa.Utils.Utils;
import com.clt.newcarspa.database.RealmDBHelper;
import com.clt.newcarspa.model.GalleryUploadModel;
import com.clt.newcarspa.model.ReturnRealmModel;
import com.clt.newcarspa.model.UserLoginDetails;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import io.realm.Realm;

import static com.clt.newcarspa.Helper.FileUtils.CARSPA_DIRECTORY_PATH;

/**
 * Created by Karthik Thumathy on 07-Jan-19.
 */
public class GalleryActivity extends BaseActivity implements AddGalleryAdapter.ButtonAdapterCallback {

    private Realm mRealm;
    private RealmDBHelper realmDBHelper;
    private ProgressBarHandler mProgressBarHandler;
    private Toolbar toolbar;
    private ImageView imgBack, imgAttach;
    private TextView toolbarTitle;
    private RecyclerView rvGallery;
    private AddGalleryAdapter addImageAdapter;
    private GridLayoutManager mLayoutManager;

    private String uniqueId, nextId;
    private List<UserLoginDetails> userLoginDetails = new ArrayList<>();
    private String loginUserId, loginUserName, loginShopId;
    private List<GalleryUploadModel> jobCardImageList = new ArrayList<>();
    private List<GalleryUploadModel> jobCardImageListNew;

    /*--- Image Upload---*/
    private static final String LOG_TAG = "IMAGEUPLOAD";
    private static final String TAG = "IMAGEUPLOAD";
    private Button btnCamera, btnFileChoose, btnAudio;

    private static final int ACTION_TAKE_PHOTO_B_PIC = 21;
    private static final int FILE_SELECT_CODE_PIC = 31;
    private String mCurrentPhotoPath;
    private String mPictureFilePath;
    private File mPicFile;
    private File mImageFileDir;
    private boolean mIsPictureSet;
    private AlbumStorageDirFactory mAlbumStorageDirFactory;
    private static final String JPEG_FILE_PREFIX = "CAMPIC_";
    private static final String JPEG_FILE_SUFFIX = ".jpg";
    private int mIVDimenPx;
    public static final String AUDIO_DIR_PATH = "/msg_aud/";
    private static final String AUDIO_FILE_PREFIX = "AMSG_AU_";
    public static final String AUDIO_FILE_SUFFIX = ".3gp";
    private File mSthatDir;
    private File mAudioFile;
    private boolean mIsAudioMode;
    private Chronometer mChronometer;
    private MediaRecorder mRecorder;
    private CardView mAudioRecCv;
    private TextView mAudioRecMsgTv;
    private Button mAudioRecCancelBtn;
    private Button mAudioRecSendBtn;
    private boolean mIsAudioRecorderNotPlayer;
    private AlertDialog attachAlertDialog;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    private int history_page = 1;
    private boolean isSearchedStatus = false;
    int pastVisiblesItems, visibleItemCount, totalItemCount;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        mRealm = Realm.getDefaultInstance();
        realmDBHelper = new RealmDBHelper(GalleryActivity.this);
        mProgressBarHandler = new ProgressBarHandler(this);
        getUserLoginDetailsFromDB();
        initView();
        getImagesListFromDB(0, 10);
        initVars();
        initSthatAndAudioDirectory();
        setListeners();
    }

    private void initView() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbarTitle = findViewById(R.id.tv_toolbarTitle);
        toolbarTitle.setText(getResources().getString(R.string.app_gallerypage));
        imgAttach = findViewById(R.id.img_attach);
        imgBack = findViewById(R.id.img_back);
        rvGallery = findViewById(R.id.rv_galleryitems);
        imgBack.setVisibility(View.VISIBLE);
    }

    private void setListeners() {
        imgAttach.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addImageAlert();
            }
        });
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        rvGallery.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();
                    if (!recyclerView.canScrollVertically(RecyclerView.FOCUS_DOWN)) {
                        if (!isLastPage) {
                            /*if (liveOrLocal) {
                                if ((visibleItemCount + pastVisiblesItems) >= totalItemCount && pastVisiblesItems >= 0) {
                                    isLoading = false;
                                    Utils.showToast(getActivity(), "total" + totalItemCount);
                                    Log.v("...", "Last Item Wow !");
                                    if (liveOrLocal) {
                                        callListJobCardService(history_page, strSearch);
                                    } else {
                                        Integer index = selectJobList.size();
                                        getDataFromDB(mStatus, index, 10);
//                                    onSetRecyclerView();
                                    }
                                    //Do pagination.. i.e. fetch new data
                                }
                            } else {*/
                            Integer index = jobCardImageList.size();
                            getImagesListFromDB(index, 10);
                            /*}*/
                        }

                    }
                }
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        });
    }

    private void getImagesListFromDB(Integer index, Integer length) {
        jobCardImageListNew = new ArrayList<>();
        ReturnRealmModel realmModel = realmDBHelper.getGalleryImages(mRealm, index, length);
        jobCardImageListNew = realmModel.getGalleryList();
        Integer gallerySize = realmModel.getGalleryListSize();
        for (int i = 0; i < jobCardImageListNew.size(); i++) {
            jobCardImageList.add(jobCardImageListNew.get(i));
        }
        if (jobCardImageList.size() >= gallerySize) {
            isLastPage = true;
        }
        if (jobCardImageList != null && jobCardImageList.size() > 0) {
            setUpImageRecyclerView(jobCardImageList);
        }
    }

    public void addImageAlert() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(GalleryActivity.this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_pic_bcast, null);
        dialogBuilder.setView(dialogView);
        attachAlertDialog = dialogBuilder.create();
        attachAlertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation_upDown;
        attachAlertDialog.setCancelable(false);
        LinearLayout llCamera = dialogView.findViewById(R.id.ll_camera);
        LinearLayout llChooseFiles = dialogView.findViewById(R.id.ll_files);
        LinearLayout llAudio = dialogView.findViewById(R.id.ll_LAudio);
        llAudio.setVisibility(View.GONE);
        Button btnCancel = dialogView.findViewById(R.id.btn_cancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attachAlertDialog.dismiss();
            }
        });
        llCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkCameraPermission()) {
                    dispatchTakePictureIntent(ACTION_TAKE_PHOTO_B_PIC);
                    attachAlertDialog.dismiss();
                }
            }
        });
        llChooseFiles.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFileChooser(FILE_SELECT_CODE_PIC);
                attachAlertDialog.dismiss();
            }
        });
        /*llAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attachAlertDialog.dismiss();
                showRecordingAlertDialog(0, "");
                if (checkRecordPermission()) {
                    if (mIsAudioMode) {
                        showAudioRecorder();
                        onRecord(true);
                    }
                }
            }
        });*/

        attachAlertDialog.show();
    }

    private void setUpImageRecyclerView(List<GalleryUploadModel> uploadedImages) {
        float scalefactor = getResources().getDisplayMetrics().density * 110;
        int number = getWindowManager().getDefaultDisplay().getWidth();
        int columns = (int) ((float) number / scalefactor) / 2;
//        Utils.showToast(getApplicationContext(), "columns: "+columns+"--scale: "+scalefactor+"--number: "+number);
        if (columns == 0 || columns == 1 || columns == 2)
            columns = 2;
        rvGallery.setVisibility(View.VISIBLE);
        mLayoutManager = new GridLayoutManager(getApplicationContext(), columns, GridLayoutManager.VERTICAL, false);
        addImageAdapter = new AddGalleryAdapter(getApplicationContext(), uploadedImages);
        rvGallery.setAdapter(addImageAdapter);
        rvGallery.setLayoutManager(mLayoutManager);
        rvGallery.setItemAnimator(new DefaultItemAnimator());
//        addImageAdapter.setOnItemClickListener(this);
        addImageAdapter.setCallback(this);
        addImageAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK
                && event.getRepeatCount() == 0) {
            onBackPressed();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onBackPressed() {
        closeAlert(GalleryActivity.this, getResources().getString(R.string.app_gallerypage));
    }

    @Override
    public void imagedelete(int position, String imgName) {
        showDeleteAlert(position, imgName);
    }

    private void deleteGalleryImg(int position, String imgName) {
        mProgressBarHandler.show();
        if (jobCardImageList != null && jobCardImageList.size() > 0) {
            for (int i = 0; i < jobCardImageList.size(); i++) {
                if (jobCardImageList.get(i).getLocaldbid().equalsIgnoreCase(imgName)) {
                    jobCardImageList.remove(i);
                }
            }
        }
        ReturnRealmModel realmModel = realmDBHelper.deleteGalleryImage(mRealm, imgName, position);
        if (realmModel.getImagedeletedStatus() == 1) {
            mProgressBarHandler.hide();
            if (realmModel.getAfterDeleteImgSize() > 0) {
                jobCardImageList = new ArrayList<>();
                getImagesListFromDB(0, 10);
            } else {
                rvGallery.setVisibility(View.GONE);
            }
        } else {
            mProgressBarHandler.hide();
        }
    }

    public void showDeleteAlert(Integer position, String imgName) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(GalleryActivity.this);
        alertDialog.setTitle("Confirm Delete...");
        alertDialog.setMessage("Are you sure you want delete this?");
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                deleteGalleryImg(position, imgName);
            }
        });
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertDialog.show();
    }

    @Override
    public void imageItem(int position, Integer id, String imgUniqueId, String imgName, String imagePath) {
        Intent intent = new Intent(GalleryActivity.this, FullScreenActivity.class);
        intent.putExtra("ImageUrl", imgName);
        intent.putExtra("nextId", "");
//        intent.putExtra(Constant.IMGID,String.valueOf(id));
//        intent.putExtra(Constant.IMGUNIQUEID,imgUniqueId);
        startActivity(intent);
        overridePendingTransitionEnter();
    }

    @Override
    public void addAudio(int position, String imgUniqueId) {

    }

    @Override
    public void addCarPart(int position, Context context, String imgUniqueId) {

    }

    private void goToLoginPage() {
        startActivity(new Intent(GalleryActivity.this, LoginActivity.class));
        finish();
    }

    private void getUserLoginDetailsFromDB() {
        userLoginDetails = realmDBHelper.getUserLoginDetail(mRealm);
        if (userLoginDetails != null || userLoginDetails.size() > 0) {
            try { //login data come db only normally
                loginUserId = userLoginDetails.get(0).getId();
                loginUserName = userLoginDetails.get(0).getUser_name();
                loginShopId = userLoginDetails.get(0).getShop_id();
            } catch (ArrayIndexOutOfBoundsException e) {
                e.printStackTrace();
                showAlertDialog(getResources().getString(R.string.msg_sessionexpire));
                goToLoginPage();
            } catch (Exception e) {
                e.printStackTrace();
                showAlertDialog(getResources().getString(R.string.msg_sessionexpire));
                goToLoginPage();
            }
        } else {
            try {
                if (Utils.getFromUserDefaults(getApplicationContext(), "LoginId") != null) {
                    loginUserId = Utils.getFromUserDefaults(getApplicationContext(), "LoginId");
                    loginUserName = Utils.getFromUserDefaults(getApplicationContext(), "LoginUserName");
                    loginShopId = Utils.getFromUserDefaults(getApplicationContext(), "LoginShopId");
                }
            } catch (ArrayIndexOutOfBoundsException e) {
                e.printStackTrace();
                showAlertDialog(getResources().getString(R.string.msg_sessionexpire));
                goToLoginPage();
            } catch (Exception e) {
                e.printStackTrace();
                showAlertDialog(getResources().getString(R.string.msg_sessionexpire));
                goToLoginPage();
            }
        }
    }
    /*------------------------------- Image Upload --------------------------------------------*/

    private void dispatchTakePictureIntent(int actionCode) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        switch (actionCode) {
            case ACTION_TAKE_PHOTO_B_PIC:
                try {
                    File f = setUpPhotoFile();
                    mCurrentPhotoPath = f.getAbsolutePath();
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, FileProvider.getUriForFile(getApplicationContext(), BuildConfig.APPLICATION_ID + ".provider", f));
                    break;
                } catch (IOException e) {
                    e.printStackTrace();
                    mCurrentPhotoPath = null;
                    break;
                }
        }
        startActivityForResult(takePictureIntent, actionCode);
    }

    private File setUpPhotoFile() throws IOException {
        File f = createImageFile();
        mCurrentPhotoPath = f.getAbsolutePath();
        return f;
    }

    private File createImageFile() throws IOException {
        return File.createTempFile(JPEG_FILE_PREFIX + new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH).format(new Date()) + "_", JPEG_FILE_SUFFIX, getAlbumDir());
    }

    private String getAlbumName() {
        return getString(R.string.app_name);
    }

    private File getAlbumDir() {
        File storageDir = null;
        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            storageDir = mAlbumStorageDirFactory.getAlbumStorageDir(getAlbumName());
            if (!(storageDir == null || storageDir.mkdirs() || storageDir.exists())) {
                Log.d("", "failed to create directory");
                return null;
            }
        }
        Log.v("STHAT", "External storage is not mounted READ/WRITE.");
        return storageDir;
    }

    private void initSthatAndAudioDirectory() {
        try {
            mImageFileDir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + CARSPA_DIRECTORY_PATH);
            mImageFileDir.mkdir();
            Log.d(LOG_TAG, "CARSPA_DIRECTORY_PATH : mImageFileDir = " + mImageFileDir + " mImageFileDir.getAbsolutePath = " + mImageFileDir.getAbsolutePath());
            mImageFileDir = new File(mImageFileDir.getAbsolutePath() + AUDIO_DIR_PATH);
            mImageFileDir.mkdir();
            Log.d(LOG_TAG, "AUDIO_DIR_PATH : mImageFileDir = " + mImageFileDir + " mImageFileDir.getAbsolutePath = " + mImageFileDir.getAbsolutePath());
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), R.string.err_init_directory, Toast.LENGTH_LONG).show();
        }
    }

    private void initVars() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO) {
            mAlbumStorageDirFactory = new FroyoAlbumDirFactory();
        } else {
            mAlbumStorageDirFactory = new BaseAlbumDirFactory();
        }
        mIVDimenPx = (int) DimenUtil.convertDpToPixel(80.0f, getApplicationContext());
    }

    private File saveBitmapToFile(File file) {
        try {
            BitmapFactory.Options o = new BitmapFactory.Options();
            FileInputStream inputStream = new FileInputStream(file);
            BitmapFactory.decodeStream(inputStream, null, o);
            inputStream.close();
            final int REQUIRED_SIZE = 100;
            int scale = 1;
            while (o.outWidth / scale / 2 >= REQUIRED_SIZE &&
                    o.outHeight / scale / 2 >= REQUIRED_SIZE) {
                scale *= 2;
            }
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = 4;
            inputStream = new FileInputStream(file);
            ;
            Bitmap selectedBitmap = BitmapFactory.decodeStream(inputStream, null, o2);
            inputStream.close();

            file.createNewFile();
            FileOutputStream outputStream = new FileOutputStream(file);

            selectedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);

            return file;
        } catch (Exception e) {
            return null;
        }
    }

    private File saveBitmapToRequriedFile(File inputFile, File outputFile) {
        try {
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = false;
            o.inSampleSize = 4;
            FileInputStream inputStream;
            int file_size = Integer.parseInt(String.valueOf(inputFile.length() / 1024));
            if (file_size > 1000) {
                inputStream = new FileInputStream(inputFile);
                BitmapFactory.decodeStream(inputStream, null, o);
                inputStream.close();
                final int REQUIRED_SIZE = 100;
                int scale = 1;
                while (o.outWidth / scale / 2 >= REQUIRED_SIZE &&
                        o.outHeight / scale / 2 >= REQUIRED_SIZE) {
                    scale *= 2;
                }
                BitmapFactory.Options o2 = new BitmapFactory.Options();
                o2.inSampleSize = scale;
                inputStream = new FileInputStream(inputFile);
                Bitmap selectedBitmap = BitmapFactory.decodeStream(inputStream, null, o2);
                inputStream.close();
                outputFile.createNewFile();
                FileOutputStream outputStream = new FileOutputStream(outputFile);
                selectedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
                return outputFile;
            } else {
                outputFile.delete();
                return inputFile;
            }
        } catch (Exception e) {
            return null;
        }
    }

    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE");
        mediaScanIntent.setData(Uri.fromFile(new File(mCurrentPhotoPath)));
        sendBroadcast(mediaScanIntent);
    }

    private void showFileChooser(int fileSelectActionCode) {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/jpeg");
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        try {
            startActivityForResult(Intent.createChooser(intent, getString(R.string.msg_get_file_appl)), fileSelectActionCode);
        } catch (android.content.ActivityNotFoundException e) {
            Toast.makeText(getApplicationContext(), R.string.err_no_filemanager, Toast.LENGTH_SHORT).show();
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intentData) {
        Log.d(TAG, "*********** onActivityResult *************** requestCode = " + requestCode
                + "   resultCode = " + resultCode);
        try {
            String filePath;
            switch (requestCode) {
                case ACTION_TAKE_PHOTO_B_PIC:
                    if (resultCode == Activity.RESULT_OK) {
                        Log.d(TAG, "*********** ACTION_TAKE_PHOTO_B...  mCurrentPhotoPath = " + mCurrentPhotoPath);
                        if (mCurrentPhotoPath != null) {
                            galleryAddPic();
                            mIsPictureSet = true;
                            mPictureFilePath = mCurrentPhotoPath;
                            mPicFile = new File(mCurrentPhotoPath);
                            saveBitmapToFile(mPicFile);
                            /*if (Utils.getInstance(JobCardCameraImagesActivity.this).isOnline()) {
//                                uploadImage(mPicFile);
                            } else {*/
                            String encodedString = getBase64String(mCurrentPhotoPath);
                            uploadImage(encodedString, mCurrentPhotoPath);
//                            decodeBase64AndSetImage(encodedString, imgAdded);
//                                showUploadAlertDialog(0, getResources().getString(R.string.msg_networkcheck), 3);
                            /*}*/
                            Log.d(TAG, "mPictureFilePath : " + mPictureFilePath);
                            mCurrentPhotoPath = null;
                        }
                    }
                    break;
                case FILE_SELECT_CODE_PIC:
                    if (resultCode == Activity.RESULT_OK) {
                        filePath = FileUtils.getAcceptableJpegFilePath(intentData, getApplicationContext());
                        if (filePath != null) {
                            mIsPictureSet = true;
                            File mInputFilePath = new File(filePath);
                            setUpPhotoFile();
                            File mOutpuFilePath = new File(mCurrentPhotoPath);
                            mPicFile = saveBitmapToRequriedFile(mInputFilePath, mOutpuFilePath);
                            /*if (Utils.getInstance(JobCardCameraImagesActivity.this).isOnline()) {
//                                uploadImage(mPicFile);
                            } else {*/
                            String encodedString = getBase64String(mPicFile.getAbsolutePath());
                            uploadImage(encodedString, mPicFile.getAbsolutePath());
//                            decodeBase64AndSetImage(encodedString, imgAdded);
//                                showUploadAlertDialog(0, getResources().getString(R.string.msg_networkcheck), 3);
                            /*}*/
                            Log.d(TAG, "File, mPicture1FilePath: " + filePath);
                        }
                    }
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void uploadImage(String encodedString, String filename) {
        mProgressBarHandler.show();
        String localDbId = UUID.randomUUID().toString();
        String imgUniqueId = UUID.randomUUID().toString();
        Integer nextId = 0;
        String fName = filename.substring(filename.lastIndexOf("/") + 1);
        if (nextId != null && loginUserId != null && loginShopId != null && encodedString != null && !fName.isEmpty()) {
            GalleryUploadModel imgUpload = new GalleryUploadModel();
            imgUpload.setLocaldbid(localDbId);
            imgUpload.setId(nextId);
            imgUpload.setUser_id(Integer.parseInt(loginUserId));
            imgUpload.setShop_id(Integer.parseInt(loginShopId));
            imgUpload.setImage_encode_str(encodedString);
            imgUpload.setImage_filename(fName);
            imgUpload.setImage_carpartname("");
            imgUpload.setImage_uniqueid(imgUniqueId);
            imgUpload.setLocal_insert("1");
            imgUpload.setSync_status("false");
            imgUpload.setRow_deleted(0);
            ReturnRealmModel realmModel = realmDBHelper.addGalleryImages(mRealm, imgUpload);
            if (realmModel.getImageUploadStatus() == 1) {
                showAlertDialog("Image upload successfully");
                getImagesListFromDB(jobCardImageList.size(), 10);
            } else {
                showAlertDialog("Image upload failed");
            }
            mProgressBarHandler.hide();
        } else {
            mProgressBarHandler.hide();
            showAlertDialog("Image upload failed");
        }
    }

    private String getBase64String(String imageFile) {
        // give your image file url in mCurrentPhotoPath
        Bitmap bitmap = BitmapFactory.decodeFile(imageFile);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        // In case you want to compress your image, here it's at 40%
        bitmap.compress(Bitmap.CompressFormat.JPEG, 40, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(byteArray, Base64.DEFAULT);
    }
}
