package com.clt.newcarspa.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.clt.newcarspa.Adapter.AddedImgAudioListAdapter;
import com.clt.newcarspa.R;
import com.clt.newcarspa.Utils.ProgressBarHandler;
import com.clt.newcarspa.Utils.TouchImageView;
import com.clt.newcarspa.database.RealmDBHelper;
import com.clt.newcarspa.model.AudioUploadModel;
import com.clt.newcarspa.model.ReturnRealmModel;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;


public class GalleryFullScreenActivity extends BaseActivity implements AddedImgAudioListAdapter.ButtonAdapterCallback{

    private Realm mRealm;
    private RealmDBHelper realmDBHelper;
    private ProgressBarHandler mProgressBarHandler;
    private TouchImageView img_zoom;
    private String jobId, imgUniqueId;
    private List<AudioUploadModel> audioList;
    private RecyclerView rvAddedAudios;
    private LinearLayoutManager mLayoutManager;
    private AddedImgAudioListAdapter addImageAudioAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_screen);
        mRealm = Realm.getDefaultInstance();
        realmDBHelper = new RealmDBHelper(GalleryFullScreenActivity.this);
        mProgressBarHandler = new ProgressBarHandler(this);
        initView();
        getBundle();
    }

    private void initView() {
        img_zoom = (TouchImageView) findViewById(R.id.img_zoom);
        rvAddedAudios = findViewById(R.id.rv_addedaudios);
        rvAddedAudios.setVisibility(View.GONE);
    }

    private void getBundle() {
        Bundle bun_data = getIntent().getExtras();
        if (bun_data != null) {
            String encodedStr = bun_data.getString("ImageUrl");
//            jobId = bun_data.getString(Constant.IMGID);
//            imgUniqueId = bun_data.getString(Constant.IMGUNIQUEID);
            onHandleUI(encodedStr);
        }
    }

    private void onHandleUI(String encodedStr) {
        decodeBase64AndSetImage(encodedStr, img_zoom);
//        getAudiosFromDB();
    }

    private void getAudiosFromDB() {
        if (jobId != null && imgUniqueId != null) {
            audioList = new ArrayList<>();
            audioList = realmDBHelper.getAudiosFromSelectedIdAndImgUniqueId(mRealm, Integer.parseInt(jobId), imgUniqueId);
            if (audioList != null && audioList.size() > 0) {
                rvAddedAudios.setVisibility(View.VISIBLE);
                setUpAudioRecyclerView();
            }
        }
    }

    private void setUpAudioRecyclerView() {
        mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        addImageAudioAdapter = new AddedImgAudioListAdapter(getApplicationContext(), audioList);
        rvAddedAudios.setAdapter(addImageAudioAdapter);
        rvAddedAudios.setLayoutManager(mLayoutManager);
        rvAddedAudios.setItemAnimator(new DefaultItemAnimator());
//        addImageAdapter.setOnItemClickListener(this);
        addImageAudioAdapter.setCallback(this);
        addImageAudioAdapter.notifyDataSetChanged();
    }

    private void decodeBase64AndSetImage(String completeImageData, ImageView imageView) {
        // Incase you're storing into aws or other places where we have extension stored in the starting.
        String imageDataBytes = completeImageData.substring(completeImageData.indexOf(",") + 1);
        InputStream stream = new ByteArrayInputStream(Base64.decode(imageDataBytes.getBytes(), Base64.DEFAULT));
        Bitmap bitmap = BitmapFactory.decodeStream(stream);
        imageView.setImageBitmap(bitmap);
    }

    private void setTitleActionbar() {

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Images");

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            overridePendingTransitionEnter();
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private boolean checkStoragePermission() {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    @Override
    public void audiodelete(int position, String audName, String imageUniqueId, Integer audioUniqueId) {
        mProgressBarHandler.show();
        ReturnRealmModel realmModel = realmDBHelper.deleteAudioFromSelectedIdAndImgUniqueId(mRealm, Integer.parseInt(jobId), imgUniqueId, position);
        if (realmModel.getAudiodeletedStatus() == 1) {
            mProgressBarHandler.hide();
            if (realmModel.getAfterDeleteImgAudSize() > 0) {
                getAudiosFromDB();
            } else {
                rvAddedAudios.setVisibility(View.GONE);
            }
        } else {
            mProgressBarHandler.hide();
        }
    }

    @Override
    public void audioItem(int position, String encodestring, String audioName, String filePath) {
        showMediaPlayer(encodestring, audioName, filePath);
    }

    /*----------Audio play-------------*/
    public void showMediaPlayer(final String encodeString, final String audioName, final String filePath) {
        final String mediafile = encodeString + audioName;
        android.app.AlertDialog.Builder dialogBuilder = new android.app.AlertDialog.Builder(GalleryFullScreenActivity.this);
        View dialogView = getLayoutInflater().inflate(R.layout.livechat_mediaplayer, null);
        dialogBuilder.setView(dialogView);
        final android.app.AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation_upDown;
        alertDialog.setCancelable(true);
        /*alertDialog.setTitle(R.string.app_name);
        alertDialog.setIcon(R.drawable.ic_action_speaker);*/
        final MediaPlayer mPlayer = new MediaPlayer();
        final TextView tv_audioname = dialogView.findViewById(R.id.tv_audioname);
        final Button playbtn = dialogView.findViewById(R.id.play);
        final Button pausebtn = dialogView.findViewById(R.id.pause);
        final Button stopbtn = dialogView.findViewById(R.id.stop);
        final Button close = dialogView.findViewById(R.id.close);
        tv_audioname.setText(audioName);
        playbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleActionPlayBack(mPlayer, encodeString, audioName, filePath, playbtn, pausebtn, alertDialog);
            }
        });
        pausebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPlayer.isPlaying()) {
                    mPlayer.pause();
                    pausebtn.setBackgroundResource(R.drawable.ic_action_play);
                } else {
                    mPlayer.start();
                    pausebtn.setBackgroundResource(R.drawable.ic_action_pause);
                }
                //handleActionPlayBack(mPlayer, mediafile, playbtn, pausebtn, alertDialog);
            }
        });
        stopbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleActionMediaStop(mPlayer, alertDialog);
            }
        });
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleActionMediaStop(mPlayer, alertDialog);
            }
        });
        alertDialog.show();
    }

    private void handleActionMediaStop(MediaPlayer mPlayer, android.app.AlertDialog alertDialog) {
        if (mPlayer.isPlaying()) {
            mPlayer.stop();
        }
        alertDialog.dismiss();
    }

    private void handleActionPlayBack(MediaPlayer mPlayer, String encodeString, String audioname, String filePath, final Button playbtn, final Button pausebtn, final android.app.AlertDialog alertDialog) {
        mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        try {
            FileOutputStream out = new FileOutputStream(new File(filePath));
            byte[] decoded = Base64.decode(encodeString, 0);

            out.write(decoded);
            out.close();
            mPlayer.setDataSource(filePath);
            mPlayer.prepare();
        } catch (Exception e) {
//            Toast.makeText(getApplicationContext(), "You might not set the URI correctly!", Toast.LENGTH_LONG).show();
        }
        mPlayer.start();
        pausebtn.setVisibility(View.VISIBLE);
        playbtn.setVisibility(View.GONE);
        mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {
//                alertDialog.dismiss();
                pausebtn.setVisibility(View.GONE);
                playbtn.setBackgroundResource(R.drawable.ic_action_play);
                playbtn.setVisibility(View.VISIBLE);
            }
        });
    }

    /*----------Audio play-------------*/
}
