package com.clt.newcarspa.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.clt.newcarspa.BuildConfig;
import com.clt.newcarspa.R;
import com.clt.newcarspa.Utils.Constant;
import com.clt.newcarspa.Utils.ProgressBarHandler;
import com.clt.newcarspa.Utils.Session;
import com.clt.newcarspa.Utils.Utils;
import com.clt.newcarspa.cltinterfaces.RestAdapter;
import com.clt.newcarspa.database.RealmDBHelper;
import com.clt.newcarspa.model.APIListResponse;
import com.clt.newcarspa.model.APIResponse;
import com.clt.newcarspa.model.JobTypeModel;
import com.clt.newcarspa.model.MyCarListItems;
import com.clt.newcarspa.model.UserLoginDetails;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Karthik Thumathy on 23-Jul-18.
 */
public class LoginActivity extends BaseActivity {

    private Realm mRealm;
    EditText etUserName, etUserPassword;
    Button btnLogin;
    private RealmDBHelper realmDBHelper;
    private ProgressBarHandler mProgressBarHandler;
    List<String> mHomeItems = new ArrayList<>();
    List<MyCarListItems> mHomePageItemsList = new ArrayList<>();
    List<JobTypeModel> jobTypeItemList = new ArrayList<>();
    List<JobTypeModel> jobTypeItemListFromDB = new ArrayList<>();

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mRealm.close();
//        realmDBHelper.realmDbClose();
    }

    @Override
    protected void onStop() {
        super.onStop();
//        realmDBHelper.realmDbClose();
    }

    @Override
    protected void onResume() {
        super.onResume();
//        realmDBHelper.with(LoginActivity.this);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_new);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        mRealm = Realm.getDefaultInstance();
        realmDBHelper = new RealmDBHelper(LoginActivity.this);
        mProgressBarHandler = new ProgressBarHandler(this); // In onCreate
        ask_permission();
        checkInternet();
        initView();
        onRestoreLoginDetails(savedInstanceState);
        getDataFromDB();
        setListeners();
        if(BuildConfig.FLAVOR.equalsIgnoreCase("dev") || BuildConfig.FLAVOR.equalsIgnoreCase("beta")){
            etUserName.setText("1234567890");
            etUserPassword.setText("admin");
        }
    }

    /*private void getDataFromDB() {
        mHomeItems = onSetHomeItems();
        mHomePageItemsList = realmDBHelper.getAllHomePageItems();
//        if (mHomePageItemsList == null || mHomePageItemsList.size() == 0) {
        onAddHomePageItems(mHomeItems);
//        }
    }*/

    private void getDataFromDB(){

        mHomeItems = onSetHomeItems();
        mHomePageItemsList = realmDBHelper.getAllHomePageItems(mRealm);

        Integer deleted = realmDBHelper.deleteHomePageItems(mRealm);
        Integer count = realmDBHelper.addHomePageItems(mRealm, UUID.randomUUID().toString(),mHomeItems);
        Utils.showLogD("deletedHomePageItems--"+deleted.toString());
        Utils.showLogD("addHomePageItemscount--"+count.toString());
    }

    private void initView() {
        etUserName = findViewById(R.id.et_loginName);
        etUserPassword = findViewById(R.id.et_loginpwd);
        btnLogin = findViewById(R.id.btn_login);
    }

    private void setListeners() {
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String user = etUserName.getText().toString().trim();
                String pass = etUserPassword.getText().toString().trim();
                loginValidation(user, pass);
            }
        });
    }

    private void loginValidation(String user, String pass) {
        if (BuildConfig.FLAVOR.equals("live")) {
            onFlavorValidate(user, pass);
        } else if (BuildConfig.FLAVOR.equals("beta")) {
            onFlavorValidate(user, pass);
        } else {
//            goToHomepage();
            onFlavorValidate(user, pass);
        }
    }

    private void onFlavorValidate(String user, String pass) {
        if (!user.equals("") && !user.equals(null)) {
            if (!pass.equals("") && !pass.equals(null)) {
                login(user, pass);
            } else {

                Utils.showToast(getApplicationContext(), getResources().getString(R.string.text_enter_ur_password));
            }
        } else {
            Utils.showToast(getApplicationContext(), getResources().getString(R.string.text_enter_ur_name));
        }
    }

    public void login(String user, String password) {
        mProgressBarHandler.show();
        UserLoginDetails details = new UserLoginDetails();
        details.setAction("login");
        details.setPhone(user);
        details.setUser_pass(password);
        Call<APIResponse<UserLoginDetails>> call = RestAdapter.getInstance().getServiceApi().getLoginInfoDetail(details);
        call.enqueue(new Callback<APIResponse<UserLoginDetails>>() {
            @Override
            public void onResponse(@NonNull Call<APIResponse<UserLoginDetails>> call,
                                   @NonNull Response<APIResponse<UserLoginDetails>> response) {
                String status;
                mProgressBarHandler.hide();
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        status = response.body().getStatus();
                        if (status.equals(Constant.RESPONSESUCCESS)) {
                            onHandleLoginSuccessResponse(response);
                        }
                    }
                } else {
                }
            }

            @Override
            public void onFailure(Call<APIResponse<UserLoginDetails>> call, Throwable t) {
                mProgressBarHandler.hide();
                String failure = "";
                if (getApplicationContext() != null) {
                    try {
                        failure = t.getMessage().toString();
                        Log.d("onFailure Response", "" + failure);
                        Utils.showToast(getApplicationContext(), getResources().getString(R.string.msg_servererror));
                    } catch (Exception e) {
                        e.printStackTrace();
                        Utils.showToast(getApplicationContext(), getResources().getString(R.string.msg_servererror) + "" + failure);
                    }
                }
            }
        });
    }

    private void onHandleLoginSuccessResponse(@NonNull Response<APIResponse<UserLoginDetails>> response) {
        UserLoginDetails userDetails = response.body().getResult();
        Integer loggedin = onAddUserLoginDetails(mRealm, userDetails);
        Utils.showLogD("AddedLogindetails--" + loggedin);
        String authToken = userDetails.getToken();
        RestAdapter.getInstance().setToken(authToken);
        Session.getInstance().setUid(LoginActivity.this, userDetails.getId());
        Session.getInstance().setUserName(LoginActivity.this, userDetails.getUser_name());
        Session.getInstance().setShopId(LoginActivity.this, userDetails.getShop_id());
        Utils.saveToUserDefaults(getApplicationContext(), "LoginId", userDetails.getId());
        Utils.saveToUserDefaults(getApplicationContext(), "LoginUserName", userDetails.getUser_name());
        Utils.saveToUserDefaults(getApplicationContext(), "LoginShopId", userDetails.getShop_id());
//        goToHomepage();
        getServicesList();
    }

    public void getServicesList() {
        JobTypeModel jobTypeModel = new JobTypeModel();
        jobTypeModel.setAction("get_job_type_list");
        Call<APIListResponse<JobTypeModel>> call = RestAdapter.getInstance().getServiceApi().getJobTypList(jobTypeModel);
        call.enqueue(new Callback<APIListResponse<JobTypeModel>>() {
            @Override
            public void onResponse(@NonNull Call<APIListResponse<JobTypeModel>> call,
                                   @NonNull Response<APIListResponse<JobTypeModel>> response) {
                String status;
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        status = response.body().getStatus();
                        if (status.equals(Constant.RESPONSESUCCESS)) {
                            onHandleJobTypeSuccessResponse(response);
                        }
                    }
                } else {
                }
            }

            @Override
            public void onFailure(Call<APIListResponse<JobTypeModel>> call, Throwable t) {
                if (getApplicationContext() != null) {
                    String failure = t.getMessage().toString();
                    Log.d("onFailure Response", "" + failure);
                }
            }
        });
    }

    private void onHandleJobTypeSuccessResponse(@NonNull Response<APIListResponse<JobTypeModel>> response) {
        jobTypeItemList = response.body().getResult();
        jobTypeItemListFromDB = realmDBHelper.getJobTypeList(mRealm);
        if (jobTypeItemListFromDB == null || jobTypeItemListFromDB.size() == 0) {
            Integer jobTypeCount = onAddJobTypeDetails(mRealm, jobTypeItemList);
        }
        goToHomepage();
    }

    private void goToHomepage() {
        startActivity(new Intent(LoginActivity.this, MainActivity.class));
        overridePendingTransitionEnter();
        finish();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("user_name", etUserName.getText().toString());
        outState.putString("password", etUserPassword.getText().toString());
    }

    private void onRestoreLoginDetails(@Nullable Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            etUserName.setText(savedInstanceState.getString("user_name"));
            etUserPassword.setText(savedInstanceState.getString("password"));
        }
    }


}
