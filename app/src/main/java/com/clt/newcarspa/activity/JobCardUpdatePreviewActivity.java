package com.clt.newcarspa.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.clt.newcarspa.Adapter.JobTypePreviewItemsAdapter;
import com.clt.newcarspa.BuildConfig;
import com.clt.newcarspa.R;
import com.clt.newcarspa.Utils.Constant;
import com.clt.newcarspa.Utils.ProgressBarHandler;
import com.clt.newcarspa.Utils.Utils;
import com.clt.newcarspa.cltinterfaces.RestAdapter;
import com.clt.newcarspa.database.RealmDBHelper;
import com.clt.newcarspa.model.APIListResponse;
import com.clt.newcarspa.model.AudioUploadModel;
import com.clt.newcarspa.model.ImageUploadModel;
import com.clt.newcarspa.model.JobCardDetailsModel;
import com.clt.newcarspa.model.JobCardDetailsRealmDBModel;
import com.clt.newcarspa.model.JobCardItemsDetailsModel;
import com.clt.newcarspa.model.JobTypeModel;
import com.clt.newcarspa.model.UserLoginDetails;
import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class JobCardUpdatePreviewActivity extends BaseActivity {

    private Realm mRealm;
    private Toolbar toolbar;
    private TextView toolbarTitle, tvTotalAmount, tvCurrentDate, tvCurrentTime, tvDeliveryDate;
    private TextView tvCustomerCode, tvVatRegisterNo, tvName, tvPostBoxNo, tvCity, tvTelephoneNo,
            tvMobileNo, tvFaxNo, tvEmailId, tvVehicleRegisterNo,
            tvVehicleModel, tvVehicleChasisNo, tvVehicleColors, tvVehicleFuel, tvVehicleOil, tvVehicleKilometer, tvServiceDeliverydate, rbServiceDayJob;
    private String customerCode, vatRegisterNo, name, postBoxNo, city, telephoneNo,
            mobileNo, faxNo, emailId, vehicleRegisterNo,
            vehicleModel, vehicleChasisNo, vehicleColors, vehicleFuel, vehicleOil, vehicleKilometer, serviceDeliverydate,
            totalAmount, currentDate, currentTime, deliveryDate, dateAdded, localInsert, syncStatus, jsonItems, status;
    private LinearLayout rgServiceJobType;
    private RadioButton rbServiceNightJob;
    private Button btnFinish, btnEdit;
    private String getRGJobType;
    private RealmDBHelper realmDBHelper;
    List<JobCardDetailsRealmDBModel> jobCard = new ArrayList<>();
    List<JobCardItemsDetailsModel> jobCardItem = new ArrayList<>();
    private List<JobTypeModel> jobSelectedItems = new ArrayList<>();
    private RecyclerView jobTypeRecyclerview;
    private JobTypePreviewItemsAdapter jobTypeAdapter;
    private LinearLayoutManager mLayoutManager;
    private String loginUserId, loginUserName, loginShopId;
    List<UserLoginDetails> userLoginDetails = new ArrayList<>();
    private ProgressBarHandler mProgressBarHandler;
    String uniqueId, nextId, viewOnly;
    private boolean liveOrLocal = false;

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mRealm.close();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_card_preview);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        checkOnline();
        mRealm = Realm.getDefaultInstance();
        realmDBHelper = new RealmDBHelper(JobCardUpdatePreviewActivity.this);
        mProgressBarHandler = new ProgressBarHandler(this);
        getUserLoginDetailsFromDB();
        initView();
        getBundle();
        setListeners();
    }

    private void checkOnline() {
        if (Utils.getInstance(JobCardUpdatePreviewActivity.this).isOnline()) {
            liveOrLocal = true;
        } else {
            liveOrLocal = false;
        }
    }

    private void getUserLoginDetailsFromDB() {
        userLoginDetails = realmDBHelper.getUserLoginDetail(mRealm);
        if (userLoginDetails != null || userLoginDetails.size() > 0) {
            try {
                loginUserId = userLoginDetails.get(0).getId();
                loginUserName = userLoginDetails.get(0).getUser_name();
                loginShopId = userLoginDetails.get(0).getShop_id();
            } catch (ArrayIndexOutOfBoundsException e) {
                e.printStackTrace();
                Utils.showToast(getApplicationContext(), getResources().getString(R.string.msg_sessionexpire));
                goToLoginPage();
            } catch (Exception e) {
                e.printStackTrace();
                Utils.showToast(getApplicationContext(), getResources().getString(R.string.msg_sessionexpire));
                goToLoginPage();
            }
        } else {
            try {
                if (Utils.getFromUserDefaults(getApplicationContext(), "LoginId") != null) {
                    loginUserId = Utils.getFromUserDefaults(getApplicationContext(), "LoginId");
                    loginUserName = Utils.getFromUserDefaults(getApplicationContext(), "LoginUserName");
                    loginShopId = Utils.getFromUserDefaults(getApplicationContext(), "LoginShopId");
                }
            } catch (ArrayIndexOutOfBoundsException e) {
                e.printStackTrace();
                Utils.showToast(getApplicationContext(), getResources().getString(R.string.msg_sessionexpire));
                goToLoginPage();
            } catch (Exception e) {
                e.printStackTrace();
                Utils.showToast(getApplicationContext(), getResources().getString(R.string.msg_sessionexpire));
                goToLoginPage();
            }
        }
    }

    private void goToLoginPage() {
        startActivity(new Intent(JobCardUpdatePreviewActivity.this, LoginActivity.class));
        finish();
    }

    private void getBundle() {
        Intent extras = getIntent();
        if (extras != null) {
            uniqueId = extras.getStringExtra("JobCardUniqueId");
            nextId = extras.getStringExtra("NextId");
            if (extras.getStringExtra("viewOnly") != null) {
                viewOnly = extras.getStringExtra("viewOnly");
                if (!viewOnly.isEmpty() && viewOnly.equalsIgnoreCase("0")) {
                    btnEdit.setVisibility(View.INVISIBLE);
                } else {
                    btnEdit.setVisibility(View.VISIBLE);
                }
            }
            getDateFromLiveOrLocal();
        }
    }

    private void getDateFromLiveOrLocal() {
        if (liveOrLocal) {
            List<JobCardDetailsRealmDBModel> list = realmDBHelper.isJobCardInLocal(mRealm, Integer.parseInt(nextId));
            if (list != null && list.size() > 0) {
                getPreviewDataFromDB(nextId, uniqueId);
            } else {
                callJobCardIdDetailsService();
            }
        } else {
            getPreviewDataFromDB(nextId, uniqueId);
        }
//        Utils.showToast(getApplicationContext(), "liveorlocal--" + liveOrLocal + "--NextId--" + nextId);
    }

    private void callJobCardIdDetailsService() {
        mProgressBarHandler.show();
        JobCardDetailsRealmDBModel model1 = new JobCardDetailsRealmDBModel();
        model1.setAction("get_job_card_list_byid");
        model1.setUser_id(Integer.valueOf(loginUserId));
        model1.setShop_id(Integer.valueOf(loginShopId));
        model1.setId(Integer.parseInt(nextId));
        Call<APIListResponse<JobCardDetailsRealmDBModel>> call = RestAdapter.getInstance().getServiceApi().getNewJobCardIdDetails(model1);
        call.enqueue(new Callback<APIListResponse<JobCardDetailsRealmDBModel>>() {
            @Override
            public void onResponse(@NonNull Call<APIListResponse<JobCardDetailsRealmDBModel>> call,
                                   @NonNull Response<APIListResponse<JobCardDetailsRealmDBModel>> response) {
                mProgressBarHandler.hide();
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        List<JobCardDetailsRealmDBModel> result = response.body().getResult();
                        getDataFromLive(result);
                    }
                } else {
                }
            }

            @Override
            public void onFailure(Call<APIListResponse<JobCardDetailsRealmDBModel>> call, Throwable t) {
                mProgressBarHandler.hide();
                if (getApplicationContext() != null) {
                    String failure = t.getMessage().toString();
                    Log.d("onFailure Response", "" + failure);
                    if (BuildConfig.FLAVOR.equals("dev")) {
                        Utils.showToast(getApplicationContext(), getResources().getString(R.string.msg_servererror) + "--" + failure);
                    } else {
                        Utils.showToast(getApplicationContext(), getResources().getString(R.string.msg_servererror));
//                        errorAlert(getApplicationContext(), getResources().getString(R.string.msg_servererror));
                    }

                }
            }
        });
    }

    private void getDataFromLive(List<JobCardDetailsRealmDBModel> result) {
        customerCode = result.get(0).getCustomer_code();
        vatRegisterNo = result.get(0).getVat_reg_no();
        name = result.get(0).getName();
        postBoxNo = result.get(0).getPo_box();
        city = result.get(0).getCity();
        telephoneNo = result.get(0).getTel_no();
        mobileNo = result.get(0).getMobile_no();
        faxNo = result.get(0).getFax_no();
        emailId = result.get(0).getEmail();
        vehicleRegisterNo = result.get(0).getReg_no();
        vehicleModel = result.get(0).getModel();
        vehicleChasisNo = result.get(0).getChassis_no();
        vehicleColors = result.get(0).getColours();
        vehicleFuel = result.get(0).getFuel();
        vehicleOil = result.get(0).getOil();
        vehicleKilometer = result.get(0).getKilometer();
        serviceDeliverydate = result.get(0).getDelivery_date();
        totalAmount = result.get(0).getTotal_amount();
        currentDate = result.get(0).getAdvisor_date();
        currentTime = result.get(0).getAdvisor_time();
        currentTime = result.get(0).getAdvisor_time();
        getRGJobType = result.get(0).getJob_type();
        deliveryDate = result.get(0).getDelivery_date();
        dateAdded = result.get(0).getDate_added();
        status = result.get(0).getStatus();
        jobCardItem = result.get(0).getItems_new();
        List<ImageUploadModel> imgList = result.get(0).getImages();
        List<AudioUploadModel> audList = result.get(0).getAudios();
        onHandleUIFromDB();
        if (jobCardItem != null || jobCardItem.size() > 0) {
            setupRecyclerView();
        }
    }

    public void errorAlert(Context context, String screenName) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
//        alertDialog.setTitle(getResources().getString(R.string.app_name));
        alertDialog.setMessage(Html.fromHtml(screenName));
        alertDialog.setPositiveButton(getResources().getString(R.string.msg_ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                callJobCardIdDetailsService();
            }
        });
        alertDialog.show();
    }

    private void getPreviewDataFromDB(String nextId, String uniqueId) {
        jobCard = realmDBHelper.getJobCardDetail(mRealm, Integer.parseInt(nextId));
        jobCardItem = realmDBHelper.getCardItemsDetail(mRealm, Integer.parseInt(nextId));
        if (jobCard != null || jobCard.size() > 0) {
//            for (int i = 0; i < jobCard.size(); i++) {
            int i = 0;
            JobCardDetailsRealmDBModel card = jobCard.get(i);
            customerCode = card.getCustomer_code();
            vatRegisterNo = card.getVat_reg_no();
            name = card.getName();
            postBoxNo = card.getPo_box();
            city = card.getCity();
            telephoneNo = card.getTel_no();
            mobileNo = card.getMobile_no();
            faxNo = card.getFax_no();
            emailId = card.getEmail();
            vehicleRegisterNo = card.getReg_no();
            vehicleModel = card.getModel();
            vehicleChasisNo = card.getChassis_no();
            vehicleColors = card.getColours();
            vehicleFuel = card.getFuel();
            vehicleOil = card.getOil();
            vehicleKilometer = card.getKilometer();
            serviceDeliverydate = card.getDelivery_date();
            totalAmount = card.getTotal_amount();
            currentDate = card.getAdvisor_date();
            currentTime = card.getAdvisor_time();
            currentTime = card.getAdvisor_time();
            getRGJobType = card.getJob_type();
            deliveryDate = card.getDelivery_date();
            dateAdded = card.getDate_added();
            status = card.getStatus();

            onHandleUIFromDB();
//            }
        }
        if (jobCardItem != null || jobCardItem.size() > 0) {
            setupRecyclerView();
        }
//        Utils.showToast(getApplicationContext(),"jobCardItem--size--"+jobCardItem.size());
    }

    private void onHandleUIFromDB() {
        tvCustomerCode.setText(customerCode);
        tvVatRegisterNo.setText(vatRegisterNo);
        tvName.setText(name);
        tvPostBoxNo.setText(postBoxNo);
        tvCity.setText(city);
//        tvTelephoneNo.setText(telephoneNo);
        tvMobileNo.setText(mobileNo);
//        tvFaxNo.setText(faxNo);
        tvEmailId.setText(emailId);
        tvCurrentDate.setText(currentDate);
        tvCurrentTime.setText(currentTime);
        tvVehicleRegisterNo.setText(vehicleRegisterNo);
        tvVehicleModel.setText(vehicleModel);
        tvVehicleChasisNo.setText(vehicleChasisNo);
        tvVehicleColors.setText(vehicleColors);
        tvVehicleFuel.setText(vehicleFuel);
        tvVehicleOil.setText(vehicleOil);
        tvVehicleKilometer.setText(vehicleKilometer);
        tvDeliveryDate.setText(serviceDeliverydate);
        tvTotalAmount.setText(totalAmount);
        rbServiceNightJob.setText(getRGJobType);
    }

    private void initView() {
        toolbar = findViewById(R.id.toolbar);
        toolbarTitle = findViewById(R.id.tv_toolbarTitle);
        setSupportActionBar(toolbar);
        showToolBarBackBtn();
        tvCustomerCode = findViewById(R.id.tv_customercode);
        tvVatRegisterNo = findViewById(R.id.tv_vatregno);
        tvName = findViewById(R.id.tv_name);
        tvPostBoxNo = findViewById(R.id.tv_postboxno);
        tvCity = findViewById(R.id.tv_city);
        tvTelephoneNo = findViewById(R.id.tv_telephoneno);
        tvMobileNo = findViewById(R.id.tv_mobileno);
        tvFaxNo = findViewById(R.id.tv_faxno);
        tvEmailId = findViewById(R.id.tv_email);
        tvCurrentDate = findViewById(R.id.tv_currentdate);
        tvCurrentTime = findViewById(R.id.tv_currenttime);
        tvVehicleRegisterNo = findViewById(R.id.tv_regno);
        tvVehicleModel = findViewById(R.id.tv_model);
        tvVehicleChasisNo = findViewById(R.id.tv_chasisno);
        tvVehicleColors = findViewById(R.id.tv_color);
        tvVehicleFuel = findViewById(R.id.tv_fuel);
        tvVehicleOil = findViewById(R.id.tv_oil);
        tvVehicleKilometer = findViewById(R.id.tv_kilometer);
        tvDeliveryDate = findViewById(R.id.tv_deliverydate);
        jobTypeRecyclerview = findViewById(R.id.rv_jobtypeitems);
        tvTotalAmount = findViewById(R.id.tv_totalamt);
        rgServiceJobType = findViewById(R.id.rg_daynightgroup);
        rbServiceNightJob = findViewById(R.id.rb_nightjob);
        btnFinish = findViewById(R.id.btn_sendjobcard);
        btnEdit = findViewById(R.id.btn_editjobcard);
        onHandleUI();
    }

    private void setupRecyclerView() {
        mLayoutManager = new LinearLayoutManager(this);
        jobTypeAdapter = new JobTypePreviewItemsAdapter(this, jobCardItem);
        jobTypeRecyclerview.setAdapter(jobTypeAdapter);
        jobTypeRecyclerview.setLayoutManager(mLayoutManager);
        jobTypeRecyclerview.setItemAnimator(new DefaultItemAnimator());
//        jobTypeAdapter.setOnItemClickListener(JobCardPreviewActivity.this);
//        jobTypeAdapter.setCallback(JobCardPreviewActivity.this);
//        ViewCompat.setNestedScrollingEnabled(jobTypeRecyclerview, false);
    }


    public void showToolBarBackBtn() {
        hideToolbarAppName();
        if (getSupportActionBar() != null) {
            toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_action_back_btn));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    private void setListeners() {
        btnFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                callInsertJobCardService();
                mProgressBarHandler.show();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mProgressBarHandler.hide();
//                        goToHome(MainActivity.class);
                        finish();
                    }
                }, Constant.LOADER_TIME_OUT);
            }
        });
        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mProgressBarHandler.show();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mProgressBarHandler.hide();
                        goToEdit();
                    }
                }, Constant.LOADER_TIME_OUT);
            }
        });
    }

    private void goToEdit() {
        Intent i = new Intent(JobCardUpdatePreviewActivity.this, UpdateJobCardActivity.class);
        i.putExtra("JobCardUniqueId", uniqueId);
        i.putExtra("NextId", nextId);
        startActivity(i);
        overridePendingTransitionExit();
        finish();
    }

    private void onHandleUI() {
        toolbarTitle.setText(getResources().getString(R.string.app_updatejobcardpreviewpage));
//        if (liveOrLocal) {
//            btnEdit.setText("Edit");
//        } else {
//            btnEdit.setText("View");
//        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }
//        else if (id == R.id.action_settings) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    public void callInsertJobCardService() {
        Date date = null;
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
            date = formatter.parse(dateAdded);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.d("11111111", "22222222222222");
        final JobCardDetailsModel cardDetail = new JobCardDetailsModel();
        cardDetail.setAction("set_job_card");
        Log.d("11111111", "3333" + new Gson().toJson(cardDetail));

        cardDetail.setUser_id(Integer.parseInt(loginUserId));
        cardDetail.setShop_id(Integer.parseInt(loginShopId));
        cardDetail.setCustomer_code(customerCode);
        cardDetail.setVat_reg_no(vatRegisterNo);
        cardDetail.setName(name);
        cardDetail.setPo_box(postBoxNo);
        cardDetail.setCity(city);
        cardDetail.setTel_no(telephoneNo);
        cardDetail.setMobile_no(mobileNo);
        cardDetail.setFax_no(faxNo);
        Log.d("11111111", "444444" + new Gson().toJson(cardDetail));
        cardDetail.setEmail(emailId);
        cardDetail.setAdvisor_date(currentDate);
        cardDetail.setAdvisor_time(currentTime);
        cardDetail.setReg_no(vehicleRegisterNo);
        cardDetail.setModel(vehicleModel);
        cardDetail.setChassis_no(vehicleChasisNo);
        cardDetail.setColours(vehicleColors);
        cardDetail.setFuel(vehicleFuel);
        cardDetail.setOil(vehicleOil);
        cardDetail.setKilometer(vehicleKilometer);
        cardDetail.setAdvisor_id(Integer.parseInt(loginUserId));
        cardDetail.setAdvisor_name(loginUserName);
        cardDetail.setDelivery_date(deliveryDate);
        cardDetail.setJob_type(getRGJobType);
        cardDetail.setTotal_amount(String.valueOf(totalAmount));
        cardDetail.setDate_added(dateAdded);
        cardDetail.setCreated_date(date);
        cardDetail.setStatus(status);
//        cardDetail.setLocal_insert(true);
//        cardDetail.setSync_status(false);

        List<JobCardItemsDetailsModel> itemsDetailsList = new ArrayList<>();
        JobCardItemsDetailsModel itemsDetailsModel1;
        for (int i = 0; i < jobCardItem.size(); i++) {
            itemsDetailsModel1 = new JobCardItemsDetailsModel();
            itemsDetailsModel1.setId(jobCardItem.get(i).getService_id());
            itemsDetailsModel1.setName(jobCardItem.get(i).getName());
            itemsDetailsModel1.setOther_name(jobCardItem.get(i).getOther_name());
            itemsDetailsModel1.setPrice(jobCardItem.get(i).getPrice());
            itemsDetailsModel1.setJob_card_id(jobCardItem.get(i).getJob_card_id());
            itemsDetailsList.add(itemsDetailsModel1);

        }
        String items = new Gson().toJson(itemsDetailsList);
        cardDetail.setItems(items);
        Log.d("11111111", "555555--" + new Gson().toJson(cardDetail));
        Log.d("11111111", "66666--" + jobCardItem.size());
        Log.d("11111111", "77777--" + new Gson().toJson(itemsDetailsList));
    }

    private void goToHome(Class validLogin) {
        Intent intent = new Intent(JobCardUpdatePreviewActivity.this, validLogin);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
        overridePendingTransitionEnter();
        finish();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK
                && event.getRepeatCount() == 0) {
            onBackPressed();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onBackPressed() {
        saveAndCloseAlert(JobCardUpdatePreviewActivity.this, getResources().getString(R.string.app_updatejobcardpreviewpage));
    }
}
