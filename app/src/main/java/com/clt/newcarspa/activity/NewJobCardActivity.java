package com.clt.newcarspa.activity;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.clt.newcarspa.Adapter.JobTypeItemsAdapter;
import com.clt.newcarspa.BuildConfig;
import com.clt.newcarspa.R;
import com.clt.newcarspa.Utils.Constant;
import com.clt.newcarspa.Utils.ProgressBarHandler;
import com.clt.newcarspa.Utils.Utils;
import com.clt.newcarspa.Utils.Validation;
import com.clt.newcarspa.cltinterfaces.OnItemClickListener;
import com.clt.newcarspa.cltinterfaces.RestAdapter;
import com.clt.newcarspa.database.RealmDBHelper;
import com.clt.newcarspa.model.APIListResponse;
import com.clt.newcarspa.model.JobCardDetailsModel;
import com.clt.newcarspa.model.JobCardDetailsRealmDBModel;
import com.clt.newcarspa.model.JobTypeModel;
import com.clt.newcarspa.model.ReturnRealmModel;
import com.clt.newcarspa.model.UserLoginDetails;
import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewJobCardActivity extends BaseActivity implements OnItemClickListener, JobTypeItemsAdapter.ButtonAdapterCallback {

    private Realm mRealm;
    private Toolbar toolbar;
    private FloatingActionButton fab;
    private TextView toolbarTitle, tvTotalAmount, tvCurrentDate, tvCurrentTime, tvDeliveryDate;
    private EditText etCustomerCode, etVatRegisterNo, etName, etPostBoxNo, etCity, etTelephoneNo,
            etMobileNo, etFaxNo, etEmailId, etVehicleRegisterNo,
            etVehicleModel, etVehicleChasisNo, etVehicleColors, etVehicleFuel, etVehicleOil, etVehicleKilometer, etServiceDeliverydate;
    private RadioGroup rgServiceJobType;
    private RadioButton rbServiceDayJob, rbServiceNightJob;
    private String getRGJobType;
    private Button btnNext;
    private RecyclerView jobTyperRecyclerview;
    private JobTypeItemsAdapter jobTypeAdapter;
    private RealmDBHelper realmDBHelper;
    private LinearLayoutManager mLayoutManager;
    private List<UserLoginDetails> userLoginDetails = new ArrayList<>();
    private JobTypeModel jobTypeObj = new JobTypeModel();
    List<JobTypeModel> jobTypeList = new ArrayList<>();
    private double grandTotal = 0;
    private int mYear, mMonth, mDay, mHour, mMinute;
    private List<JobTypeModel> selectedJobTypeItemsList = new ArrayList<>();
    private String loginUserId, loginUserName, loginShopId;
    private AlertDialog alertDialog;
    private EditText etCustomAddExtraJobTypeName, etCustomAddExtraJobTypePrice;
    private TextView tvCustomCancel, tvCustomSend;
    private String jobCardUniqueId;
    private Integer fromNew = 0;
    private ProgressBarHandler mProgressBarHandler;
    private Integer newInsert = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_newjob_parent);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        mRealm = Realm.getDefaultInstance();
        realmDBHelper = new RealmDBHelper(NewJobCardActivity.this);
        mProgressBarHandler = new ProgressBarHandler(this);

        initializeCollapsingLayout();
        getUserLoginDetailsFromDB();
        initView();
        onRestoreLoginDetails(savedInstanceState);
        getDataFromDB();
        setListeners();
    }

    private void initializeCollapsingLayout() {
        toolbar = findViewById(R.id.toolbar);
        fab = findViewById(R.id.fab);
        fab.setVisibility(View.GONE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Typeface typeface = ResourcesCompat.getFont(NewJobCardActivity.this, R.font.roboto_condensed);
        CollapsingToolbarLayout collapsingToolbar = findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setCollapsedTitleTypeface(typeface);
        collapsingToolbar.setExpandedTitleTypeface(typeface);
        collapsingToolbar.setTitle(getResources().getString(R.string.app_newjobcardpage));
        loadBackdrop();
    }

    private void loadBackdrop() {
        final ImageView imageView = findViewById(R.id.backdrop);
        Glide.with(this).load(getResources().getDrawable(R.drawable.carbg5)).apply(RequestOptions.fitCenterTransform()).into(imageView);
    }

    private void getUserLoginDetailsFromDB() {
        userLoginDetails = realmDBHelper.getUserLoginDetail(mRealm);
        if (userLoginDetails != null || userLoginDetails.size() > 0) {
            try {
                loginUserId = userLoginDetails.get(0).getId();
                loginUserName = userLoginDetails.get(0).getUser_name();
                loginShopId = userLoginDetails.get(0).getShop_id();
            } catch (ArrayIndexOutOfBoundsException e) {
                e.printStackTrace();
                Utils.showToast(getApplicationContext(), getResources().getString(R.string.msg_sessionexpire));
                goToLoginPage();
            } catch (Exception e) {
                e.printStackTrace();
                Utils.showToast(getApplicationContext(), getResources().getString(R.string.msg_sessionexpire));
                goToLoginPage();
            }
        } else {
            try {
                if (Utils.getFromUserDefaults(getApplicationContext(), "LoginId") != null) {
                    loginUserId = Utils.getFromUserDefaults(getApplicationContext(), "LoginId");
                    loginUserName = Utils.getFromUserDefaults(getApplicationContext(), "LoginUserName");
                    loginShopId = Utils.getFromUserDefaults(getApplicationContext(), "LoginShopId");
                }
            } catch (ArrayIndexOutOfBoundsException e) {
                e.printStackTrace();
                Utils.showToast(getApplicationContext(), getResources().getString(R.string.msg_sessionexpire));
                goToLoginPage();
            } catch (Exception e) {
                e.printStackTrace();
                Utils.showToast(getApplicationContext(), getResources().getString(R.string.msg_sessionexpire));
                goToLoginPage();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (!mRealm.isClosed()) {
            mRealm.close();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    private void goToLoginPage() {
        startActivity(new Intent(NewJobCardActivity.this, LoginActivity.class));
        finish();
    }

    private void getDataFromDB() {
        jobTypeList = realmDBHelper.getJobTypeList(mRealm);
        if (jobTypeList != null && jobTypeList.size() > 0) {
            setupRecyclerView();
        } else {
            // right now do nothing but
            // need to session clear if no data;
        }
    }

    private void initView() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbarTitle = findViewById(R.id.tv_toolbarTitle);
        etCustomerCode = findViewById(R.id.et_jc_customercode);
        etVatRegisterNo = findViewById(R.id.et_jc_vatregno);
        etName = findViewById(R.id.et_jc_name);
        etPostBoxNo = findViewById(R.id.et_jc_postbox);
        etCity = findViewById(R.id.et_jc_city);
        etTelephoneNo = findViewById(R.id.et_jc_telno);
        etMobileNo = findViewById(R.id.et_jc_mobileno);
        etFaxNo = findViewById(R.id.et_jc_faxno);
        etEmailId = findViewById(R.id.et_jc_email);
        tvCurrentDate = findViewById(R.id.tv_currentdate);
        tvCurrentTime = findViewById(R.id.tv_currenttime);
        etVehicleRegisterNo = findViewById(R.id.et_vi_regno);
        etVehicleModel = findViewById(R.id.et_vi_model);
        etVehicleChasisNo = findViewById(R.id.et_vi_chasisno);
        etVehicleColors = findViewById(R.id.et_vi_color);
        etVehicleFuel = findViewById(R.id.et_vi_fuel);
        etVehicleOil = findViewById(R.id.et_vi_oil);
        etVehicleKilometer = findViewById(R.id.et_vi_kilometer);
        jobTyperRecyclerview = findViewById(R.id.rv_jobtype);
        tvDeliveryDate = findViewById(R.id.tv_sa_deliverydate);
        rgServiceJobType = findViewById(R.id.rg_daynightgroup);
        rbServiceDayJob = findViewById(R.id.rb_dayjob);
        rbServiceNightJob = findViewById(R.id.rb_nightjob);
        tvTotalAmount = findViewById(R.id.tv_totalamt);
        btnNext = findViewById(R.id.btn_sendjobcard);
        showToolBarBackBtn();
        onHandleUI();
        if (BuildConfig.FLAVOR.equalsIgnoreCase("dev") || BuildConfig.FLAVOR.equalsIgnoreCase("beta")) {
            int i = 1;
            etCustomerCode.setText("cust00" + i);
            etVatRegisterNo.setText("vatRegNo00" + i);
            etName.setText("name00" + i);
            etPostBoxNo.setText("postBox" + i);
            etCity.setText("city" + i);
            etTelephoneNo.setText("9944584622");
            etMobileNo.setText("9944584622");
            etFaxNo.setText("");
            etEmailId.setText("emailId@gmail.com");
            etVehicleRegisterNo.setText("vehicleRegNo" + i);
            etVehicleModel.setText("vehicleModel" + i);
            etVehicleChasisNo.setText("vehicleChasisNo" + i);
            etVehicleColors.setText("vehicleColor" + i);
            etVehicleFuel.setText("fuel");
            etVehicleOil.setText("500ml");
            etVehicleKilometer.setText("5km");
            i++;
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("customerCode", etCustomerCode.getText().toString());
        outState.putString("vatRegNo", etVatRegisterNo.getText().toString());
        outState.putString("name", etName.getText().toString());
        outState.putString("postBox", etPostBoxNo.getText().toString());
        outState.putString("city", etCity.getText().toString());
//        outState.putString("telephoneNo", etTelephoneNo.getText().toString());
        outState.putString("mobileNo", etMobileNo.getText().toString());
//        outState.putString("faxNo", etFaxNo.getText().toString());
        outState.putString("emailId", etEmailId.getText().toString());
        outState.putString("vehicleRegNo", etVehicleRegisterNo.getText().toString());
        outState.putString("vehicleModel", etVehicleModel.getText().toString());
        outState.putString("vehicleChasisNo", etVehicleChasisNo.getText().toString());
        outState.putString("vehicleColor", etVehicleColors.getText().toString());
        outState.putString("vehicleFuel", etVehicleFuel.getText().toString());
        outState.putString("vehicleOil", etVehicleOil.getText().toString());
        outState.putString("vehicleKilometer", etVehicleKilometer.getText().toString());
        outState.putString("deliveryDate", tvDeliveryDate.getText().toString());
        outState.putInt("jobType", rgServiceJobType.getCheckedRadioButtonId());
    }

    private void onRestoreLoginDetails(@Nullable Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            etCustomerCode.setText(savedInstanceState.getString("customerCode"));
            etVatRegisterNo.setText(savedInstanceState.getString("vatRegNo"));
            etName.setText(savedInstanceState.getString("name"));
            etPostBoxNo.setText(savedInstanceState.getString("postBox"));
            etCity.setText(savedInstanceState.getString("city"));
//            etTelephoneNo.setText(savedInstanceState.getString("telephoneNo"));
            etMobileNo.setText(savedInstanceState.getString("mobileNo"));
//            etFaxNo.setText(savedInstanceState.getString("faxNo"));
            etEmailId.setText(savedInstanceState.getString("emailId"));
            etVehicleRegisterNo.setText(savedInstanceState.getString("vehicleRegNo"));
            etVehicleModel.setText(savedInstanceState.getString("vehicleModel"));
            etVehicleChasisNo.setText(savedInstanceState.getString("vehicleChasisNo"));
            etVehicleColors.setText(savedInstanceState.getString("vehicleColor"));
            tvDeliveryDate.setText(savedInstanceState.getString("deliveryDate"));
            etVehicleFuel.setText(savedInstanceState.getString("vehicleFuel"));
            etVehicleOil.setText(savedInstanceState.getString("vehicleOil"));
            etVehicleKilometer.setText(savedInstanceState.getString("vehicleKilometer"));
            int selectedjobtype = savedInstanceState.getInt("jobType");
            if (selectedjobtype == 0) {
                ((RadioButton) rgServiceJobType.getChildAt(0)).setChecked(true);
            } else {
                ((RadioButton) rgServiceJobType.getChildAt(1)).setChecked(true);
            }
        }
    }

    public void showToolBarBackBtn() {
        hideToolbarAppName();
        if (getSupportActionBar() != null) {
            toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_action_back_btn));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    private void setListeners() {
        rgServiceJobType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // find which radio button is selected
                if (checkedId == R.id.rb_dayjob) {
                    Utils.showLogD("choice: DayJob");
                } else {
                    Utils.showLogD("choice: NightJob");
                }
            }

        });
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToCarViewPage();
            }
        });
        tvCurrentDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentDatePicker(tvCurrentDate);
            }
        });
        tvDeliveryDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentDatePicker(tvDeliveryDate);
            }
        });
        tvCurrentTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentTimePicker(tvCurrentTime);
            }
        });
    }

    private void goToCarViewPage() {
        Date date = null;
        Calendar c = Calendar.getInstance();
        SimpleDateFormat cd = new SimpleDateFormat("dd-MM-yyyy");
        String createDate = cd.format(c.getTime());
        date = stringToDate(createDate);
        String dateString = stringToString(createDate);
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String formattedDate = df.format(c.getTime());
        if (Validation.getInstance().isStringEmpty(getResources().getString(R.string.errmsg_jc_customercode), etCustomerCode, NewJobCardActivity.this)
//                && Validation.getInstance().isStringEmpty(getResources().getString(R.string.errmsg_jc_vatregisterno), etVatRegisterNo, NewJobCardActivity.this)
                && Validation.getInstance().isStringEmpty(getResources().getString(R.string.errmsg_jc_name), etName, NewJobCardActivity.this)
                && Validation.getInstance().isStringEmpty(getResources().getString(R.string.errmsg_jc_postbox), etPostBoxNo, NewJobCardActivity.this)
                && Validation.getInstance().isStringEmpty(getResources().getString(R.string.errmsg_jc_city), etCity, NewJobCardActivity.this)
//                && Validation.getInstance().isStringEmpty(getResources().getString(R.string.errmsg_jc_telephoneno), etTelephoneNo, NewJobCardActivity.this)
//                && Validation.getInstance().isStringEmpty(getResources().getString(R.string.errmsg_jc_mobileno), etMobileNo, NewJobCardActivity.this)
//                && Validation.getInstance().isStringEmpty(getResources().getString(R.string.errmsg_jc_faxno), etFaxNo, NewJobCardActivity.this)
                && Validation.getInstance().isStringEmpty(getResources().getString(R.string.errmsg_jc_email), etEmailId, NewJobCardActivity.this)
                && Validation.getInstance().email(etEmailId, NewJobCardActivity.this)
                && Validation.getInstance().isStringEmptyTextview(getResources().getString(R.string.errmsg_jc_currentdate), tvCurrentDate, NewJobCardActivity.this)
                && Validation.getInstance().isStringEmptyTextview(getResources().getString(R.string.errmsg_jc_currenttime), tvCurrentTime, NewJobCardActivity.this)
                && Validation.getInstance().isStringEmpty(getResources().getString(R.string.errmsg_jc_vi_regno), etVehicleRegisterNo, NewJobCardActivity.this)
                && Validation.getInstance().isStringEmpty(getResources().getString(R.string.errmsg_jc_vi_model), etVehicleModel, NewJobCardActivity.this)
                && Validation.getInstance().isStringEmpty(getResources().getString(R.string.errmsg_jc_vi_chasisno), etVehicleChasisNo, NewJobCardActivity.this)
                && Validation.getInstance().isStringEmpty(getResources().getString(R.string.errmsg_jc_vi_color), etVehicleColors, NewJobCardActivity.this)
                && Validation.getInstance().isStringEmpty(getResources().getString(R.string.errmsg_jc_vi_fuel), etVehicleFuel, NewJobCardActivity.this)
                && Validation.getInstance().isStringEmpty(getResources().getString(R.string.errmsg_jc_vi_oil), etVehicleOil, NewJobCardActivity.this)
                && Validation.getInstance().isStringEmpty(getResources().getString(R.string.errmsg_jc_vi_kilometer), etVehicleKilometer, NewJobCardActivity.this)
                && Validation.getInstance().isStringEmptyTextview(getResources().getString(R.string.errmsg_jc_deliverydate), tvDeliveryDate, NewJobCardActivity.this)) {
            /*if (etTelephoneNo.getText().toString().length() >= 15) {
                if (etMobileNo.getText().toString().length() >= 15) {
                    if (etFaxNo.getText().toString().length() >= 15) {*/
            if (selectedJobTypeItemsList != null || selectedJobTypeItemsList.size() > 0) {
                if (grandTotal > 0.0) {
                    mProgressBarHandler.show();
                    String customerCode = etCustomerCode.getText().toString().trim();
                    String vatRegisterNo = etVatRegisterNo.getText().toString().trim();
                    String name = etName.getText().toString().trim();
                    String postBoxNo = etPostBoxNo.getText().toString().trim();
                    String city = etCity.getText().toString().trim();
                    String telephoneNo = "";//etTelephoneNo.getText().toString().trim();
                    String mobileNo = etMobileNo.getText().toString().trim();
                    String faxNo = "";//tFaxNo.getText().toString().trim();
                    String emailId = etEmailId.getText().toString().trim();
                    String currentDate = tvCurrentDate.getText().toString().trim();
                    String currentTime = tvCurrentTime.getText().toString().trim();
                    String vehicleRegisterNo = etVehicleRegisterNo.getText().toString().trim();
                    String vehicleModel = etVehicleModel.getText().toString().trim();
                    String vehicleChasisNo = etVehicleChasisNo.getText().toString().trim();
                    String vehicleColors = etVehicleColors.getText().toString().trim();
                    String vehicleFuel = etVehicleFuel.getText().toString().trim();
                    String vehicleOil = etVehicleOil.getText().toString().trim();
                    String vehicleKilometer = etVehicleKilometer.getText().toString().trim();
                    String deliveryDate = tvDeliveryDate.getText().toString().trim();
                    String jobDayNight = getDayNightJob();
                    jobCardUniqueId = UUID.randomUUID().toString();
                    JobCardDetailsModel cardDetail = new JobCardDetailsModel();
                    cardDetail.setLocaldbid(jobCardUniqueId);
                    cardDetail.setUser_id(Integer.parseInt(loginUserId));
                    cardDetail.setShop_id(Integer.parseInt(loginShopId));
                    cardDetail.setCustomer_code(customerCode);
                    cardDetail.setVat_reg_no(vatRegisterNo);
                    cardDetail.setName(name);
                    cardDetail.setPo_box(postBoxNo);
                    cardDetail.setCity(city);
                    cardDetail.setTel_no(telephoneNo);
                    cardDetail.setMobile_no(mobileNo);
                    cardDetail.setFax_no(faxNo);
                    cardDetail.setEmail(emailId);
                    cardDetail.setAdvisor_date(currentDate);
                    cardDetail.setAdvisor_time(currentTime);
                    cardDetail.setReg_no(vehicleRegisterNo);
                    cardDetail.setModel(vehicleModel);
                    cardDetail.setChassis_no(vehicleChasisNo);
                    cardDetail.setColours(vehicleColors);
                    cardDetail.setFuel(vehicleFuel);
                    cardDetail.setOil(vehicleOil);
                    cardDetail.setKilometer(vehicleKilometer);
                    cardDetail.setAdvisor_id(Integer.parseInt(loginUserId));
                    cardDetail.setAdvisor_name(loginUserName);
                    cardDetail.setDelivery_date(deliveryDate);
                    cardDetail.setJob_type(jobDayNight);
                    cardDetail.setTotal_amount(String.valueOf(grandTotal));
                    cardDetail.setDate_added(formattedDate);
                    cardDetail.setCreated_date(date); // Date
                    cardDetail.setCreated_date_str(dateString); // Date
                    cardDetail.setAction(Constant.SETJOBCARD);
                    cardDetail.setStatus(Constant.PENDINGSTATUS);
                    cardDetail.setItems(new Gson().toJson(selectedJobTypeItemsList));
                    cardDetail.setLocal_insert("1");
                    cardDetail.setSync_status("false");
                    cardDetail.setRow_deleted(0);
                    final ReturnRealmModel rowAdded = realmDBHelper.addJobCard(mRealm, cardDetail, selectedJobTypeItemsList);
//                    Utils.showToastDev(getApplicationContext(), "fine-"+rowAdded.isStatus()+"--"+rowAdded.getNextId());
                    if (rowAdded.isStatus()) {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                mProgressBarHandler.hide();
                                goToCarViewPage(rowAdded);
                            }
                        }, Constant.LOADER_TIME_OUT);
                                   /* callInsertJobCardService(rowAdded.getNextId(), cardDetail, new CommunicationListener() {
                                        @Override
                                        public void onJobCardSuccessResponse(Integer nextid, String status, JobCardDetailsModel cardDetailsModel) {
                                            if (status.equals(Constant.RESPONSESUCCESS)) {
                                                Utils.showToast(getApplicationContext(), "" + status);

                                            }
                                        }
                                    });*/
                    } else {
                        mProgressBarHandler.hide();
                        Utils.showToast(getApplicationContext(), getResources().getString(R.string.msg_servererror));
                    }
                } else {
                    mProgressBarHandler.hide();
                    Utils.showToast(getApplicationContext(), getResources().getString(R.string.msg_plsselectjobtype));
                }
            } else {
                mProgressBarHandler.hide();
                Utils.showToast(getApplicationContext(), getResources().getString(R.string.msg_plsselectjobtype));
            }
                    /*} else {
                        mProgressBarHandler.hide();
                        Utils.showToast(getApplicationContext(), getResources().getString(R.string.msg_faxnolenght));
                    }
                } else {
                    mProgressBarHandler.hide();
                    Utils.showToast(getApplicationContext(), getResources().getString(R.string.msg_mobilenolenght));
                }
            } else {
                mProgressBarHandler.hide();
                Utils.showToast(getApplicationContext(), getResources().getString(R.string.msg_telephonenolenght));
            }*/
        }
//        startActivity(new Intent(NewJobCardActivity.this, JobCardPreviewActivity.class));
    }

    private void goToCarViewPage(ReturnRealmModel rowAdded) {
        String nextid = String.valueOf(rowAdded.getNextId());
//        Intent i = new Intent(NewJobCardActivity.this, JobCardCarViewActivity.class);
        Intent i = new Intent(NewJobCardActivity.this, JobCardCameraImagesActivity.class);
        i.putExtra("JobCardUniqueId", jobCardUniqueId);
        i.putExtra("NextId", nextid);// jobCardId
        i.putExtra("fromWhere", fromNew);
        startActivity(i);
        overridePendingTransitionEnter();
        finish();
    }

    public void callInsertJobCardService(final Integer nextid, JobCardDetailsRealmDBModel
            carddetail, final CommunicationListener listener) {
        Call<APIListResponse<JobCardDetailsRealmDBModel>> call = RestAdapter.getInstance().getServiceApi().setJobCard(carddetail);
        call.enqueue(new Callback<APIListResponse<JobCardDetailsRealmDBModel>>() {
            @Override
            public void onResponse(@NonNull Call<APIListResponse<JobCardDetailsRealmDBModel>> call,
                                   @NonNull Response<APIListResponse<JobCardDetailsRealmDBModel>> response) {
                String status;
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        status = response.body().getStatus();
                        if (status.equals(Constant.RESPONSESUCCESS)) {
                            listener.onJobCardSuccessResponse(nextid, status, response.body().getResult().get(0));
                        }
                    }
                } else {
                }
            }

            @Override
            public void onFailure(Call<APIListResponse<JobCardDetailsRealmDBModel>> call, Throwable t) {
                if (getApplicationContext() != null) {
                    String failure = t.getMessage().toString();
                    Log.d("onFailure Response", "" + failure);
                    Utils.showToast(getApplicationContext(), getResources().getString(R.string.msg_servererror));
                }
            }
        });
    }

    private String getDayNightJob() {
        int selectedId = rgServiceJobType.getCheckedRadioButtonId();
        if (selectedId == rbServiceDayJob.getId()) {
            getRGJobType = rbServiceDayJob.getText().toString().trim();
        } else {
            getRGJobType = rbServiceNightJob.getText().toString().trim();
        }
        return getRGJobType;
    }

    private void onHandleUI() {
//        toolbarTitle.setText(getResources().getString(R.string.app_newjobcardpage));
        setCurrentDateTime();
    }

    private void setCurrentDateTime() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String formattedDate = df.format(c.getTime());
        tvCurrentDate.setText(formattedDate);
        SimpleDateFormat dt = new SimpleDateFormat("HH:mm");
        String formattedTime = dt.format(c.getTime());
        tvCurrentTime.setText(formattedTime);
        if (BuildConfig.FLAVOR.equalsIgnoreCase("dev") || BuildConfig.FLAVOR.equalsIgnoreCase("beta")) {
            tvDeliveryDate.setText(formattedDate);
        }
    }

    public void currentDatePicker(final TextView txtDate) {
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        getFormattedDate(year, monthOfYear, dayOfMonth, txtDate);
//                        txtDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation_upDown;
        datePickerDialog.show();
    }

    public void currentTimePicker(final TextView txtTime) {
        // Get Current Time
        final Calendar c = Calendar.getInstance();
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        // Launch Time Picker Dialog
        TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        txtTime.setText(hourOfDay + ":" + minute);
                    }
                }, mHour, mMinute, false);
        timePickerDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation_upDown;
        timePickerDialog.show();
    }

    private void setupRecyclerView() {
        mLayoutManager = new LinearLayoutManager(this);
        jobTypeAdapter = new JobTypeItemsAdapter(this, jobTypeList);
        jobTyperRecyclerview.setAdapter(jobTypeAdapter);
        jobTyperRecyclerview.setLayoutManager(mLayoutManager);
        jobTyperRecyclerview.setItemAnimator(new DefaultItemAnimator());
        jobTypeAdapter.setOnItemClickListener(NewJobCardActivity.this);
        jobTypeAdapter.setCallback(NewJobCardActivity.this);
        ViewCompat.setNestedScrollingEnabled(jobTyperRecyclerview, false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }
//        else if (id == R.id.action_settings) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(View view, int position) {
//        Utils.showToast(getApplicationContext(), "pos--" + jobTypeList.get(position).getName());
    }

    @Override
    public void onItemClickWithId(View view, int position, Integer id) {
        // do Nothing
    }

    @Override
    public void getJobTypeCheckedItems(List<JobTypeModel> typeSelectedItems) {
        Utils.getInstance(NewJobCardActivity.this).hideKeyboard(NewJobCardActivity.this);
        if (typeSelectedItems != null || typeSelectedItems.size() > 0) {
            selectedJobTypeItemsList = typeSelectedItems;
            grandTotal = jobTypeObj.getCheckedPrice(typeSelectedItems);
//            for (int i = 0; i < typeSelectedItems.size(); i++) {
//                Utils.showLogD("selecteditems--" + typeSelectedItems.get(i).getName() + "--" + typeSelectedItems.get(i).getPrice() + "--" + grandTotal);
//            }
            Utils.showLogD("---------------------------------------------------------------------------");
            calculateTotal();
        }
    }

    private void calculateTotal() {
        tvTotalAmount.setText("" + String.format(Locale.ENGLISH, "%.2f", grandTotal));// + " "+getResources().getString(R.string.text_currency)
    }

    public void showRateAlertDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(NewJobCardActivity.this);
        LayoutInflater inflater = NewJobCardActivity.this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.customdialog_addjobcardextrafield, null);
        dialogBuilder.setView(dialogView);
        alertDialog = dialogBuilder.create();
        alertDialog.setCancelable(false);
        /*alertDialog.setTitle(R.string.new_rate_driver);
        alertDialog.setIcon(R.drawable.ic_launcher);*/
        etCustomAddExtraJobTypeName = dialogView.findViewById(R.id.et_jc_extrajobtypename);
        etCustomAddExtraJobTypePrice = dialogView.findViewById(R.id.et_jc_extrajobtypeprice);
        tvCustomCancel = dialogView.findViewById(R.id.tv_cancel);
        tvCustomSend = dialogView.findViewById(R.id.tv_send);
        tvCustomSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String customAddExtraName = etCustomAddExtraJobTypeName.getText().toString().trim();
                String customAddExtraPrice = etCustomAddExtraJobTypePrice.getText().toString().trim();
            }
        });
        tvCustomCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    public interface CommunicationListener {
        void onJobCardSuccessResponse(Integer nextid, String status, JobCardDetailsRealmDBModel cardDetailsModel);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK
                && event.getRepeatCount() == 0) {
            onBackPressed();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onBackPressed() {
        if (BuildConfig.FLAVOR.equalsIgnoreCase("beta") || BuildConfig.FLAVOR.equalsIgnoreCase("dev")) {
            overridePendingTransitionExit();
            finish();
        } else {
            closeAlert(NewJobCardActivity.this, getResources().getString(R.string.app_newjobcardpage));
        }
    }

}
