package com.clt.newcarspa.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.clt.newcarspa.Adapter.MyCarListAdapter;
import com.clt.newcarspa.Adapter.autoimageslider.SlidingImage_Adapter;
import com.clt.newcarspa.Adapter.autoimageslider.ViewPagerCustomDuration;
import com.clt.newcarspa.BuildConfig;
import com.clt.newcarspa.R;
import com.clt.newcarspa.Utils.Constant;
import com.clt.newcarspa.Utils.ProgressBarHandler;
import com.clt.newcarspa.Utils.Session;
import com.clt.newcarspa.Utils.Utils;
import com.clt.newcarspa.cltinterfaces.OnItemClickListener;
import com.clt.newcarspa.cltinterfaces.RestAdapter;
import com.clt.newcarspa.database.RealmDBHelper;
import com.clt.newcarspa.model.APIListResponse;
import com.clt.newcarspa.model.APIResponse;
import com.clt.newcarspa.model.AudioUploadModel;
import com.clt.newcarspa.model.CarPartsModel;
import com.clt.newcarspa.model.ErrorReasonModel;
import com.clt.newcarspa.model.GalleryUploadModel;
import com.clt.newcarspa.model.ImageUploadModel;
import com.clt.newcarspa.model.JobCardDetailsRealmDBModel;
import com.clt.newcarspa.model.JobCardItemsDetailsModel;
import com.clt.newcarspa.model.MyCarListItems;
import com.clt.newcarspa.model.ReturnRealmModel;
import com.clt.newcarspa.model.UserLoginDetails;
import com.google.gson.Gson;
import com.viewpagerindicator.CirclePageIndicator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import io.realm.Realm;
import io.realm.RealmList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by Karthik Thumathy on 23-Jul-18.
 */
public class MainActivity extends BaseActivity implements OnItemClickListener {

    private Realm mRealm;
    List<String> mHomeItems = new ArrayList<>();
    //    private GridLayoutManager mGridLayout;
    private LinearLayoutManager mGridLayout;
    //    private StaggeredGridLayoutManager mGridLayout;
    private RealmDBHelper realmDBHelper;
    private ProgressBarHandler mProgressBarHandler;
    private Toolbar toolbar;
    private TextView toolbarTitle, tvTempView;
    private RecyclerView recyclerView;
    private MyCarListAdapter myCarListAdapter;
    private List<MyCarListItems> myCarListItems = new ArrayList<>();
    private List<UserLoginDetails> myLoginDetails = new ArrayList<>();
    private Integer numberOfColumns = 2;
    private List<JobCardDetailsRealmDBModel> pushedCardLists = new ArrayList<>();
    private List<JobCardDetailsRealmDBModel> CardLists = new ArrayList<>();
    private List<JobCardItemsDetailsModel> itemsDetailsList = new ArrayList<>();
    private List<ImageUploadModel> imageCardItemList = new ArrayList<>();
    private List<AudioUploadModel> audioCardItemList = new ArrayList<>();
    private String loginUserId, loginUserName, loginShopId;
    private List<UserLoginDetails> userLoginDetails = new ArrayList<>();
    private List<CarPartsModel> carPartsList;
    private List<CarPartsModel> cPartsList;
    private GridLayoutManager gridLayoutManager;
    private ViewPagerCustomDuration mPager;
    private CirclePageIndicator indicator;
    private static int NUM_PAGES = 0;
    private int currentPage = 0;
    private List<GalleryUploadModel> jobCardImageList;

    private CardView cvScroll;

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mRealm.close();
//        realmDBHelper.realmDbClose();
    }

    @Override
    protected void onStop() {
        super.onStop();
//        realmDBHelper.realmDbClose();
    }

    @Override
    protected void onStart() {
        super.onStart();
//        realmDBHelper = new RealmDBHelper(MainActivity.this);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getHomepageList();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        realmDBHelper = new RealmDBHelper(MainActivity.this);
        mRealm = Realm.getDefaultInstance();
        initView();
        getUserLoginDetailsFromDB();
        mProgressBarHandler = new ProgressBarHandler(MainActivity.this);
        getHomepageList();
    }

    private void getHomepageList() {
        myCarListItems = realmDBHelper.getAllHomePageItems(mRealm);
        realmDBHelper.getAllJobCardDetail(mRealm);
        if (myCarListItems != null && myCarListItems.size() > 0) {
            setupRecyclerView();
        } else {
            mHomeItems = onSetHomeItems();
//            onAddHomePageItems(mHomeItems);
            if (myCarListItems != null && myCarListItems.size() > 0) {
                setupRecyclerView();
            } else {
                // right now do nothing but
                // need to session clear if no data;
            }
        }
        getImagesListFromDB();
    }

    private void getUserLoginDetailsFromDB() {
        userLoginDetails = realmDBHelper.getUserLoginDetail(mRealm);
        if (userLoginDetails != null || userLoginDetails.size() > 0) {
            try {
                loginUserId = userLoginDetails.get(0).getId();
                loginUserName = userLoginDetails.get(0).getUser_name();
                loginShopId = userLoginDetails.get(0).getShop_id();
            } catch (ArrayIndexOutOfBoundsException e) {
                e.printStackTrace();
                Utils.showToast(getApplicationContext(), getResources().getString(R.string.msg_sessionexpire));
                goToLoginPage();
            } catch (Exception e) {
                e.printStackTrace();
                Utils.showToast(getApplicationContext(), getResources().getString(R.string.msg_sessionexpire));
                goToLoginPage();
            }
        } else {
            try {
                if (Utils.getFromUserDefaults(getApplicationContext(), "LoginId") != null) {
                    loginUserId = Utils.getFromUserDefaults(getApplicationContext(), "LoginId");
                    loginUserName = Utils.getFromUserDefaults(getApplicationContext(), "LoginUserName");
                    loginShopId = Utils.getFromUserDefaults(getApplicationContext(), "LoginShopId");
                }
            } catch (ArrayIndexOutOfBoundsException e) {
                e.printStackTrace();
                Utils.showToast(getApplicationContext(), getResources().getString(R.string.msg_sessionexpire));
                goToLoginPage();
            } catch (Exception e) {
                e.printStackTrace();
                Utils.showToast(getApplicationContext(), getResources().getString(R.string.msg_sessionexpire));
                goToLoginPage();
            }
        }
    }

    private void goToLoginPage() {
        startActivity(new Intent(MainActivity.this, LoginActivity.class));
        finish();
    }

    private void initView() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbarTitle = findViewById(R.id.tv_toolbarTitle);
//        tvTempView = findViewById(R.id.tv_tempview);
        cvScroll = findViewById(R.id.card_class);
        mPager = findViewById(R.id.pager);
        indicator = findViewById(R.id.indicator);
        recyclerView = findViewById(R.id.rv_homeitems);
        hideToolbarAppName();
        onHandleUI();
        checkCarPartsListFromDB();
//        tvTempView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                startActivity(new Intent(MainActivity.this, JobCardListActivity.class));
//            }
//        });
    }

    private void getImagesListFromDB() {
        jobCardImageList = new ArrayList<>();
        ReturnRealmModel realmModel = realmDBHelper.getGalleryImages(mRealm, 0, 10);
        jobCardImageList = realmModel.getGalleryList();
        if (jobCardImageList != null && jobCardImageList.size() > 0) {
            cvScroll.setVisibility(View.VISIBLE);
            setAdvertisentImages();
        }else{
            cvScroll.setVisibility(View.GONE);
        }
    }

    private void setAdvertisentImages() {
        mPager.setScrollDurationFactor(6);
            mPager.setAdapter(new SlidingImage_Adapter(getApplicationContext(), jobCardImageList));
            indicator.setViewPager(mPager);
//            mPager.setCallback(this);
        final float density = getResources().getDisplayMetrics().density;
        //Set circle indicator radius
        indicator.setRadius(3 * density);

        NUM_PAGES = jobCardImageList.size();
        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                mPager.setCurrentItem(currentPage++, true);
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 1000, 7000);
        // Pager listener over indicator
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                currentPage = position;
            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {
            }

            @Override
            public void onPageScrollStateChanged(int pos) {
            }
        });
        indicator.notifyDataSetChanged();
    }

    private void checkCarPartsListFromDB() {
        cPartsList = new ArrayList<>();
        cPartsList = realmDBHelper.getCartPartsList(mRealm);
        if (cPartsList != null && cPartsList.size() == 0) {
            getCarPartsList();
        }
    }

    private void onHandleUI() {
        toolbarTitle.setText(getResources().getString(R.string.app_homepage));
//        if (BuildConfig.FLAVOR.equalsIgnoreCase("beta")) {
//            tvTempView.setVisibility(View.VISIBLE);
//        }
    }

    private void setupRecyclerView() {
        if (Session.getInstance().getIsTablet(MainActivity.this)) {
            gridLayoutManager = new GridLayoutManager(MainActivity.this, 2, GridLayoutManager.VERTICAL, false);
        } else {
            mGridLayout = new LinearLayoutManager(MainActivity.this);
        }
//        mGridLayout = new LinearLayoutManager(MainActivity.this);
//        mGridLayout = new GridLayoutManager(MainActivity.this, numberOfColumns);
        myCarListAdapter = new MyCarListAdapter(this, myCarListItems);
        recyclerView.setAdapter(myCarListAdapter);
        if (Session.getInstance().getIsTablet(MainActivity.this)) {
            recyclerView.setLayoutManager(gridLayoutManager);
        } else {
            recyclerView.setLayoutManager(mGridLayout);
        }
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        myCarListAdapter.setOnItemClickListener(MainActivity.this);
    }

    public void getCarPartsList() {
        CarPartsModel details = new CarPartsModel();
        details.setAction(Constant.CARPARTSlIST);
        Call<APIListResponse<CarPartsModel>> call = RestAdapter.getInstance().getServiceApi().getCarPartsList(details);
        call.enqueue(new Callback<APIListResponse<CarPartsModel>>() {
            @Override
            public void onResponse(@NonNull Call<APIListResponse<CarPartsModel>> call,
                                   @NonNull Response<APIListResponse<CarPartsModel>> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (onCheckAPIStatus(Integer.parseInt(response.body().getStatus()))) {
                            carPartsList = new ArrayList<>();
                            carPartsList = response.body().getResult();
                            if (carPartsList != null && carPartsList.size() > 0) {
                                ReturnRealmModel realmModel = realmDBHelper.addCarPartsItems(mRealm, carPartsList);
                                if (realmModel.getAddedCarPartsStatus() == 1) {
//                                    Utils.showToast(getApplicationContext(), "status--"+realmModel.getAddedCarPartsStatus());
                                }
                            }
                        } else {
                            onHandleCarPartListErrorResponse(response);
                        }
                    }
                } else {
                    onHandleCarPartListErrorResponse(response);
                }
            }

            @Override
            public void onFailure(Call<APIListResponse<CarPartsModel>> call, Throwable t) {
                String failure = "";
                if (getApplicationContext() != null) {
                    try {
                        failure = t.getMessage().toString();
                        Log.d("onFailure Response", "" + failure);
                        Utils.showToast(getApplicationContext(), getResources().getString(R.string.msg_servererror));
                    } catch (Exception e) {
                        e.printStackTrace();
                        Utils.showToast(getApplicationContext(), getResources().getString(R.string.msg_servererror) + "" + failure);
                    }
                }
            }
        });
    }

    private void onHandleCarPartListErrorResponse(@NonNull Response<APIListResponse<CarPartsModel>> response) {
        try {
            if (response.errorBody() != null) {
                ErrorReasonModel model = new Gson().fromJson(response.errorBody().string(), ErrorReasonModel.class);
                int reason = model.getStatus();
                showAlertDialog(onCheckErrorStatus(getApplicationContext(), reason));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void goToStoreDetailPage(int pos) {
        MyCarListItems myCarList = myCarListItems.get(pos);

        if (myCarList.getItemName().equals(Constant.NEWJOBCARD)) {
            startActivity(new Intent(MainActivity.this, NewJobCardActivity.class));
            overridePendingTransitionEnter();
        } else if (myCarList.getItemName().equals(Constant.VIEWJOBCARDLIST)) {
            ReturnRealmModel realmmodel = realmDBHelper.getPushAllJobCardDetail(mRealm);
            if (realmmodel != null) {
                if (realmmodel.getJobCard() != null || realmmodel.getJobCard().size() > 0) {
//                    Utils.showToast(getApplicationContext(),"jobcardlist:--"+realmmodel.getJobCard().size());
                    int jobCardSize = realmmodel.getJobCard().size();
                    if (jobCardSize >= 10) {
                        checkToPushLocalData();
                    }else{
                        startActivity(new Intent(MainActivity.this, JobCardListActivity.class));
                        overridePendingTransitionEnter();
                    }
                }
            }else {
                startActivity(new Intent(MainActivity.this, JobCardListActivity.class));
                overridePendingTransitionEnter();
            }
        } else if (myCarList.getItemName().equals(Constant.SETTINGS)) {
//            startActivity(new Intent(MainActivity.this, CustomerSignatureActivity.class));
            startActivity(new Intent(MainActivity.this, SettingActivity.class));
            overridePendingTransitionEnter();
        } else if (myCarList.getItemName().equals(Constant.REPORT)) {
            startActivity(new Intent(MainActivity.this, ReportsActivity.class));
            overridePendingTransitionEnter();
        } else if (myCarList.getItemName().equals(Constant.TODAYDELIVERY)) {
            startActivity(new Intent(MainActivity.this, TodayDeliveryActivity.class));
            overridePendingTransitionEnter();
        } else if (myCarList.getItemName().equals(Constant.GALLERY)) {
            startActivity(new Intent(MainActivity.this, GalleryActivity.class));
            overridePendingTransitionEnter();
        } else if (myCarList.getItemName().equals(Constant.PUSH)) {
            if (Utils.getInstance(MainActivity.this).isInternet()) {
//                pushToServer();
            }
        } else if (myCarList.getItemName().equals(Constant.LOGOUT)) {
            if (Utils.getInstance(MainActivity.this).isInternet()) {
                logout();

            }
        }


    }

    private void checkToPushLocalData() {
        new AlertDialog.Builder(MainActivity.this)
                .setTitle(getResources().getString(R.string.app_name))
                .setMessage("Push your local data?")
                .setCancelable(true)
                .setPositiveButton(getResources().getString(R.string.msg_ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        startActivity(new Intent(MainActivity.this, SettingActivity.class));
                        overridePendingTransitionEnter();
                    }
                })
                .setNegativeButton(getResources().getString(R.string.msg_later), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        startActivity(new Intent(MainActivity.this, JobCardListActivity.class));
                        overridePendingTransitionEnter();
                    }
                })
                .create()
                .show();
    }

    private void pushToServer() {
        String mPushStr = null;
        RealmList<JobCardDetailsRealmDBModel> pushCardList = new RealmList<>();
        List<JobCardItemsDetailsModel> pushCardItemsList;
        List<ImageUploadModel> imageItemsList;
        List<AudioUploadModel> audioItemsList;
        ReturnRealmModel realmmodel = realmDBHelper.getPushAllJobCardDetail(mRealm);
        if (realmmodel != null) {
            if (realmmodel.getJobCard() != null || realmmodel.getJobCard().size() > 0) {
                JobCardDetailsRealmDBModel mJobCardList;
                JobCardItemsDetailsModel mJobCardItemsList;
                ImageUploadModel mImageObj;
                AudioUploadModel mAudioObj;

                imageCardItemList = getRealmImageListToList(realmmodel);
                audioCardItemList = getRealmAudioListToList(realmmodel);
                CardLists = getRealmToCardList(realmmodel);
                itemsDetailsList = getRealmListToList(realmmodel);
                for (int i = 0; i < CardLists.size(); i++) {
                    pushCardItemsList = new ArrayList<>();
                    mJobCardList = CardLists.get(i);
                    if (realmmodel.getJobCardItems() != null && realmmodel.getJobCardItems().size() > 0) {
                        for (int j = 0; j < itemsDetailsList.size(); j++) {
                            mJobCardItemsList = itemsDetailsList.get(j);
                            if (mJobCardList.getId().equals(mJobCardItemsList.getJob_card_id())) {
                                pushCardItemsList.add(mJobCardItemsList);
                                Utils.showLogD("20001----" + new Gson().toJson(mJobCardItemsList));
                            }
                        }
                        if (pushCardItemsList.size() > 0) {
                            String items = new Gson().toJson(pushCardItemsList);
                            mJobCardList.setItems(items);
                        }
                    }
                    imageItemsList = new ArrayList<>();
                    if (imageCardItemList != null && imageCardItemList.size() > 0) {
                        for (int p = 0; p < imageCardItemList.size(); p++) {
                            mImageObj = imageCardItemList.get(p);
                            if (mJobCardList.getId().equals(mImageObj.getId())) {

                                imageItemsList.add(mImageObj);
                            }
                        }
                        if (imageItemsList.size() > 0) {
                            String imageItems = new Gson().toJson(imageItemsList);
                            mJobCardList.setItem_images(imageItems);
                        }
                    }

                    audioItemsList = new ArrayList<>();
                    if (audioCardItemList != null && audioCardItemList.size() > 0) {
                        for (int p = 0; p < audioCardItemList.size(); p++) {
                            mAudioObj = audioCardItemList.get(p);
                            if (mJobCardList.getId().equals(mAudioObj.getId())) {
                                audioItemsList.add(mAudioObj);
                            }
                        }
                        if (audioItemsList.size() > 0) {
                            String audioItems = new Gson().toJson(audioItemsList);
                            mJobCardList.setItem_audios(audioItems);
                        }
                    }

                    pushCardList.add(mJobCardList);
                }
                if (pushCardList.size() > 0) {
                    mPushStr = new Gson().toJson(pushCardList);
                }

                if (realmmodel.getJobCard().size() > 0) {
                    callPushJobCardService(pushCardList);
                } else {
                    Utils.showToast(getApplicationContext(), getResources().getString(R.string.text_norecordfound));
                }
            } else {
                Utils.showToast(getApplicationContext(), getResources().getString(R.string.text_norecordfound));
            }
        }
    }

    public void callPushJobCardService(RealmList<JobCardDetailsRealmDBModel> pushStr) {
//            , final NewJobCardActivity.CommunicationListener listener) {
        mProgressBarHandler.show();
        JobCardDetailsRealmDBModel carddetail = new JobCardDetailsRealmDBModel();
        carddetail.setAction(Constant.PUSHJOBCARDLIST);
        carddetail.setJob_card_pushstr(pushStr);
        Call<APIListResponse<JobCardDetailsRealmDBModel>> call = RestAdapter.getInstance().getServiceApi().pushJobCardList(carddetail);
        call.enqueue(new Callback<APIListResponse<JobCardDetailsRealmDBModel>>() {
            @Override
            public void onResponse(@NonNull Call<APIListResponse<JobCardDetailsRealmDBModel>> call,
                                   @NonNull Response<APIListResponse<JobCardDetailsRealmDBModel>> response) {
                String status;
                if (response.isSuccessful()) {
                    mProgressBarHandler.hide();
                    if (response.body() != null) {
                        status = response.body().getStatus();
                        if (status.equals(Constant.RESPONSESUCCESS)) {
                            pushedCardLists = response.body().getResult();
                            Utils.showToast(getApplicationContext(), "push successfully");
                            realmDBHelper.updatePushedJobCardStatus(mRealm, pushedCardLists);
//                            listener.onJobCardSuccessResponse(nextid, status, response.body().getResult().get(0));
                        }
                    }
                } else {
                    mProgressBarHandler.hide();
                }
                realmDBHelper.getAllJobCardDetail(mRealm);
            }

            @Override
            public void onFailure(Call<APIListResponse<JobCardDetailsRealmDBModel>> call, Throwable t) {
                mProgressBarHandler.hide();
                if (getApplicationContext() != null) {
                    String failure = t.getMessage().toString();
                    Log.d("onFailure Response", "" + failure);
                    if (BuildConfig.FLAVOR.equals("dev")) {
                        Utils.showToast(getApplicationContext(), getResources().getString(R.string.msg_servererror) + "--" + failure);
                    } else {
                        Utils.showToast(getApplicationContext(), getResources().getString(R.string.msg_servererror));
                    }
                }
            }
        });
    }

    private void underProcess() {
        Utils.showToast(MainActivity.this, "" + getResources().getString(R.string.msg_underProcess));
    }

    private void logout() {
        mProgressBarHandler.show();
        UserLoginDetails details = new UserLoginDetails();
        details.setAction("log_out");
        details.setId(loginUserId);
        Call<APIResponse<UserLoginDetails>> call = RestAdapter.getInstance().getServiceApi().getLoginInfoDetail(details);
        call.enqueue(new Callback<APIResponse<UserLoginDetails>>() {
            @Override
            public void onResponse(@NonNull Call<APIResponse<UserLoginDetails>> call,
                                   @NonNull Response<APIResponse<UserLoginDetails>> response) {
                String status;
                mProgressBarHandler.hide();
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        status = response.body().getStatus();
                        if (status.equals(Constant.RESPONSESUCCESS)) {
                            Session.getInstance().setUid(MainActivity.this, null);
                            Session.getInstance().setUserName(MainActivity.this, null);
                            goToActivity(LoginActivity.class);
                        }
                    }
                } else {
                }
            }

            @Override
            public void onFailure(Call<APIResponse<UserLoginDetails>> call, Throwable t) {
                mProgressBarHandler.hide();
                String failure = "";
                if (getApplicationContext() != null) {
                    try {
                        failure = t.getMessage().toString();
                        Log.d("onFailure Response", "" + failure);
                        Utils.showToast(getApplicationContext(), getResources().getString(R.string.msg_servererror));
                    } catch (Exception e) {
                        e.printStackTrace();
                        if (BuildConfig.FLAVOR.equals("dev")) {
                            Utils.showToast(getApplicationContext(), getResources().getString(R.string.msg_servererror) + "" + failure);
                        }
                    }
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
//        if (id == R.id.action_settings) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(View view, int position) {
        goToStoreDetailPage(position);
    }

    @Override
    public void onItemClickWithId(View view, int position, Integer id) {
        // do Nothing
    }

    private void goToActivity(Class validLogin) {
        Intent intent = new Intent(MainActivity.this, validLogin);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
        overridePendingTransitionExit();
        finish();
    }
}
