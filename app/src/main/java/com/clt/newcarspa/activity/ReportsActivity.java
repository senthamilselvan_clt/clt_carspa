package com.clt.newcarspa.activity;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.clt.newcarspa.Adapter.TodayDeliveryJobCardAdapter;
import com.clt.newcarspa.BuildConfig;
import com.clt.newcarspa.R;
import com.clt.newcarspa.Utils.ProgressBarHandler;
import com.clt.newcarspa.Utils.Utils;
import com.clt.newcarspa.cltinterfaces.OnItemClickListener;
import com.clt.newcarspa.cltinterfaces.RestAdapter;
import com.clt.newcarspa.database.RealmDBHelper;
import com.clt.newcarspa.model.APIListResponse;
import com.clt.newcarspa.model.JobCardDetailsRealmDBModel;
import com.clt.newcarspa.model.JobCardItemsDetailsModel;
import com.clt.newcarspa.model.ReturnRealmModel;
import com.clt.newcarspa.model.UserLoginDetails;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ReportsActivity extends BaseActivity implements OnItemClickListener, TodayDeliveryJobCardAdapter.ButtonAdapterCallback {

    private Realm mRealm;
    private Toolbar toolbar;
    private TextView toolbarTitle, tvFromDate, tvToDate, tvNoRecord;
    private Button btnFilter;
    private RecyclerView rvTodaydeliveryList;
    private int mYear, mMonth, mDay, mHour, mMinute;
    private RealmDBHelper realmDBHelper;
    private ProgressBarHandler mProgressBarHandler;
    private LinearLayoutManager mLayoutManager;
    private TodayDeliveryJobCardAdapter todayDeliveryAdapter;
    private List<JobCardDetailsRealmDBModel> todayJobCardList = new ArrayList<>();
    private List<JobCardItemsDetailsModel> itemsDetailsList;

    private AlertDialog alertDialog;
    private TextView tvPrevCustomerCode, tvPrevVatRegisterNo, tvPrevName, tvPrevPostBoxNo, tvPrevCity, tvPrevTelephoneNo,
            tvPrevMobileNo, tvPrevFaxNo, tvPrevEmailId, tvPrevVehicleRegisterNo, tvPrevDeliveryDate, tvPrevTotalAmount, tvPrevTxtJobType,
            tvPrevVehicleModel, tvPrevVehicleChasisNo, tvPrevVehicleColors, tvPrevVehicleFuel, tvPrevVehicleOil, tvPrevVehicleKilometer, tvPrevCurrentDate, tvPrevCurrentTime, tvPrevHeaderTitle, tvPrevToolbarTitle,
            tvTitle, tvPrice;
    private LinearLayout layout_items;
    private RelativeLayout rlHeader;
    private View[] child;
    private LayoutInflater inflater1;
    private Toolbar prevToolbar;
    private ImageView imgPrevHeaderClose, imgPrevHeaderRight, imgPrevHeaderLeft;
    private RecyclerView jobPrevTypeRecyclerview;
    private LinearLayout rgPrevServiceJobType, llInflateLayout;
    private RadioButton rbPrevServiceNightJob;
    private Button btnPrevFinish, btnPrevEdit;
    private List<JobCardItemsDetailsModel> cardItemsList = new ArrayList<>();
    private List<JobCardDetailsRealmDBModel> todayJobCard = new ArrayList<>();
    private boolean liveOrLocal = false;
    private String loginUserId, loginUserName, loginShopId;
    private List<UserLoginDetails> userLoginDetails = new ArrayList<>();
    private String toDate = "";
    private String fromDate = "";

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mRealm.close();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        onHandleLiveOrLocal();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        mRealm = Realm.getDefaultInstance();
        realmDBHelper = new RealmDBHelper(ReportsActivity.this);
        mProgressBarHandler = new ProgressBarHandler(this);
        onHandleLiveOrLocal();
        getUserLoginDetailsFromDB();
        initView();
        onRestoreLoginDetails(savedInstanceState);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("fromDate", tvFromDate.getText().toString());
        outState.putString("toDate", tvToDate.getText().toString());
    }

    private void onRestoreLoginDetails(@Nullable Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            tvFromDate.setText(savedInstanceState.getString("fromDate"));
            tvToDate.setText(savedInstanceState.getString("toDate"));
            if (liveOrLocal) {

            }else {
                getDataFromDB();
            }
        }
    }

    private void onHandleLiveOrLocal() {
        if (Utils.getInstance(getApplicationContext()).isOnline()) {
            liveOrLocal = true;
        } else {
            liveOrLocal = false;
        }
    }

    private void callLiveOrLocalData(String from, String to) {
        if (liveOrLocal) {
            if (!from.equals("") && from != null && !from.isEmpty()) {

                String[] fromArrayString = from.split("-");
                String[] toArrayString = to.split("-");

                String str0 = fromArrayString[0];
                String str1 = fromArrayString[1];
                String str2 = fromArrayString[2];
                String str3 = toArrayString[0];
                String str4 = toArrayString[1];
                String str5 = toArrayString[2];
                String dateFrom = str2+"-"+str1+"-"+str0;
                String dateTo = str5+"-"+str4+"-"+str3;
                getTodayDelivery(dateFrom, dateTo);
            }else{
                Utils.showToast(getApplicationContext(),getResources().getString(R.string.text_selectdeliverydate));
            }
        }else{
            getDataFromDB();
        }
    }

    private void getUserLoginDetailsFromDB() {
//        loginUserId = Session.getInstance().getGid(getApplicationContext());
//        loginShopId = Session.getInstance().getShopId(getApplicationContext());
        userLoginDetails = realmDBHelper.getUserLoginDetail(mRealm);
        if (userLoginDetails != null || userLoginDetails.size() > 0) {
            try {
                loginUserId = userLoginDetails.get(0).getId();
                loginUserName = userLoginDetails.get(0).getUser_name();
                loginShopId = userLoginDetails.get(0).getShop_id();
            } catch (ArrayIndexOutOfBoundsException e) {
                e.printStackTrace();
                Utils.showToast(getApplicationContext(), getResources().getString(R.string.msg_sessionexpire));
                goToLoginPage();
            } catch (Exception e) {
                e.printStackTrace();
                Utils.showToast(getApplicationContext(), getResources().getString(R.string.msg_sessionexpire));
                goToLoginPage();
            }
        } else {
            try {
                if (Utils.getFromUserDefaults(getApplicationContext(), "LoginId") != null) {
                    loginUserId = Utils.getFromUserDefaults(getApplicationContext(), "LoginId");
                    loginUserName = Utils.getFromUserDefaults(getApplicationContext(), "LoginUserName");
                    loginShopId = Utils.getFromUserDefaults(getApplicationContext(), "LoginShopId");
                }
            } catch (ArrayIndexOutOfBoundsException e) {
                e.printStackTrace();
                Utils.showToast(getApplicationContext(), getResources().getString(R.string.msg_sessionexpire));
                goToLoginPage();
            } catch (Exception e) {
                e.printStackTrace();
                Utils.showToast(getApplicationContext(), getResources().getString(R.string.msg_sessionexpire));
                goToLoginPage();
            }
        }
    }

    private void goToLoginPage() {
        startActivity(new Intent(ReportsActivity.this, LoginActivity.class));
        finish();
    }


    public void getTodayDelivery(String fromDate, String toDate){
        mProgressBarHandler.show();
        JobCardDetailsRealmDBModel model1 = new JobCardDetailsRealmDBModel();
        model1.setAction("get_job_card_list");
        model1.setUser_id(Integer.valueOf(loginUserId));
        model1.setShop_id(Integer.valueOf(loginShopId));
        model1.setFrom_date(fromDate);
        model1.setTo_date(toDate);
        Call<APIListResponse<JobCardDetailsRealmDBModel>> call = RestAdapter.getInstance().getServiceApi().getJobCardList(model1);
        call.enqueue(new Callback<APIListResponse<JobCardDetailsRealmDBModel>>() {
            @Override
            public void onResponse(@NonNull Call<APIListResponse<JobCardDetailsRealmDBModel>> call,
                                   @NonNull Response<APIListResponse<JobCardDetailsRealmDBModel>> response) {
                mProgressBarHandler.hide();
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        todayJobCard = response.body().getResult();
                        if (todayJobCard != null) {
                            if (todayJobCard.size() > 0) {
                                rvTodaydeliveryList.setVisibility(View.VISIBLE);
                                tvNoRecord.setVisibility(View.GONE);
                                onSetRecyclerView();
                            } else {
                                rvTodaydeliveryList.setVisibility(View.GONE);
                                tvNoRecord.setVisibility(View.VISIBLE);
                            }
                        } else {
                            rvTodaydeliveryList.setVisibility(View.GONE);
                            tvNoRecord.setVisibility(View.VISIBLE);
                        }
                    }
                } else {
                }
            }

            @Override
            public void onFailure(Call<APIListResponse<JobCardDetailsRealmDBModel>> call, Throwable t) {
                mProgressBarHandler.hide();
                if (getApplicationContext() != null) {
                    String failure = t.getMessage().toString();
                    Log.d("onFailure Response", "" + failure);
                    if (BuildConfig.FLAVOR.equals("dev")) {
                        Utils.showToast(getApplicationContext(), getResources().getString(R.string.msg_servererror)+"--"+failure);
                    }else {
                        Utils.showToast(getApplicationContext(), getResources().getString(R.string.msg_servererror));
                    }

                }
            }
        });
    }

    private void initView() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbarTitle = findViewById(R.id.tv_toolbarTitle);
        tvFromDate = findViewById(R.id.tv_fromdate);
        tvToDate = findViewById(R.id.tv_todate);
        tvNoRecord = findViewById(R.id.tv_norecord);
        btnFilter = findViewById(R.id.btn_filter);
        rvTodaydeliveryList = findViewById(R.id.rv_todaydeliverylist);
        hideToolbarAppName();
        showToolBarBackBtn();
        onHandleUI();
        setListeners();
    }

    public void getDataFromDB() {
        String mFromDate = tvFromDate.getText().toString().trim();
        String mToDate = tvToDate.getText().toString().trim();
        Date mFrom = stringToDate(mFromDate);
        Date mTo = stringToDate(mToDate);

        if (!mFromDate.equals("") || !mFromDate.equals(null) || !mToDate.equals("") || !mToDate.equals(null)) {
            ReturnRealmModel realmmodel = realmDBHelper.getJobCardReport(mRealm, mFrom, mTo);
            if (realmmodel != null) {
                if (realmmodel.getJobCard() != null || realmmodel.getJobCard().size() > 0) {
                    JobCardDetailsRealmDBModel mJobCardList;
                    JobCardItemsDetailsModel mJobCardItemsList;
                    todayJobCard.clear();
                    todayJobCardList = getRealmToCardList(realmmodel);
                    itemsDetailsList = getRealmListToList(realmmodel);
                    if (todayJobCardList != null || todayJobCardList.size() > 0) {
                        for (int i = 0; i < todayJobCardList.size(); i++) {
                            mJobCardList = todayJobCardList.get(i);
                            Utils.showLogD("1009--" + todayJobCardList.size() + "--" + mJobCardList.getId() + "--" + mJobCardList.getName() + "--" + mJobCardList.getCustomer_code());
                            if (realmmodel.getJobCardItems().size() > 0) {
                                for (int j = 0; j < itemsDetailsList.size(); j++) {
                                    mJobCardItemsList = itemsDetailsList.get(j);
                                    Utils.showLogD("1009--" + itemsDetailsList.size() + "--" + mJobCardItemsList.getJob_card_id() + "--" + mJobCardItemsList.getName() + "--" + mJobCardItemsList.getId());
                                    if (mJobCardList.getId().equals(mJobCardItemsList.getJob_card_id())) {
                                        mJobCardItemsList.setId(mJobCardItemsList.getService_id());
                                        mJobCardItemsList.setName(mJobCardItemsList.getName());
                                        mJobCardItemsList.setOther_name(mJobCardItemsList.getOther_name());
                                        mJobCardItemsList.setPrice(mJobCardItemsList.getPrice());
                                        mJobCardItemsList.setJob_card_id(mJobCardItemsList.getJob_card_id());
                                        cardItemsList.add(mJobCardItemsList);
                                    }
                                }
                                if (cardItemsList.size() > 0) {
                                    String items = new Gson().toJson(cardItemsList);
                                    mJobCardList.setItems(items);
                                }
                                todayJobCard.add(mJobCardList);
                            }
                        }
                        if (todayJobCard != null) {
                            if (todayJobCard.size() > 0) {
                                rvTodaydeliveryList.setVisibility(View.VISIBLE);
                                tvNoRecord.setVisibility(View.GONE);
                                onSetRecyclerView();
                            } else {
                                rvTodaydeliveryList.setVisibility(View.GONE);
                                tvNoRecord.setVisibility(View.VISIBLE);
                            }
                        } else {
                            rvTodaydeliveryList.setVisibility(View.GONE);
                            tvNoRecord.setVisibility(View.VISIBLE);
                        }
                    }
                } else {
                    rvTodaydeliveryList.setVisibility(View.GONE);
                    tvNoRecord.setVisibility(View.VISIBLE);
                }
            } else {
                rvTodaydeliveryList.setVisibility(View.GONE);
                tvNoRecord.setVisibility(View.VISIBLE);
            }
        } else {
            Utils.showToast(getApplicationContext(), getResources().getString(R.string.text_selectdate));
        }
    }

    private void onSetRecyclerView() {
        mLayoutManager = new LinearLayoutManager(ReportsActivity.this);
        todayDeliveryAdapter = new TodayDeliveryJobCardAdapter(getApplicationContext(), todayJobCard);
        rvTodaydeliveryList.setAdapter(todayDeliveryAdapter);
        rvTodaydeliveryList.setLayoutManager(mLayoutManager);
        rvTodaydeliveryList.setItemAnimator(new DefaultItemAnimator());
        todayDeliveryAdapter.setOnItemClickListener(this);
        todayDeliveryAdapter.setCallback(this);
    }

    public void setListeners() {
        tvFromDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fromDatePicker(tvFromDate);
            }
        });
        tvToDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toDatePicker(tvToDate);
            }
        });
        btnFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (liveOrLocal) {
                    String from = tvFromDate.getText().toString().trim();
                    String to = tvToDate.getText().toString().trim();
                    String[] fromArrayString = from.split("-");
                    String[] toArrayString = to.split("-");

                    String str0 = fromArrayString[0];
                    String str1 = fromArrayString[1];
                    String str2 = fromArrayString[2];
                    String str3 = toArrayString[0];
                    String str4 = toArrayString[1];
                    String str5 = toArrayString[2];
                    String dateFrom = str2+"-"+str1+"-"+str0;
                    String dateTo = str5+"-"+str4+"-"+str3;
                    getTodayDelivery(dateFrom, dateTo);
                }else {
                    getDataFromDB();
                }
            }
        });
    }

    private void fromDatePicker(final TextView txtDate) {
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        getFormattedDate(year, monthOfYear, dayOfMonth, txtDate);
                        fromDate = getFormattedDateReverse(year, monthOfYear, dayOfMonth, txtDate);
//                        txtDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation_upDown;
        datePickerDialog.show();
    }

    public void toDatePicker(final TextView txtDate) {
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        getFormattedDate(year, monthOfYear, dayOfMonth, txtDate);
                        toDate = getFormattedDateReverse(year, monthOfYear, dayOfMonth, txtDate);
//                        txtDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation_upDown;
        datePickerDialog.show();
    }

    public void showToolBarBackBtn() {
        hideToolbarAppName();
        if (getSupportActionBar() != null) {
            toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_action_back_btn));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    private void onHandleUI() {
        toolbarTitle.setText(getResources().getString(R.string.app_reportpage));
        Calendar c = Calendar.getInstance();
        SimpleDateFormat cd = new SimpleDateFormat("dd-MM-yyyy");
        String currentDate = cd.format(c.getTime());
        tvFromDate.setText(currentDate);
        tvToDate.setText(currentDate);
        String from = tvFromDate.getText().toString().trim();
        String to = tvToDate.getText().toString().trim();
        if (liveOrLocal) {
            tvFromDate.setText(currentDate);
            tvToDate.setText(currentDate);
            callLiveOrLocalData(from, to);
        }else{
            tvFromDate.setText(currentDate);
            tvToDate.setText(currentDate);
            getDataFromDB();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }
//        else if (id == R.id.action_settings) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK
                && event.getRepeatCount() == 0) {
            onBackPressed();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onBackPressed() {
        closeAlert(ReportsActivity.this, getResources().getString(R.string.app_reportpage));
    }

    @Override
    public void onItemClick(View view, int position) {

    }

    @Override
    public void onItemClickWithId(View view, int position, Integer id) {
//        showPreviewDialog(position, id);
        Intent i = new Intent(ReportsActivity.this, JobCardUpdatePreviewActivity.class);
        i.putExtra("JobCardUniqueId", todayJobCard.get(position).getLocaldbid());
        i.putExtra("NextId", String.valueOf(id));// jobCardId
        i.putExtra("viewOnly", "0");// can't update
        startActivity(i);
    }

    public void showPreviewDialog(final int position, final Integer jobCardId) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ReportsActivity.this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.activity_job_card_preview, null);
        dialogBuilder.setView(dialogView);
        alertDialog = dialogBuilder.create();
        alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation_upDown;
        alertDialog.setCancelable(false);
        initPreviewView(dialogView);
        JobCardDetailsRealmDBModel selectedCardDetail = todayJobCard.get(position);
        onHandlePreviewUI(todayJobCard.get(position), jobCardId);
        tvPrevHeaderTitle.setText(getResources().getString(R.string.text_detailreport));
        imgPrevHeaderClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (alertDialog != null && alertDialog.isShowing()) alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    private void onHandlePreviewUI(JobCardDetailsRealmDBModel selectedCardDetail, Integer jobCardId) {
        tvPrevCustomerCode.setText("" + selectedCardDetail.getCustomer_code());
        tvPrevVatRegisterNo.setText("" + selectedCardDetail.getVat_reg_no());
        tvPrevName.setText("" + selectedCardDetail.getName());
        tvPrevPostBoxNo.setText("" + selectedCardDetail.getPo_box());
        tvPrevCity.setText("" + selectedCardDetail.getCity());
        tvPrevTelephoneNo.setText("" + selectedCardDetail.getTel_no());
        tvPrevMobileNo.setText("" + selectedCardDetail.getMobile_no());
        tvPrevFaxNo.setText("" + selectedCardDetail.getFax_no());
        tvPrevEmailId.setText("" + selectedCardDetail.getEmail());
        tvPrevCurrentDate.setText("" + selectedCardDetail.getAdvisor_date());
        tvPrevCurrentTime.setText("" + selectedCardDetail.getAdvisor_time());
        tvPrevVehicleRegisterNo.setText("" + selectedCardDetail.getReg_no());
        tvPrevVehicleModel.setText("" + selectedCardDetail.getModel());
        tvPrevVehicleChasisNo.setText("" + selectedCardDetail.getChassis_no());
        tvPrevVehicleColors.setText("" + selectedCardDetail.getColours());
        tvPrevVehicleFuel.setText("" + selectedCardDetail.getFuel());
        tvPrevVehicleOil.setText("" + selectedCardDetail.getOil());
        tvPrevVehicleKilometer.setText("" + selectedCardDetail.getKilometer());
        tvPrevDeliveryDate.setText("" + selectedCardDetail.getDelivery_date());
        String json = selectedCardDetail.getItems();

        List<JobCardItemsDetailsModel> modelList = new Gson().fromJson(json, new TypeToken<List<JobCardItemsDetailsModel>>() {
        }.getType());
        if (modelList != null || modelList.size() > 0) {
            addItems(modelList, jobCardId);
            for (int i = 0; i < modelList.size(); i++) {
                Utils.showLogD("1010--" + modelList.size() + "--" + modelList.get(i).getJob_card_id() + "--" + modelList.get(i).getName());
            }
        }
        tvPrevTotalAmount.setText("" + selectedCardDetail.getTotal_amount() +" "+ getResources().getString(R.string.text_currency));
        rbPrevServiceNightJob.setText("" + selectedCardDetail.getJob_type());
    }

    private void addItems(List<JobCardItemsDetailsModel> modelList, Integer jobCardId) {
        layout_items.removeAllViews();
        child = new View[modelList.size()];
        inflater1 = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        for (int i = 0; i < modelList.size(); i++) {
//            if (jobCardId == modelList.get(i).getJob_card_id()) {
                JobCardItemsDetailsModel model = modelList.get(i);
                child[i] = inflater1.inflate(R.layout.add_item_row, null);
                tvTitle = child[i].findViewById(R.id.tvTitle);
                tvPrice = child[i].findViewById(R.id.tvPrice);
                tvTitle.setText(String.valueOf(model.getName()));
                tvPrice.setText(String.valueOf(model.getPrice()));
                layout_items.addView(child[i]);
//            }
        }
    }

    private void initPreviewView(View dialogView) {
        prevToolbar = dialogView.findViewById(R.id.toolbar);
        prevToolbar.setVisibility(View.GONE);
        tvPrevToolbarTitle = dialogView.findViewById(R.id.tv_toolbarTitle);
        tvPrevToolbarTitle.setVisibility(View.GONE);
        rlHeader = dialogView.findViewById(R.id.rl_header);
        rlHeader.setVisibility(View.VISIBLE);
        tvPrevHeaderTitle = dialogView.findViewById(R.id.tv_header_title);
        imgPrevHeaderClose = dialogView.findViewById(R.id.img_header_close);
        imgPrevHeaderRight = dialogView.findViewById(R.id.img_right);
        imgPrevHeaderRight.setVisibility(View.GONE);
        imgPrevHeaderLeft = dialogView.findViewById(R.id.img_left);
        imgPrevHeaderLeft.setVisibility(View.GONE);
        tvPrevCustomerCode = dialogView.findViewById(R.id.tv_customercode);
        tvPrevVatRegisterNo = dialogView.findViewById(R.id.tv_vatregno);
        tvPrevName = dialogView.findViewById(R.id.tv_name);
        tvPrevPostBoxNo = dialogView.findViewById(R.id.tv_postboxno);
        tvPrevCity = dialogView.findViewById(R.id.tv_city);
        tvPrevTelephoneNo = dialogView.findViewById(R.id.tv_telephoneno);
        tvPrevMobileNo = dialogView.findViewById(R.id.tv_mobileno);
        tvPrevFaxNo = dialogView.findViewById(R.id.tv_faxno);
        tvPrevEmailId = dialogView.findViewById(R.id.tv_email);
        llInflateLayout = dialogView.findViewById(R.id.ll_inflatelayout);
        llInflateLayout.setVisibility(View.VISIBLE);
        tvPrevCurrentDate = dialogView.findViewById(R.id.tv_currentdate);
        tvPrevCurrentTime = dialogView.findViewById(R.id.tv_currenttime);
        tvPrevVehicleRegisterNo = dialogView.findViewById(R.id.tv_regno);
        tvPrevVehicleModel = dialogView.findViewById(R.id.tv_model);
        tvPrevVehicleChasisNo = dialogView.findViewById(R.id.tv_chasisno);
        tvPrevVehicleColors = dialogView.findViewById(R.id.tv_color);
        tvPrevVehicleFuel = dialogView.findViewById(R.id.tv_fuel);
        tvPrevVehicleOil = dialogView.findViewById(R.id.tv_oil);
        tvPrevVehicleKilometer = dialogView.findViewById(R.id.tv_kilometer);
        tvPrevDeliveryDate = dialogView.findViewById(R.id.tv_deliverydate);
        jobPrevTypeRecyclerview = dialogView.findViewById(R.id.rv_jobtypeitems);
        tvPrevTxtJobType = dialogView.findViewById(R.id.tv_txtjobtype);
        tvPrevTxtJobType.setVisibility(View.GONE);
        jobPrevTypeRecyclerview.setVisibility(View.GONE);
        layout_items = dialogView.findViewById(R.id.layout_items);
        layout_items.setVisibility(View.VISIBLE);
        tvPrevTotalAmount = dialogView.findViewById(R.id.tv_totalamt);
        rgPrevServiceJobType = dialogView.findViewById(R.id.rg_daynightgroup);
        rbPrevServiceNightJob = dialogView.findViewById(R.id.rb_nightjob);
        btnPrevEdit = dialogView.findViewById(R.id.btn_editjobcard);
        btnPrevEdit.setVisibility(View.GONE);
        btnPrevFinish = dialogView.findViewById(R.id.btn_sendjobcard);
        btnPrevFinish.setVisibility(View.GONE);
    }
}
