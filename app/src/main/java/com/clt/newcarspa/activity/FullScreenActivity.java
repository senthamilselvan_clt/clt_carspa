package com.clt.newcarspa.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.clt.newcarspa.Adapter.AddedImgAudioListAdapter;
import com.clt.newcarspa.BuildConfig;
import com.clt.newcarspa.R;
import com.clt.newcarspa.Utils.Constant;
import com.clt.newcarspa.Utils.ProgressBarHandler;
import com.clt.newcarspa.Utils.TouchImageView;
import com.clt.newcarspa.Utils.Utils;
import com.clt.newcarspa.cltinterfaces.RestAdapter;
import com.clt.newcarspa.database.RealmDBHelper;
import com.clt.newcarspa.model.APIListResponse;
import com.clt.newcarspa.model.APIResponse;
import com.clt.newcarspa.model.AudioUploadModel;
import com.clt.newcarspa.model.ImageUploadModel;
import com.clt.newcarspa.model.JobCardDetailsRealmDBModel;
import com.clt.newcarspa.model.ReturnRealmModel;
import com.clt.newcarspa.model.UserLoginDetails;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class FullScreenActivity extends BaseActivity implements AddedImgAudioListAdapter.ButtonAdapterCallback {

    private Realm mRealm;
    private RealmDBHelper realmDBHelper;
    private ProgressBarHandler mProgressBarHandler;
    private TouchImageView img_zoom;
    private String jobId, imgUniqueId;
    private List<AudioUploadModel> audioList;
    private RecyclerView rvAddedAudios;
    private LinearLayoutManager mLayoutManager;
    private AddedImgAudioListAdapter addImageAudioAdapter;
    private String nextId;
    private boolean liveOrLocal = false;
    private List<UserLoginDetails> userLoginDetails = new ArrayList<>();
    private String loginUserId, loginUserName, loginShopId;
    private List<ImageUploadModel> jobCardImageList;
    private String liveEncodedString = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_screen);
        checkOnline();
        mRealm = Realm.getDefaultInstance();
        realmDBHelper = new RealmDBHelper(FullScreenActivity.this);
        getUserLoginDetailsFromDB();
        mProgressBarHandler = new ProgressBarHandler(this);
        initView();
        getBundle();
    }


    private void checkOnline() {
        if (Utils.getInstance(FullScreenActivity.this).isOnline()) {
            liveOrLocal = true;
        } else {
            liveOrLocal = false;
        }
    }

    private void initView() {
        img_zoom = (TouchImageView) findViewById(R.id.img_zoom);
        rvAddedAudios = findViewById(R.id.rv_addedaudios);
    }

    private void getBundle() {
        Bundle bun_data = getIntent().getExtras();
        if (bun_data != null) {
            nextId = bun_data.getString("nextId");
            String encodedStr = bun_data.getString("ImageUrl");
            jobId = bun_data.getString(Constant.IMGID);
            imgUniqueId = bun_data.getString(Constant.IMGUNIQUEID);
            if (liveOrLocal) {
                List<JobCardDetailsRealmDBModel> list = realmDBHelper.isJobCardInLocal(mRealm, Integer.parseInt(nextId));
                if (list != null && list.size() > 0) {
                    onHandleUI(encodedStr);
                }else{
                    callJobCardIdDetailsService();
                }
            } else {
                onHandleUI(encodedStr);
            }
        }
    }

    private void callJobCardIdDetailsService() {
        mProgressBarHandler.show();
        JobCardDetailsRealmDBModel model1 = new JobCardDetailsRealmDBModel();
        model1.setAction("get_job_card_list_byid");
        model1.setUser_id(Integer.valueOf(loginUserId));
        model1.setShop_id(Integer.valueOf(loginShopId));
        model1.setId(Integer.parseInt(nextId));
        Call<APIListResponse<JobCardDetailsRealmDBModel>> call = RestAdapter.getInstance().getServiceApi().getNewJobCardIdDetails(model1);
        call.enqueue(new Callback<APIListResponse<JobCardDetailsRealmDBModel>>() {
            @Override
            public void onResponse(@NonNull Call<APIListResponse<JobCardDetailsRealmDBModel>> call,
                                   @NonNull Response<APIListResponse<JobCardDetailsRealmDBModel>> response) {
                mProgressBarHandler.hide();
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        List<JobCardDetailsRealmDBModel> result = response.body().getResult();
                        if (result != null && result.size() > 0) {
                            jobCardIdDetailSuccessResponse(result);
                        }
                    }
                } else {
                }
            }

            @Override
            public void onFailure(Call<APIListResponse<JobCardDetailsRealmDBModel>> call, Throwable t) {
                mProgressBarHandler.hide();
                if (getApplicationContext() != null) {
                    String failure = t.getMessage().toString();
                    Log.d("onFailure Response", "" + failure);
                    if (BuildConfig.FLAVOR.equals("dev")) {
                        Utils.showToast(getApplicationContext(), getResources().getString(R.string.msg_servererror) + "--" + failure);
                    } else {
                        Utils.showToast(getApplicationContext(), getResources().getString(R.string.msg_servererror));
//                        saveAndCloseAlert(getApplicationContext(), getResources().getString(R.string.msg_servererror));
                    }

                }
            }
        });
    }

    private void jobCardIdDetailSuccessResponse(List<JobCardDetailsRealmDBModel> result) {
        jobCardImageList = new ArrayList<>();
        audioList = new ArrayList<>();
        List<AudioUploadModel> jobCardAudioListOne = new ArrayList<>();
        jobCardImageList = result.get(0).getImages() != null ? result.get(0).getImages() : new ArrayList<>();
        jobCardAudioListOne = result.get(0).getAudios() != null ? result.get(0).getAudios() : new ArrayList<>();
        if (jobCardImageList != null && jobCardImageList.size() > 0) {
            for (int i = 0; i < jobCardImageList.size(); i++) {
                if (jobCardImageList.get(i).getImage_uniqueid().equals(imgUniqueId)) {
                    liveEncodedString = jobCardImageList.get(i).getImage_encode_str();
                }
            }
            if (!liveEncodedString.equals("")) {
                decodeBase64AndSetImage(liveEncodedString, img_zoom);
            }
        }
        if (jobCardAudioListOne != null && jobCardAudioListOne.size() > 0) {
            for (int i = 0; i < jobCardAudioListOne.size(); i++) {
                if (jobCardAudioListOne.get(i).getImage_uniqueid().equals(imgUniqueId)) {
                    AudioUploadModel audio = new AudioUploadModel();
                    audio.setId(jobCardAudioListOne.get(i).getId());
                    audio.setUser_id(jobCardAudioListOne.get(i).getUser_id());
                    audio.setShop_id(jobCardAudioListOne.get(i).getShop_id());
                    audio.setAudio_uniqueid(jobCardAudioListOne.get(i).getAudio_uniqueid());
                    audio.setImage_uniqueid(jobCardAudioListOne.get(i).getImage_uniqueid());
                    audio.setImage_uniqueid(jobCardAudioListOne.get(i).getImage_uniqueid());
                    audio.setAudio_encode_str(jobCardAudioListOne.get(i).getAudio_encode_str());
                    audio.setAudio_status(jobCardAudioListOne.get(i).getAudio_status());
                    audio.setAudio_filepath(jobCardAudioListOne.get(i).getAudio_filepath());
                    audio.setAudio_filename(jobCardAudioListOne.get(i).getAudio_filename());
                    audio.setLocal_insert(jobCardAudioListOne.get(i).getLocal_insert());
                    audio.setSync_status(jobCardAudioListOne.get(i).getSync_status());
                    audio.setLocaldbid(jobCardAudioListOne.get(i).getLocaldbid());
                    audioList.add(audio);
                }
            }
            if (audioList != null && audioList.size() > 0) {
                rvAddedAudios.setVisibility(View.VISIBLE);
                setUpAudioRecyclerView();
            }else{
                rvAddedAudios.setVisibility(View.GONE);
            }
        }
    }

    private void onHandleUI(String encodedStr) {
        decodeBase64AndSetImage(encodedStr, img_zoom);
        getAudiosFromDB();
    }

    private void getAudiosFromDB() {
        if (jobId != null && imgUniqueId != null) {
            audioList = new ArrayList<>();
            audioList = realmDBHelper.getAudiosFromSelectedIdAndImgUniqueId(mRealm, Integer.parseInt(jobId), imgUniqueId);
            if (audioList != null && audioList.size() > 0) {
                rvAddedAudios.setVisibility(View.VISIBLE);
                setUpAudioRecyclerView();
            }
        }
    }

    private void setUpAudioRecyclerView() {
        mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        addImageAudioAdapter = new AddedImgAudioListAdapter(getApplicationContext(), audioList);
        rvAddedAudios.setAdapter(addImageAudioAdapter);
        rvAddedAudios.setLayoutManager(mLayoutManager);
        rvAddedAudios.setItemAnimator(new DefaultItemAnimator());
//        addImageAdapter.setOnItemClickListener(this);
        addImageAudioAdapter.setCallback(this);
        addImageAudioAdapter.notifyDataSetChanged();
    }

    private void decodeBase64AndSetImage(String completeImageData, ImageView imageView) {
        // Incase you're storing into aws or other places where we have extension stored in the starting.
        String imageDataBytes = completeImageData.substring(completeImageData.indexOf(",") + 1);
        InputStream stream = new ByteArrayInputStream(Base64.decode(imageDataBytes.getBytes(), Base64.DEFAULT));
        Bitmap bitmap = BitmapFactory.decodeStream(stream);
        imageView.setImageBitmap(bitmap);
    }

    private void setTitleActionbar() {

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Images");

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            overridePendingTransitionEnter();
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private boolean checkStoragePermission() {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    @Override
    public void audiodelete(int position, String audName, String imageUniqueId, Integer audioUniqueId) {
        if (liveOrLocal) {
            List<JobCardDetailsRealmDBModel> list = realmDBHelper.isJobCardInLocal(mRealm, Integer.parseInt(nextId));
            if (list != null && list.size() > 0) {
                offLineAudioDelete(position);
            } else {
                AudioUploadModel model1 = new AudioUploadModel();
                model1.setAction("delete_jobcard_audio");
                model1.setUser_id(Integer.valueOf(loginUserId));
                model1.setShop_id(Integer.valueOf(loginShopId));
                model1.setId(Integer.parseInt(nextId));
                model1.setImage_uniqueid(imageUniqueId);
                model1.setAudio_uniqueid(audioUniqueId);
                callAudioDeleteService(model1);
            }
        }else{
            offLineAudioDelete(position);
        }
    }

    private void offLineAudioDelete(Integer position){
        mProgressBarHandler.show();
        ReturnRealmModel realmModel = realmDBHelper.deleteAudioFromSelectedIdAndImgUniqueId(mRealm, Integer.parseInt(jobId), imgUniqueId, position);
        if (realmModel.getAudiodeletedStatus() == 1) {
            mProgressBarHandler.hide();
            if (realmModel.getAfterDeleteImgAudSize() > 0) {
                getAudiosFromDB();
            } else {
                rvAddedAudios.setVisibility(View.GONE);
            }
        } else {
            mProgressBarHandler.hide();
        }
    }

    @Override
    public void audioItem(int position, String encodestring, String audioName, String filePath) {
        showMediaPlayer(encodestring, audioName, filePath);
    }

    /*----------Audio play-------------*/
    public void showMediaPlayer(final String encodeString, final String audioName, final String filePath) {
        final String mediafile = encodeString + audioName;
        android.app.AlertDialog.Builder dialogBuilder = new android.app.AlertDialog.Builder(FullScreenActivity.this);
        View dialogView = getLayoutInflater().inflate(R.layout.livechat_mediaplayer, null);
        dialogBuilder.setView(dialogView);
        final android.app.AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation_upDown;
        alertDialog.setCancelable(true);
        /*alertDialog.setTitle(R.string.app_name);
        alertDialog.setIcon(R.drawable.ic_action_speaker);*/
        final MediaPlayer mPlayer = new MediaPlayer();
        final TextView tv_audioname = dialogView.findViewById(R.id.tv_audioname);
        final Button playbtn = dialogView.findViewById(R.id.play);
        final Button pausebtn = dialogView.findViewById(R.id.pause);
        final Button stopbtn = dialogView.findViewById(R.id.stop);
        final Button close = dialogView.findViewById(R.id.close);
        tv_audioname.setText(audioName);
        playbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleActionPlayBack(mPlayer, encodeString, audioName, filePath, playbtn, pausebtn, alertDialog);
            }
        });
        pausebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPlayer.isPlaying()) {
                    mPlayer.pause();
                    pausebtn.setBackgroundResource(R.drawable.ic_action_play);
                } else {
                    mPlayer.start();
                    pausebtn.setBackgroundResource(R.drawable.ic_action_pause);
                }
                //handleActionPlayBack(mPlayer, mediafile, playbtn, pausebtn, alertDialog);
            }
        });
        stopbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleActionMediaStop(mPlayer, alertDialog);
            }
        });
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleActionMediaStop(mPlayer, alertDialog);
            }
        });
        alertDialog.show();
    }

    private void handleActionMediaStop(MediaPlayer mPlayer, android.app.AlertDialog alertDialog) {
        if (mPlayer.isPlaying()) {
            mPlayer.stop();
        }
        alertDialog.dismiss();
    }

    private void handleActionPlayBack(MediaPlayer mPlayer, String encodeString, String audioname, String filePath, final Button playbtn, final Button pausebtn, final android.app.AlertDialog alertDialog) {
        mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        try {
            FileOutputStream out = new FileOutputStream(new File(filePath));
            byte[] decoded = Base64.decode(encodeString, 0);

            out.write(decoded);
            out.close();
            mPlayer.setDataSource(filePath);
            mPlayer.prepare();
        } catch (Exception e) {
//            Toast.makeText(getApplicationContext(), "You might not set the URI correctly!", Toast.LENGTH_LONG).show();
        }
        mPlayer.start();
        pausebtn.setVisibility(View.VISIBLE);
        playbtn.setVisibility(View.GONE);
        mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {
//                alertDialog.dismiss();
                pausebtn.setVisibility(View.GONE);
                playbtn.setBackgroundResource(R.drawable.ic_action_play);
                playbtn.setVisibility(View.VISIBLE);
            }
        });
    }

    /*----------Audio play-------------*/

    private void goToLoginPage() {
        startActivity(new Intent(FullScreenActivity.this, LoginActivity.class));
        finish();
    }

    private void getUserLoginDetailsFromDB() {
        userLoginDetails = realmDBHelper.getUserLoginDetail(mRealm);
        if (userLoginDetails != null || userLoginDetails.size() > 0) {
            try { //login data come db only normally
                loginUserId = userLoginDetails.get(0).getId();
                loginUserName = userLoginDetails.get(0).getUser_name();
                loginShopId = userLoginDetails.get(0).getShop_id();
            } catch (ArrayIndexOutOfBoundsException e) {
                e.printStackTrace();
                showAlertDialog(getResources().getString(R.string.msg_sessionexpire));
                goToLoginPage();
            } catch (Exception e) {
                e.printStackTrace();
                showAlertDialog(getResources().getString(R.string.msg_sessionexpire));
                goToLoginPage();
            }
        } else {
            try {
                if (Utils.getFromUserDefaults(getApplicationContext(), "LoginId") != null) {
                    loginUserId = Utils.getFromUserDefaults(getApplicationContext(), "LoginId");
                    loginUserName = Utils.getFromUserDefaults(getApplicationContext(), "LoginUserName");
                    loginShopId = Utils.getFromUserDefaults(getApplicationContext(), "LoginShopId");
                }
            } catch (ArrayIndexOutOfBoundsException e) {
                e.printStackTrace();
                showAlertDialog(getResources().getString(R.string.msg_sessionexpire));
                goToLoginPage();
            } catch (Exception e) {
                e.printStackTrace();
                showAlertDialog(getResources().getString(R.string.msg_sessionexpire));
                goToLoginPage();
            }
        }
    }

    private void callAudioDeleteService(AudioUploadModel model1) {
        mProgressBarHandler.show();
        Call<APIResponse<AudioUploadModel>> call = RestAdapter.getInstance().getServiceApi().uploadaudioToServer(model1);
        call.enqueue(new Callback<APIResponse<AudioUploadModel>>() {
            @Override
            public void onResponse(@NonNull Call<APIResponse<AudioUploadModel>> call,
                                   @NonNull Response<APIResponse<AudioUploadModel>> response) {
                mProgressBarHandler.hide();
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        Utils.showToast(getApplicationContext(), "Deleted successfully");
                        callJobCardIdDetailsService();
                    }
                } else {
                    Utils.showToast(getApplicationContext(), "" + response.code() + " - " + response.message());
                }
            }

            @Override
            public void onFailure(Call<APIResponse<AudioUploadModel>> call, Throwable t) {
                mProgressBarHandler.hide();
                if (getApplicationContext() != null) {
                    String failure = t.getMessage().toString();
                    Log.d("onFailure Response", "" + failure);
                    if (BuildConfig.FLAVOR.equals("dev")) {
                        Utils.showToast(getApplicationContext(), getResources().getString(R.string.msg_servererror) + "--" + failure);
                    } else {
                        Utils.showToast(getApplicationContext(), getResources().getString(R.string.msg_servererror));
//                        saveAndCloseAlert(getApplicationContext(), getResources().getString(R.string.msg_servererror));
                    }

                }
            }
        });
    }
}
