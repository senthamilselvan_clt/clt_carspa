package com.clt.newcarspa.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.TextView;

import com.clt.newcarspa.BuildConfig;
import com.clt.newcarspa.Fragment.JobCardListViewFragment;
import com.clt.newcarspa.R;
import com.clt.newcarspa.Utils.Constant;
import com.clt.newcarspa.Utils.ProgressBarHandler;
import com.clt.newcarspa.Utils.Utils;
import com.clt.newcarspa.cltinterfaces.RecyclerviewListener;
import com.clt.newcarspa.cltinterfaces.RestAdapter;
import com.clt.newcarspa.database.RealmDBHelper;
import com.clt.newcarspa.model.APIListResponse;
import com.clt.newcarspa.model.JobCardDetailsRealmDBModel;
import com.clt.newcarspa.model.JobCardItemsDetailsModel;
import com.clt.newcarspa.model.ReturnRealmModel;
import com.clt.newcarspa.model.UserLoginDetails;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewJobCardListActivity extends BaseActivity implements SearchView.OnQueryTextListener, RecyclerviewListener {

    private Realm mRealm;
    private Toolbar toolbar;
    private TextView toolbarTitle;
    private ViewPagerAdapter adapter;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private List<String> get_status;
    public String search;
    private List<UserLoginDetails> userLoginDetails = new ArrayList<>();
    private List<JobCardDetailsRealmDBModel> selectJobList, selectJobListCopy;
    private RealmDBHelper realmDBHelper;
    private String loginUserId, loginUserName, loginShopId;
    private ProgressBarHandler mProgressBarHandler;
    private boolean liveOrLocal = false;
    private List<JobCardDetailsRealmDBModel> pushCardList = new ArrayList<>();
    private List<JobCardItemsDetailsModel> pushCardItemsList = new ArrayList<>();
    private List<JobCardDetailsRealmDBModel> CardLists = new ArrayList<>();
    private List<JobCardItemsDetailsModel> itemsDetailsList = new ArrayList<>();

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mRealm.close();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_job_card_list);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        mRealm = Realm.getDefaultInstance();
        realmDBHelper = new RealmDBHelper(ViewJobCardListActivity.this);
        mProgressBarHandler = new ProgressBarHandler(this);
        getUserLoginDetailsFromDB();
        initView();
        onCallServiceJobCard();
    }

    private void onCallServiceJobCard() {
        if (Utils.getInstance(getApplicationContext()).isOnline()) {
            liveOrLocal = true;
            callListJobCardService();
        } else {
            getDataFromDB();
        }
    }

    private void getDataFromDB() {
        ReturnRealmModel realmmodel = realmDBHelper.getListingAllJobCard(mRealm);
        if (realmmodel != null) {
            if (realmmodel.getJobCard() != null || realmmodel.getJobCard().size() > 0) {
                JobCardDetailsRealmDBModel mJobCardList;
                JobCardItemsDetailsModel mJobCardItemsList;

                CardLists = getRealmToCardList(realmmodel);
                itemsDetailsList = getRealmListToList(realmmodel);

                for (int i = 0; i < CardLists.size(); i++) {
                    mJobCardList = CardLists.get(i);
                    Utils.showLogD("1009--"+CardLists.size()+"--"+mJobCardList.getId()+"--"+mJobCardList.getName()+"--"+mJobCardList.getCustomer_code());
                    if (realmmodel.getJobCardItems().size() > 0) {
                        for (int j = 0; j < itemsDetailsList.size(); j++) {
                            mJobCardItemsList = itemsDetailsList.get(j);
                            Utils.showLogD("1009--"+itemsDetailsList.size()+"--"+mJobCardItemsList.getJob_card_id()+"--"+mJobCardItemsList.getName()+"--"+mJobCardItemsList.getId());
                            if (mJobCardList.getId().equals(mJobCardItemsList.getJob_card_id())) {
                                mJobCardItemsList.setId(mJobCardItemsList.getService_id());
                                mJobCardItemsList.setName(mJobCardItemsList.getName());
                                mJobCardItemsList.setOther_name(mJobCardItemsList.getOther_name());
                                mJobCardItemsList.setPrice(mJobCardItemsList.getPrice());
                                mJobCardItemsList.setJob_card_id(mJobCardItemsList.getJob_card_id());
                                pushCardItemsList.add(mJobCardItemsList);
                            }
                        }
                        if (pushCardItemsList.size() > 0) {
                            String items = new Gson().toJson(pushCardItemsList);
                            mJobCardList.setItems(items);
                        }
                        pushCardList.add(mJobCardList);
                    }
                }
                if (pushCardList != null || pushCardList.size() > 0) {
                    for (int i = 0; i < pushCardList.size(); i++) {
                        JobCardDetailsRealmDBModel psList = pushCardList.get(i);
                        Utils.showLogD("1008--"+psList.getId()+"--"+psList.getName()+"--"+psList.getCustomer_code());
                    }
                }
            }
            setUpViewPager();
        }
    }

    public void onRecyclerViewListClear(){
        /*pushCardList.clear();
        pushCardItemsList.clear();
        CardLists.clear();
        itemsDetailsList.clear();
        getDataFromDB();*/
    }

    public void showToolBarBackBtn() {
        hideToolbarAppName();
        if (getSupportActionBar() != null) {
            toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_action_back_btn));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    public void hideToolbarAppName() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);//for hiding the app name
        }
    }

    private void callListJobCardService() {
        mProgressBarHandler.show();
        JobCardDetailsRealmDBModel model1 = new JobCardDetailsRealmDBModel();
        model1.setAction("get_job_card_list");
        model1.setUser_id(Integer.valueOf(loginUserId));
        model1.setShop_id(Integer.valueOf(loginShopId));
        Call<APIListResponse<JobCardDetailsRealmDBModel>> call = RestAdapter.getInstance().getServiceApi().getJobCardList(model1);
        call.enqueue(new Callback<APIListResponse<JobCardDetailsRealmDBModel>>() {
            @Override
            public void onResponse(@NonNull Call<APIListResponse<JobCardDetailsRealmDBModel>> call,
                                   @NonNull Response<APIListResponse<JobCardDetailsRealmDBModel>> response) {
                mProgressBarHandler.hide();
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        onHandleJobCardListSuccessResponse(response);
                    }
                } else {
                }
            }

            @Override
            public void onFailure(Call<APIListResponse<JobCardDetailsRealmDBModel>> call, Throwable t) {
                mProgressBarHandler.hide();
                if (getApplicationContext() != null) {
                    String failure = t.getMessage().toString();
                    Log.d("onFailure Response", "" + failure);
                    if (BuildConfig.FLAVOR.equals("dev")) {
                        Utils.showToast(getApplicationContext(), getResources().getString(R.string.msg_servererror)+"--"+failure);
                    }else {
                        Utils.showToast(getApplicationContext(), getResources().getString(R.string.msg_servererror));
                    }

                }
            }
        });
    }

    private void onHandleJobCardListSuccessResponse(@NonNull Response<APIListResponse<JobCardDetailsRealmDBModel>> response) {
        String status;
        status = response.body().getStatus();
        if (status.equals(Constant.RESPONSESUCCESS)) {
            selectJobList = new ArrayList<JobCardDetailsRealmDBModel>();
            if (response.body().getResult() != null || response.body().getResult().size() > 0) {
                selectJobList = response.body().getResult();
                setUpViewPager();
            }
        }
    }

    private void initView() {
        toolbar = findViewById(R.id.toolbar);
        toolbarTitle = findViewById(R.id.tv_toolbarTitle);
        viewPager = findViewById(R.id.viewpager);
        tabLayout = findViewById(R.id.tabs);
        setSupportActionBar(toolbar);
        showToolBarBackBtn();
        onHandleUI();
    }

    private void setUpViewPager() {
        String[] arryStr = getApplicationContext().getResources().getStringArray(R.array.service_status);
        get_status = new ArrayList<>();
        for (int i = 0; i < arryStr.length; i++) {
            if (!arryStr[i].equals("Delivered"))
                get_status.add(arryStr[i]);
        }
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void onHandleUI() {
        toolbarTitle.setText(getResources().getString(R.string.app_viewjobcardpage));
    }

    private void getUserLoginDetailsFromDB() {
        userLoginDetails = realmDBHelper.getUserLoginDetail(mRealm);
        if (userLoginDetails != null || userLoginDetails.size() > 0) {
            try {
                loginUserId = userLoginDetails.get(0).getId();
                loginUserName = userLoginDetails.get(0).getUser_name();
                loginShopId = userLoginDetails.get(0).getShop_id();
            } catch (ArrayIndexOutOfBoundsException e) {
                e.printStackTrace();
                Utils.showToast(getApplicationContext(), getResources().getString(R.string.msg_sessionexpire));
                goToLoginPage();
            } catch (Exception e) {
                e.printStackTrace();
                Utils.showToast(getApplicationContext(), getResources().getString(R.string.msg_sessionexpire));
                goToLoginPage();
            }
        } else {
            try {
                if (Utils.getFromUserDefaults(getApplicationContext(), "LoginId") != null) {
                    loginUserId = Utils.getFromUserDefaults(getApplicationContext(), "LoginId");
                    loginUserName = Utils.getFromUserDefaults(getApplicationContext(), "LoginUserName");
                    loginShopId = Utils.getFromUserDefaults(getApplicationContext(), "LoginShopId");
                }
            } catch (ArrayIndexOutOfBoundsException e) {
                e.printStackTrace();
                Utils.showToast(getApplicationContext(), getResources().getString(R.string.msg_sessionexpire));
                goToLoginPage();
            } catch (Exception e) {
                e.printStackTrace();
                Utils.showToast(getApplicationContext(), getResources().getString(R.string.msg_sessionexpire));
                goToLoginPage();
            }
        }
    }

    private void goToLoginPage() {
        startActivity(new Intent(ViewJobCardListActivity.this, LoginActivity.class));
        finish();
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }

    private void setupViewPager(final ViewPager viewPager) {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapter);
    }

    @Override
    public void onRecyclerViewListClear(boolean jobcardstatus) {
        if (jobcardstatus) {
            onRecyclerViewListClear();
        }
    }

    private class ViewPagerAdapter extends FragmentPagerAdapter {

        ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            Log.d("TitelTabGetItem", "" + get_status.get(position));

            selectJobListCopy = new ArrayList<>();
            if (liveOrLocal) {
                for (JobCardDetailsRealmDBModel cartForm : selectJobList) {
                    if (get_status.get(position).equals(cartForm.getStatus())) {
                        selectJobListCopy.add(cartForm);
                    }
                }
            }else{
                for (JobCardDetailsRealmDBModel cartForm : pushCardList) {
                    if (get_status.get(position).equals(cartForm.getStatus())) {
                        selectJobListCopy.add(cartForm);
                    }
                }
            }
            return setFragments(selectJobListCopy);
        }

        @Override
        public int getCount() {
            return get_status == null ? 0 : get_status.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            Log.d("TitelTab", "" + get_status.get(position));
            return get_status.get(position);
        }
    }

    @NonNull
    private JobCardListViewFragment setFragments(List<JobCardDetailsRealmDBModel> selectJobListCopy) {
        Bundle bundle = new Bundle();
        bundle.putBoolean("liveOrLocal", liveOrLocal);
        bundle.putString("list", new Gson().toJson(selectJobListCopy));
        JobCardListViewFragment fragment = new JobCardListViewFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            overridePendingTransitionExit();
        }
// else if (id == R.id.action_settings) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if ( keyCode == KeyEvent.KEYCODE_BACK
                && event.getRepeatCount() == 0) {
            onBackPressed();
            overridePendingTransitionExit();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onBackPressed() {
        closeAlert(ViewJobCardListActivity.this, getResources().getString(R.string.app_viewjobcardpage));
    }
}
