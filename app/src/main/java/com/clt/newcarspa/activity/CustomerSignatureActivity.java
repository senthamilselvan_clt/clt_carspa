package com.clt.newcarspa.activity;

import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.clt.newcarspa.R;
import com.clt.newcarspa.Utils.Utils;
import com.github.gcacace.signaturepad.views.SignaturePad;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class CustomerSignatureActivity extends AppCompatActivity {

    private ImageView imgbackbtn;
    private SignaturePad signaturePad;
    private Button btnSave, btnClear;
    private File f;
    private Bitmap mbitmap;
    private String folderName = "CLT";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_customer_signature);
        initView();
        createFolder(folderName);
        setListeners();
    }

    private void initView() {
        imgbackbtn = findViewById(R.id.img_backbtn);
        signaturePad = findViewById(R.id.signature_pad);
        btnSave = findViewById(R.id.btn_save);
        btnClear = findViewById(R.id.btn_clear);
        btnSave.setEnabled(false);
        btnClear.setEnabled(false);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        signaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {

            }

            @Override
            public void onSigned() {
                btnSave.setEnabled(true);
                btnClear.setEnabled(true);
            }

            @Override
            public void onClear() {
                btnSave.setEnabled(false);
                btnClear.setEnabled(false);
            }
        });
    }

    private void createFolder(String fname) {
        String myfolder = Environment.getExternalStorageDirectory() + "/" + fname;
        f = new File(myfolder);
        if (!f.exists()) {
            if (!f.mkdir()) {
                f.mkdir();
            }
        } else {
//            Toast.makeText(this, myfolder+" already exits.", Toast.LENGTH_SHORT).show();
        }
    }

    private void setListeners() {
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //write code for saving the signature here
//                saveSignatureBitmap();
                imgbackbtn.setVisibility(View.GONE);
                btnSave.setVisibility(View.GONE);
                btnClear.setVisibility(View.GONE);
                screenShot(v);
            }
        });

        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*signaturePad.clear();*/
                imgbackbtn.setVisibility(View.VISIBLE);
                btnSave.setVisibility(View.VISIBLE);
                btnClear.setVisibility(View.VISIBLE);
                signaturePad.clear();
            }
        });
    }

    public void screenShot(View view) {
        mbitmap = getBitmapOFRootView(btnSave);
//        imageView.setImageBitmap(mbitmap);
        createImage(mbitmap);
    }

    public Bitmap getBitmapOFRootView(View v) {
        View rootview = v.getRootView();
        rootview.setDrawingCacheEnabled(true);
        Bitmap bitmap1 = rootview.getDrawingCache();
        return bitmap1;
    }

    public void createImage(Bitmap bmp) {
        imgbackbtn.setVisibility(View.VISIBLE);
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 40, bytes);
        long millisecond = System.currentTimeMillis();
        String filename = "screenshot_" + millisecond + ".png";
        File file = new File(f, filename);
        try {
            file.createNewFile();
            FileOutputStream outputStream = new FileOutputStream(file);
            outputStream.write(bytes.toByteArray());
            outputStream.close();
            onCheckImageCreatedOrNot(filename);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void onCheckImageCreatedOrNot(String filename) {
        String myfolder = Environment.getExternalStorageDirectory() + "/" + folderName + "/" + filename;
        File f1 = new File(myfolder);
        if (f1.exists()) {
            btnSave.setVisibility(View.GONE);
            btnClear.setVisibility(View.GONE);
            Utils.showToast(getApplicationContext(), "" + filename + " File created successfully");
            finish();
        } else {
            btnSave.setVisibility(View.VISIBLE);
            btnClear.setVisibility(View.VISIBLE);
            Utils.showToast(getApplicationContext(), "" + filename + " File is not created \n Please try again later");
        }
    }


    private void saveSignatureBitmap() {
        Toast.makeText(getApplicationContext(), "Signature Saved", Toast.LENGTH_SHORT).show();
        File file = new File(f, System.currentTimeMillis() + ".png");
        FileOutputStream out = null;
//                Bitmap bitmap = signaturePad.getSignatureBitmap();
        Bitmap bitmap = signaturePad.getTransparentSignatureBitmap();
        try {
            out = new FileOutputStream(file);
            if (bitmap != null) {
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
            } else {
                throw new FileNotFoundException();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.flush();
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
