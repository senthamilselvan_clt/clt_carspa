package com.clt.newcarspa.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AlertDialog;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.clt.newcarspa.R;
import com.clt.newcarspa.Utils.Constant;
import com.clt.newcarspa.Utils.ProgressBarHandler;
import com.clt.newcarspa.Utils.Utils;
import com.clt.newcarspa.database.RealmDBHelper;
import com.clt.newcarspa.model.JobCardCarViewModelDB;
import com.clt.newcarspa.model.ReturnRealmModel;

import io.realm.Realm;

public class JobCardCarViewActivity extends BaseActivity {

    private Realm mRealm;
    private ConstraintLayout clParentLayout;
    private ImageView imgBackBtn, imgBackBumper, imgRightTailLight, imgLeftTailLight, imgTailGate, imgRightBackWheel,
            imgLeftBackWheel, imgRightBackDoor, imgLeftBackDoor, imgRightFrontDoor, imgLeftFrontDoor, imgSteering,
            imgFrontWindScreen, imgRightFrontWheel, imgLeftFrontWheel, imgBonnet, imgRightFrontLight, imgLeftFrontLight,
            imgRightFrontFender, imgLeftFrontFender, imgFrontBumper;
    private String textBackBumper = "0";
    private String textRightTailLight = "0";
    private String textLeftTailLight = "0";
    private String textTailGate = "0";
    private String textRightBackWheel = "0";
    private String textLeftBackWheel = "0";
    private String textRightBackDoor = "0";
    private String textLeftBackDoor = "0";
    private String textRightFrontDoor = "0";
    private String textLeftFrontDoor = "0";
    private String textSteering = "0";
    private String textFrontWindScreen = "0";
    private String textRightFrontWheel = "0";
    private String textLeftFrontWheel = "0";
    private String textBonnet = "0";
    private String textRightFrontLight = "0";
    private String textLeftFrontLight = "0";
    private String textRightFrontFender = "0";
    private String textLeftFrontFender = "0";
    private String textFrontBumper = "0";
    private TextView takeScreenShot;
    private String uniqueId, nextId;
    private RealmDBHelper realmDBHelper;
    private ProgressBarHandler mProgressBarHandler;

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mRealm.close();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cardisplay);
//        hideStatusBar(view);
        mRealm = Realm.getDefaultInstance();
        realmDBHelper = new RealmDBHelper(JobCardCarViewActivity.this);
        mProgressBarHandler = new ProgressBarHandler(this);
        showToolBarBackBtn();
        getBundle();
        initView();
        setListeners();
    }


    private void getBundle() {
        Intent extras = getIntent();
        uniqueId = extras.getStringExtra("JobCardUniqueId");
        nextId = extras.getStringExtra("NextId");
//        Utils.showToast(getApplicationContext(), "NextId--" + nextId + "--uniqueid--" + uniqueId);
    }

    private void initView() {
        clParentLayout = findViewById(R.id.constraintparent);
        imgBackBtn = findViewById(R.id.img_backbtn);
        imgBackBumper = findViewById(R.id.img_back_bumper);
        imgRightTailLight = findViewById(R.id.img_righttail_light);
        imgLeftTailLight = findViewById(R.id.img_lefttail_light);
        imgTailGate = findViewById(R.id.img_tailgate);
        imgRightBackWheel = findViewById(R.id.img_rightback_wheel);
        imgLeftBackWheel = findViewById(R.id.img_leftback_wheel);
        imgRightBackDoor = findViewById(R.id.img_rightback_door);
        imgLeftBackDoor = findViewById(R.id.img_leftback_door);
        imgRightFrontDoor = findViewById(R.id.img_rightfront_door);
        imgLeftFrontDoor = findViewById(R.id.img_leftfront_door);
        imgSteering = findViewById(R.id.img_steering);
        imgFrontWindScreen = findViewById(R.id.img_front_windscreen);
        imgRightFrontWheel = findViewById(R.id.img_rightfront_wheel);
        imgLeftFrontWheel = findViewById(R.id.img_leftfront_wheel);
        imgBonnet = findViewById(R.id.img_bonnet);
        imgRightFrontLight = findViewById(R.id.img_rightfront_light);
        imgLeftFrontLight = findViewById(R.id.img_leftfront_light);
        imgRightFrontFender = findViewById(R.id.img_right_fender);
        imgLeftFrontFender = findViewById(R.id.img_left_fender);
        imgFrontBumper = findViewById(R.id.img_front_bumper);
        takeScreenShot = findViewById(R.id.tv_screenshot);
        takeScreenShot.setText(getResources().getString(R.string.text_preview));
//        takeScreenShot.setVisibility(View.GONE);
    }

    private void setListeners() {
        imgBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeAlert();
            }
        });
        imgBackBumper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (textBackBumper.contains("0")) {
                    textBackBumper = "1";
                    imgBackBumper.setImageDrawable(getResources().getDrawable(R.drawable.one));
                } else {
                    textBackBumper = "0";
                    imgBackBumper.setImageDrawable(getResources().getDrawable(R.drawable.one_outline));
                }
//                Utils.showToast(getApplicationContext(), "string--" + textBackBumper);
            }
        });
        imgRightTailLight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (textRightTailLight.contains("0")) {
                    textRightTailLight = "1";
                    imgRightTailLight.setImageDrawable(getResources().getDrawable(R.drawable.two));
                } else {
                    textRightTailLight = "0";
                    imgRightTailLight.setImageDrawable(getResources().getDrawable(R.drawable.two_outline));
                }
            }
        });
        imgLeftTailLight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (textLeftTailLight.contains("0")) {
                    textLeftTailLight = "1";
                    imgLeftTailLight.setImageDrawable(getResources().getDrawable(R.drawable.three));
                } else {
                    textLeftTailLight = "0";
                    imgLeftTailLight.setImageDrawable(getResources().getDrawable(R.drawable.three_outline));
                }
            }
        });
        imgTailGate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (textTailGate.contains("0")) {
                    textTailGate = "1";
                    imgTailGate.setImageDrawable(getResources().getDrawable(R.drawable.five));
                } else {
                    textTailGate = "0";
                    imgTailGate.setImageDrawable(getResources().getDrawable(R.drawable.five_outline));
                }
            }
        });
        imgRightBackWheel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (textRightBackWheel.contains("0")) {
                    textRightBackWheel = "1";
                    imgRightBackWheel.setImageDrawable(getResources().getDrawable(R.drawable.four));
                } else {
                    textRightBackWheel = "0";
                    imgRightBackWheel.setImageDrawable(getResources().getDrawable(R.drawable.four_outline));
                }
            }
        });
        imgLeftBackWheel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (textLeftBackWheel.contains("0")) {
                    textLeftBackWheel = "1";
                    imgLeftBackWheel.setImageDrawable(getResources().getDrawable(R.drawable.four));
                } else {
                    textLeftBackWheel = "0";
                    imgLeftBackWheel.setImageDrawable(getResources().getDrawable(R.drawable.four_outline));
                }
            }
        });
        imgRightBackDoor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (textRightBackDoor.contains("0")) {
                    textRightBackDoor = "1";
                    imgRightBackDoor.setImageDrawable(getResources().getDrawable(R.drawable.eight));
                } else {
                    textRightBackDoor = "0";
                    imgRightBackDoor.setImageDrawable(getResources().getDrawable(R.drawable.eight_outline));
                }
            }
        });
        imgLeftBackDoor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (textLeftBackDoor.contains("0")) {
                    textLeftBackDoor = "1";
                    imgLeftBackDoor.setImageDrawable(getResources().getDrawable(R.drawable.nine));
                } else {
                    textLeftBackDoor = "0";
                    imgLeftBackDoor.setImageDrawable(getResources().getDrawable(R.drawable.nine_outline));
                }
            }
        });
        imgRightFrontDoor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (textRightFrontDoor.contains("0")) {
                    textRightFrontDoor = "1";
                    imgRightFrontDoor.setImageDrawable(getResources().getDrawable(R.drawable.eleven));
                } else {
                    textRightFrontDoor = "0";
                    imgRightFrontDoor.setImageDrawable(getResources().getDrawable(R.drawable.eleven_outline));
                }
            }
        });
        imgLeftFrontDoor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (textLeftFrontDoor.contains("0")) {
                    textLeftFrontDoor = "1";
                    imgLeftFrontDoor.setImageDrawable(getResources().getDrawable(R.drawable.ten));
                } else {
                    textLeftFrontDoor = "0";
                    imgLeftFrontDoor.setImageDrawable(getResources().getDrawable(R.drawable.ten_outline));
                }
            }
        });
        imgSteering.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (textSteering.contains("0")) {
                    textSteering = "1";
                    imgSteering.setImageDrawable(getResources().getDrawable(R.drawable.twenty_one));
                } else {
                    textSteering = "0";
                    imgSteering.setImageDrawable(getResources().getDrawable(R.drawable.twentyone_outline));
                }
            }
        });
        imgFrontWindScreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (textFrontWindScreen.contains("0")) {
                    textFrontWindScreen = "1";
                    imgFrontWindScreen.setImageDrawable(getResources().getDrawable(R.drawable.twelve));
                } else {
                    textFrontWindScreen = "0";
                    imgFrontWindScreen.setImageDrawable(getResources().getDrawable(R.drawable.twelve_outline));
                }
            }
        });
        imgRightFrontWheel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (textRightFrontWheel.contains("0")) {
                    textRightFrontWheel = "1";
                    imgRightFrontWheel.setImageDrawable(getResources().getDrawable(R.drawable.four));
                } else {
                    textRightFrontWheel = "0";
                    imgRightFrontWheel.setImageDrawable(getResources().getDrawable(R.drawable.four_outline));
                }
            }
        });
        imgLeftFrontWheel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (textLeftFrontWheel.contains("0")) {
                    textLeftFrontWheel = "1";
                    imgLeftFrontWheel.setImageDrawable(getResources().getDrawable(R.drawable.four));
                } else {
                    textLeftFrontWheel = "0";
                    imgLeftFrontWheel.setImageDrawable(getResources().getDrawable(R.drawable.four_outline));
                }
            }
        });
        imgBonnet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (textBonnet.contains("0")) {
                    textBonnet = "1";
                    imgBonnet.setImageDrawable(getResources().getDrawable(R.drawable.thirteen));
                } else {
                    textBonnet = "0";
                    imgBonnet.setImageDrawable(getResources().getDrawable(R.drawable.thirteen_outline));
                }
            }
        });
        imgRightFrontLight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (textRightFrontLight.contains("0")) {
                    textRightFrontLight = "1";
                    imgRightFrontLight.setImageDrawable(getResources().getDrawable(R.drawable.sixteen));
                } else {
                    textRightFrontLight = "0";
                    imgRightFrontLight.setImageDrawable(getResources().getDrawable(R.drawable.sixteen_outline));
                }
            }
        });
        imgLeftFrontLight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (textLeftFrontLight.contains("0")) {
                    textLeftFrontLight = "1";
                    imgLeftFrontLight.setImageDrawable(getResources().getDrawable(R.drawable.seventeen));
                } else {
                    textLeftFrontLight = "0";
                    imgLeftFrontLight.setImageDrawable(getResources().getDrawable(R.drawable.seventeen_outline));
                }
            }
        });
        imgRightFrontFender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (textRightFrontFender.contains("0")) {
                    textRightFrontFender = "1";
                    imgRightFrontFender.setImageDrawable(getResources().getDrawable(R.drawable.nineteen));
                } else {
                    textRightFrontFender = "0";
                    imgRightFrontFender.setImageDrawable(getResources().getDrawable(R.drawable.nineteen_outline));
                }
                imgRightFrontFender.setImageDrawable(getResources().getDrawable(R.drawable.nineteen));
            }
        });
        imgLeftFrontFender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (textLeftFrontFender.contains("0")) {
                    textLeftFrontFender = "1";
                    imgLeftFrontFender.setImageDrawable(getResources().getDrawable(R.drawable.eighteen));
                } else {
                    textLeftFrontFender = "0";
                    imgLeftFrontFender.setImageDrawable(getResources().getDrawable(R.drawable.eighteen_outline));
                }
                imgLeftFrontFender.setImageDrawable(getResources().getDrawable(R.drawable.eighteen));
            }
        });
        imgFrontBumper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (textFrontBumper.contains("0")) {
                    textFrontBumper = "1";
                    imgFrontBumper.setImageDrawable(getResources().getDrawable(R.drawable.twenty));
                } else {
                    textFrontBumper = "0";
                    imgFrontBumper.setImageDrawable(getResources().getDrawable(R.drawable.twenty_outline));
                }
            }
        });
        takeScreenShot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mProgressBarHandler.show();
                JobCardCarViewModelDB carImgDetails = new JobCardCarViewModelDB();
                carImgDetails.setLocaldbid(uniqueId);
                carImgDetails.setJob_card_id(Integer.parseInt(nextId));
                carImgDetails.setImg_back_bumper(textBackBumper);
                carImgDetails.setImgRightTailLight(textRightTailLight);
                carImgDetails.setImgLeftTailLight(textLeftTailLight);
                carImgDetails.setImgTailGate(textTailGate);
                carImgDetails.setImgRightBackWheel(textRightBackWheel);
                carImgDetails.setImgLeftBackWheel(textLeftBackWheel);
                carImgDetails.setImgRightBackDoor(textRightBackDoor);
                carImgDetails.setImgLeftBackDoor(textLeftBackDoor);
                carImgDetails.setImgRightFrontDoor(textRightFrontDoor);
                carImgDetails.setImgLeftFrontDoor(textLeftFrontDoor);
                carImgDetails.setImgSteering(textSteering);
                carImgDetails.setImgFrontWindScreen(textFrontWindScreen);
                carImgDetails.setImgRightFrontWheel(textRightFrontWheel);
                carImgDetails.setImgLeftFrontWheel(textLeftFrontWheel);
                carImgDetails.setImgBonnet(textBonnet);
                carImgDetails.setImgRightFrontLight(textRightFrontLight);
                carImgDetails.setImgLeftFrontLight(textLeftFrontLight);
                carImgDetails.setImgRightFrontFender(textRightFrontFender);
                carImgDetails.setImgLeftFrontFender(textLeftFrontFender);
                carImgDetails.setImgFrontBumper(textFrontBumper);
                carImgDetails.setRow_deleted(0);
//                takeScreenshot(ScreenshotType.FULL);
                final ReturnRealmModel rowAdded = realmDBHelper.addJobCardCarViewDetails(mRealm, carImgDetails);
                if (rowAdded.isStatus()) {
                    Integer jobcarid = rowAdded.getNextId();
                    realmDBHelper.getJobCardCarViewDetails(mRealm, jobcarid);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mProgressBarHandler.hide();
                            goToPreviewPage();
                        }
                    }, Constant.LOADER_TIME_OUT);
                } else {
                    mProgressBarHandler.hide();
                    Utils.showToast(getApplicationContext(), getResources().getString(R.string.msg_servererror));
                }
            }
        });
    }

    private void goToPreviewPage() {
        Intent i = new Intent(JobCardCarViewActivity.this, JobCardPreviewActivity.class);
        i.putExtra("JobCardUniqueId", uniqueId);
        i.putExtra("NextId", nextId);// jobCardId
        startActivity(i);
        finish();
    }

    /*private void takeScreenshot(ScreenshotType screenshotType) {
        Bitmap b = null;
        switch (screenshotType) {
            case FULL:
                //If Screenshot type is FULL take full page screenshot i.e our root content.
                b = ScreenshotUtils.getScreenShot(rootContent);
                break;
        }

        //If bitmap is not null
       *//* if (b != null) {
            showScreenShotImage(b);//show bitmap over imageview

            File saveFile = ScreenshotUtils.getMainDirectoryName(MainActivity.this);//get the path to save screenshot
            File file = ScreenshotUtils.store(b, "screenshot" + screenshotType + ".jpg", saveFile);//save the screenshot to selected path
            shareScreenshot(file);//finally share screenshot
        } else
            //If bitmap is null show toast message
            Toast.makeText(MainActivity.this, R.string.screenshot_take_failed, Toast.LENGTH_SHORT).show();
*//*
    }
*/
    public void showToolBarBackBtn() {
        hideToolbarAppName();
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setDisplayShowHomeEnabled(false);
            getSupportActionBar().hide();
        }
    }
//    public void hideStatusBar(View view){
//        //Hide the status bar on Android 4.0 and Lower
//        if (Build.VERSION.SDK_INT < 16) {
//            Window w=getWindow();
//            w.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
//        }
//        else{
//            View decorView = getWindow().getDecorView();
//            // Hide the status bar.
//            int visibility = View.SYSTEM_UI_FLAG_FULLSCREEN;
//            decorView.setSystemUiVisibility(visibility);
//        }
//    }

    public void closeAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(JobCardCarViewActivity.this);
        alertDialog.setTitle(getResources().getString(R.string.app_name));
        alertDialog.setMessage(getResources().getString(R.string.msg_closeit));
        alertDialog.setPositiveButton(getResources().getString(R.string.msg_yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        alertDialog.setNegativeButton(getResources().getString(R.string.msg_no), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertDialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            closeAlert(JobCardCarViewActivity.this, getResources().getString(R.string.app_jobcardcarview));
        }

        return super.onOptionsItemSelected(item);
    }
}
