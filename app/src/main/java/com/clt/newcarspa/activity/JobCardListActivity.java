package com.clt.newcarspa.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.TextView;

import com.clt.newcarspa.Adapter.JobCardListAdapter;
import com.clt.newcarspa.R;
import com.clt.newcarspa.Utils.ProgressBarHandler;
import com.clt.newcarspa.Utils.Utils;
import com.clt.newcarspa.database.RealmDBHelper;
import com.clt.newcarspa.model.UserLoginDetails;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;

/**
 * Created by Karthik Thumathy on 31-Dec-18.
 */
public class JobCardListActivity extends BaseActivity implements  TabLayout.OnTabSelectedListener {

    private Realm mRealm;
    private RealmDBHelper realmDBHelper;
    private ProgressBarHandler mProgressBarHandler;
    private Toolbar toolbar;
    private TextView toolbarTitle;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private int mTabPosition = 0;
    private boolean liveOrLocal = false;
    private List<UserLoginDetails> userLoginDetails = new ArrayList<>();
    private String loginUserId, loginUserName, loginShopId;
    private JobCardListAdapter cardAdapter;
    private TabLayout.Tab mTagSales;

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mRealm.close();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_job_card_list);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        mRealm = Realm.getDefaultInstance();
        realmDBHelper = new RealmDBHelper(JobCardListActivity.this);
        mProgressBarHandler = new ProgressBarHandler(this);
        getUserLoginDetailsFromDB();
        initView();
        tabsCreation();
//        viewPager.setCurrentItem(1);
    }


    private void initView() {
        toolbar = findViewById(R.id.toolbar);
        toolbarTitle = findViewById(R.id.tv_toolbarTitle);
        viewPager = findViewById(R.id.viewpager);
        tabLayout = findViewById(R.id.tabs);
        setSupportActionBar(toolbar);
        showToolBarBackBtn();
        onHandleUI();
        if (Utils.getInstance(getApplicationContext()).isOnline()) {
            liveOrLocal = true;
        }
    }


    public void tabsCreation() {
        /*new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mProgressBarHandler.hide();
            }
        }, SPLASH_TIME_OUT);*/
//        if (tabLayout.getTabCount() != 0) {
//            tabLayout.removeAllTabs();
//        }
        mTagSales = tabLayout.newTab().setText(getResources().getString(R.string.app_viewjobcardpage));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.text_jobpending)).setTag(mTagSales));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.text_jobprogress)).setTag(mTagSales));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.text_jobcompleted)).setTag(mTagSales));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        cardAdapter = new JobCardListAdapter(getSupportFragmentManager(), tabLayout.getTabCount(), liveOrLocal, loginUserId, loginShopId);
        viewPager.setAdapter(cardAdapter);
//        viewPager.addOnPageChangeListener(new TabLayout.
//                TabLayoutOnPageChangeListener(tabLayout));
        viewPager.setOffscreenPageLimit(2);
       /* tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
//                selectedPos = tab.getPosition();
//                autoQuestionTab.setText(String.valueOf(questionLists.get(selectedPos).getSerial_no()));
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });*/
        tabLayout.setOnTabSelectedListener(this);

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                if (tabLayout != null) {
                    mTabPosition = position;
                    TabLayout.Tab tab = tabLayout.getTabAt(position);
                    tab.select();
                }
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            @Override
            public void onPageScrollStateChanged(int arg0) {
            }
        });
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        if (tab.getTag().equals(mTagSales)) {
            if (viewPager != null) {
                viewPager.setCurrentItem(tab.getPosition());
                mTabPosition = tab.getPosition();
            }
        }
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    public void showToolBarBackBtn() {
        hideToolbarAppName();
        if (getSupportActionBar() != null) {
            toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_action_back_btn));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    public void hideToolbarAppName() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);//for hiding the app name
        }
    }

    private void onHandleUI() {
        toolbarTitle.setText(getResources().getString(R.string.app_viewjobcardpage));
    }

    private void getUserLoginDetailsFromDB() {
        userLoginDetails = realmDBHelper.getUserLoginDetail(mRealm);
        if (userLoginDetails != null || userLoginDetails.size() > 0) {
            try {
                loginUserId = userLoginDetails.get(0).getId();
                loginUserName = userLoginDetails.get(0).getUser_name();
                loginShopId = userLoginDetails.get(0).getShop_id();
            } catch (ArrayIndexOutOfBoundsException e) {
                e.printStackTrace();
                Utils.showToast(getApplicationContext(), getResources().getString(R.string.msg_sessionexpire));
                goToLoginPage();
            } catch (Exception e) {
                e.printStackTrace();
                Utils.showToast(getApplicationContext(), getResources().getString(R.string.msg_sessionexpire));
                goToLoginPage();
            }
        } else {
            try {
                if (Utils.getFromUserDefaults(getApplicationContext(), "LoginId") != null) {
                    loginUserId = Utils.getFromUserDefaults(getApplicationContext(), "LoginId");
                    loginUserName = Utils.getFromUserDefaults(getApplicationContext(), "LoginUserName");
                    loginShopId = Utils.getFromUserDefaults(getApplicationContext(), "LoginShopId");
                }
            } catch (ArrayIndexOutOfBoundsException e) {
                e.printStackTrace();
                Utils.showToast(getApplicationContext(), getResources().getString(R.string.msg_sessionexpire));
                goToLoginPage();
            } catch (Exception e) {
                e.printStackTrace();
                Utils.showToast(getApplicationContext(), getResources().getString(R.string.msg_sessionexpire));
                goToLoginPage();
            }
        }
    }

    private void goToLoginPage() {
        startActivity(new Intent(JobCardListActivity.this, LoginActivity.class));
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            overridePendingTransitionExit();
        }
// else if (id == R.id.action_settings) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if ( keyCode == KeyEvent.KEYCODE_BACK
                && event.getRepeatCount() == 0) {
            onBackPressed();
            overridePendingTransitionExit();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
