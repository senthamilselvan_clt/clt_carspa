package com.clt.newcarspa.activity;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.clt.newcarspa.BuildConfig;
import com.clt.newcarspa.R;
import com.clt.newcarspa.Utils.Constant;
import com.clt.newcarspa.Utils.ProgressBarHandler;
import com.clt.newcarspa.Utils.Utils;
import com.clt.newcarspa.cltinterfaces.RestAdapter;
import com.clt.newcarspa.database.RealmDBHelper;
import com.clt.newcarspa.model.APIListResponse;
import com.clt.newcarspa.model.APIResponse;
import com.clt.newcarspa.model.AudioUploadModel;
import com.clt.newcarspa.model.ImageUploadModel;
import com.clt.newcarspa.model.JobCardDetailsRealmDBModel;
import com.clt.newcarspa.model.JobCardItemsDetailsModel;
import com.clt.newcarspa.model.MyCarListItems;
import com.clt.newcarspa.model.ReturnRealmModel;
import com.clt.newcarspa.model.SyncJobCardModel;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SettingActivity extends BaseActivity {
    private Realm mRealm;
    private RealmDBHelper realmDBHelper;
    private ProgressBarHandler mProgressBarHandler;
    private Toolbar toolbar;
    private ImageView imgBack;
    private TextView toolbarTitle, tvVersion;
    private CardView cvSync, cvPush;
    private SwitchCompat switchGallery;

    private List<JobCardDetailsRealmDBModel> pushedCardLists = new ArrayList<>();
    private List<JobCardDetailsRealmDBModel> CardLists = new ArrayList<>();
    private List<JobCardItemsDetailsModel> itemsDetailsList = new ArrayList<>();
    private List<ImageUploadModel> imageCardItemList = new ArrayList<>();
    private List<AudioUploadModel> audioCardItemList = new ArrayList<>();
    private MyCarListItems myCarListItems = new MyCarListItems();
    private Integer gallery = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        mRealm = Realm.getDefaultInstance();
        realmDBHelper = new RealmDBHelper(SettingActivity.this);
        mProgressBarHandler = new ProgressBarHandler(this);
        initView();
        setListeners();
        setVersionCode();
        setGallerySettings();
    }

    private void setGallerySettings() {
        myCarListItems = realmDBHelper.getGalleryDetail(mRealm);
        if (myCarListItems != null) {
            if (myCarListItems.getIsDisplay() == 1) {
                switchGallery.setChecked(true);
                gallery = 1;
            }else{
                switchGallery.setChecked(false);
                gallery = 0;
            }
        }
    }

    private void initView() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbarTitle = findViewById(R.id.tv_toolbarTitle);
        toolbarTitle.setText(getResources().getString(R.string.app_settingspage));
        imgBack = findViewById(R.id.img_back);
        imgBack.setVisibility(View.VISIBLE);
        cvSync = findViewById(R.id.cv_sync);
        cvSync.setVisibility(View.GONE);// same functionality did for push also
        cvPush = findViewById(R.id.cv_push);
        switchGallery = findViewById(R.id.switch_gallery);
        tvVersion = findViewById(R.id.tv_version);
    }

    private void setListeners() {
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        cvSync.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.getInstance(SettingActivity.this).isInternet()) {
                    pushToServer();
                }
            }
        });
        cvPush.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.getInstance(SettingActivity.this).isInternet()) {
                    pushToServer(new CommunicationListener() {
                        @Override
                        public void onResponse(String status) {
//                            Utils.showToast(getApplicationContext(), ""+status);
                        }
                    });
                }
            }
        });
       /* switchGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Integer gallery = 0;
                if (switchGallery.isChecked()) {
                    switchGallery.setChecked(false);
                    gallery = 0;
                }else{
                    switchGallery.setChecked(true);
                    gallery = 1;
                }
                Utils.showToast(getApplicationContext(),"status:"+switchGallery.isChecked()+"--gallery--"+gallery);
            }
        });*/
        switchGallery.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    switchGallery.setChecked(true);
                    gallery = 1;
                }else{
                    switchGallery.setChecked(false);
                    gallery = 0;
                }
//                Utils.showToast(getApplicationContext(),"status:"+switchGallery.isChecked()+"--gallery--"+gallery);
                showGallery(gallery);
            }
        });
    }

    private void showGallery(Integer gallery) {
        boolean showOrNotGallery = realmDBHelper.updateHomePageItems(mRealm, gallery);
        if (showOrNotGallery) {
//            Utils.showToast(getApplicationContext(), "Show Gallery");
        }else {
//            Utils.showToast(getApplicationContext(), "Show Gallery");
        }
    }

    private void setVersionCode() {
        try {
            String versionname = "";
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            versionname = "App version " + pInfo.versionName + " (" + pInfo.versionCode + ")";
            tvVersion.setText(versionname);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK
                && event.getRepeatCount() == 0) {
            onBackPressed();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onBackPressed() {
        closeAlert(SettingActivity.this, getResources().getString(R.string.app_settingspage));
    }

    private void pushToServer() {
        String mPushStr = null;
        RealmList<JobCardDetailsRealmDBModel> pushCardList = new RealmList<>();
        List<JobCardItemsDetailsModel> pushCardItemsList;
        List<ImageUploadModel> imageItemsList;
        List<AudioUploadModel> audioItemsList;
        ReturnRealmModel realmmodel = realmDBHelper.getPushAllJobCardDetail(mRealm);
        if (realmmodel != null) {
            if (realmmodel.getJobCard() != null || realmmodel.getJobCard().size() > 0) {
                JobCardDetailsRealmDBModel mJobCardList;
                JobCardItemsDetailsModel mJobCardItemsList;
                ImageUploadModel mImageObj;
                AudioUploadModel mAudioObj;

                imageCardItemList = getRealmImageListToList(realmmodel);
                audioCardItemList = getRealmAudioListToList(realmmodel);
                CardLists = getRealmToCardList(realmmodel);
                itemsDetailsList = getRealmListToList(realmmodel);
                for (int i = 0; i < CardLists.size(); i++) {
                    pushCardItemsList = new ArrayList<>();
                    mJobCardList = CardLists.get(i);
                    if (realmmodel.getJobCardItems() != null && realmmodel.getJobCardItems().size() > 0) {
                        for (int j = 0; j < itemsDetailsList.size(); j++) {
                            mJobCardItemsList = itemsDetailsList.get(j);
                            if (mJobCardList.getId().equals(mJobCardItemsList.getJob_card_id())) {
                                pushCardItemsList.add(mJobCardItemsList);
                                Utils.showLogD("20001----" + new Gson().toJson(mJobCardItemsList));
                            }
                        }
                        if (pushCardItemsList.size() > 0) {
                            String items = new Gson().toJson(pushCardItemsList);
                            mJobCardList.setItems(items);
                        }
                    }
                    imageItemsList = new ArrayList<>();
                    if (imageCardItemList != null && imageCardItemList.size() > 0) {
                        for (int p = 0; p < imageCardItemList.size(); p++) {
                            mImageObj = imageCardItemList.get(p);
                            if (mJobCardList.getId().equals(mImageObj.getId())) {

                                imageItemsList.add(mImageObj);
                            }
                        }
                        if (imageItemsList.size() > 0) {
                            String imageItems = new Gson().toJson(imageItemsList);
                            mJobCardList.setItem_images(imageItems);
                        }
                    }

                    audioItemsList = new ArrayList<>();
                    if (audioCardItemList != null && audioCardItemList.size() > 0) {
                        for (int p = 0; p < audioCardItemList.size(); p++) {
                            mAudioObj = audioCardItemList.get(p);
                            if (mJobCardList.getId().equals(mAudioObj.getId())) {
                                audioItemsList.add(mAudioObj);
                            }
                        }
                        if (audioItemsList.size() > 0) {
                            String audioItems = new Gson().toJson(audioItemsList);
                            mJobCardList.setItem_audios(audioItems);
                        }
                    }

                    pushCardList.add(mJobCardList);
                }
                if (pushCardList.size() > 0) {
                    mPushStr = new Gson().toJson(pushCardList);
                }

                if (realmmodel.getJobCard().size() > 0) {
                    callPushJobCardService(pushCardList);
                } else {
//                    deleteAllDBRecords();
                    Utils.showToast(getApplicationContext(), "no records found");
                }
            } else {
//                deleteAllDBRecords();
                Utils.showToast(getApplicationContext(), "no records found");
            }
        }
    }

    private void deleteAllDBRecords() {
        Integer deleted = realmDBHelper.deleteAllDBItems(mRealm);
        if (deleted == 1) {
            callGetAllDataFromServer();
        }
    }

    public void callPushJobCardService(RealmList<JobCardDetailsRealmDBModel> pushStr) {
//            , final NewJobCardActivity.CommunicationListener listener) {
        mProgressBarHandler.show();
        JobCardDetailsRealmDBModel carddetail = new JobCardDetailsRealmDBModel();
        carddetail.setAction(Constant.PUSHJOBCARDLIST);
        carddetail.setJob_card_pushstr(pushStr);
        Call<APIListResponse<JobCardDetailsRealmDBModel>> call = RestAdapter.getInstance().getServiceApi().pushJobCardList(carddetail);
        call.enqueue(new Callback<APIListResponse<JobCardDetailsRealmDBModel>>() {
            @Override
            public void onResponse(@NonNull Call<APIListResponse<JobCardDetailsRealmDBModel>> call,
                                   @NonNull Response<APIListResponse<JobCardDetailsRealmDBModel>> response) {
                String status;
                if (response.isSuccessful()) {
                    mProgressBarHandler.hide();
                    if (response.body() != null) {
                        status = response.body().getStatus();
                        if (status.equals(Constant.RESPONSESUCCESS)) {
                            pushedCardLists = response.body().getResult();
                            Utils.showToast(getApplicationContext(), "push successfully");
                            ReturnRealmModel realmModel = realmDBHelper.updatePushedJobCardStatus(mRealm, pushedCardLists);
                            if (realmModel.isStatus()) {
                                deleteAllDBRecords();
                            } else {
                                Utils.showToast(getApplicationContext(), "push Failed");
                            }
//                            listener.onJobCardSuccessResponse(nextid, status, response.body().getResult().get(0));
                        }
                    }
                } else {
                    mProgressBarHandler.hide();
                }
                realmDBHelper.getAllJobCardDetail(mRealm);
            }

            @Override
            public void onFailure(Call<APIListResponse<JobCardDetailsRealmDBModel>> call, Throwable t) {
                mProgressBarHandler.hide();
                if (getApplicationContext() != null) {
                    String failure = t.getMessage().toString();
                    Log.d("onFailure Response", "" + failure);
                    if (BuildConfig.FLAVOR.equals("dev")) {
                        Utils.showToast(getApplicationContext(), getResources().getString(R.string.msg_servererror) + "--" + failure);
                    } else {
                        Utils.showToast(getApplicationContext(), getResources().getString(R.string.msg_servererror));
                    }
                }
            }
        });
    }

    public void callGetAllDataFromServer() {
        mProgressBarHandler.show();
        SyncJobCardModel carddetail = new SyncJobCardModel();
        carddetail.setAction(Constant.GETALLJOBCARDLIST);
        Call<APIResponse<SyncJobCardModel>> call = RestAdapter.getInstance().getServiceApi().getAllCardListFromServer(carddetail);
        call.enqueue(new Callback<APIResponse<SyncJobCardModel>>() {
            @Override
            public void onResponse(@NonNull Call<APIResponse<SyncJobCardModel>> call,
                                   @NonNull Response<APIResponse<SyncJobCardModel>> response) {
                String status;
                if (response.isSuccessful()) {
                    mProgressBarHandler.hide();
                    if (response.body() != null) {
                        status = response.body().getStatus();
                        List<SyncJobCardModel.JobCardDetailsRealmDBModel> serverList = response.body().getResult().getData();
                        ReturnRealmModel realmModel = realmDBHelper.addExistingJobCardFromServer(mRealm, serverList);
                        if (realmModel.getAddedExistingJobCardFromServerStatus() == 1) {
                            Utils.showToast(getApplicationContext(), "Sync Successfully");
                        }else{
                            Utils.showToast(getApplicationContext(), "Sync Failed");
                        }
                    }
                } else {
                    mProgressBarHandler.hide();
                }
            }

            @Override
            public void onFailure(Call<APIResponse<SyncJobCardModel>> call, Throwable t) {
                mProgressBarHandler.hide();
                if (getApplicationContext() != null) {
                    String failure = t.getMessage().toString();
                    Log.d("onFailure Response", "" + failure);
                    if (BuildConfig.FLAVOR.equals("dev")) {
                        Utils.showToast(getApplicationContext(), getResources().getString(R.string.msg_servererror) + "--" + failure);
                    } else {
                        Utils.showToast(getApplicationContext(), getResources().getString(R.string.msg_servererror));
                    }
                }
            }
        });
    }
}
