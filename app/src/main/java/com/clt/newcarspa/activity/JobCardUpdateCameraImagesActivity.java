package com.clt.newcarspa.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.clt.newcarspa.Adapter.CarPartsItemsAdapter;
import com.clt.newcarspa.Adapter.UpdateAudioListAdapter;
import com.clt.newcarspa.Adapter.UpdateImageListAdapter;
import com.clt.newcarspa.BuildConfig;
import com.clt.newcarspa.Helper.AlbumStorageDirFactory;
import com.clt.newcarspa.Helper.BaseAlbumDirFactory;
import com.clt.newcarspa.Helper.DimenUtil;
import com.clt.newcarspa.Helper.FileUtils;
import com.clt.newcarspa.Helper.FroyoAlbumDirFactory;
import com.clt.newcarspa.R;
import com.clt.newcarspa.Utils.Constant;
import com.clt.newcarspa.Utils.ProgressBarHandler;
import com.clt.newcarspa.Utils.Utils;
import com.clt.newcarspa.cltinterfaces.RestAdapter;
import com.clt.newcarspa.database.RealmDBHelper;
import com.clt.newcarspa.model.APIListResponse;
import com.clt.newcarspa.model.APIResponse;
import com.clt.newcarspa.model.AudioUploadModel;
import com.clt.newcarspa.model.CarPartsModel;
import com.clt.newcarspa.model.ErrorReasonModel;
import com.clt.newcarspa.model.ImageUploadModel;
import com.clt.newcarspa.model.JobCardDetailsRealmDBModel;
import com.clt.newcarspa.model.ReturnRealmModel;
import com.clt.newcarspa.model.UserLoginDetails;
import com.google.gson.Gson;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.clt.newcarspa.Helper.FileUtils.CARSPA_DIRECTORY_PATH;

/**
 * Created by Karthik Thumathy on 16-Oct-18.
 */
public class JobCardUpdateCameraImagesActivity extends BaseActivity implements UpdateImageListAdapter.ButtonAdapterCallback, UpdateAudioListAdapter.ButtonAdapterCallback, CarPartsItemsAdapter.ButtonAdapterCallback {

    private Realm mRealm;
    private RealmDBHelper realmDBHelper;
    private ProgressBarHandler mProgressBarHandler;
    private Toolbar toolbar;
    private TextView toolbarTitle;
    private ImageView imgAttach;
    private RecyclerView rvImages, rvAudios;
    private LinearLayoutManager mLayoutManager;
    private LinearLayoutManager cLayoutManager;
    private UpdateImageListAdapter addImageAdapter;
    private UpdateAudioListAdapter addAudioAdapter;
    private CarPartsItemsAdapter carPartAdapter;

    private String uniqueId, nextId;
    private List<UserLoginDetails> userLoginDetails = new ArrayList<>();
    private String loginUserId, loginUserName, loginShopId;
    private List<ImageUploadModel> jobCardImageList;
    private List<AudioUploadModel> jobCardAudioList;
    private Button btnNext;
    private Integer fromWhere = 0;

    /*--- Image Upload---*/
    private static final String LOG_TAG = "IMAGEUPLOAD";
    private static final String TAG = "IMAGEUPLOAD";
    private Button btnCamera, btnFileChoose, btnAudio;

    private static final int ACTION_TAKE_PHOTO_B_PIC = 1;
    private static final int FILE_SELECT_CODE_PIC = 11;
    private String mCurrentPhotoPath;
    private String mPictureFilePath;
    private File mPicFile;
    private File mImageFileDir;
    private boolean mIsPictureSet;
    private AlbumStorageDirFactory mAlbumStorageDirFactory;
    private static final String JPEG_FILE_PREFIX = "CAMPIC_";
    private static final String JPEG_FILE_SUFFIX = ".jpg";
    private int mIVDimenPx;
    public static final String AUDIO_DIR_PATH = "/msg_aud/";
    private static final String AUDIO_FILE_PREFIX = "AMSG_AU_";
    public static final String AUDIO_FILE_SUFFIX = ".3gp";
    private File mSthatDir;
    private File mAudioFile;
    private boolean mIsAudioMode;
    private Chronometer mChronometer;
    private MediaRecorder mRecorder;
    private CardView mAudioRecCv;
    private TextView mAudioRecMsgTv;
    private Button mAudioRecCancelBtn;
    private Button mAudioRecSendBtn;
    private boolean mIsAudioRecorderNotPlayer;
    private AlertDialog attachAlertDialog;
    private List<CarPartsModel> cPartsList;
    private AlertDialog carPartsAlertDialog;
    private boolean liveOrLocal = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cameraimages);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        checkOnline();
        mRealm = Realm.getDefaultInstance();
        realmDBHelper = new RealmDBHelper(JobCardUpdateCameraImagesActivity.this);
        mProgressBarHandler = new ProgressBarHandler(this);
        getUserLoginDetailsFromDB();
        initView();
        setListeners();
        getBundle();
        initVars();
        initSthatAndAudioDirectory();
    }

    private void checkOnline() {
        if (Utils.getInstance(JobCardUpdateCameraImagesActivity.this).isOnline()) {
            liveOrLocal = true;
        } else {
            liveOrLocal = false;
        }
    }

    private void goToLoginPage() {
        startActivity(new Intent(JobCardUpdateCameraImagesActivity.this, LoginActivity.class));
        finish();
    }

    private void getUserLoginDetailsFromDB() {
        userLoginDetails = realmDBHelper.getUserLoginDetail(mRealm);
        if (userLoginDetails != null || userLoginDetails.size() > 0) {
            try { //login data come db only normally
                loginUserId = userLoginDetails.get(0).getId();
                loginUserName = userLoginDetails.get(0).getUser_name();
                loginShopId = userLoginDetails.get(0).getShop_id();
            } catch (ArrayIndexOutOfBoundsException e) {
                e.printStackTrace();
                showAlertDialog(getResources().getString(R.string.msg_sessionexpire));
                goToLoginPage();
            } catch (Exception e) {
                e.printStackTrace();
                showAlertDialog(getResources().getString(R.string.msg_sessionexpire));
                goToLoginPage();
            }
        } else {
            try {
                if (Utils.getFromUserDefaults(getApplicationContext(), "LoginId") != null) {
                    loginUserId = Utils.getFromUserDefaults(getApplicationContext(), "LoginId");
                    loginUserName = Utils.getFromUserDefaults(getApplicationContext(), "LoginUserName");
                    loginShopId = Utils.getFromUserDefaults(getApplicationContext(), "LoginShopId");
                }
            } catch (ArrayIndexOutOfBoundsException e) {
                e.printStackTrace();
                showAlertDialog(getResources().getString(R.string.msg_sessionexpire));
                goToLoginPage();
            } catch (Exception e) {
                e.printStackTrace();
                showAlertDialog(getResources().getString(R.string.msg_sessionexpire));
                goToLoginPage();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (!mRealm.isClosed()) {
            mRealm.close();
        }
    }

    private void setUpAudioRecyclerView(List<AudioUploadModel> uploadedAudios) {
        rvAudios.setVisibility(View.VISIBLE);
        mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        addAudioAdapter = new UpdateAudioListAdapter(getApplicationContext(), uploadedAudios);
        rvAudios.setAdapter(addAudioAdapter);
        rvAudios.setLayoutManager(mLayoutManager);
        rvAudios.setItemAnimator(new DefaultItemAnimator());
//        addImageAdapter.setOnItemClickListener(this);
        addAudioAdapter.setCallback(this);
        addAudioAdapter.notifyDataSetChanged();
    }

    private void setUpImageRecyclerView(List<ImageUploadModel> uploadedImages) {
        rvImages.setVisibility(View.VISIBLE);
        mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        addImageAdapter = new UpdateImageListAdapter(getApplicationContext(), uploadedImages);
        rvImages.setAdapter(addImageAdapter);
        rvImages.setLayoutManager(mLayoutManager);
        rvImages.setItemAnimator(new DefaultItemAnimator());
//        addImageAdapter.setOnItemClickListener(this);
        addImageAdapter.setCallback(this);
        addImageAdapter.notifyDataSetChanged();
    }

    private void setListeners() {
        imgAttach.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addImageAlert();
            }
        });
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToPreviewPage();
            }
        });
    }

    private void initView() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbarTitle = findViewById(R.id.tv_toolbarTitle);
        imgAttach = findViewById(R.id.img_attach);
        btnNext = findViewById(R.id.btn_next);
        rvImages = findViewById(R.id.rv_addedimages);
        rvAudios = findViewById(R.id.rv_addedaudios);

        cPartsList = new ArrayList<>();
        cPartsList = realmDBHelper.getCartPartsList(mRealm);
        if (cPartsList != null && cPartsList.size() == 0) {
            getCarPartsList();
        }
    }


    public void getCarPartsList() {
        CarPartsModel details = new CarPartsModel();
        details.setAction(Constant.CARPARTSlIST);
        Call<APIListResponse<CarPartsModel>> call = RestAdapter.getInstance().getServiceApi().getCarPartsList(details);
        call.enqueue(new Callback<APIListResponse<CarPartsModel>>() {
            @Override
            public void onResponse(@NonNull Call<APIListResponse<CarPartsModel>> call,
                                   @NonNull Response<APIListResponse<CarPartsModel>> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (onCheckAPIStatus(Integer.parseInt(response.body().getStatus()))) {
                            cPartsList = new ArrayList<>();
                            cPartsList = response.body().getResult();
                            if (cPartsList != null && cPartsList.size() > 0) {
                                ReturnRealmModel realmModel = realmDBHelper.addCarPartsItems(mRealm, cPartsList);
                                if (realmModel.getAddedCarPartsStatus() == 1) {
//                                    Utils.showToast(getApplicationContext(), "status--"+realmModel.getAddedCarPartsStatus());
                                }
                            }
                        } else {
                            onHandleCarPartListErrorResponse(response);
                        }
                    }
                } else {
                    onHandleCarPartListErrorResponse(response);
                }
            }

            @Override
            public void onFailure(Call<APIListResponse<CarPartsModel>> call, Throwable t) {
                String failure = "";
                if (getApplicationContext() != null) {
                    try {
                        failure = t.getMessage().toString();
                        Log.d("onFailure Response", "" + failure);
                        Utils.showToast(getApplicationContext(), getResources().getString(R.string.msg_servererror));
                    } catch (Exception e) {
                        e.printStackTrace();
                        Utils.showToast(getApplicationContext(), getResources().getString(R.string.msg_servererror) + "" + failure);
                    }
                }
            }
        });
    }

    private void onHandleCarPartListErrorResponse(@NonNull Response<APIListResponse<CarPartsModel>> response) {
        try {
            if (response.errorBody() != null) {
                ErrorReasonModel model = new Gson().fromJson(response.errorBody().string(), ErrorReasonModel.class);
                int reason = model.getStatus();
                showAlertDialog(onCheckErrorStatus(getApplicationContext(), reason));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void getBundle() {
        Intent extras = getIntent();
        uniqueId = extras.getStringExtra("JobCardUniqueId");
        nextId = extras.getStringExtra("NextId");
        fromWhere = extras.getIntExtra("fromWhere", 0);
        onHandleUI();
//        Utils.showToast(getApplicationContext(), "NextId--" + nextId + "--uniqueid--" + uniqueId);
    }

    private void onHandleUI() {
        toolbarTitle.setText(getResources().getString(R.string.text_updateimage));
//        if (liveOrLocal) {
//            imgAttach.setVisibility(View.VISIBLE);
//        }else{
//            imgAttach.setVisibility(View.GONE);
//        }
        if (fromWhere == 1) {
            btnNext.setVisibility(View.GONE);
            if (liveOrLocal) {
                List<JobCardDetailsRealmDBModel> list = realmDBHelper.isJobCardInLocal(mRealm, Integer.parseInt(nextId));
                if (list != null && list.size() > 0) {
                    getImagesListFromDB();
                    getAudiosListFromDB();
                }else {
                    callJobCardIdDetailsService();
                }
            } else {
                getImagesListFromDB();
                getAudiosListFromDB();
            }
        }
    }

    private void callJobCardIdDetailsService() {
        mProgressBarHandler.show();
        JobCardDetailsRealmDBModel model1 = new JobCardDetailsRealmDBModel();
        model1.setAction("get_job_card_list_byid");
        model1.setUser_id(Integer.valueOf(loginUserId));
        model1.setShop_id(Integer.valueOf(loginShopId));
        model1.setId(Integer.parseInt(nextId));
        Call<APIListResponse<JobCardDetailsRealmDBModel>> call = RestAdapter.getInstance().getServiceApi().getNewJobCardIdDetails(model1);
        call.enqueue(new Callback<APIListResponse<JobCardDetailsRealmDBModel>>() {
            @Override
            public void onResponse(@NonNull Call<APIListResponse<JobCardDetailsRealmDBModel>> call,
                                   @NonNull Response<APIListResponse<JobCardDetailsRealmDBModel>> response) {
                mProgressBarHandler.hide();
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        List<JobCardDetailsRealmDBModel> result = response.body().getResult();
                        jobCardImageList = new ArrayList<>();
                        jobCardAudioList = new ArrayList<>();
                        List<AudioUploadModel> jobCardAudioListOne = new ArrayList<>();
                        jobCardImageList = result.get(0).getImages() != null ? result.get(0).getImages() : new ArrayList<>();
                        jobCardAudioListOne = result.get(0).getAudios() != null ? result.get(0).getAudios() : new ArrayList<>();
                        if (jobCardImageList != null && jobCardImageList.size() > 0) {
                            rvImages.setVisibility(View.VISIBLE);
                            setUpImageRecyclerView(jobCardImageList);
                        }else{
                            rvImages.setVisibility(View.GONE);
                        }
                        if (jobCardAudioListOne != null && jobCardAudioListOne.size() > 0) {
                            for (int i = 0; i < jobCardAudioListOne.size(); i++) {
                                if (jobCardAudioListOne.get(i).getImage_uniqueid().equals("0")) {
                                    AudioUploadModel audio = new AudioUploadModel();
                                    audio = jobCardAudioListOne.get(i);
                                    jobCardAudioList.add(audio);
                                }
                            }
                            if (jobCardAudioList != null && jobCardAudioList.size() > 0) {
                                rvAudios.setVisibility(View.VISIBLE);
                                setUpAudioRecyclerView(jobCardAudioList);
                            }else{
                             rvAudios.setVisibility(View.GONE);
                            }
                        }
                    }
                } else {
                }
            }

            @Override
            public void onFailure(Call<APIListResponse<JobCardDetailsRealmDBModel>> call, Throwable t) {
                mProgressBarHandler.hide();
                if (getApplicationContext() != null) {
                    String failure = t.getMessage().toString();
                    Log.d("onFailure Response", "" + failure);
                    if (BuildConfig.FLAVOR.equals("dev")) {
                        Utils.showToast(getApplicationContext(), getResources().getString(R.string.msg_servererror) + "--" + failure);
                    } else {
                        Utils.showToast(getApplicationContext(), getResources().getString(R.string.msg_servererror));
//                        saveAndCloseAlert(getApplicationContext(), getResources().getString(R.string.msg_servererror));
                    }

                }
            }
        });
    }

    public void addImageAlert() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(JobCardUpdateCameraImagesActivity.this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_pic_bcast, null);
        dialogBuilder.setView(dialogView);
        attachAlertDialog = dialogBuilder.create();
        attachAlertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation_upDown;
        attachAlertDialog.setCancelable(false);
        LinearLayout llCamera = dialogView.findViewById(R.id.ll_camera);
        LinearLayout llChooseFiles = dialogView.findViewById(R.id.ll_files);
        LinearLayout llAudio = dialogView.findViewById(R.id.ll_LAudio);
        Button btnCancel = dialogView.findViewById(R.id.btn_cancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attachAlertDialog.dismiss();
            }
        });
        llCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkCameraPermission()) {
                    dispatchTakePictureIntent(ACTION_TAKE_PHOTO_B_PIC);
                    attachAlertDialog.dismiss();
                }
            }
        });
        llChooseFiles.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFileChooser(FILE_SELECT_CODE_PIC);
                attachAlertDialog.dismiss();
            }
        });
        llAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attachAlertDialog.dismiss();
                showRecordingAlertDialog(0, "");
                if (checkRecordPermission()) {
                    if (mIsAudioMode) {
                        showAudioRecorder();
                        onRecord(true);
                    }
                }
            }
        });

        attachAlertDialog.show();
    }

    /*----------Audio play-------------*/
    public void showMediaPlayer(final String encodeString, final String audioName, final String filePath) {
        final String mediafile = encodeString + audioName;
        android.app.AlertDialog.Builder dialogBuilder = new android.app.AlertDialog.Builder(JobCardUpdateCameraImagesActivity.this);
        View dialogView = getLayoutInflater().inflate(R.layout.livechat_mediaplayer, null);
        dialogBuilder.setView(dialogView);
        final android.app.AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation_upDown;
        alertDialog.setCancelable(true);
        /*alertDialog.setTitle(R.string.app_name);
        alertDialog.setIcon(R.drawable.ic_action_speaker);*/
        final MediaPlayer mPlayer = new MediaPlayer();
        final TextView tv_audioname = dialogView.findViewById(R.id.tv_audioname);
        final Button playbtn = dialogView.findViewById(R.id.play);
        final Button pausebtn = dialogView.findViewById(R.id.pause);
        final Button stopbtn = dialogView.findViewById(R.id.stop);
        final Button close = dialogView.findViewById(R.id.close);
        tv_audioname.setText(audioName);
        playbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleActionPlayBack(mPlayer, encodeString, audioName, filePath, playbtn, pausebtn, alertDialog);
            }
        });
        pausebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPlayer.isPlaying()) {
                    mPlayer.pause();
                    pausebtn.setBackgroundResource(R.drawable.ic_action_play);
                } else {
                    mPlayer.start();
                    pausebtn.setBackgroundResource(R.drawable.ic_action_pause);
                }
                //handleActionPlayBack(mPlayer, mediafile, playbtn, pausebtn, alertDialog);
            }
        });
        stopbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleActionMediaStop(mPlayer, alertDialog);
            }
        });
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleActionMediaStop(mPlayer, alertDialog);
            }
        });
        alertDialog.show();
    }

    private void handleActionMediaStop(MediaPlayer mPlayer, android.app.AlertDialog alertDialog) {
        if (mPlayer.isPlaying()) {
            mPlayer.stop();
        }
        alertDialog.dismiss();
    }

    private void handleActionPlayBack(MediaPlayer mPlayer, String encodeString, String audioname, String filePath, final Button playbtn, final Button pausebtn, final android.app.AlertDialog alertDialog) {
        mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        try {
            FileOutputStream out = new FileOutputStream(new File(filePath));
            byte[] decoded = Base64.decode(encodeString, 0);

            out.write(decoded);
            out.close();
            mPlayer.setDataSource(filePath);
            mPlayer.prepare();
        } catch (Exception e) {
//            Toast.makeText(getApplicationContext(), "You might not set the URI correctly!", Toast.LENGTH_LONG).show();
        }
        mPlayer.start();
        pausebtn.setVisibility(View.VISIBLE);
        playbtn.setVisibility(View.GONE);
        mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {
//                alertDialog.dismiss();
                pausebtn.setVisibility(View.GONE);
                playbtn.setBackgroundResource(R.drawable.ic_action_play);
                playbtn.setVisibility(View.VISIBLE);
            }
        });
    }

    /*----------Audio play-------------*/
    private void startRecording() {
        try {
            mRecorder = new MediaRecorder();
            mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            mRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
            mRecorder.setOutputFile(mAudioFile.getAbsolutePath());
            mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);

            try {
                mRecorder.prepare();
            } catch (IOException e) {
                e.printStackTrace();
                Log.e(LOG_TAG, "prepare() failed");
            }

            mRecorder.start();
            mChronometer.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void stopRecording() {
        try {
            mRecorder.stop();
            mRecorder.release();
            mRecorder = null;
            mChronometer.stop();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideAudioRecorder() {
        mAudioRecCv.setVisibility(View.GONE);
        mChronometer.stop();
    }


    private void onRecord(boolean start) {
        if (start) {
            if (initAudioFile("1"))
                startRecording();
        } else {
            stopRecording();
        }
    }

    private void showAudioRecorder() {
        mAudioRecMsgTv.setText(R.string.Recording_msg);
        mAudioRecCancelBtn.setText(R.string.cancel);
        mAudioRecSendBtn.setText(R.string.Send);
        mIsAudioRecorderNotPlayer = true;
        mChronometer.setVisibility(View.VISIBLE);
        mChronometer.setBase(SystemClock.elapsedRealtime());
        mAudioRecCv.setVisibility(View.VISIBLE);
    }

    public void showRecordingAlertDialog(final Integer imgWithAudio, final String imgUniqueId) {
        android.app.AlertDialog.Builder dialogBuilder = new android.app.AlertDialog.Builder(JobCardUpdateCameraImagesActivity.this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.activity_livechatrecording, null);
        dialogBuilder.setView(dialogView);
        final android.app.AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation_upDown;
        alertDialog.setCancelable(false);
        mAudioRecCv = dialogView.findViewById(R.id.cv_audio);
        mChronometer = dialogView.findViewById(R.id.chronometer);
        mAudioRecMsgTv = dialogView.findViewById(R.id.tv_cv_msg);
        mAudioRecCancelBtn = dialogView.findViewById(R.id.btn_cv_cancel);
        mAudioRecSendBtn = (Button) dialogView.findViewById(R.id.btn_cv_send);
        mAudioRecCancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mIsAudioRecorderNotPlayer) {
                    onRecord(false);
                    hideAudioRecorder();
                }
                alertDialog.dismiss();
            }
        });
        mAudioRecSendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mIsAudioRecorderNotPlayer) {
                    onRecord(false);
                    hideAudioRecorder();
                    try {
                        // Audio file convert to encoded string then i'll store it as a string
                        FileInputStream in = new FileInputStream(mAudioFile);
                        byte fileContent[] = new byte[(int) mAudioFile.length()];

                        in.read(fileContent, 0, fileContent.length);
                        String encodedstring = Base64.encodeToString(fileContent, 0);
                        if (imgWithAudio == 0) {
                            uploadAudio(encodedstring, mAudioFile.getName(), mImageFileDir.getAbsolutePath().toString(), "0");
                        } else {
                            uploadAudio(encodedstring, mAudioFile.getName(), mImageFileDir.getAbsolutePath().toString(), imgUniqueId);
                        }
//                        showMediaPlayer(encodedstring, mAudioFile.getName());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                alertDialog.dismiss();
            }
        });
        mIsAudioMode = true;
        alertDialog.show();
    }

    private boolean initAudioFile(String orderid) {
        String fileName = AUDIO_FILE_PREFIX + "_" + new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH).format(new Date()) + AUDIO_FILE_SUFFIX;
        mAudioFile = new File(mImageFileDir.getAbsolutePath(), fileName);
        try {
            if (!mAudioFile.exists())
                Log.d(LOG_TAG, "file.createNewFile() = " + mAudioFile.createNewFile());
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), R.string.err_init_audio_file, Toast.LENGTH_LONG).show();
            return false;
        }
    }
    /*---------------------------------------Audio Upload --------------------------------------------*/
    /*------------------------------- Image Upload --------------------------------------------*/

    private void dispatchTakePictureIntent(int actionCode) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        switch (actionCode) {
            case ACTION_TAKE_PHOTO_B_PIC:
                try {
                    File f = setUpPhotoFile();
                    mCurrentPhotoPath = f.getAbsolutePath();
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, FileProvider.getUriForFile(getApplicationContext(), BuildConfig.APPLICATION_ID + ".provider", f));
                    break;
                } catch (IOException e) {
                    e.printStackTrace();
                    mCurrentPhotoPath = null;
                    break;
                }
        }
        startActivityForResult(takePictureIntent, actionCode);
    }

    private File setUpPhotoFile() throws IOException {
        File f = createImageFile();
        mCurrentPhotoPath = f.getAbsolutePath();
        return f;
    }

    private File createImageFile() throws IOException {
        return File.createTempFile(JPEG_FILE_PREFIX + new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH).format(new Date()) + "_", JPEG_FILE_SUFFIX, getAlbumDir());
    }

    private String getAlbumName() {
        return getString(R.string.app_name);
    }

    private File getAlbumDir() {
        File storageDir = null;
        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            storageDir = mAlbumStorageDirFactory.getAlbumStorageDir(getAlbumName());
            if (!(storageDir == null || storageDir.mkdirs() || storageDir.exists())) {
                Log.d("", "failed to create directory");
                return null;
            }
        }
        Log.v("STHAT", "External storage is not mounted READ/WRITE.");
        return storageDir;
    }

    private void initSthatAndAudioDirectory() {
        try {
            mImageFileDir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + CARSPA_DIRECTORY_PATH);
            mImageFileDir.mkdir();
            Log.d(LOG_TAG, "CARSPA_DIRECTORY_PATH : mImageFileDir = " + mImageFileDir + " mImageFileDir.getAbsolutePath = " + mImageFileDir.getAbsolutePath());
            mImageFileDir = new File(mImageFileDir.getAbsolutePath() + AUDIO_DIR_PATH);
            mImageFileDir.mkdir();
            Log.d(LOG_TAG, "AUDIO_DIR_PATH : mImageFileDir = " + mImageFileDir + " mImageFileDir.getAbsolutePath = " + mImageFileDir.getAbsolutePath());
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), R.string.err_init_directory, Toast.LENGTH_LONG).show();
        }
    }

    private void initVars() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO) {
            mAlbumStorageDirFactory = new FroyoAlbumDirFactory();
        } else {
            mAlbumStorageDirFactory = new BaseAlbumDirFactory();
        }
        mIVDimenPx = (int) DimenUtil.convertDpToPixel(80.0f, getApplicationContext());
    }

    private File saveBitmapToFile(File file) {
        try {
            BitmapFactory.Options o = new BitmapFactory.Options();
            FileInputStream inputStream = new FileInputStream(file);
            BitmapFactory.decodeStream(inputStream, null, o);
            inputStream.close();
            final int REQUIRED_SIZE = 100;
            int scale = 1;
            while (o.outWidth / scale / 2 >= REQUIRED_SIZE &&
                    o.outHeight / scale / 2 >= REQUIRED_SIZE) {
                scale *= 2;
            }
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = 4;
            inputStream = new FileInputStream(file);
            ;
            Bitmap selectedBitmap = BitmapFactory.decodeStream(inputStream, null, o2);
            inputStream.close();

            file.createNewFile();
            FileOutputStream outputStream = new FileOutputStream(file);

            selectedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);

            return file;
        } catch (Exception e) {
            return null;
        }
    }

    private File saveBitmapToRequriedFile(File inputFile, File outputFile) {
        try {
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = false;
            o.inSampleSize = 4;
            FileInputStream inputStream;
            int file_size = Integer.parseInt(String.valueOf(inputFile.length() / 1024));
            if (file_size > 1000) {
                inputStream = new FileInputStream(inputFile);
                BitmapFactory.decodeStream(inputStream, null, o);
                inputStream.close();
                final int REQUIRED_SIZE = 100;
                int scale = 1;
                while (o.outWidth / scale / 2 >= REQUIRED_SIZE &&
                        o.outHeight / scale / 2 >= REQUIRED_SIZE) {
                    scale *= 2;
                }
                BitmapFactory.Options o2 = new BitmapFactory.Options();
                o2.inSampleSize = scale;
                inputStream = new FileInputStream(inputFile);
                Bitmap selectedBitmap = BitmapFactory.decodeStream(inputStream, null, o2);
                inputStream.close();
                outputFile.createNewFile();
                FileOutputStream outputStream = new FileOutputStream(outputFile);
                selectedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
                return outputFile;
            } else {
                outputFile.delete();
                return inputFile;
            }
        } catch (Exception e) {
            return null;
        }
    }

    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE");
        mediaScanIntent.setData(Uri.fromFile(new File(mCurrentPhotoPath)));
        sendBroadcast(mediaScanIntent);
    }

    private void showFileChooser(int fileSelectActionCode) {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/jpeg");
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        try {
            startActivityForResult(Intent.createChooser(intent, getString(R.string.msg_get_file_appl)), fileSelectActionCode);
        } catch (android.content.ActivityNotFoundException e) {
            Toast.makeText(getApplicationContext(), R.string.err_no_filemanager, Toast.LENGTH_SHORT).show();
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intentData) {
        Log.d(TAG, "*********** onActivityResult *************** requestCode = " + requestCode
                + "   resultCode = " + resultCode);
        try {
            String filePath;
            switch (requestCode) {
                case ACTION_TAKE_PHOTO_B_PIC:
                    if (resultCode == Activity.RESULT_OK) {
                        Log.d(TAG, "*********** ACTION_TAKE_PHOTO_B...  mCurrentPhotoPath = " + mCurrentPhotoPath);
                        if (mCurrentPhotoPath != null) {
                            galleryAddPic();
                            mIsPictureSet = true;
                            mPictureFilePath = mCurrentPhotoPath;
                            mPicFile = new File(mCurrentPhotoPath);
                            saveBitmapToFile(mPicFile);
                            /*if (Utils.getInstance(JobCardCameraImagesActivity.this).isOnline()) {
//                                uploadImage(mPicFile);
                            } else {*/
                            String encodedString = getBase64String(mCurrentPhotoPath);
                            uploadImage(encodedString, mCurrentPhotoPath);
//                            decodeBase64AndSetImage(encodedString, imgAdded);
//                                showUploadAlertDialog(0, getResources().getString(R.string.msg_networkcheck), 3);
                            /*}*/
                            Log.d(TAG, "mPictureFilePath : " + mPictureFilePath);
                            mCurrentPhotoPath = null;
                        }
                    }
                    break;
                case FILE_SELECT_CODE_PIC:
                    if (resultCode == Activity.RESULT_OK) {
                        filePath = FileUtils.getAcceptableJpegFilePath(intentData, getApplicationContext());
                        if (filePath != null) {
                            mIsPictureSet = true;
                            File mInputFilePath = new File(filePath);
                            setUpPhotoFile();
                            File mOutpuFilePath = new File(mCurrentPhotoPath);
                            mPicFile = saveBitmapToRequriedFile(mInputFilePath, mOutpuFilePath);
                            /*if (Utils.getInstance(JobCardCameraImagesActivity.this).isOnline()) {
//                                uploadImage(mPicFile);
                            } else {*/
                            String encodedString = getBase64String(mPicFile.getAbsolutePath());
                            uploadImage(encodedString, mPicFile.getAbsolutePath());
//                            decodeBase64AndSetImage(encodedString, imgAdded);
//                                showUploadAlertDialog(0, getResources().getString(R.string.msg_networkcheck), 3);
                            /*}*/
                            Log.d(TAG, "File, mPicture1FilePath: " + filePath);
                        }
                    }
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void uploadAudio(String encodedString, String filename, String filePath, String imgUniqueId) {
        if (liveOrLocal) {
            List<JobCardDetailsRealmDBModel> list = realmDBHelper.isJobCardInLocal(mRealm, Integer.parseInt(nextId));
            if (list != null && list.size() > 0) {
                offLineAudioUpload(encodedString, filename, filePath, imgUniqueId);
            }else {
                AudioUploadModel model1 = new AudioUploadModel();
                model1.setAction("set_audio_upload");
                model1.setUser_id(Integer.valueOf(loginUserId));
                model1.setShop_id(Integer.valueOf(loginShopId));
                model1.setId(Integer.parseInt(nextId));
                model1.setAudio_encode_str(encodedString);
                model1.setAudio_filename(filename);
                model1.setAudio_filepath(filePath);
                model1.setImage_uniqueid(imgUniqueId);
                if (imgUniqueId.equals("0")) {
                    model1.setAudio_status(0);
                } else {
                    model1.setAudio_status(1);
                }
                callUploadAudioService(model1);
            }
        } else {
            offLineAudioUpload(encodedString, filename, filePath, imgUniqueId);
        }
    }

    private void offLineAudioUpload(String encodedString, String filename, String filePath, String imgUniqueId){
        mProgressBarHandler.show();
        String audioUniqueId = UUID.randomUUID().toString();
        String fName = filename.substring(filename.lastIndexOf("/") + 1);
        if (nextId != null && loginUserId != null && loginShopId != null && encodedString != null && !fName.isEmpty()) { //uniqueId != null &&
            AudioUploadModel audioUpload = new AudioUploadModel();
            audioUpload.setLocaldbid(audioUniqueId);
            audioUpload.setId(Integer.parseInt(nextId));
            audioUpload.setUser_id(Integer.parseInt(loginUserId));
            audioUpload.setShop_id(Integer.parseInt(loginShopId));
            audioUpload.setAudio_encode_str(encodedString);
            audioUpload.setAudio_filename(fName);
            audioUpload.setAudio_filepath(filePath + "/" + filename);
            audioUpload.setImage_uniqueid(imgUniqueId);
            audioUpload.setLocal_insert("1");
            audioUpload.setSync_status("false");
            audioUpload.setRow_deleted(0);
            ReturnRealmModel realmModel = realmDBHelper.addAudios(mRealm, audioUpload);
            if (realmModel.getAudioUploadStatus() == 1) {
                showAlertDialog("Audio upload successfully");
                getAudiosListFromDB();
            } else {
                showAlertDialog("Audio upload failed");
            }
            mProgressBarHandler.hide();
        } else {

        }
    }

    private void uploadImage(String encodedString, String filename) {
        if (liveOrLocal) {
            List<JobCardDetailsRealmDBModel> list = realmDBHelper.isJobCardInLocal(mRealm, Integer.parseInt(nextId));
            if (list != null && list.size() > 0) {
                offLineImageUpload(encodedString, filename);
            }else {
                ImageUploadModel model1 = new ImageUploadModel();
                model1.setAction("set_image_upload");
                model1.setUser_id(Integer.valueOf(loginUserId));
                model1.setShop_id(Integer.valueOf(loginShopId));
                model1.setId(Integer.parseInt(nextId));
                model1.setImage_encode_str(encodedString);
                model1.setImage_filename(filename);
                model1.setCar_parts_id(0);
                callUploadImagesService(model1);
            }
        } else {
            offLineImageUpload(encodedString, filename);
        }
    }

    private void offLineImageUpload(String encodedString, String filename){
        mProgressBarHandler.show();
        String localDbId = UUID.randomUUID().toString();
        String imgUniqueId = UUID.randomUUID().toString();
        String fName = filename.substring(filename.lastIndexOf("/") + 1);
        if (nextId != null && loginUserId != null && loginShopId != null && encodedString != null && !fName.isEmpty()) { //uniqueId != null &&
            ImageUploadModel imgUpload = new ImageUploadModel();
            imgUpload.setLocaldbid(localDbId);
            imgUpload.setId(Integer.parseInt(nextId));
            imgUpload.setUser_id(Integer.parseInt(loginUserId));
            imgUpload.setShop_id(Integer.parseInt(loginShopId));
            imgUpload.setImage_encode_str(encodedString);
            imgUpload.setImage_filename(fName);
            imgUpload.setImage_carpartname("");
            imgUpload.setImage_uniqueid(imgUniqueId);
            imgUpload.setLocal_insert("1");
            imgUpload.setSync_status("false");
            imgUpload.setRow_deleted(0);
            ReturnRealmModel realmModel = realmDBHelper.addImages(mRealm, imgUpload);
            if (realmModel.getImageUploadStatus() == 1) {
                showAlertDialog("Image upload successfully");
                getImagesListFromDB();
            } else {
                showAlertDialog("Image upload failed");
            }
            mProgressBarHandler.hide();
        } else {

        }
    }

    private void goToPreviewPage() {
        Intent i = new Intent(JobCardUpdateCameraImagesActivity.this, JobCardUpdatePreviewActivity.class);
        i.putExtra("JobCardUniqueId", uniqueId);
        i.putExtra("NextId", nextId);// jobCardId
        startActivity(i);
        overridePendingTransitionEnter();
        finish();
    }

    private String getBase64String(String imageFile) {
        // give your image file url in mCurrentPhotoPath
        Bitmap bitmap = BitmapFactory.decodeFile(imageFile);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        // In case you want to compress your image, here it's at 40%
        bitmap.compress(Bitmap.CompressFormat.JPEG, 40, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(byteArray, Base64.DEFAULT);
    }

    private void decodeBase64AndSetImage(String completeImageData, ImageView imageView) {
        // Incase you're storing into aws or other places where we have extension stored in the starting.
        String imageDataBytes = completeImageData.substring(completeImageData.indexOf(",") + 1);
        InputStream stream = new ByteArrayInputStream(Base64.decode(imageDataBytes.getBytes(), Base64.DEFAULT));
        Bitmap bitmap = BitmapFactory.decodeStream(stream);
        imageView.setImageBitmap(bitmap);
    }
    /*------------------------------- Image Upload --------------------------------------------*/

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public boolean checkCameraPermission() {
        final int PERMISSIONS_REQUEST_CAMERA = 125;
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(JobCardUpdateCameraImagesActivity.this, new String[]{Manifest.permission.CAMERA}, PERMISSIONS_REQUEST_CAMERA);
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public boolean checkRecordPermission() {
        final int PERMISSIONS_REQUEST_RECORD_AUDIO = 126;
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(JobCardUpdateCameraImagesActivity.this, new String[]{Manifest.permission.RECORD_AUDIO}, PERMISSIONS_REQUEST_RECORD_AUDIO);
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    @Override
    public void imagedelete(int position, String imgName) {
        showDeleteAlert(position);
    }

    public void showDeleteAlert(Integer position) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(JobCardUpdateCameraImagesActivity.this);
        alertDialog.setTitle("Confirm Delete...");
        alertDialog.setMessage("Are you sure you want delete this?");
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                deleteGalleryImg(position);
            }
        });
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertDialog.show();
    }

    private void deleteGalleryImg(int position) {
        if (liveOrLocal) {
            List<JobCardDetailsRealmDBModel> list = realmDBHelper.isJobCardInLocal(mRealm, Integer.parseInt(nextId));
            if (list != null && list.size() > 0) {
                offLineImageDelete(position);
            }else{
                ImageUploadModel model = new ImageUploadModel();
                model.setAction("delete_jobcard_image");
                model.setUser_id(Integer.valueOf(loginUserId));
                model.setShop_id(Integer.valueOf(loginShopId));
                model.setImage_uniqueid(jobCardImageList.get(position).getImage_uniqueid());
                model.setId(Integer.parseInt(nextId));
                callImageDeleteService(model);
            }
        } else {
            offLineImageDelete(position);
        }
    }

    private void offLineImageDelete(int position){
        mProgressBarHandler.show();
        ReturnRealmModel realmModel = realmDBHelper.deleteImageFromId(mRealm, Integer.parseInt(nextId), position);
        if (realmModel.getImagedeletedStatus() == 1) {
            mProgressBarHandler.hide();
            if (realmModel.getAfterDeleteImgSize() > 0) {
                getImagesListFromDB();
            } else {
                rvImages.setVisibility(View.GONE);
            }
        } else {
            mProgressBarHandler.hide();
        }
    }

    private void getAudiosListFromDB() {
        jobCardAudioList = new ArrayList<>();
        jobCardAudioList = realmDBHelper.getAudiosFromId(mRealm, Integer.parseInt(nextId));
        if (jobCardAudioList != null && jobCardAudioList.size() > 0) {
            rvAudios.setVisibility(View.VISIBLE);
            setUpAudioRecyclerView(jobCardAudioList);
        }else{
            rvAudios.setVisibility(View.GONE);
        }
    }

    private void getImagesListFromDB() {
        jobCardImageList = new ArrayList<>();
        jobCardImageList = realmDBHelper.getImagesFromId(mRealm, Integer.parseInt(nextId));
        if (jobCardImageList != null && jobCardImageList.size() > 0) {
            rvImages.setVisibility(View.VISIBLE);
            setUpImageRecyclerView(jobCardImageList);
        }else{
            rvImages.setVisibility(View.GONE);
        }
    }

    @Override
    public void imageItem(int position, Integer id, String imgUniqueId, String imgName, String imagePath) {
        Intent intent = new Intent(JobCardUpdateCameraImagesActivity.this, FullScreenActivity.class);
        intent.putExtra("ImageUrl", imgName);
        intent.putExtra("nextId", nextId);
        intent.putExtra(Constant.IMGID, String.valueOf(id));
        intent.putExtra(Constant.IMGUNIQUEID, imgUniqueId);
        startActivity(intent);
        overridePendingTransitionEnter();
    }

    private void showCarParts(String imgUniqueId) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(JobCardUpdateCameraImagesActivity.this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.carparts_custom_layout, null);
        dialogBuilder.setView(dialogView);
        carPartsAlertDialog = dialogBuilder.create();
        carPartsAlertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation_upDown;
        carPartsAlertDialog.setCancelable(false);
        TextView tvAddAudio = dialogView.findViewById(R.id.tv_addaudio);
        RecyclerView rvCarParts = dialogView.findViewById(R.id.rv_carparts);
        if (cPartsList != null && cPartsList.size() > 0) {
            setUpCarPartsRecyclerView(rvCarParts, imgUniqueId);
        }
        ImageView imgClose = dialogView.findViewById(R.id.img_close);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                carPartsAlertDialog.dismiss();
            }
        });
        carPartsAlertDialog.show();
    }

    private void setUpCarPartsRecyclerView(RecyclerView rvCarPart, String selectedImgUniqueId) {
        cLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        carPartAdapter = new CarPartsItemsAdapter(getApplicationContext(), cPartsList, carPartsAlertDialog, selectedImgUniqueId);
        rvCarPart.setAdapter(carPartAdapter);
        rvCarPart.setLayoutManager(cLayoutManager);
        rvCarPart.setItemAnimator(new DefaultItemAnimator());
//        carPartAdapter.setOnItemClickListener(this);
        carPartAdapter.setCallback(this);
        carPartAdapter.notifyDataSetChanged();
    }

    @Override
    public void addAudio(int position, String imgUniqueId) {
//        Utils.showToast(getApplicationContext(), imgUniqueId);
        showRecordingAlertDialog(1, imgUniqueId);
        if (checkRecordPermission()) {
            if (mIsAudioMode) {
                showAudioRecorder();
                onRecord(true);
            }
        }
    }

    @Override
    public void addCarPart(int position, Context context, String imgUniqueId) {
        showCarParts(imgUniqueId);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK
                && event.getRepeatCount() == 0) {
            onBackPressed();
            overridePendingTransitionExit();
        }
        return super.onKeyDown(keyCode, event);
    }

    private void goToEdit() {
        Intent i = new Intent(JobCardUpdateCameraImagesActivity.this, UpdateJobCardActivity.class);
        i.putExtra("JobCardUniqueId", uniqueId);
        i.putExtra("NextId", nextId);
        startActivity(i);
        overridePendingTransitionExit();
        finish();
    }

    @Override
    public void onBackPressed() {
        if (BuildConfig.FLAVOR.equalsIgnoreCase("beta") || BuildConfig.FLAVOR.equalsIgnoreCase("dev")) {
            if (fromWhere == 1) {
                goToEdit();
            } else {
                finish();
            }
        } else {
            if (fromWhere == 1) {
                goToEdit();
            } else {
                closeAlert(JobCardUpdateCameraImagesActivity.this, getResources().getString(R.string.app_newjobcardpage));
            }
        }
    }

    @Override
    public void audiodelete(int position, String audName, String imageUniqueId, Integer audioUniqueId) {
        if (liveOrLocal) {
            List<JobCardDetailsRealmDBModel> list = realmDBHelper.isJobCardInLocal(mRealm, Integer.parseInt(nextId));
            if (list != null && list.size() > 0) {
                offLineAudioDelete(position, audName);
            }else{
                AudioUploadModel model1 = new AudioUploadModel();
                model1.setAction("delete_jobcard_audio");
                model1.setUser_id(Integer.valueOf(loginUserId));
                model1.setShop_id(Integer.valueOf(loginShopId));
                model1.setId(Integer.parseInt(nextId));
                model1.setImage_uniqueid(imageUniqueId);
                model1.setAudio_uniqueid(audioUniqueId);
                callAudioDeleteService(model1);
            }
        }else {
            offLineAudioDelete(position, audName);
        }
    }

    private void offLineAudioDelete(int position, String audName){
        mProgressBarHandler.show();
        ReturnRealmModel realmModel = realmDBHelper.deleteAudioFromId(mRealm, Integer.parseInt(nextId), position);
        if (realmModel.getAudiodeletedStatus() == 1) {
            mProgressBarHandler.hide();
            if (realmModel.getAfterDeleteAudSize() > 0) {
                getAudiosListFromDB();
            } else {
                rvAudios.setVisibility(View.GONE);
            }
        } else {
            mProgressBarHandler.hide();
        }
    }

    @Override
    public void audioItem(int position, String encodestring, String audioName, String filePath) {
        showMediaPlayer(encodestring, audioName, filePath);
    }

    @Override
    public void getSelectedCarPart(int position, Integer carPartId, String carPartName, AlertDialog carPartDialog, String selectedImgUniqueId) {
        if (carPartDialog != null) {
            carPartDialog.dismiss();
        }
        if (liveOrLocal) {
            List<JobCardDetailsRealmDBModel> list = realmDBHelper.isJobCardInLocal(mRealm, Integer.parseInt(nextId));
            if (list != null && list.size() > 0) {
                offLineCarPartUpdated(carPartName, selectedImgUniqueId);
            }else {
                ImageUploadModel model1 = new ImageUploadModel();
                model1.setAction("update_image_carparts");
                model1.setUser_id(Integer.valueOf(loginUserId));
                model1.setShop_id(Integer.valueOf(loginShopId));
                model1.setId(Integer.parseInt(nextId));
                model1.setImage_uniqueid(selectedImgUniqueId);
                model1.setCar_parts_id(carPartId);
                callUploadImagesService(model1);
            }
        } else {
            offLineCarPartUpdated(carPartName, selectedImgUniqueId);
        }
    }

    private void offLineCarPartUpdated(String carPartName, String selectedImgUniqueId){
        ReturnRealmModel realmModel = realmDBHelper.UpdateCarPartImages(mRealm, selectedImgUniqueId, carPartName);
        if (realmModel.getImageCarPartUpdateStatus() == 1) {
            getImagesListFromDB();
            Utils.showToast(getApplicationContext(), "Updated Successfully");
        }
    }

    private void callUploadImagesService(ImageUploadModel model1) {
        mProgressBarHandler.show();
        Call<APIResponse<ImageUploadModel>> call = RestAdapter.getInstance().getServiceApi().uploadImageToServer(model1);
        call.enqueue(new Callback<APIResponse<ImageUploadModel>>() {
            @Override
            public void onResponse(@NonNull Call<APIResponse<ImageUploadModel>> call,
                                   @NonNull Response<APIResponse<ImageUploadModel>> response) {
                mProgressBarHandler.hide();
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        Utils.showToast(getApplicationContext(), "Updated successfully");
                        callJobCardIdDetailsService();
                    }
                } else {
                    Utils.showToast(getApplicationContext(), "" + response.code() + " - " + response.message());
                }
            }

            @Override
            public void onFailure(Call<APIResponse<ImageUploadModel>> call, Throwable t) {
                mProgressBarHandler.hide();
                if (getApplicationContext() != null) {
                    String failure = t.getMessage().toString();
                    Log.d("onFailure Response", "" + failure);
                    if (BuildConfig.FLAVOR.equals("dev")) {
                        Utils.showToast(getApplicationContext(), getResources().getString(R.string.msg_servererror) + "--" + failure);
                    } else {
                        Utils.showToast(getApplicationContext(), getResources().getString(R.string.msg_servererror));
//                        saveAndCloseAlert(getApplicationContext(), getResources().getString(R.string.msg_servererror));
                    }

                }
            }
        });
    }

    private void callImageDeleteService(ImageUploadModel model1) {
        mProgressBarHandler.show();
        Call<APIResponse<ImageUploadModel>> call = RestAdapter.getInstance().getServiceApi().uploadImageToServer(model1);
        call.enqueue(new Callback<APIResponse<ImageUploadModel>>() {
            @Override
            public void onResponse(@NonNull Call<APIResponse<ImageUploadModel>> call,
                                   @NonNull Response<APIResponse<ImageUploadModel>> response) {
                mProgressBarHandler.hide();
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        Utils.showToast(getApplicationContext(), "Deleted successfully");
                        callJobCardIdDetailsService();
                    }
                } else {
                    Utils.showToast(getApplicationContext(), "" + response.code() + " - " + response.message());
                }
            }

            @Override
            public void onFailure(Call<APIResponse<ImageUploadModel>> call, Throwable t) {
                mProgressBarHandler.hide();
                if (getApplicationContext() != null) {
                    String failure = t.getMessage().toString();
                    Log.d("onFailure Response", "" + failure);
                    if (BuildConfig.FLAVOR.equals("dev")) {
                        Utils.showToast(getApplicationContext(), getResources().getString(R.string.msg_servererror) + "--" + failure);
                    } else {
                        Utils.showToast(getApplicationContext(), getResources().getString(R.string.msg_servererror));
//                        saveAndCloseAlert(getApplicationContext(), getResources().getString(R.string.msg_servererror));
                    }

                }
            }
        });
    }

    private void callUploadAudioService(AudioUploadModel model1) {
        mProgressBarHandler.show();
        Call<APIResponse<AudioUploadModel>> call = RestAdapter.getInstance().getServiceApi().uploadaudioToServer(model1);
        call.enqueue(new Callback<APIResponse<AudioUploadModel>>() {
            @Override
            public void onResponse(@NonNull Call<APIResponse<AudioUploadModel>> call,
                                   @NonNull Response<APIResponse<AudioUploadModel>> response) {
                mProgressBarHandler.hide();
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        Utils.showToast(getApplicationContext(), "Updated successfully");
                        callJobCardIdDetailsService();
                    }
                } else {
                    Utils.showToast(getApplicationContext(), "" + response.code() + " - " + response.message());
                }
            }

            @Override
            public void onFailure(Call<APIResponse<AudioUploadModel>> call, Throwable t) {
                mProgressBarHandler.hide();
                if (getApplicationContext() != null) {
                    String failure = t.getMessage().toString();
                    Log.d("onFailure Response", "" + failure);
                    if (BuildConfig.FLAVOR.equals("dev")) {
                        Utils.showToast(getApplicationContext(), getResources().getString(R.string.msg_servererror) + "--" + failure);
                    } else {
                        Utils.showToast(getApplicationContext(), getResources().getString(R.string.msg_servererror));
//                        saveAndCloseAlert(getApplicationContext(), getResources().getString(R.string.msg_servererror));
                    }

                }
            }
        });
    }

    private void callAudioDeleteService(AudioUploadModel model1) {
        mProgressBarHandler.show();
        Call<APIResponse<AudioUploadModel>> call = RestAdapter.getInstance().getServiceApi().uploadaudioToServer(model1);
        call.enqueue(new Callback<APIResponse<AudioUploadModel>>() {
            @Override
            public void onResponse(@NonNull Call<APIResponse<AudioUploadModel>> call,
                                   @NonNull Response<APIResponse<AudioUploadModel>> response) {
                mProgressBarHandler.hide();
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        Utils.showToast(getApplicationContext(), "Deleted successfully");
                        callJobCardIdDetailsService();
                    }
                } else {
                    Utils.showToast(getApplicationContext(), "" + response.code() + " - " + response.message());
                }
            }

            @Override
            public void onFailure(Call<APIResponse<AudioUploadModel>> call, Throwable t) {
                mProgressBarHandler.hide();
                if (getApplicationContext() != null) {
                    String failure = t.getMessage().toString();
                    Log.d("onFailure Response", "" + failure);
                    if (BuildConfig.FLAVOR.equals("dev")) {
                        Utils.showToast(getApplicationContext(), getResources().getString(R.string.msg_servererror) + "--" + failure);
                    } else {
                        Utils.showToast(getApplicationContext(), getResources().getString(R.string.msg_servererror));
//                        saveAndCloseAlert(getApplicationContext(), getResources().getString(R.string.msg_servererror));
                    }

                }
            }
        });
    }
}
