package com.clt.newcarspa.activity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.clt.newcarspa.Adapter.EditJobTypeItemsAdapter;
import com.clt.newcarspa.R;
import com.clt.newcarspa.Utils.Constant;
import com.clt.newcarspa.Utils.ProgressBarHandler;
import com.clt.newcarspa.Utils.Utils;
import com.clt.newcarspa.Utils.Validation;
import com.clt.newcarspa.cltinterfaces.OnItemClickListener;
import com.clt.newcarspa.database.RealmDBHelper;
import com.clt.newcarspa.model.JobCardDetailsModel;
import com.clt.newcarspa.model.JobCardDetailsRealmDBModel;
import com.clt.newcarspa.model.JobCardItemsDetailsModel;
import com.clt.newcarspa.model.JobTypeModel;
import com.clt.newcarspa.model.ReturnRealmModel;
import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import io.realm.Realm;

/**
 * Created by Karthik Thumathy on 03-Aug-18.
 */
public class EditJobCardActivity extends BaseActivity implements OnItemClickListener, EditJobTypeItemsAdapter.ButtonAdapterCallback {

    private Realm mRealm;
    private Toolbar toolbar;
    private FloatingActionButton fab;
    private TextView toolbarTitle, tvTotalAmount, tvCurrentDate, tvCurrentTime, tvDeliveryDate;
    private EditText etCustomerCode, etVatRegisterNo, etName, etPostBoxNo, etCity, etTelephoneNo,
            etMobileNo, etFaxNo, etEmailId, etVehicleRegisterNo,
            etVehicleModel, etVehicleChasisNo, etVehicleColors, etVehicleFuel, etVehicleOil, etVehicleKilometer, etServiceDeliverydate;
    private ImageView imgGallery;
    private RadioGroup rgServiceJobType;
    private RadioButton rbServiceDayJob, rbServiceNightJob;
    private String getRGJobType;
    private Button btnNext;
    private RecyclerView jobTyperRecyclerview;
    private EditJobTypeItemsAdapter jobTypeAdapter;
    private LinearLayoutManager mLayoutManager;
    private RealmDBHelper realmDBHelper;
    private ProgressBarHandler mProgressBarHandler;
    private double grandTotal = 0;
    private int mYear, mMonth, mDay, mHour, mMinute;
    private List<JobTypeModel> selectedJobTypeItemsList = new ArrayList<>();
    List<JobCardDetailsRealmDBModel> jobCard = new ArrayList<>();
    List<JobCardItemsDetailsModel> jobCardItem = new ArrayList<>();
    List<JobTypeModel> jobTypeList = new ArrayList<>();
    private JobTypeModel jobTypeObj = new JobTypeModel();
    private String customerCode, vatRegisterNo, name, postBoxNo, city, telephoneNo,
            mobileNo, faxNo, emailId, vehicleRegisterNo,
            vehicleModel, vehicleChasisNo, vehicleColors, serviceDeliverydate,
            totalAmount, currentDate, currentTime, deliveryDate, dateAdded, localInsert, syncStatus, jsonItems, status;
    private String uniqueId, nextId;
    private Integer fromEdit = 1;

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mRealm.close();
//        realmDBHelper.realmDbClose();
    }

    @Override
    protected void onStop() {
        super.onStop();
//        realmDBHelper.realmDbClose();
    }

    @Override
    protected void onResume() {
        super.onResume();
//        realmDBHelper.with(EditJobCardActivity.this);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_newjob_parent);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        mRealm = Realm.getDefaultInstance();
        realmDBHelper = new RealmDBHelper(EditJobCardActivity.this);
        mProgressBarHandler = new ProgressBarHandler(this);
        initializeCollapsingLayout();
        initView();
        setListeners();
        getBundle();
    }

    private void initializeCollapsingLayout() {
        toolbar = findViewById(R.id.toolbar);
        fab = findViewById(R.id.fab);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        CollapsingToolbarLayout collapsingToolbar = findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setTitle(getResources().getString(R.string.app_editjobcardpage));
        loadBackdrop();
    }

    private void loadBackdrop() {
        final ImageView imageView = findViewById(R.id.backdrop);
        Glide.with(this).load(getResources().getDrawable(R.drawable.carbg14)).apply(RequestOptions.fitCenterTransform()).into(imageView);
    }

    private void initView() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
//        toolbarTitle = findViewById(R.id.tv_toolbarTitle);
//        imgGallery = findViewById(R.id.img_gallery);
//        imgGallery.setVisibility(View.VISIBLE);
        etCustomerCode = findViewById(R.id.et_jc_customercode);
        etVatRegisterNo = findViewById(R.id.et_jc_vatregno);
        etName = findViewById(R.id.et_jc_name);
        etPostBoxNo = findViewById(R.id.et_jc_postbox);
        etCity = findViewById(R.id.et_jc_city);
        etTelephoneNo = findViewById(R.id.et_jc_telno);
        etMobileNo = findViewById(R.id.et_jc_mobileno);
        etFaxNo = findViewById(R.id.et_jc_faxno);
        etEmailId = findViewById(R.id.et_jc_email);
        tvCurrentDate = findViewById(R.id.tv_currentdate);
        tvCurrentTime = findViewById(R.id.tv_currenttime);
        etVehicleRegisterNo = findViewById(R.id.et_vi_regno);
        etVehicleModel = findViewById(R.id.et_vi_model);
        etVehicleChasisNo = findViewById(R.id.et_vi_chasisno);
        etVehicleColors = findViewById(R.id.et_vi_color);
        etVehicleFuel = findViewById(R.id.et_vi_fuel);
        etVehicleOil = findViewById(R.id.et_vi_oil);
        etVehicleKilometer = findViewById(R.id.et_vi_kilometer);
        jobTyperRecyclerview = findViewById(R.id.rv_jobtype);
        tvDeliveryDate = findViewById(R.id.tv_sa_deliverydate);
        rgServiceJobType = findViewById(R.id.rg_daynightgroup);
        rbServiceDayJob = findViewById(R.id.rb_dayjob);
        rbServiceNightJob = findViewById(R.id.rb_nightjob);
        tvTotalAmount = findViewById(R.id.tv_totalamt);
        btnNext = findViewById(R.id.btn_sendjobcard);
        showToolBarBackBtn();
        onHandleUI();
    }

    public void showToolBarBackBtn() {
        hideToolbarAppName();
        if (getSupportActionBar() != null) {
            toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_action_back_btn));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    private void setListeners() {
        rgServiceJobType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // find which radio button is selected
                if (checkedId == R.id.rb_dayjob) {
                    Utils.showLogD("choice: DayJob");
                } else {
                    Utils.showLogD("choice: NightJob");
                }
            }

        });
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goTofinish();
            }
        });
        tvCurrentDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentDatePicker(tvCurrentDate);
            }
        });
        tvDeliveryDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentDatePicker(tvDeliveryDate);
            }
        });
        tvCurrentTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentTimePicker(tvCurrentTime);
            }
        });
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToCarViewPage();
            }
        });
    }


    private void goToCarViewPage() {
        Intent i = new Intent(EditJobCardActivity.this, JobCardCameraImagesActivity.class);
        i.putExtra("JobCardUniqueId", uniqueId);
        i.putExtra("NextId", nextId);// jobCardId
        i.putExtra("fromWhere", fromEdit);
        startActivity(i);
        overridePendingTransitionEnter();
        finish();
    }


    private void goTofinish() {
        getRGJobType = getDayNightJob();
        Date date = null;
//        Utils.showToast(getApplicationContext(), "jobtype--" + getRGJobType);
        Calendar c = Calendar.getInstance();
        SimpleDateFormat cd = new SimpleDateFormat("dd-MM-yyyy");
        String createDate = cd.format(c.getTime());
        date = stringToDate(createDate);
        String dateString = stringToString(createDate);
       /* try {
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
            date = formatter.parse(createDate);
        }catch (Exception e){
            e.printStackTrace();
        }*/
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String formattedDate = df.format(c.getTime());
        if (Validation.getInstance().isStringEmpty(getResources().getString(R.string.errmsg_jc_customercode), etCustomerCode, EditJobCardActivity.this)
//                && Validation.getInstance().isStringEmpty(getResources().getString(R.string.errmsg_jc_vatregisterno), etVatRegisterNo, EditJobCardActivity.this)
                && Validation.getInstance().isStringEmpty(getResources().getString(R.string.errmsg_jc_name), etName, EditJobCardActivity.this)
                && Validation.getInstance().isStringEmpty(getResources().getString(R.string.errmsg_jc_postbox), etPostBoxNo, EditJobCardActivity.this)
                && Validation.getInstance().isStringEmpty(getResources().getString(R.string.errmsg_jc_city), etCity, EditJobCardActivity.this)
//                && Validation.getInstance().isStringEmpty(getResources().getString(R.string.errmsg_jc_telephoneno), etTelephoneNo, EditJobCardActivity.this)
//                && Validation.getInstance().isStringEmpty(getResources().getString(R.string.errmsg_jc_mobileno), etMobileNo, EditJobCardActivity.this)
//                && Validation.getInstance().isStringEmpty(getResources().getString(R.string.errmsg_jc_faxno), etFaxNo, EditJobCardActivity.this)
                && Validation.getInstance().isStringEmpty(getResources().getString(R.string.errmsg_jc_email), etEmailId, EditJobCardActivity.this)
                && Validation.getInstance().email(etEmailId, EditJobCardActivity.this)
                && Validation.getInstance().isStringEmptyTextview(getResources().getString(R.string.errmsg_jc_currentdate), tvCurrentDate, EditJobCardActivity.this)
                && Validation.getInstance().isStringEmptyTextview(getResources().getString(R.string.errmsg_jc_currenttime), tvCurrentTime, EditJobCardActivity.this)
                && Validation.getInstance().isStringEmpty(getResources().getString(R.string.errmsg_jc_vi_regno), etVehicleRegisterNo, EditJobCardActivity.this)
                && Validation.getInstance().isStringEmpty(getResources().getString(R.string.errmsg_jc_vi_model), etVehicleModel, EditJobCardActivity.this)
                && Validation.getInstance().isStringEmpty(getResources().getString(R.string.errmsg_jc_vi_chasisno), etVehicleChasisNo, EditJobCardActivity.this)
                && Validation.getInstance().isStringEmpty(getResources().getString(R.string.errmsg_jc_vi_color), etVehicleColors, EditJobCardActivity.this)
                && Validation.getInstance().isStringEmpty(getResources().getString(R.string.errmsg_jc_vi_fuel), etVehicleFuel, EditJobCardActivity.this)
                && Validation.getInstance().isStringEmpty(getResources().getString(R.string.errmsg_jc_vi_oil), etVehicleOil, EditJobCardActivity.this)
                && Validation.getInstance().isStringEmpty(getResources().getString(R.string.errmsg_jc_vi_kilometer), etVehicleKilometer, EditJobCardActivity.this)
                && Validation.getInstance().isStringEmptyTextview(getResources().getString(R.string.errmsg_jc_deliverydate), tvDeliveryDate, EditJobCardActivity.this)) {
//            if (etTelephoneNo.getText().toString().length() >= 10) {
//                if (etMobileNo.getText().toString().length() >= 10) {
//                    if (etFaxNo.getText().toString().length() >= 10) {
                        if (selectedJobTypeItemsList != null || selectedJobTypeItemsList.size() > 0) {
                            if (grandTotal > 0.0) {
                                mProgressBarHandler.show();
                                String customerCode = etCustomerCode.getText().toString().trim();
                                String vatRegisterNo = etVatRegisterNo.getText().toString().trim();
                                String name = etName.getText().toString().trim();
                                String postBoxNo = etPostBoxNo.getText().toString().trim();
                                String city = etCity.getText().toString().trim();
                                String telephoneNo = etTelephoneNo.getText().toString().trim();
                                String mobileNo = etMobileNo.getText().toString().trim();
                                String faxNo = etFaxNo.getText().toString().trim();
                                String emailId = etEmailId.getText().toString().trim();
                                String currentDate = tvCurrentDate.getText().toString().trim();
                                String currentTime = tvCurrentTime.getText().toString().trim();
                                String vehicleRegisterNo = etVehicleRegisterNo.getText().toString().trim();
                                String vehicleModel = etVehicleModel.getText().toString().trim();
                                String vehicleChasisNo = etVehicleChasisNo.getText().toString().trim();
                                String vehicleColors = etVehicleColors.getText().toString().trim();
                                String vehicleFuel = etVehicleFuel.getText().toString().trim();
                                String vehicleOil = etVehicleOil.getText().toString().trim();
                                String vehicleKilometer = etVehicleKilometer.getText().toString().trim();
                                String deliveryDate = tvDeliveryDate.getText().toString().trim();
                                String jobDayNight = getDayNightJob();
//                                jobCardUniqueId = UUID.randomUUID().toString();
                                JobCardDetailsModel cardDetail = new JobCardDetailsModel();
//                                cardDetail.setLocaldbid(jobCardUniqueId);
//                                cardDetail.setUser_id(Integer.parseInt(loginUserId));
//                                cardDetail.setShop_id(Integer.parseInt(loginShopId));
                                cardDetail.setCustomer_code(customerCode);
                                cardDetail.setVat_reg_no(vatRegisterNo);
                                cardDetail.setName(name);
                                cardDetail.setPo_box(postBoxNo);
                                cardDetail.setCity(city);
                                cardDetail.setTel_no(telephoneNo);
                                cardDetail.setMobile_no(mobileNo);
                                cardDetail.setFax_no(faxNo);
                                cardDetail.setEmail(emailId);
                                cardDetail.setAdvisor_date(currentDate);
                                cardDetail.setAdvisor_time(currentTime);
                                cardDetail.setReg_no(vehicleRegisterNo);
                                cardDetail.setModel(vehicleModel);
                                cardDetail.setChassis_no(vehicleChasisNo);
                                cardDetail.setColours(vehicleColors);
                                cardDetail.setFuel(vehicleFuel);
                                cardDetail.setOil(vehicleOil);
                                cardDetail.setKilometer(vehicleKilometer);
//                                cardDetail.setAdvisor_id(Integer.parseInt(loginUserId));
//                                cardDetail.setAdvisor_name(loginUserName);
                                cardDetail.setDelivery_date(deliveryDate);
                                cardDetail.setJob_type(jobDayNight);
                                cardDetail.setTotal_amount(String.valueOf(grandTotal));
                                cardDetail.setDate_added(formattedDate);
                                cardDetail.setCreated_date(date); // Date
                                cardDetail.setCreated_date_str(dateString); // Date
                                cardDetail.setAction(Constant.SETJOBCARD);
                                cardDetail.setStatus(Constant.PENDINGSTATUS);
                                cardDetail.setItems(new Gson().toJson(selectedJobTypeItemsList));
                                cardDetail.setLocal_insert(localInsert);
                                cardDetail.setSync_status("false");
                                cardDetail.setRow_deleted(0);
                                final ReturnRealmModel rowAdded = realmDBHelper.updateJobCard(mRealm, Integer.parseInt(nextId), cardDetail, selectedJobTypeItemsList);
                                if (rowAdded.isStatus()) {
                                    mProgressBarHandler.hide();
                                    goToPreview();
                                } else {
                                    mProgressBarHandler.hide();
                                    Utils.showToast(getApplicationContext(), getResources().getString(R.string.msg_servererror));
                                }
                            } else {
                                mProgressBarHandler.hide();
                                Utils.showToast(getApplicationContext(), getResources().getString(R.string.msg_plsselectjobtype));
                            }
                        } else {
                            mProgressBarHandler.hide();
                            Utils.showToast(getApplicationContext(), getResources().getString(R.string.msg_plsselectjobtype));
                        }
//                    } else {
//                        mProgressBarHandler.hide();
//                        Utils.showToast(getApplicationContext(), getResources().getString(R.string.msg_faxnolenght));
//                    }
//                } else {
//                    mProgressBarHandler.hide();
//                    Utils.showToast(getApplicationContext(), getResources().getString(R.string.msg_mobilenolenght));
//                }
//            } else {
//                mProgressBarHandler.hide();
//                Utils.showToast(getApplicationContext(), getResources().getString(R.string.msg_telephonenolenght));
//            }
        }
    }

    private void getBundle() {
        Intent extras = getIntent();
        if (extras != null) {
            uniqueId = extras.getStringExtra("JobCardUniqueId");
            nextId = extras.getStringExtra("NextId");
            getPreviewDataFromDB(nextId, uniqueId);
        }
    }

    private void getPreviewDataFromDB(String nextId, String uniqueId) {
        jobCard = realmDBHelper.getJobCardDetail(mRealm, Integer.parseInt(nextId));
        jobCardItem = realmDBHelper.getCardItemsDetail(mRealm, Integer.parseInt(nextId));
        if (jobCard != null || jobCard.size() > 0) {
            int i = 0;
            JobCardDetailsRealmDBModel card = jobCard.get(i);
            localInsert = card.getLocal_insert();
            etCustomerCode.setText(card.getCustomer_code());
            etVatRegisterNo.setText(card.getVat_reg_no());
            etName.setText(card.getName());
            etPostBoxNo.setText(card.getPo_box());
            etCity.setText(card.getCity());
            etTelephoneNo.setText(card.getTel_no());
            etMobileNo.setText(card.getMobile_no());
            etFaxNo.setText(card.getFax_no());
            etEmailId.setText(card.getEmail());
            tvCurrentDate.setText(card.getAdvisor_date());
            tvCurrentTime.setText(card.getAdvisor_time());
            etVehicleRegisterNo.setText(card.getReg_no());
            etVehicleModel.setText(card.getModel());
            etVehicleChasisNo.setText(card.getChassis_no());
            etVehicleColors.setText(card.getColours());
            etVehicleFuel.setText(card.getFuel());
            etVehicleOil.setText(card.getOil());
            etVehicleKilometer.setText(card.getKilometer());
            tvDeliveryDate.setText(card.getDelivery_date());
            if (card.getJob_type().contains(getResources().getString(R.string.text_jc_dayjob))) {
                ((RadioButton) rgServiceJobType.getChildAt(0)).setChecked(true);
            } else {
                ((RadioButton) rgServiceJobType.getChildAt(1)).setChecked(true);
            }
//            tvTotalAmount.setText(card.getTotal_amount());
//            rbServiceNightJob.setText(getRGJobType);
        }
        if (jobCardItem != null || jobCardItem.size() > 0) {
//            setupRecyclerView();
        }
        jobTypeList = realmDBHelper.getJobTypeList(mRealm);
        if (jobTypeList != null && jobTypeList.size() > 0) {
            setupRecyclerView();
        } else {
            // right now do nothing but
            // need to session clear if no data;
        }
    }

    private void onHandleUIFromDB() {

    }

    private String getDayNightJob() {
        int selectedId = rgServiceJobType.getCheckedRadioButtonId();
        if (selectedId == rbServiceDayJob.getId()) {
            getRGJobType = rbServiceDayJob.getText().toString().trim();
        } else {
            getRGJobType = rbServiceNightJob.getText().toString().trim();
        }
        return getRGJobType;
    }

   /* private String getDayNightJob(String selectedJobtype) {
//        int selectedId = rgServiceJobType.getCheckedRadioButtonId();
        if (selectedJobtype.contains(getResources().getString(R.string.text_jc_dayjob))) {
            ((RadioButton)rgServiceJobType.getChildAt(0)).setChecked(true);
            getRGJobType = rbServiceDayJob.getText().toString().trim();
        } else {
            ((RadioButton)rgServiceJobType.getChildAt(1)).setChecked(true);
            getRGJobType = rbServiceNightJob.getText().toString().trim();
        }
        return getRGJobType;
    }*/

    private void onHandleUI() {
//        toolbarTitle.setText(getResources().getString(R.string.app_editjobcardpage));
        btnNext.setText(getResources().getString(R.string.msg_next));
        setCurrentDateTime();
    }

    private void setCurrentDateTime() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String formattedDate = df.format(c.getTime());
        tvCurrentDate.setText(formattedDate);
        SimpleDateFormat dt = new SimpleDateFormat("HH:mm");
        String formattedTime = dt.format(c.getTime());
        tvCurrentTime.setText(formattedTime);
    }

    public void currentDatePicker(final TextView txtDate) {
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        getFormattedDate(year, monthOfYear, dayOfMonth, txtDate);
//                        txtDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation_upDown;
        datePickerDialog.show();
    }

    public void currentTimePicker(final TextView txtTime) {
        // Get Current Time
        final Calendar c = Calendar.getInstance();
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        // Launch Time Picker Dialog
        TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        txtTime.setText(hourOfDay + ":" + minute);
                    }
                }, mHour, mMinute, false);
        timePickerDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation_upDown;
        timePickerDialog.show();
    }

    private void setupRecyclerView() {

        mLayoutManager = new LinearLayoutManager(this);
        jobTypeAdapter = new EditJobTypeItemsAdapter(this, jobTypeList, jobCardItem, true);
        jobTyperRecyclerview.setAdapter(jobTypeAdapter);
        jobTyperRecyclerview.setLayoutManager(mLayoutManager);
        jobTyperRecyclerview.setItemAnimator(new DefaultItemAnimator());
        jobTypeAdapter.setOnItemClickListener(EditJobCardActivity.this);
        jobTypeAdapter.setCallback(EditJobCardActivity.this);
        ViewCompat.setNestedScrollingEnabled(jobTyperRecyclerview, false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(View view, int position) {
//        Utils.showToast(getApplicationContext(), "pos--" + jobTypeList.get(position).getName());
    }

    @Override
    public void onItemClickWithId(View view, int position, Integer id) {

    }

    @Override
    public void getJobTypeCheckedItems(List<JobTypeModel> typeSelectedItems) {
        if (typeSelectedItems != null || typeSelectedItems.size() > 0) {
            selectedJobTypeItemsList = typeSelectedItems;
            grandTotal = jobTypeObj.getCheckedPrice(typeSelectedItems);
            for (int i = 0; i < typeSelectedItems.size(); i++) {
                Utils.showLogD("2000---selecteditems--" + typeSelectedItems.get(i).getName() + "--" + typeSelectedItems.get(i).getPrice() + "--" + grandTotal);
            }
            Utils.showLogD("---------------------------------------------------------------------------");
            calculateTotal();
        }
    }

    private void calculateTotal() {
        tvTotalAmount.setText("" + String.format(Locale.ENGLISH, "%.2f", grandTotal));// + " "+getResources().getString(R.string.text_currency)
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK
                && event.getRepeatCount() == 0) {
            onBackPressed();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onBackPressed() {
        closeAlert(EditJobCardActivity.this, getResources().getString(R.string.app_editjobcardpage));
        goToPreview();
    }

    private void goToPreview() {
        Intent i = new Intent(EditJobCardActivity.this, JobCardPreviewActivity.class);
        i.putExtra("JobCardUniqueId", uniqueId);
        i.putExtra("NextId", nextId);
        startActivity(i);
        overridePendingTransitionExit();
    }

}
