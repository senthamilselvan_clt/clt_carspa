package com.clt.newcarspa.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.WindowManager;
import android.widget.ImageView;

import com.clt.newcarspa.R;
import com.clt.newcarspa.Utils.Session;
import com.clt.newcarspa.Utils.Utils;

/**
 * Created by Karthik Thumathy on 23-Jul-18.
 */
public class SplashActivity extends BaseActivity {

    public static final int SPLASH_TIME_OUT = 3000;
    private ImageView imgSplash;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        boolean istablet = Utils.getInstance(SplashActivity.this).isTablet(SplashActivity.this);
        Session.getInstance().setIsTablet(SplashActivity.this, istablet);
        onHandleSplash();
    }

    public void onHandleSplash(){
        imgSplash = findViewById(R.id.img_splash);
        /*Glide.with(this)
                .load(R.drawable.carspa_splash)
                .into(imgSplash);*/
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                onCheckValidUser();
            }
        }, SPLASH_TIME_OUT);
    }

    private void onCheckValidUser() {
//        Utils.showLogD("id--"+Utils.getFromUserDefaults(getApplicationContext(),Constant.LOGINUSERID));
        if(Session.getInstance().isApplicationExit(getApplicationContext())){
            goToHome(MainActivity.class);
        }else {
            goToHome(LoginActivity.class);
        }
    }

    private void goToHome(Class validLogin) {
        Intent intent = new Intent(SplashActivity.this, validLogin);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
        overridePendingTransitionEnter();
//        overridePendingTransition(R.anim.slide_right, R.anim.slide_left);
        finish();
    }
}
