package com.clt.newcarspa.Fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.clt.newcarspa.Adapter.AnotherJobTypePreviewItemsAdapter;
import com.clt.newcarspa.Adapter.JobCardListViewAdapter;
import com.clt.newcarspa.R;
import com.clt.newcarspa.Utils.Constant;
import com.clt.newcarspa.Utils.ProgressBarHandler;
import com.clt.newcarspa.Utils.Utils;
import com.clt.newcarspa.activity.EditJobCardActivity;
import com.clt.newcarspa.activity.JobCardListActivity;
import com.clt.newcarspa.activity.ViewJobCardListActivity;
import com.clt.newcarspa.cltinterfaces.OnItemClickListener;
import com.clt.newcarspa.cltinterfaces.RecyclerviewListener;
import com.clt.newcarspa.cltinterfaces.RestAdapter;
import com.clt.newcarspa.database.RealmDBHelper;
import com.clt.newcarspa.model.APIListResponse;
import com.clt.newcarspa.model.JobCardDetailsRealmDBModel;
import com.clt.newcarspa.model.JobCardItemsDetailsModel;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class JobCardListViewFragment extends Fragment implements OnItemClickListener, JobCardListViewAdapter.ButtonAdapterCallback {

    private Realm mRealm;
    private RecyclerView rv_JobList;
    private EditText etSearch;
    private String strSearch;
    private JobCardListViewAdapter listAdapter;
    private LinearLayoutManager mLayoutManager;
    private List<JobCardDetailsRealmDBModel> selectJobList, selectJobListCopy;

    private AlertDialog alertDialog;
    private TextView tvPrevCustomerCode, tvPrevVatRegisterNo, tvPrevName, tvPrevPostBoxNo, tvPrevCity, tvPrevTelephoneNo,
            tvPrevMobileNo, tvPrevFaxNo, tvPrevEmailId, tvPrevVehicleRegisterNo, tvPrevDeliveryDate, tvPrevTotalAmount, tvPrevTxtJobType,
            tvPrevVehicleModel, tvPrevVehicleChasisNo, tvPrevVehicleColors, tvPrevVehicleFuel, tvPrevVehicleOil, tvPrevVehicleKilometer, tvPrevCurrentDate, tvPrevCurrentTime, tvPrevHeaderTitle, tvPrevEdit, tvPrevToolbarTitle,
            tvTitle, tvPrice;
    private LinearLayout layout_items;
    private RelativeLayout rlHeader;
    private Toolbar prevToolbar;
    private ImageView imgPrevHeaderClose, imgPrevHeaderRight, imgPrevHeaderLeft;
    private RecyclerView jobPrevTypeRecyclerview;
    private AnotherJobTypePreviewItemsAdapter previewItemsAdapter;
    private LinearLayout rgPrevServiceJobType, llInflateLayout;
    private RadioButton rbPrevServiceNightJob;
    private Button btnPrevFinish, btnPrevEdit;

    private View[] child;
    private LayoutInflater inflater1;
    private ProgressBarHandler mProgressBarHandler;
    private String list = null;
    private boolean liveOrLocal = false;
    private RealmDBHelper realmDBHelper;

    private List<JobCardDetailsRealmDBModel> pushCardList = new ArrayList<>();
    private List<JobCardItemsDetailsModel> pushCardItemsList = new ArrayList<>();
    private List<JobCardDetailsRealmDBModel> CardLists = new ArrayList<>();
    private List<JobCardItemsDetailsModel> itemsDetailsList = new ArrayList<>();
    private RecyclerviewListener listener;

    public JobCardListViewFragment() {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof RecyclerviewListener) {
            listener = (RecyclerviewListener) context;
        } else {
            // Throw an error!
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mRealm.close();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.content_job_card_list_view, container, false);
        mRealm = Realm.getDefaultInstance();
        realmDBHelper = new RealmDBHelper(getActivity());
        mProgressBarHandler = new ProgressBarHandler(getActivity());
        initView(view);
        getBundle();
        setListeners();
        return view;
    }

    private void initView(View view) {
        rv_JobList = view.findViewById(R.id.rv_JobList);
        etSearch = view.findViewById(R.id.etSearch);
        String query = ((ViewJobCardListActivity) getActivity()).search;
        filter(query);
    }

    private void setListeners() {
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                strSearch = etSearch.getText().toString();
                if (selectJobListCopy != null) {
                    if (selectJobListCopy.size() != 0) {
                        selectJobList = new ArrayList<>();
                        for (JobCardDetailsRealmDBModel cartForm : selectJobListCopy) {

                            if (String.valueOf(cartForm.getId()).equals(strSearch) ||
                                    cartForm.getMobile_no().contains(strSearch) ||
                                    cartForm.getReg_no().contains(strSearch)) {

                                selectJobList.add(cartForm);
                            }
                        }

                        if (selectJobList != null || selectJobList.size() > 0) {
//                            onSetRecyclerView();
                        }
//                        listAdapter = new JobCardListViewAdapter(getActivity(), selectJobList);
//                        rv_JobList.setAdapter(listAdapter);

                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void onSetRecyclerView() {
//        mLayoutManager = new LinearLayoutManager(getActivity());
//        listAdapter = new JobCardListViewAdapter(getContext(), selectJobList);
//        rv_JobList.setAdapter(listAdapter);
//        rv_JobList.setLayoutManager(mLayoutManager);
//        rv_JobList.setItemAnimator(new DefaultItemAnimator());
//        listAdapter.setOnItemClickListener(this);
//        listAdapter.setCallback(this);
    }

    private void getBundle() {
        Bundle extras = getArguments();
        if (extras != null) {
            list = extras.getString("list");
            liveOrLocal = extras.getBoolean("liveOrLocal");
//            if (liveOrLocal) {
//                Utils.showToast(getActivity(), ""+liveOrLocal);
            if (!list.equals(null) || !list.equalsIgnoreCase(null) || list != null) {
                selectJobList = (List<JobCardDetailsRealmDBModel>) new Gson().fromJson(list, new TypeToken<List<JobCardDetailsRealmDBModel>>() {
                }.getType());
                selectJobListCopy = (List<JobCardDetailsRealmDBModel>) new Gson().fromJson(list, new TypeToken<List<JobCardDetailsRealmDBModel>>() {
                }.getType());

                Log.d("List--Count", "===" + selectJobList.size());
//                onSetRecyclerView();
            }
//            } else {
//                Utils.showToast(getActivity(), "local");
//            }
        }

    }

    public void filter(final String query) {
        Log.d("Frg+" + query, "");
        if (selectJobListCopy != null && query != null) {
            if (selectJobListCopy.size() != 0) {
                selectJobList = new ArrayList<>();
                for (JobCardDetailsRealmDBModel cartForm : selectJobListCopy) {

                    if (String.valueOf(cartForm.getId()).equals(strSearch) ||
                            cartForm.getMobile_no().contains(strSearch) ||
                            cartForm.getReg_no().contains(strSearch)) {
                        selectJobList.add(cartForm);
                    }
                }

                if (selectJobList != null || selectJobList.size() > 0) {
//                    onSetRecyclerView();
                }
//                listAdapter = new JobCardListViewAdapter(getActivity(), selectJobList);
//                rv_JobList.setAdapter(listAdapter);
            }
        }
    }


    @Override
    public void onItemClick(View view, int position) {

    }

    @Override
    public void onItemClickWithId(View view, int position, Integer id) {
        showPreviewDialog(position, id);
    }

    @Override
    public void callUpdateStatusService(JobCardDetailsRealmDBModel model, Integer pos) {
        boolean updatestatus = false;
        if (liveOrLocal) {
            callUpdateStatusServiceImpl(model);
        } else {
//            Utils.showToastDev(getActivity(),"status--"+model.getStatus());
            updatestatus = realmDBHelper.updateDBJobCardStatus(mRealm, model);
            if (updatestatus) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mProgressBarHandler.hide();
                        Utils.showToast(getActivity(), "Updated Successfully");
                        goToActivity(JobCardListActivity.class);
                    }
                }, Constant.LOADER_TIME_OUT);
            }
        }

    }

    private void callUpdateStatusServiceImpl(JobCardDetailsRealmDBModel model) {
        mProgressBarHandler.show();
        Call<APIListResponse<JobCardDetailsRealmDBModel>> call = RestAdapter.getInstance().getServiceApi().setJobCard(model);
        call.enqueue(new Callback<APIListResponse<JobCardDetailsRealmDBModel>>() {
            @Override
            public void onResponse(@NonNull Call<APIListResponse<JobCardDetailsRealmDBModel>> call,
                                   @NonNull Response<APIListResponse<JobCardDetailsRealmDBModel>> response) {
                String status;
                mProgressBarHandler.hide();
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        status = response.body().getStatus();
                        if (status.equals(Constant.RESPONSESUCCESS)) {
                            Utils.showToast(getContext(), "" + status);
                            goToActivity(ViewJobCardListActivity.class);
                        }
                    }
                } else {
                }
            }

            @Override
            public void onFailure(Call<APIListResponse<JobCardDetailsRealmDBModel>> call, Throwable t) {
                mProgressBarHandler.hide();
                if (getContext() != null) {
                    String failure = t.getMessage().toString();
                    Log.d("onFailure Response", "" + failure);
                    Utils.showToast(getContext(), getResources().getString(R.string.msg_servererror));
                }
            }
        });
    }

    private void goToActivity(Class validLogin) {
        Intent intent = new Intent(getContext(), validLogin);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
        getActivity().finish();
    }

    public void showPreviewDialog(final int position, final Integer jobCardId) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.activity_job_card_preview, null);
        dialogBuilder.setView(dialogView);
        alertDialog = dialogBuilder.create();
        alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation_upDown;
        alertDialog.setCancelable(false);
        initPreviewView(dialogView);
        JobCardDetailsRealmDBModel selectedCardDetail = selectJobList.get(position);
        List<JobCardDetailsRealmDBModel> isJobCardFromDB = realmDBHelper.isExistingJobCard(mRealm, jobCardId);
//        Utils.showToast(requireActivity(), "list--" + isJobCardFromDB.size());
        onHandlePreviewUI(selectJobList.get(position), jobCardId, isJobCardFromDB.size());
        tvPrevHeaderTitle.setText(getResources().getString(R.string.text_preview));
        imgPrevHeaderClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (alertDialog != null && alertDialog.isShowing()) alertDialog.dismiss();
            }
        });
        tvPrevEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToEditPage(selectJobList.get(position).getLocaldbid(), String.valueOf(jobCardId));
            }
        });
       /* imgPrevHeaderLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pos;
                if (position > 0 || position <= selectJobList.size()) {
                    pos = position;
                    pos--;
                } else {
                }
            }
        });
        imgPrevHeaderRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pos;
                if (position > 0 || position <= selectJobList.size()) {
                    pos = position;
                    pos++;
                } else {
                }
            }
        });*/
        alertDialog.show();
    }

    private void goToEditPage(String uniqueId, String nextId) {
        Intent i = new Intent(requireActivity(), EditJobCardActivity.class);
        i.putExtra("JobCardUniqueId", uniqueId);
        i.putExtra("NextId", nextId);// jobCardId
        startActivity(i);
    }

    private void onHandlePreviewUI(JobCardDetailsRealmDBModel selectedCardDetail, Integer jobCardId, Integer isJobCardFromDB) {
        tvPrevCustomerCode.setText("" + selectedCardDetail.getCustomer_code());
        tvPrevVatRegisterNo.setText("" + selectedCardDetail.getVat_reg_no());
        tvPrevName.setText("" + selectedCardDetail.getName());
        tvPrevPostBoxNo.setText("" + selectedCardDetail.getPo_box());
        tvPrevCity.setText("" + selectedCardDetail.getCity());
        tvPrevTelephoneNo.setText("" + selectedCardDetail.getTel_no());
        tvPrevMobileNo.setText("" + selectedCardDetail.getMobile_no());
        tvPrevFaxNo.setText("" + selectedCardDetail.getFax_no());
        tvPrevEmailId.setText("" + selectedCardDetail.getEmail());
        tvPrevCurrentDate.setText("" + selectedCardDetail.getAdvisor_date());
        tvPrevCurrentTime.setText("" + selectedCardDetail.getAdvisor_time());
        tvPrevVehicleRegisterNo.setText("" + selectedCardDetail.getReg_no());
        tvPrevVehicleModel.setText("" + selectedCardDetail.getModel());
        tvPrevVehicleChasisNo.setText("" + selectedCardDetail.getChassis_no());
        tvPrevVehicleColors.setText("" + selectedCardDetail.getColours());
        tvPrevVehicleFuel.setText("" + selectedCardDetail.getFuel());
        tvPrevVehicleOil.setText("" + selectedCardDetail.getOil());
        tvPrevVehicleKilometer.setText("" + selectedCardDetail.getKilometer());
        tvPrevDeliveryDate.setText("" + selectedCardDetail.getDelivery_date());
        String json = selectedCardDetail.getItems();

        List<JobCardItemsDetailsModel> modelList = new Gson().fromJson(json, new TypeToken<List<JobCardItemsDetailsModel>>() {
        }.getType());
//        if(isJobCardFromDB == 0){
//            ReturnRealmModel realmModel = realmDBHelper.addExistingJobCardFromServer(mRealm, selectedCardDetail, modelList);
//            if (realmModel.getAddedExistingJobCardFromServerStatus() == 1) {
//                Utils.showToast(requireActivity(), "added");
//            }
//        }
        if (modelList != null || modelList.size() > 0) {
            addItems(modelList, jobCardId);
            for (int i = 0; i < modelList.size(); i++) {
                Utils.showLogD("1010--" + modelList.size() + "--" + modelList.get(i).getJob_card_id() + "--" + modelList.get(i).getName());
            }
        }
        tvPrevTotalAmount.setText("" + selectedCardDetail.getTotal_amount() + " " + getResources().getString(R.string.text_currency));
        rbPrevServiceNightJob.setText("" + selectedCardDetail.getJob_type());
    }

    private void addItems(List<JobCardItemsDetailsModel> modelList, Integer jobCardId) {
        layout_items.removeAllViews();
        child = new View[modelList.size()];
        inflater1 = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        for (int i = 0; i < modelList.size(); i++) {
            if (liveOrLocal) {
                JobCardItemsDetailsModel model = modelList.get(i);
                child[i] = inflater1.inflate(R.layout.add_item_row, null);
                tvTitle = child[i].findViewById(R.id.tvTitle);
                tvPrice = child[i].findViewById(R.id.tvPrice);
                tvTitle.setText(String.valueOf(model.getName()));
                tvPrice.setText(String.valueOf(model.getPrice()));
                layout_items.addView(child[i]);
            }else {
                if (jobCardId == modelList.get(i).getJob_card_id()) {
                    JobCardItemsDetailsModel model = modelList.get(i);
                    child[i] = inflater1.inflate(R.layout.add_item_row, null);
                    tvTitle = child[i].findViewById(R.id.tvTitle);
                    tvPrice = child[i].findViewById(R.id.tvPrice);
                    tvTitle.setText(String.valueOf(model.getName()));
                    tvPrice.setText(String.valueOf(model.getPrice()));
                    layout_items.addView(child[i]);
                }
            }
        }
    }

    private void initPreviewView(View dialogView) {
        prevToolbar = dialogView.findViewById(R.id.toolbar);
        prevToolbar.setVisibility(View.GONE);
        tvPrevToolbarTitle = dialogView.findViewById(R.id.tv_toolbarTitle);
        tvPrevToolbarTitle.setVisibility(View.GONE);
        rlHeader = dialogView.findViewById(R.id.rl_header);
        rlHeader.setVisibility(View.VISIBLE);
        tvPrevHeaderTitle = dialogView.findViewById(R.id.tv_header_title);
        tvPrevEdit = dialogView.findViewById(R.id.tv_edit);
        tvPrevEdit.setVisibility(View.VISIBLE);
//        tvPrevEdit.setText("Edit");
        imgPrevHeaderClose = dialogView.findViewById(R.id.img_header_close);
        imgPrevHeaderRight = dialogView.findViewById(R.id.img_right);
        imgPrevHeaderRight.setVisibility(View.GONE);
        imgPrevHeaderLeft = dialogView.findViewById(R.id.img_left);
        imgPrevHeaderLeft.setVisibility(View.GONE);
        tvPrevCustomerCode = dialogView.findViewById(R.id.tv_customercode);
        tvPrevVatRegisterNo = dialogView.findViewById(R.id.tv_vatregno);
        tvPrevName = dialogView.findViewById(R.id.tv_name);
        tvPrevPostBoxNo = dialogView.findViewById(R.id.tv_postboxno);
        tvPrevCity = dialogView.findViewById(R.id.tv_city);
        tvPrevTelephoneNo = dialogView.findViewById(R.id.tv_telephoneno);
        tvPrevMobileNo = dialogView.findViewById(R.id.tv_mobileno);
        llInflateLayout = dialogView.findViewById(R.id.ll_inflatelayout);
        llInflateLayout.setVisibility(View.VISIBLE);
        tvPrevFaxNo = dialogView.findViewById(R.id.tv_faxno);
        tvPrevEmailId = dialogView.findViewById(R.id.tv_email);
        tvPrevCurrentDate = dialogView.findViewById(R.id.tv_currentdate);
        tvPrevCurrentTime = dialogView.findViewById(R.id.tv_currenttime);
        tvPrevVehicleRegisterNo = dialogView.findViewById(R.id.tv_regno);
        tvPrevVehicleModel = dialogView.findViewById(R.id.tv_model);
        tvPrevVehicleChasisNo = dialogView.findViewById(R.id.tv_chasisno);
        tvPrevVehicleColors = dialogView.findViewById(R.id.tv_color);
        tvPrevVehicleFuel = dialogView.findViewById(R.id.tv_fuel);
        tvPrevVehicleOil = dialogView.findViewById(R.id.tv_oil);
        tvPrevVehicleKilometer = dialogView.findViewById(R.id.tv_kilometer);
        tvPrevDeliveryDate = dialogView.findViewById(R.id.tv_deliverydate);
        jobPrevTypeRecyclerview = dialogView.findViewById(R.id.rv_jobtypeitems);
        tvPrevTxtJobType = dialogView.findViewById(R.id.tv_txtjobtype);
        tvPrevTxtJobType.setVisibility(View.GONE);
        jobPrevTypeRecyclerview.setVisibility(View.GONE);
        layout_items = dialogView.findViewById(R.id.layout_items);
        layout_items.setVisibility(View.VISIBLE);
        tvPrevTotalAmount = dialogView.findViewById(R.id.tv_totalamt);
        rgPrevServiceJobType = dialogView.findViewById(R.id.rg_daynightgroup);
        rbPrevServiceNightJob = dialogView.findViewById(R.id.rb_nightjob);
        btnPrevEdit = dialogView.findViewById(R.id.btn_editjobcard);
        btnPrevEdit.setVisibility(View.GONE);
        btnPrevFinish = dialogView.findViewById(R.id.btn_sendjobcard);
        btnPrevFinish.setVisibility(View.GONE);
    }
}
