package com.clt.newcarspa.Fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.clt.newcarspa.Adapter.AnotherJobTypePreviewItemsAdapter;
import com.clt.newcarspa.Adapter.JobCardCompletedListViewAdapter;
import com.clt.newcarspa.BuildConfig;
import com.clt.newcarspa.R;
import com.clt.newcarspa.Utils.Constant;
import com.clt.newcarspa.Utils.ProgressBarHandler;
import com.clt.newcarspa.Utils.Utils;
import com.clt.newcarspa.activity.EditJobCardActivity;
import com.clt.newcarspa.activity.JobCardListActivity;
import com.clt.newcarspa.activity.JobCardUpdatePreviewActivity;
import com.clt.newcarspa.cltinterfaces.OnItemClickListener;
import com.clt.newcarspa.cltinterfaces.RecyclerviewListener;
import com.clt.newcarspa.cltinterfaces.RestAdapter;
import com.clt.newcarspa.database.RealmDBHelper;
import com.clt.newcarspa.model.APIListResponse;
import com.clt.newcarspa.model.APIResponse;
import com.clt.newcarspa.model.ImageUploadModel;
import com.clt.newcarspa.model.JobCardDetailsRealmDBModel;
import com.clt.newcarspa.model.JobCardItemsDetailsModel;
import com.clt.newcarspa.model.NewJobCardViewListModel;
import com.clt.newcarspa.model.ReturnRealmModel;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class JobCardCompletedFragment extends Fragment implements OnItemClickListener, JobCardCompletedListViewAdapter.ButtonAdapterCallback {

    private Realm mRealm;
    private RecyclerView rv_JobList;
    private EditText etSearch;
    private String strSearch = "";
    private JobCardCompletedListViewAdapter listAdapter;
    private LinearLayoutManager mLayoutManager;
    private List<JobCardDetailsRealmDBModel> selectJobList = new ArrayList<JobCardDetailsRealmDBModel>();
    private List<JobCardDetailsRealmDBModel> selectJobListCopy;

    private AlertDialog alertDialog;
    private TextView tvPrevCustomerCode, tvPrevVatRegisterNo, tvPrevName, tvPrevPostBoxNo, tvPrevCity, tvPrevTelephoneNo,
            tvPrevMobileNo, tvPrevFaxNo, tvPrevEmailId, tvPrevVehicleRegisterNo, tvPrevDeliveryDate, tvPrevTotalAmount, tvPrevTxtJobType,
            tvPrevVehicleModel, tvPrevVehicleChasisNo, tvPrevVehicleColors, tvPrevVehicleFuel, tvPrevVehicleOil, tvPrevVehicleKilometer, tvPrevCurrentDate, tvPrevCurrentTime, tvPrevHeaderTitle, tvPrevEdit, tvPrevToolbarTitle,
            tvTitle, tvPrice;
    private LinearLayout layout_items;
    private RelativeLayout rlHeader;
    private Toolbar prevToolbar;
    private ImageView imgPrevHeaderClose, imgPrevHeaderRight, imgPrevHeaderLeft;
    private RecyclerView jobPrevTypeRecyclerview;
    private AnotherJobTypePreviewItemsAdapter previewItemsAdapter;
    private LinearLayout rgPrevServiceJobType, llInflateLayout;
    private RadioButton rbPrevServiceNightJob;
    private Button btnPrevFinish, btnPrevEdit;

    private View[] child;
    private LayoutInflater inflater1;
    private ProgressBarHandler mProgressBarHandler;
    private String list = null;
    private boolean liveOrLocal = false;
    private String loginUserId, loginShopId;
    private Integer mPosition;
    private String mStatus;
    private RealmDBHelper realmDBHelper;

    private List<JobCardDetailsRealmDBModel> pushCardList = new ArrayList<>();
    private RealmList<JobCardItemsDetailsModel> pushCardItemsList = new RealmList<>();
    private List<JobCardDetailsRealmDBModel> CardLists = new ArrayList<>();
    private List<JobCardItemsDetailsModel> itemsDetailsList = new ArrayList<>();
    private RealmList<ImageUploadModel> itemsImagesList = new RealmList<>();
    private RecyclerviewListener listener;

    private boolean mTyping = false;
    private Handler mTypingHandler = new Handler();
    private static final int TYPING_TIMER_LENGTH = 900;
    private boolean isQuantityAdjusted;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    private int history_page = 1;
    private boolean isSearchedStatus = false;
    int pastVisiblesItems, visibleItemCount, totalItemCount;

    public JobCardCompletedFragment() {

    }

    public static JobCardCompletedFragment newInstance(Integer position, boolean liveOrLocal, String loginUserId, String loginShopId) {
        JobCardCompletedFragment fragment = new JobCardCompletedFragment();
        Bundle bundle = new Bundle();
        bundle.putString("position", String.valueOf(position));
        bundle.putString("loginUserId", loginUserId);
        bundle.putString("loginShopId", loginShopId);
        bundle.putBoolean("liveOrLocal", liveOrLocal);
//        bundle.putParcelableArrayList(Constants.DRIVERLIST, (ArrayList<? extends Parcelable>) driverList);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof RecyclerviewListener) {
            listener = (RecyclerviewListener) context;
        } else {
            // Throw an error!
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mRealm.close();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.content_job_card_list_view, container, false);
        mRealm = Realm.getDefaultInstance();
        realmDBHelper = new RealmDBHelper(getActivity());
        mProgressBarHandler = new ProgressBarHandler(getActivity());
        mStatus = "Completed";//'Pending','Progress','Completed','Delivered'
        initView(view);
        onSetRecyclerView();
        getBundle();
        setListeners();
        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (isVisibleToUser && isResumed()) {
            if (selectJobList != null && selectJobList.size() == 0) {
                if (liveOrLocal) {
                    getDataFromDBForPending(mStatus, 0, 10);
                    callListJobCardService(history_page, strSearch);
                } else {
                    getDataFromDB(mStatus, 0, 10);
                }
            }
//            Utils.showToast(getActivity(), "" + liveOrLocal);
        }
    }

    private void initView(View view) {
        rv_JobList = view.findViewById(R.id.rv_JobList);
        etSearch = view.findViewById(R.id.etSearch);
//        String query = ((JobCardListActivity) getActivity()).search;
//        filter(query);
    }


    private void getDataFromDBForPending(String mStatus, Integer index, Integer length) {
        ReturnRealmModel realmmodel = realmDBHelper.getJobCardPending(mRealm, mStatus, index, length);
        if (realmmodel != null) {
            if (realmmodel.getJobCard() != null || realmmodel.getJobCard().size() > 0) {
                JobCardDetailsRealmDBModel mJobCardList;
                JobCardItemsDetailsModel mJobCardItemsList;
//                ImageUploadModel mImgItemList;
                selectJobListCopy = new ArrayList<>();
                CardLists = getRealmToCardList(realmmodel);
                itemsDetailsList = getRealmListToList(realmmodel);
                itemsImagesList = getImagesList(realmmodel);

                for (int i = 0; i < CardLists.size(); i++) {
                    mJobCardList = CardLists.get(i);
                    Utils.showLogD("1009--" + CardLists.size() + "--" + mJobCardList.getId() + "--" + mJobCardList.getName() + "--" + mJobCardList.getCustomer_code());
                    if (realmmodel.getJobCardItems().size() > 0) {
                        for (int j = 0; j < itemsDetailsList.size(); j++) {
                            mJobCardItemsList = itemsDetailsList.get(j);
                            Utils.showLogD("1009--" + itemsDetailsList.size() + "--" + mJobCardItemsList.getJob_card_id() + "--" + mJobCardItemsList.getName() + "--" + mJobCardItemsList.getId());
                            if (mJobCardList.getId().equals(mJobCardItemsList.getJob_card_id())) {
                                mJobCardItemsList.setId(mJobCardItemsList.getService_id());
                                mJobCardItemsList.setName(mJobCardItemsList.getName());
                                mJobCardItemsList.setOther_name(mJobCardItemsList.getOther_name());
                                mJobCardItemsList.setPrice(mJobCardItemsList.getPrice());
                                mJobCardItemsList.setJob_card_id(mJobCardItemsList.getJob_card_id());
                                pushCardItemsList.add(mJobCardItemsList);

                            }
                        }
                        if (pushCardItemsList.size() > 0) {
                            String items = new Gson().toJson(pushCardItemsList);
                            mJobCardList.setItems(items);
                            mJobCardList.setItems_new(pushCardItemsList);
                        }
                        pushCardList.add(mJobCardList);// for testing purpose
                        selectJobListCopy.add(mJobCardList);
                    }
                }
                for (int i = 0; i < selectJobListCopy.size(); i++) {
                    selectJobList.add(selectJobListCopy.get(i));
                }
                listAdapter.notifyDataSetChanged();
                if (pushCardList != null || pushCardList.size() > 0) {
                    for (int i = 0; i < pushCardList.size(); i++) {
                        JobCardDetailsRealmDBModel psList = pushCardList.get(i);
                        Utils.showLogD("1008--" + psList.getId() + "--" + psList.getName() + "--" + psList.getCustomer_code());
                    }
                }
//                onSetRecyclerView();
            }
        }
    }

    private void getDataFromDB(String mStatus, Integer index, Integer length) {
        ReturnRealmModel realmmodel = realmDBHelper.getJobCard(mRealm, mStatus, index, length);
        if (realmmodel != null) {
            if (realmmodel.getJobCard() != null || realmmodel.getJobCard().size() > 0) {
                JobCardDetailsRealmDBModel mJobCardList;
                JobCardItemsDetailsModel mJobCardItemsList;
//                ImageUploadModel mImgItemList;
                selectJobListCopy = new ArrayList<>();
                CardLists = getRealmToCardList(realmmodel);
                itemsDetailsList = getRealmListToList(realmmodel);
                itemsImagesList = getImagesList(realmmodel);

                for (int i = 0; i < CardLists.size(); i++) {
                    mJobCardList = CardLists.get(i);
                    Utils.showLogD("1009--" + CardLists.size() + "--" + mJobCardList.getId() + "--" + mJobCardList.getName() + "--" + mJobCardList.getCustomer_code());
                    if (realmmodel.getJobCardItems().size() > 0) {
                        for (int j = 0; j < itemsDetailsList.size(); j++) {
                            mJobCardItemsList = itemsDetailsList.get(j);
                            Utils.showLogD("1009--" + itemsDetailsList.size() + "--" + mJobCardItemsList.getJob_card_id() + "--" + mJobCardItemsList.getName() + "--" + mJobCardItemsList.getId());
                            if (mJobCardList.getId().equals(mJobCardItemsList.getJob_card_id())) {
                                mJobCardItemsList.setId(mJobCardItemsList.getService_id());
                                mJobCardItemsList.setName(mJobCardItemsList.getName());
                                mJobCardItemsList.setOther_name(mJobCardItemsList.getOther_name());
                                mJobCardItemsList.setPrice(mJobCardItemsList.getPrice());
                                mJobCardItemsList.setJob_card_id(mJobCardItemsList.getJob_card_id());
                                pushCardItemsList.add(mJobCardItemsList);

                            }
                        }
                        if (pushCardItemsList.size() > 0) {
                            String items = new Gson().toJson(pushCardItemsList);
                            mJobCardList.setItems(items);
                            mJobCardList.setItems_new(pushCardItemsList);
                        }
                        pushCardList.add(mJobCardList);// for testing purpose
                        selectJobListCopy.add(mJobCardList);
                    }
                }
                for (int i = 0; i < selectJobListCopy.size(); i++) {
                    selectJobList.add(selectJobListCopy.get(i));
                }
                listAdapter.notifyDataSetChanged();
                if (pushCardList != null || pushCardList.size() > 0) {
                    for (int i = 0; i < pushCardList.size(); i++) {
                        JobCardDetailsRealmDBModel psList = pushCardList.get(i);
                        Utils.showLogD("1008--" + psList.getId() + "--" + psList.getName() + "--" + psList.getCustomer_code());
                    }
                }
//                onSetRecyclerView();
            }
        }
    }

    private void callListJobCardService(final int mhistoryPage, final String strSearch) {
        mProgressBarHandler.show();
        NewJobCardViewListModel model1 = new NewJobCardViewListModel();
        model1.setAction("get_all_job_card_list");
        model1.setUser_id(Integer.valueOf(loginUserId));
        model1.setShop_id(Integer.valueOf(loginShopId));
        model1.setStatus(mStatus);
        model1.setSearch(strSearch);
        if (strSearch.length() >= 3) {
            isSearchedStatus = true;
        }
        Call<APIResponse<NewJobCardViewListModel>> call = RestAdapter.getInstance().getServiceApi().getNewJobCardList(model1);
        call.enqueue(new Callback<APIResponse<NewJobCardViewListModel>>() {
            @Override
            public void onResponse(@NonNull Call<APIResponse<NewJobCardViewListModel>> call,
                                   @NonNull Response<APIResponse<NewJobCardViewListModel>> response) {
                mProgressBarHandler.hide();
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        selectJobList = response.body().getResult().getData();
                        selectJobListCopy = response.body().getResult().getData();
                        onHandleJobCardListSuccessResponse(selectJobList, mhistoryPage);
                    }
                } else {
                }
            }

            @Override
            public void onFailure(Call<APIResponse<NewJobCardViewListModel>> call, Throwable t) {
                mProgressBarHandler.hide();
                if (getActivity() != null) {
                    String failure = t.getMessage().toString();
                    Log.d("onFailure Response", "" + failure);
                    if (BuildConfig.FLAVOR.equals("dev")) {
                        Utils.showToast(requireActivity(), getResources().getString(R.string.msg_servererror) + "--" + failure);
                    } else {
                        Utils.showToast(requireActivity(), getResources().getString(R.string.msg_servererror));
                    }

                }
            }
        });
    }

    private void onHandleJobCardListSuccessResponse(List<JobCardDetailsRealmDBModel> selectJobList, int mhistoryPage) {

        if (selectJobList.size() > 0) {
            rv_JobList.setVisibility(View.VISIBLE);
            if (isSearchedStatus) {
                listAdapter.clear();
                isSearchedStatus = false;
            }
            if (history_page > 1) {
                listAdapter.removeLoadingFooter();
            }
            listAdapter.addAll(selectJobList);
            listAdapter.notifyDataSetChanged();
            if (selectJobList.size() >= 50) {
                history_page = mhistoryPage + 1;
                listAdapter.addLoadingFooter();
                for (int i = 0; i < selectJobList.size(); i++) {
//                    Utils.showLogD("10009---name:--"+selectJobList.get(i).getContact_name()+"--orderid--"+historyResult.get(i).getOrder_id());
                }
//                                    Utils.showToast(requireActivity(), "" + cakesResult.size() + "--" + cakesList.size());
            } else {
                isLastPage = true;
            }
        } else {
            isLoading = false;
            rv_JobList.setVisibility(View.GONE);
//            tvNoRecordFound.setVisibility(View.VISIBLE);
        }

    }

    private void setListeners() {
        rv_JobList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();
                    if (!recyclerView.canScrollVertically(RecyclerView.FOCUS_DOWN)) {
                        if (!isLastPage) {
                            if (liveOrLocal) {
                                if ((visibleItemCount + pastVisiblesItems) >= totalItemCount && pastVisiblesItems >= 0) {
                                    isLoading = false;
                                    Utils.showToast(getActivity(), "total" + totalItemCount);
                                    Log.v("...", "Last Item Wow !");
                                    if (liveOrLocal) {
                                        callListJobCardService(history_page, strSearch);
                                    } else {
                                        Integer index = selectJobList.size();
                                        getDataFromDB(mStatus, index, 10);
//                                    onSetRecyclerView();
                                    }
                                    //Do pagination.. i.e. fetch new data
                                }
                            } else {
                                Integer index = selectJobList.size();
                                getDataFromDB(mStatus, index, 10);
                            }
                        }

                    }
                }
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        });
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                strSearch = etSearch.getText().toString();
//                Utils.showToast(requireActivity(), "before--"+before+"--count--"+count);
                if (selectJobListCopy != null) {
                    if (selectJobListCopy.size() != 0) {
                        selectJobList = new ArrayList<>();
                        for (JobCardDetailsRealmDBModel cartForm : selectJobListCopy) {

                            if (String.valueOf(cartForm.getId()).equals(strSearch) ||
                                    cartForm.getMobile_no().contains(strSearch) ||
                                    cartForm.getReg_no().contains(strSearch)) {

                                selectJobList.add(cartForm);
                            }
                        }

                        if (selectJobList != null || selectJobList.size() > 0) {
                            onSetRecyclerView();
                        }
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void onSetRecyclerView() {
        mLayoutManager = new LinearLayoutManager(getActivity());
        listAdapter = new JobCardCompletedListViewAdapter(getContext(), selectJobList);
        rv_JobList.setAdapter(listAdapter);
        rv_JobList.setLayoutManager(mLayoutManager);
        rv_JobList.setItemAnimator(new DefaultItemAnimator());
        listAdapter.setOnItemClickListener(this);
        listAdapter.setCallback(this);
    }

    private void getBundle() {
        Bundle extras = getArguments();
        if (extras != null) {
            list = extras.getString("list");
            liveOrLocal = extras.getBoolean("liveOrLocal");
            loginUserId = extras.getString("loginUserId");
            loginShopId = extras.getString("loginShopId");
            mPosition = Integer.parseInt(extras.getString("position"));
//            Utils.showToast(requireActivity(), "position--" + mPosition + "--liveOrLocal--" + liveOrLocal);
//            if (liveOrLocal) {
//                Utils.showToast(getActivity(), ""+liveOrLocal);
            /*if (!list.equals(null) || !list.equalsIgnoreCase(null) || list != null) {
                selectJobList = (List<JobCardDetailsRealmDBModel>) new Gson().fromJson(list, new TypeToken<List<JobCardDetailsRealmDBModel>>() {
                }.getType());
                selectJobListCopy = (List<JobCardDetailsRealmDBModel>) new Gson().fromJson(list, new TypeToken<List<JobCardDetailsRealmDBModel>>() {
                }.getType());

                Log.d("List--Count", "===" + selectJobList.size());
                onSetRecyclerView();
            }*/
//            } else {
//                Utils.showToast(getActivity(), "local");
//            }

            if (selectJobList != null && selectJobList.size() == 0) {
                if (liveOrLocal) {
                    getDataFromDBForPending(mStatus, 0, 10);
                    callListJobCardService(history_page, strSearch);
                } else {
                    getDataFromDB(mStatus, 0, 10);
                }
            }
        }

    }


    @Override
    public void onItemClick(View view, int position) {

    }

    @Override
    public void onItemClickWithId(View view, int position, Integer id) {
        Log.d("list: ", "" + selectJobList);
        goToUpdatePreviewPage(selectJobList.get(position).getLocaldbid(), String.valueOf(id));
    }

    @Override
    public void callUpdateStatusService(JobCardDetailsRealmDBModel model, Integer pos) {
        boolean updatestatus = false;
        if (liveOrLocal) {
            List<JobCardDetailsRealmDBModel> list = realmDBHelper.isJobCardInLocal(mRealm, model.getId());
            if (list != null && list.size() > 0) {
                updatestatus = realmDBHelper.updateDBJobCardStatus(mRealm, model);
                if (updatestatus) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mProgressBarHandler.hide();
                            Utils.showToast(getActivity(), "Updated Successfully");
                            goToActivity(JobCardListActivity.class);
                        }
                    }, Constant.LOADER_TIME_OUT);
                }
            } else {
                callUpdateStatusServiceImpl(model);
            }
        } else {
//            Utils.showToastDev(getActivity(),"status--"+model.getStatus());
            updatestatus = realmDBHelper.updateDBJobCardStatus(mRealm, model);
            if (updatestatus) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mProgressBarHandler.hide();
                        Utils.showToast(getActivity(), "Updated Successfully");
                        goToActivity(JobCardListActivity.class);
                    }
                }, Constant.LOADER_TIME_OUT);
            }
        }

    }

    private void callUpdateStatusServiceImpl(JobCardDetailsRealmDBModel model) {
        mProgressBarHandler.show();
        Call<APIListResponse<JobCardDetailsRealmDBModel>> call = RestAdapter.getInstance().getServiceApi().setJobCard(model);
        call.enqueue(new Callback<APIListResponse<JobCardDetailsRealmDBModel>>() {
            @Override
            public void onResponse(@NonNull Call<APIListResponse<JobCardDetailsRealmDBModel>> call,
                                   @NonNull Response<APIListResponse<JobCardDetailsRealmDBModel>> response) {
                String status;
                mProgressBarHandler.hide();
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        status = response.body().getStatus();
                        if (status.equals(Constant.RESPONSESUCCESS)) {
                            Utils.showToast(getContext(), "" + status);
                            goToActivity(JobCardListActivity.class);
                        }
                    }
                } else {
                }
            }

            @Override
            public void onFailure(Call<APIListResponse<JobCardDetailsRealmDBModel>> call, Throwable t) {
                mProgressBarHandler.hide();
                if (getContext() != null) {
                    String failure = t.getMessage().toString();
                    Log.d("onFailure Response", "" + failure);
                    Utils.showToast(getContext(), getResources().getString(R.string.msg_servererror));
                }
            }
        });
    }

    private void goToActivity(Class validLogin) {
        Intent intent = new Intent(getContext(), validLogin);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
        getActivity().finish();
    }

    public void showPreviewDialog(final int position, final Integer jobCardId) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.activity_job_card_preview, null);
        dialogBuilder.setView(dialogView);
        alertDialog = dialogBuilder.create();
        alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation_upDown;
        alertDialog.setCancelable(false);
        initPreviewView(dialogView);
        JobCardDetailsRealmDBModel selectedCardDetail = selectJobList.get(position);
        List<JobCardDetailsRealmDBModel> isJobCardFromDB = realmDBHelper.isExistingJobCard(mRealm, jobCardId);
//        Utils.showToast(requireActivity(), "list--" + isJobCardFromDB.size());
        onHandlePreviewUI(selectJobList.get(position), jobCardId, isJobCardFromDB.size());
        tvPrevHeaderTitle.setText(getResources().getString(R.string.text_preview));
        imgPrevHeaderClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (alertDialog != null && alertDialog.isShowing()) alertDialog.dismiss();
            }
        });
        tvPrevEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToEditPage(selectJobList.get(position).getLocaldbid(), String.valueOf(jobCardId));
            }
        });

        alertDialog.show();
    }

    private void goToUpdatePreviewPage(String uniqueId, String nextId) {
        Intent i = new Intent(requireActivity(), JobCardUpdatePreviewActivity.class);
        i.putExtra("JobCardUniqueId", uniqueId);
        i.putExtra("NextId", nextId);// jobCardId
        startActivity(i);
    }

    private void goToEditPage(String uniqueId, String nextId) {
        Intent i = new Intent(requireActivity(), EditJobCardActivity.class);
        i.putExtra("JobCardUniqueId", uniqueId);
        i.putExtra("NextId", nextId);// jobCardId
        startActivity(i);
    }

    private void onHandlePreviewUI(JobCardDetailsRealmDBModel selectedCardDetail, Integer jobCardId, Integer isJobCardFromDB) {
        tvPrevCustomerCode.setText("" + selectedCardDetail.getCustomer_code());
        tvPrevVatRegisterNo.setText("" + selectedCardDetail.getVat_reg_no());
        tvPrevName.setText("" + selectedCardDetail.getName());
        tvPrevPostBoxNo.setText("" + selectedCardDetail.getPo_box());
        tvPrevCity.setText("" + selectedCardDetail.getCity());
        tvPrevTelephoneNo.setText("" + selectedCardDetail.getTel_no());
        tvPrevMobileNo.setText("" + selectedCardDetail.getMobile_no());
        tvPrevFaxNo.setText("" + selectedCardDetail.getFax_no());
        tvPrevEmailId.setText("" + selectedCardDetail.getEmail());
        tvPrevCurrentDate.setText("" + selectedCardDetail.getAdvisor_date());
        tvPrevCurrentTime.setText("" + selectedCardDetail.getAdvisor_time());
        tvPrevVehicleRegisterNo.setText("" + selectedCardDetail.getReg_no());
        tvPrevVehicleModel.setText("" + selectedCardDetail.getModel());
        tvPrevVehicleChasisNo.setText("" + selectedCardDetail.getChassis_no());
        tvPrevVehicleColors.setText("" + selectedCardDetail.getColours());
        tvPrevVehicleFuel.setText("" + selectedCardDetail.getFuel());
        tvPrevVehicleOil.setText("" + selectedCardDetail.getOil());
        tvPrevVehicleKilometer.setText("" + selectedCardDetail.getKilometer());
        tvPrevDeliveryDate.setText("" + selectedCardDetail.getDelivery_date());
        String json = selectedCardDetail.getItems();

//        List<JobCardItemsDetailsModel> modelList = new Gson().fromJson(json, new TypeToken<List<JobCardItemsDetailsModel>>() {
//        }.getType());

//        if (modelList != null || modelList.size() > 0) {
//            addItems(modelList, jobCardId);
//            for (int i = 0; i < modelList.size(); i++) {
//                Utils.showLogD("1010--" + modelList.size() + "--" + modelList.get(i).getJob_card_id() + "--" + modelList.get(i).getName());
//            }
//        }
        tvPrevTotalAmount.setText("" + selectedCardDetail.getTotal_amount() + " " + getResources().getString(R.string.text_currency));
        rbPrevServiceNightJob.setText("" + selectedCardDetail.getJob_type());
    }

    private void addItems(List<JobCardItemsDetailsModel> modelList, Integer jobCardId) {
        layout_items.removeAllViews();
        child = new View[modelList.size()];
        inflater1 = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        for (int i = 0; i < modelList.size(); i++) {
            if (liveOrLocal) {
                JobCardItemsDetailsModel model = modelList.get(i);
                child[i] = inflater1.inflate(R.layout.add_item_row, null);
                tvTitle = child[i].findViewById(R.id.tvTitle);
                tvPrice = child[i].findViewById(R.id.tvPrice);
                tvTitle.setText(String.valueOf(model.getName()));
                tvPrice.setText(String.valueOf(model.getPrice()));
                layout_items.addView(child[i]);
            } else {
                if (jobCardId == modelList.get(i).getJob_card_id()) {
                    JobCardItemsDetailsModel model = modelList.get(i);
                    child[i] = inflater1.inflate(R.layout.add_item_row, null);
                    tvTitle = child[i].findViewById(R.id.tvTitle);
                    tvPrice = child[i].findViewById(R.id.tvPrice);
                    tvTitle.setText(String.valueOf(model.getName()));
                    tvPrice.setText(String.valueOf(model.getPrice()));
                    layout_items.addView(child[i]);
                }
            }
        }
    }

    private void initPreviewView(View dialogView) {
        prevToolbar = dialogView.findViewById(R.id.toolbar);
        prevToolbar.setVisibility(View.GONE);
        tvPrevToolbarTitle = dialogView.findViewById(R.id.tv_toolbarTitle);
        tvPrevToolbarTitle.setVisibility(View.GONE);
        rlHeader = dialogView.findViewById(R.id.rl_header);
        rlHeader.setVisibility(View.VISIBLE);
        tvPrevHeaderTitle = dialogView.findViewById(R.id.tv_header_title);
        tvPrevEdit = dialogView.findViewById(R.id.tv_edit);
        tvPrevEdit.setVisibility(View.VISIBLE);
//        tvPrevEdit.setText("Edit");
        imgPrevHeaderClose = dialogView.findViewById(R.id.img_header_close);
        imgPrevHeaderRight = dialogView.findViewById(R.id.img_right);
        imgPrevHeaderRight.setVisibility(View.GONE);
        imgPrevHeaderLeft = dialogView.findViewById(R.id.img_left);
        imgPrevHeaderLeft.setVisibility(View.GONE);
        tvPrevCustomerCode = dialogView.findViewById(R.id.tv_customercode);
        tvPrevVatRegisterNo = dialogView.findViewById(R.id.tv_vatregno);
        tvPrevName = dialogView.findViewById(R.id.tv_name);
        tvPrevPostBoxNo = dialogView.findViewById(R.id.tv_postboxno);
        tvPrevCity = dialogView.findViewById(R.id.tv_city);
        tvPrevTelephoneNo = dialogView.findViewById(R.id.tv_telephoneno);
        tvPrevMobileNo = dialogView.findViewById(R.id.tv_mobileno);
        llInflateLayout = dialogView.findViewById(R.id.ll_inflatelayout);
        llInflateLayout.setVisibility(View.VISIBLE);
        tvPrevFaxNo = dialogView.findViewById(R.id.tv_faxno);
        tvPrevEmailId = dialogView.findViewById(R.id.tv_email);
        tvPrevCurrentDate = dialogView.findViewById(R.id.tv_currentdate);
        tvPrevCurrentTime = dialogView.findViewById(R.id.tv_currenttime);
        tvPrevVehicleRegisterNo = dialogView.findViewById(R.id.tv_regno);
        tvPrevVehicleModel = dialogView.findViewById(R.id.tv_model);
        tvPrevVehicleChasisNo = dialogView.findViewById(R.id.tv_chasisno);
        tvPrevVehicleColors = dialogView.findViewById(R.id.tv_color);
        tvPrevVehicleFuel = dialogView.findViewById(R.id.tv_fuel);
        tvPrevVehicleOil = dialogView.findViewById(R.id.tv_oil);
        tvPrevVehicleKilometer = dialogView.findViewById(R.id.tv_kilometer);
        tvPrevDeliveryDate = dialogView.findViewById(R.id.tv_deliverydate);
        jobPrevTypeRecyclerview = dialogView.findViewById(R.id.rv_jobtypeitems);
        tvPrevTxtJobType = dialogView.findViewById(R.id.tv_txtjobtype);
        tvPrevTxtJobType.setVisibility(View.GONE);
        jobPrevTypeRecyclerview.setVisibility(View.GONE);
        layout_items = dialogView.findViewById(R.id.layout_items);
        layout_items.setVisibility(View.VISIBLE);
        tvPrevTotalAmount = dialogView.findViewById(R.id.tv_totalamt);
        rgPrevServiceJobType = dialogView.findViewById(R.id.rg_daynightgroup);
        rbPrevServiceNightJob = dialogView.findViewById(R.id.rb_nightjob);
        btnPrevEdit = dialogView.findViewById(R.id.btn_editjobcard);
        btnPrevEdit.setVisibility(View.GONE);
        btnPrevFinish = dialogView.findViewById(R.id.btn_sendjobcard);
        btnPrevFinish.setVisibility(View.GONE);
    }

    public List<JobCardItemsDetailsModel> getRealmListToList(ReturnRealmModel realmmodel) {
        List<JobCardItemsDetailsModel> itemsDetailsList = new ArrayList<>();
        JobCardItemsDetailsModel itemsDetailsModel1;
        if (realmmodel.getJobCardItems() != null || realmmodel.getJobCardItems().size() > 0) {
            for (int k = 0; k < realmmodel.getJobCardItems().size(); k++) {
                itemsDetailsModel1 = new JobCardItemsDetailsModel();
                JobCardItemsDetailsModel item = realmmodel.getJobCardItems().get(k);
//            itemsDetailsModel1.setId(realmmodel.getJobCardItems().get(k).getService_id());
                itemsDetailsModel1.setName(item.getName());
                itemsDetailsModel1.setOther_name(item.getOther_name());
                itemsDetailsModel1.setPrice(item.getPrice());
                itemsDetailsModel1.setJob_card_id(item.getJob_card_id());
                itemsDetailsModel1.setService_id(item.getService_id());
                itemsDetailsList.add(itemsDetailsModel1);
            }
            for (int i = 0; i < itemsDetailsList.size(); i++) {
                Utils.showLogD("Pen1009---" + i + "--" + itemsDetailsList.get(i).getId() + "--" + itemsDetailsList.get(i).getName() + "--" + itemsDetailsList.get(i).getJob_card_id() + "--" +
                        itemsDetailsList.get(i).getService_id());
            }
        }
        return itemsDetailsList;
    }

    public RealmList<ImageUploadModel> getImagesList(ReturnRealmModel realmmodel) {
        RealmList<ImageUploadModel> imgUploadList = new RealmList<>();
        ImageUploadModel imgUpload;
        if (realmmodel.getImageItems() != null || realmmodel.getImageItems().size() > 0) {
            for (int k = 0; k < realmmodel.getImageItems().size(); k++) {
                imgUpload = new ImageUploadModel();
                ImageUploadModel item = realmmodel.getImageItems().get(k);
                imgUpload.setLocaldbid(item.getLocaldbid());
                imgUpload.setId(item.getId());
                imgUpload.setUser_id(item.getUser_id());
                imgUpload.setShop_id(item.getShop_id());
                imgUpload.setImage_encode_str(item.getImage_encode_str());
                imgUpload.setImage_filename(item.getImage_filename());
                imgUpload.setImage_carpartname(item.getImage_carpartname());
                imgUpload.setImage_uniqueid(item.getImage_uniqueid());
                imgUpload.setLocal_insert(item.getLocal_insert());
                imgUpload.setSync_status(item.getSync_status());
                imgUpload.setRow_deleted(item.getRow_deleted());
                imgUpload.setFromServerRecord(item.getFromServerRecord());
                imgUploadList.add(imgUpload);
            }
            for (int i = 0; i < imgUploadList.size(); i++) {
                Utils.showLogD("Pen10010---" + i + "--" + imgUploadList.get(i).getId() + "--" + imgUploadList.get(i).getImage_encode_str() + "--" + imgUploadList.get(i).getImage_filename());
            }
        }
        return imgUploadList;
    }

    public List<JobCardDetailsRealmDBModel> getRealmToCardList(ReturnRealmModel realmmodel) {
        List<JobCardDetailsRealmDBModel> cdList = new ArrayList<>();
        JobCardDetailsRealmDBModel cards;
        if (realmmodel.getJobCard() != null || realmmodel.getJobCard().size() > 0) {
            for (int l = 0; l < realmmodel.getJobCard().size(); l++) {
                cards = new JobCardDetailsRealmDBModel();
                JobCardDetailsRealmDBModel cardDetail = realmmodel.getJobCard().get(l);
                if (cardDetail.getStatus().equalsIgnoreCase(mStatus)) {
                    cards.setLocaldbid(cardDetail.getLocaldbid());
                    cards.setId(cardDetail.getId());
                    cards.setUser_id(cardDetail.getUser_id());
                    cards.setShop_id(cardDetail.getShop_id());
                    cards.setInvoice_no(cardDetail.getInvoice_no());
                    cards.setCustomer_code(cardDetail.getCustomer_code());
                    cards.setVat_reg_no(cardDetail.getVat_reg_no());
                    cards.setName(cardDetail.getName());
                    cards.setPo_box(cardDetail.getPo_box());
                    cards.setCity(cardDetail.getCity());
                    cards.setTel_no(cardDetail.getTel_no());
                    cards.setMobile_no(cardDetail.getMobile_no());
                    cards.setFax_no(cardDetail.getFax_no());
                    cards.setEmail(cardDetail.getEmail());
                    cards.setAdvisor_date(cardDetail.getAdvisor_date());
                    cards.setAdvisor_time(cardDetail.getAdvisor_time());
                    cards.setReg_no(cardDetail.getReg_no());
                    cards.setModel(cardDetail.getModel());
                    cards.setChassis_no(cardDetail.getChassis_no());
                    cards.setColours(cardDetail.getColours());
                    cards.setFuel(cardDetail.getFuel());
                    cards.setOil(cardDetail.getOil());
                    cards.setKilometer(cardDetail.getKilometer());
                    cards.setAdvisor_id(cardDetail.getAdvisor_id());
                    cards.setAdvisor_name(cardDetail.getAdvisor_name());
                    cards.setDelivery_date(cardDetail.getDelivery_date());
                    cards.setJob_type(cardDetail.getJob_type());
                    cards.setTotal_amount(cardDetail.getTotal_amount());
                    cards.setDate_added(cardDetail.getDate_added());
                    cards.setDate_update(cardDetail.getDate_update());
                    cards.setCreated_date(cardDetail.getCreated_date());
                    cards.setCreated_date_str(cardDetail.getCreated_date_str());
                    cards.setAction(cardDetail.getAction());
                    cards.setStatus(cardDetail.getStatus());
                    cards.setItems(cardDetail.getItems());
                    cards.setLocal_insert(cardDetail.getLocal_insert());
                    cards.setSync_status(cardDetail.getSync_status());
                    cards.setRow_deleted(cardDetail.getRow_deleted());
                    cards.setOld_id(cardDetail.getOld_id());
                    cards.setNew_id(cardDetail.getNew_id());
                    cards.setJob_card_pushstr(cardDetail.getJob_card_pushstr());
                    cdList.add(cards);
                }
            }
        }
        for (int i = 0; i < cdList.size(); i++) {
            Utils.showLogD("Pen1008--" + cdList.get(i).getId() + "--" + cdList.get(i).getName() + "--" + cdList.get(i).getCustomer_code());
        }
        return cdList;
    }
}
