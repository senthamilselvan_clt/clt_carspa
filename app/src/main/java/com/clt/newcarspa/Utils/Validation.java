package com.clt.newcarspa.Utils;

import android.content.Context;
import android.text.TextUtils;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.TextView;

import com.clt.newcarspa.R;

/**
 * Created by Siva Kumar on 22-04-2016.
 */
public class Validation {

    private static Validation FormValidation = getInstance();

    public static Validation getInstance() {
        if (FormValidation == null) {
            synchronized (Validation.class) {
                FormValidation = new Validation();
            }
        }

        return FormValidation;
    }

    public boolean isStringLength(String errormsg, EditText string,Context context) {
        if(errormsg.equals(null)) {
            errormsg = "";
        }
        String stringText = string.getText().toString();

        if(stringText.length() >= 10) {
            string.setError(errormsg);
            string.requestFocus();
            return false;
        }
        string.setError(null);
        return true;
    }

    public boolean isStringEmpty(String errormsg, EditText string,Context context) {
        if(errormsg.equals(null)) {
            errormsg = "";
        }
        String stringText = string.getText().toString();

        if(stringText.isEmpty() || stringText == null) {
            string.setError((errormsg.equals("") || errormsg.equals(null)) ? context.getResources().getString(R.string.msg_fieldcannotblank): errormsg);
            string.requestFocus();
            return false;
        }
        string.setError(null);
        return true;
    }

    public boolean isStringEmptyTextview(String errormsg, TextView string, Context context) {
        if(errormsg.equals(null)) {
            errormsg = "";
        }
        String stringText = string.getText().toString();

        if(stringText.isEmpty() || stringText == null) {
            string.setError((errormsg.equals("") || errormsg.equals(null)) ? context.getResources().getString(R.string.msg_fieldcannotblank): errormsg);
            string.requestFocus();
            return false;
        }
        string.setError(null);
        return true;
    }

    public boolean isStringNotEqualtoZero(EditText string,Context context) {

        String stringText = string.getText().toString();
        Float num= Float.parseFloat(stringText);

        if(num==0) {
            string.setError("Its equal to Zero");
            string.requestFocus();
            return false;
        }
        string.setError(null);
        return true;
    }


    public boolean isStrEmptyAtv(AutoCompleteTextView string,Context context){
        String stringText = string.getText().toString();

        if(stringText.isEmpty() || stringText == null) {
            string.setError(context.getResources().getString(R.string.msg_fieldcannotblank));
            string.requestFocus();
            return false;
        }
        string.setError(null);
        return true;
    }

    public boolean email(EditText email,Context context){
        String emailText = email.getText().toString();

        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

        if (emailText.matches(emailPattern)) {
            email.setError(null);
            return true;

        }else {
            if(emailText.isEmpty()) {
                email.setError(context.getResources().getString(R.string.msg_fieldcannotblank));
                email.requestFocus();
            } else {
                email.setError(context.getResources().getString(R.string.msg_validemail));
                email.requestFocus();
            }

        }
        return false;
    }
    public static boolean isDigits(String str) {
        try {
            Integer.parseInt(str);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }
    public boolean isLengthMin(EditText string,Context context,Integer min) {

        String stringText = string.getText().toString();

        if(TextUtils.isEmpty(stringText) || stringText.length() < min){
            string.setError("Field must have "+min+" characters");
            string.requestFocus();
            return false;
        }
        string.setError(null);
        return true;
    }
    public boolean isLengthMax(EditText string,Context context,Integer max) {

        String stringText = string.getText().toString();

        if(stringText.length() > max){
            string.setError("Field must have "+max+" characters");
            string.requestFocus();
            return false;
        }
        string.setError(null);
        return true;
    }
}
