package com.clt.newcarspa.Utils;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.clt.newcarspa.BuildConfig;
import com.clt.newcarspa.R;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.net.URL;
import java.util.Collection;



/**
 * Created by Karthik Thumathy on 23-Jul-18.
 */
public class Utils {

    static Context context;
    private static Utils instance = new Utils();
    private static boolean close = false;
    ConnectivityManager connectivityManager;

    public static Utils getInstance(Context ctx) {
        context = ctx.getApplicationContext();
        return instance;
    }

    public static boolean isTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_LARGE;
    }

    public static boolean closeAlert(Context context){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle(context.getResources().getString(R.string.app_name));
        alertDialog.setMessage(context.getResources().getString(R.string.msg_closeit));
        alertDialog.setPositiveButton(context.getResources().getString(R.string.msg_yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                close = true;
            }
        });
        alertDialog.setNegativeButton(context.getResources().getString(R.string.msg_no), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                close = false;
                dialog.cancel();
            }
        });
        alertDialog.show();
        return close;
    }

    public static void showLogD(String msg) {
        Log.d("--Debug--", "" + msg);
    }

    public static void showLogE(String msg) {
        Log.e("--Error--", "" + msg);
    }

    public static void showLogI(String msg) {
        Log.i("--Info--", "" + msg);
    }

    public static void showToastShort(String msg) {
        Toast.makeText(context, "" + msg, Toast.LENGTH_SHORT).show();
    }

    public static void showToastDev(Context context, String msg) {
        if(BuildConfig.FLAVOR.equals("dev")) {
            Toast.makeText(context, "" + msg, Toast.LENGTH_SHORT).show();
        }
    }

    public static void showToast(Context context, String msg) {
        Toast.makeText(context, "" + msg, Toast.LENGTH_SHORT).show();
    }

    public static void showToast(Context context, @StringRes int msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    public static void showToastlong(Context context, String msg) {
        Toast.makeText(context, "Msg--" + msg, Toast.LENGTH_LONG).show();
    }

    public static void handleFragmentTransaction(Fragment selectedFragment, FragmentManager fManager, String selectedTags){
       /* FragmentTransaction fTrans = fManager.beginTransaction();
        fTrans.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.popenter,
                R.anim.popexit);
        fTrans.replace(R.id.container_body, selectedFragment, selectedTags);
        fTrans.addToBackStack(selectedTags);
        fTrans.commit();*/
    }

    public static void hideKeyboard(Activity activity) {
        try {
            InputMethodManager inputManager = (InputMethodManager) activity
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            if (inputManager != null && activity.getCurrentFocus() != null)
                inputManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = null;
        if (connectivityManager != null)
            activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnected()) {
            return true;
        }
        return false;
    }

    public static boolean isInternet() {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnected()) {
            return true;
        }else {
            Utils.showToast(context,context.getResources().getString(R.string.msg_networkcheck));
        }
        return false;
    }


    public static void saveToUserDefaults(Context context, String key,
                                          String value) {

        Log.d("Utils", "Saving:" + key + ":" + value);
        SharedPreferences preferences = context.getSharedPreferences(
                Constant.SHARED_PREFS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.commit();

    }

    public static String getFromUserDefaults(Context context, String key) {
        Log.d("Utils", "Get:" + key);
        SharedPreferences preferences = context.getSharedPreferences(
                Constant.SHARED_PREFS, Context.MODE_PRIVATE);
        return preferences.getString(key, "");
    }

    public static void saveIntegerToUserDefaults(Context context, String key,
                                                 int value) {

        Log.d("Utils", "Saving:" + key + ":" + value);
        SharedPreferences preferences = context.getSharedPreferences(
                Constant.SHARED_PREFS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(key, value);
        editor.commit();

    }

    public static int getIntegerFromUserDefaults(Context context, String key) {
        Log.d("Utils", "Get:" + key);
        SharedPreferences preferences = context.getSharedPreferences(
                Constant.SHARED_PREFS, Context.MODE_PRIVATE);
        return preferences.getInt(key, 0);
    }


    public static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email)
                && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static void clearArrays(Collection... array) {
        for (Collection e : array) e.clear();
    }

    public static boolean isValid(String url)
    {
        try {
            new URL(url).toURI();
            return true;
        }
        catch (Exception e) {
            return false;
        }
    }

    public static File saveBitmapToFile(File file) {
        try {
            BitmapFactory.Options o = new BitmapFactory.Options();
            FileInputStream inputStream = new FileInputStream(file);
            BitmapFactory.decodeStream(inputStream, null, o);
            inputStream.close();
            final int REQUIRED_SIZE = 100;
            int scale = 1;
            while (o.outWidth / scale / 2 >= REQUIRED_SIZE &&
                    o.outHeight / scale / 2 >= REQUIRED_SIZE) {
                scale *= 2;
            }

            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = 4;
            inputStream = new FileInputStream(file);
            ;
            Bitmap selectedBitmap = BitmapFactory.decodeStream(inputStream, null, o2);
            inputStream.close();

            file.createNewFile();
            FileOutputStream outputStream = new FileOutputStream(file);

            selectedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);

            return file;
        } catch (Exception e) {
            return null;
        }
    }

    public static File saveBitmapToRequriedFile(File inputFile, File outputFile) {
        try {
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = false;
            o.inSampleSize = 4;
            FileInputStream inputStream;
            int file_size = Integer.parseInt(String.valueOf(inputFile.length() / 1024));
            if (file_size > 1000) {
                inputStream = new FileInputStream(inputFile);
                BitmapFactory.decodeStream(inputStream, null, o);
                inputStream.close();
                final int REQUIRED_SIZE = 100;
                int scale = 1;
                while (o.outWidth / scale / 2 >= REQUIRED_SIZE &&
                        o.outHeight / scale / 2 >= REQUIRED_SIZE) {
                    scale *= 2;
                }

                BitmapFactory.Options o2 = new BitmapFactory.Options();
                o2.inSampleSize = scale;
                inputStream = new FileInputStream(inputFile);

                Bitmap selectedBitmap = BitmapFactory.decodeStream(inputStream, null, o2);
                inputStream.close();
                outputFile.createNewFile();
                FileOutputStream outputStream = new FileOutputStream(outputFile);

                selectedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);

                return outputFile;
            } else {
                outputFile.delete();
                return inputFile;
            }
        } catch (Exception e) {
            return null;
        }
    }


    // Progress code Start

    // Progress code End
}
