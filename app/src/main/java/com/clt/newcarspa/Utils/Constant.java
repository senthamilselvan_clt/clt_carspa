package com.clt.newcarspa.Utils;

/**
 * Created by Karthik Thumathy on 23-Jul-18.
 */
public class Constant {

    public final static String SHARED_PREFS = "MyCar";
    public final static String NEWJOBCARD = "New Job Card";
    public final static String VIEWJOBCARDLIST = "View Job Card List";
    public final static String REPORT = "Report";
    public final static String TODAYDELIVERY = "Today Delivery";
    public final static String CREATEINVOICE = "Create Invoice";
    public final static String FINDINVOICE = "Find Invoice";
    public final static String DRAFT = "Draft";
    public final static String CREDITINVOICE = "Credit Invoice";
    public final static String SETTINGS = "Settings";
    public final static String GALLERY = "Gallery";
    public final static String PUSH = "Push";
    public final static String LOGOUT = "Logout";
    public final static String ACTIONLOGIN = "login";
    public final static String PENDINGSTATUS = "Pending";
    public final static String PROGRESSSTATUS = "Progress";
    public final static String COMPLETEDSTATUS = "Completed";
    public final static String DELIVEREDSTATUS = "Delivered";

    public final static String LOGINUSERID = "loginuserid";
    public final static String LOGINUSERNAME = "loginusername";
    public final static String LOGINUSERPHONE = "loginuserphone";
    public final static int LOADER_TIME_OUT = 500;
    public final static String RESPONSESUCCESS = "success";


    public final static String SETJOBCARD = "set_job_card";
    public final static String PUSHJOBCARDLIST = "push_job_card_list";
    public final static String CARPARTSlIST = "car_parts_list";
    public final static String IMGID = "jobcardId";
    public final static String IMGUNIQUEID = "imguniqueid";
    public final static String GETALLJOBCARDLIST = "get_all_job_card_list";
}
