package com.clt.newcarspa.Utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Karthik Thumathy on 25-Jul-18.
 */
public class Session {
    public static Session session;

    public static Session getInstance() {

        if (session == null) {
            synchronized (Session.class) {
                session = new Session();
            }
        }
        return session;
    }


    private String user_list;
    private String uid, userName, email, userDetails, gid, shopId;
    private String current_exam_id;
    private String reportResult, currentQuesResult;

    private String vat_value;
    private boolean isTablet;

    public void setIsTablet(Context context, boolean isTablet) {
        SharedPreferences sharedpreferences = context.getSharedPreferences("CarSpa", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();

        editor.putBoolean("isTablet", isTablet);
        editor.apply();

        this.isTablet = isTablet;
    }

    public boolean getIsTablet(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("CarSpa", Context.MODE_PRIVATE);
        isTablet = sharedPreferences.getBoolean("isTablet", false);
        return isTablet;
    }

    public void setUid(Context context, String uid) {
        SharedPreferences sharedpreferences = context.getSharedPreferences("CarSpa", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();

        editor.putString("uid", uid);
        editor.apply();

        this.uid = uid;
    }

    public String getUid(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("CarSpa", Context.MODE_PRIVATE);
        uid = sharedPreferences.getString("uid", "");
        return uid;
    }

    public void setGid(Context context, String gid) {
        SharedPreferences sharedpreferences = context.getSharedPreferences("CarSpa", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();

        editor.putString("gid", gid);
        editor.apply();

        this.uid = uid;
    }

    public String getGid(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("CarSpa", Context.MODE_PRIVATE);
        gid = sharedPreferences.getString("gid", "");
        return gid;
    }

    public String getUserName(Context context) {

        SharedPreferences sharedPreferences = context.getSharedPreferences("CarSpa", Context.MODE_PRIVATE);
        userName = sharedPreferences.getString("user_name", "");
        return userName;
    }

    public void setUserName(Context context, String userName) {
        SharedPreferences sharedpreferences = context.getSharedPreferences("CarSpa", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();

        editor.putString("user_name", userName);
        editor.apply();
        this.userName = userName;
    }

    public String getShopId(Context context) {

        SharedPreferences sharedPreferences = context.getSharedPreferences("CarSpa", Context.MODE_PRIVATE);
        shopId = sharedPreferences.getString("shopid", "");
        return shopId;
    }

    public void setShopId(Context context, String shopid) {
        SharedPreferences sharedpreferences = context.getSharedPreferences("CarSpa", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();

        editor.putString("shopid", shopid);
        editor.apply();
        this.shopId = shopid;
    }

    //get and set all registed users with status = 1 and is_active = 1
    //start
    public String getUser_list(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("CarSpa", Context.MODE_PRIVATE);
        user_list = sharedPreferences.getString("user_list", "");
        return user_list;
    }

    public void setUser_list(Context context, String user_list) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("CarSpa", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("user_list", user_list);
        editor.apply();
        this.user_list = user_list;
    }//end


    public String getEmail(Context context) {

        SharedPreferences sharedPreferences = context.getSharedPreferences("CarSpa", Context.MODE_PRIVATE);
        email = sharedPreferences.getString("email", "");
        return email;
    }

    public void setEmail(Context context, String email) {
        SharedPreferences sharedpreferences = context.getSharedPreferences("CarSpa", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();

        editor.putString("email", email);
        editor.apply();
        this.email = email;
    }


    public String getVat_value(Context context) {

        SharedPreferences sharedPreferences = context.getSharedPreferences("CarSpa", Context.MODE_PRIVATE);
        vat_value = sharedPreferences.getString("vat_value", "");
        return vat_value;
    }

    public void setVat_value(Context context, String vat_value) {
        SharedPreferences sharedpreferences = context.getSharedPreferences("CarSpa", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();

        editor.putString("vat_value", vat_value);
        editor.apply();
        this.vat_value = vat_value;
    }

    public String getCurrent_exam_id(Context context) {

        SharedPreferences sharedPreferences = context.getSharedPreferences("CarSpa", Context.MODE_PRIVATE);
        current_exam_id = sharedPreferences.getString("current_exam_id", "");
        return current_exam_id;
    }

    public void setCurrent_exam_id(Context context, String current_exam_id) {
        SharedPreferences sharedpreferences = context.getSharedPreferences("CarSpa", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();

        editor.putString("current_exam_id", current_exam_id);
        editor.apply();
        this.current_exam_id = current_exam_id;
    }

    public void setUserDetails(Context context, String userDetails) {
        SharedPreferences sharedpreferences = context.getSharedPreferences("CarSpa", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();

        editor.putString("user_details", userDetails);
        editor.apply();
        this.userDetails = userDetails;

    }

    public String getUserDetails(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("CarSpa", Context.MODE_PRIVATE);
        userDetails = sharedPreferences.getString("user_details", "");
        return userDetails;
    }

    public String getReportResult(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("CarSpa", Context.MODE_PRIVATE);
        reportResult = sharedPreferences.getString("reports_result", "");
        return reportResult;
    }

    public void setReportResult(Context context, String reportResult) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("CarSpa", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString("reports_result", reportResult);
        editor.apply();
        this.reportResult = reportResult;
    }

    public String getCurrentQuesResult(Context context) {

        SharedPreferences sharedPreferences = context.getSharedPreferences("CarSpa", Context.MODE_PRIVATE);
        currentQuesResult = sharedPreferences.getString("current_ques_result", "");
        return currentQuesResult;
    }

    public void setCurrentQuesResult(Context context, String currentQuesResult) {

        SharedPreferences sharedPreferences = context.getSharedPreferences("CarSpa", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString("current_ques_result", currentQuesResult);
        editor.apply();

        this.currentQuesResult = currentQuesResult;
    }

    public Boolean isApplicationExit(Context context) {
        String user_id = getUid(context);
        if (user_id == null || user_id.isEmpty() ||
                user_id.equals("")) {
            return false;
        }
        return true;
    }

    public void destroy(Context context) {
        SharedPreferences sharedpreferences = context.getSharedPreferences("CarSpa", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit().clear();
        editor.apply();
    }


    public void setPrinterConfig(Context context, String printerConn, String printerPort, String printerAddr, String printerSize) {
        SharedPreferences sharedpreferences = context.getSharedPreferences("PrinterConfig", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();

        editor.putString("printer_conn", printerConn);
        editor.putString("printer_port", printerPort);
        editor.putString("printer_addr", printerAddr);
        editor.putString("printer_size", printerSize);
        editor.apply();
    }

    public String getPrinterConn(Context context) {
        SharedPreferences sharedpreferences = context.getSharedPreferences("PrinterConfig", Context.MODE_PRIVATE);
        String printer_conn = sharedpreferences.getString("printer_conn", "ETHERNET");
        return printer_conn;
    }

    public String getPrinterPort(Context context) {
        SharedPreferences sharedpreferences = context.getSharedPreferences("PrinterConfig", Context.MODE_PRIVATE);
        String printer_port = sharedpreferences.getString("printer_port", "9100");
        return printer_port;
    }

    public String getPrinterAddr(Context context) {
        SharedPreferences sharedpreferences = context.getSharedPreferences("PrinterConfig", Context.MODE_PRIVATE);
        String printer_addr = sharedpreferences.getString("printer_addr", "");
        return printer_addr;
    }

    public String getPrinterSize(Context context) {
        SharedPreferences sharedpreferences = context.getSharedPreferences("PrinterConfig", Context.MODE_PRIVATE);
        String printer_size = sharedpreferences.getString("printer_size", "");
        return printer_size;
    }
}
