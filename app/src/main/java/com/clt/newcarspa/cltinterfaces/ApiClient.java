package com.clt.newcarspa.cltinterfaces;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;


public class ApiClient {
    private static Retrofit retrofit = null;
//    public final static String SOCKET_IO_URL = BuildConfig.SERVER_URL;

    public static class Apis {
       /* private static String NODE_API_URL = Servercom.SOCKET_IO_URL + "/api/";

        public static String getNodeSendOTP() {
            return Servercom.SOCKET_IO_URL + "/";
        }

        public static String getBaseNodejsUrl() {
            return NODE_API_URL;
        }
*/

//        public static String getBaseUrl() {
//            return SOCKET_IO_URL;
//        }
    }

    private static void getClient(){
        OkHttpClient.Builder client = new OkHttpClient.Builder();
        client.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Interceptor.Chain chain) throws IOException {
                Request original = chain.request();

                Request request = original.newBuilder()
                        .header("User-Agent", "Your-App-Name")
                        .header("Accept", "application/vnd.yourapi.v1.full+json")
                        .method(original.method(), original.body())
                        .build();

                return chain.proceed(request);
            }
        });
    }

    /*public static Retrofit getDefaultUrl() {
            retrofit = new Retrofit.Builder()
                    .baseUrl(Apis.getBaseUrl())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        return retrofit;
    }
*/


}
