package com.clt.newcarspa.cltinterfaces;

import com.clt.newcarspa.model.APIListResponse;
import com.clt.newcarspa.model.APIResponse;
import com.clt.newcarspa.model.AudioUploadModel;
import com.clt.newcarspa.model.CarPartsModel;
import com.clt.newcarspa.model.ImageUploadModel;
import com.clt.newcarspa.model.JobCardDetailsModel;
import com.clt.newcarspa.model.JobCardDetailsRealmDBModel;
import com.clt.newcarspa.model.JobTypeModel;
import com.clt.newcarspa.model.NewJobCardViewListModel;
import com.clt.newcarspa.model.SyncJobCardModel;
import com.clt.newcarspa.model.UserLoginDetails;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by Karthik_Thumathy on 14/6/17.
 */


public interface ApiInterfaceOne {

    public static final String LOGIN = "users.php/";
    public static final String ORDERS = "orders.php/";

    // Get Login Information
    @FormUrlEncoded
    @POST(LOGIN)
    Call<List<UserLoginDetails>> getLoginInfo(@Field("action") String action, @Field("phone") String phone, @Field("user_pass") String user_pass);

    @POST(LOGIN)
    Call<APIResponse<UserLoginDetails>> getLoginInfoDetail(@Body UserLoginDetails user);

    @POST(ORDERS)
    Call<APIListResponse<JobTypeModel>> getJobTypList(@Body JobTypeModel jobTypeModel);

    @POST(ORDERS)
    Call<APIListResponse<JobCardDetailsRealmDBModel>> setJobCard(@Body JobCardDetailsRealmDBModel jobCardModel);

    @POST(ORDERS)
    Call<APIListResponse<JobCardDetailsRealmDBModel>> getJobCardList(@Body JobCardDetailsRealmDBModel jobCardModel);

    @POST(ORDERS)
    Call<APIResponse<NewJobCardViewListModel>> getNewJobCardList(@Body NewJobCardViewListModel jobCardModel);

    @POST(ORDERS)
    Call<APIListResponse<JobCardDetailsRealmDBModel>> getNewJobCardIdDetails(@Body JobCardDetailsRealmDBModel jobCardModel);

    @POST(ORDERS)
    Call<APIListResponse<JobCardDetailsRealmDBModel>> pushJobCardList(@Body JobCardDetailsRealmDBModel jobCardDetails);

    @POST(ORDERS)
    Call<APIResponse<SyncJobCardModel>> getAllCardListFromServer(@Body SyncJobCardModel jobCardDetails);

    @POST(ORDERS)
    Call<APIListResponse<CarPartsModel>> getCarPartsList(@Body CarPartsModel jobCardDetails);

    @POST(ORDERS)
    Call<APIResponse<ImageUploadModel>> uploadImageToServer(@Body ImageUploadModel jobCardDetails);

    @POST(ORDERS)
    Call<APIResponse<AudioUploadModel>> uploadaudioToServer(@Body AudioUploadModel jobCardDetails);

    @POST(ORDERS)
    Call<APIListResponse<JobCardDetailsRealmDBModel>> updateJobCardList(@Body JobCardDetailsRealmDBModel jobCardDetails);

}