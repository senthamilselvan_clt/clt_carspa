package com.clt.newcarspa.cltinterfaces;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.clt.newcarspa.BuildConfig;
import com.clt.newcarspa.R;
import com.clt.newcarspa.Utils.DateDeserializer;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Karthik Thumathy on 23-Jul-18.
 */
public class RestAdapter {
    @SuppressLint("StaticFieldLeak")
    private static RestAdapter instance;
    private static final String KEY_TOKEN = "key.token";
    private ApiInterfaceOne apiInterface;
    private SharedPreferences preferences;
    private Context context;

    private RestAdapter(Context context) {
        this.context = context.getApplicationContext();
        preferences = context.getSharedPreferences(getClass().getSimpleName(), Context.MODE_PRIVATE);
    }

    public static void init(Context context) {
        instance = new RestAdapter(context);
    }

    public static RestAdapter getInstance() {
        return instance;
    }

    private HttpLoggingInterceptor getLoggingInterceptor() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        if (BuildConfig.DEBUG) interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        else interceptor.setLevel(HttpLoggingInterceptor.Level.NONE);
        return interceptor;
    }

    private OkHttpClient getClient() {
        return new OkHttpClient.Builder()
                .addInterceptor(getLoggingInterceptor())
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request.Builder builder = chain.request().newBuilder();
                        builder.addHeader("Content-Type", "application/json");
                        builder.addHeader("Authorization", getToken());
                        Log.d("AuthorizationToken--", "" + getToken());
                        return chain.proceed(builder.build());
                    }
                }).connectTimeout(60, TimeUnit.SECONDS)
                .build();
    }

    private String getToken() {
        return preferences.getString(KEY_TOKEN, "");
    }

    private Retrofit getAdapter() {
        Gson gson = new GsonBuilder()
//                .setDateFormat("yyyy-MM-dd hh:mm:ss.S")
                .registerTypeAdapter(Date.class, new DateDeserializer())
                .setLenient()
                .create();
        return new Retrofit.Builder()
                .baseUrl(BuildConfig.SERVER_URL)
                .client(getClient())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }

    public ApiInterfaceOne getServiceApi() {
        if (apiInterface == null) apiInterface = getAdapter().create(ApiInterfaceOne.class);
        return apiInterface;
    }

    private void reCreate() {
        apiInterface = getAdapter().create(ApiInterfaceOne.class);
    }

    public void setToken(String token) {
        preferences.edit().putString(KEY_TOKEN, token).apply();
        authtoken(token);
        reCreate();
    }

    private void authtoken(String authToken) {
        SharedPreferences settings = context
                .getSharedPreferences(context.getString(R.string.prefs),
                        Context.MODE_PRIVATE);
        SharedPreferences.Editor e = settings.edit();
        e.putString(context.getString(R.string.auth_token), authToken);
        e.commit();
    }
}
