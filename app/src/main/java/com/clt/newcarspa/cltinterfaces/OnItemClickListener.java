package com.clt.newcarspa.cltinterfaces;

import android.view.View;

/**
 * Created by Karthik on 30-06-2016.
 */

public interface OnItemClickListener {
    public void onItemClick(View view, int position);
    public void onItemClickWithId(View view, int position, Integer id);
}