package com.clt.newcarspa.database;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.clt.newcarspa.Utils.Constant;
import com.clt.newcarspa.Utils.Utils;
import com.clt.newcarspa.model.AudioUploadModel;
import com.clt.newcarspa.model.CarPartsModel;
import com.clt.newcarspa.model.GalleryUploadModel;
import com.clt.newcarspa.model.ImageUploadModel;
import com.clt.newcarspa.model.JobCardCarViewModelDB;
import com.clt.newcarspa.model.JobCardDetailsModel;
import com.clt.newcarspa.model.JobCardDetailsRealmDBModel;
import com.clt.newcarspa.model.JobCardItemsDetailsModel;
import com.clt.newcarspa.model.JobTypeModel;
import com.clt.newcarspa.model.MyCarListItems;
import com.clt.newcarspa.model.ReturnRealmModel;
import com.clt.newcarspa.model.SyncJobCardModel;
import com.clt.newcarspa.model.UserLoginDetails;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import io.realm.Sort;
import io.realm.exceptions.RealmException;

/**
 * Created by Karthik Thumathy on 23-Jul-18.
 */

public class RealmDBHelper {

    private static final String TAG = RealmDBHelper.class.getCanonicalName();
    private static Realm mRealm;
    private static RealmDBHelper instance;
    private static boolean jobCardAdded = false;
    private static boolean jobCardItemAdded = false;
    private static Integer nextId;


    public RealmDBHelper(Context context) {
        mRealm = Realm.getDefaultInstance();
    }

    public RealmDBHelper() {
        mRealm = getInstance().getRealm();
    }

    public static RealmDBHelper with(Fragment fragment) {
        if (instance == null) {
            instance = new RealmDBHelper(fragment.getActivity().getApplication());
        }
        return instance;
    }

    public static RealmDBHelper with(Activity activity) {
        if (instance == null) {
            instance = new RealmDBHelper(activity.getApplication());
        }
        return instance;
    }

    public static RealmDBHelper with(Application application) {
        if (instance == null) {
            instance = new RealmDBHelper(application);
        }
        return instance;
    }

    public static RealmDBHelper getInstance() {
        return instance;
    }

    public Realm getRealm() {
        return mRealm;
    }

    public void realmDbClose() {
        try {
            mRealm.close();
        } catch (RealmException e) {
            e.printStackTrace();
        } finally {
            mRealm.close();
        }
    }

    public Integer addHomePageItems(Realm realm, String id, List<String> itemlist) {
        Integer count = 0;
        RealmList<MyCarListItems> carList = new RealmList<>();
        try {
            realm.beginTransaction();
            for (int i = 0; i < itemlist.size(); i++) {
                MyCarListItems listItems = realm.createObject(MyCarListItems.class);
                listItems.setId(id);
                listItems.setItemName(itemlist.get(i));
                if (itemlist.get(i).equalsIgnoreCase("Gallery")) {
                    listItems.setIsDisplay(0);
                } else {
                    listItems.setIsDisplay(1);
                }
                carList.add(listItems);
            }
            realm.commitTransaction();

            List<MyCarListItems> results = realm.where(MyCarListItems.class).findAll();
            realm.beginTransaction();
            for (int i = 0; i < results.size(); i++) {
                Log.d("1000", "HomepageItems---" + results.get(i).getId() + "--" + results.get(i).getItemName());
            }
            count = results.size();
            realm.commitTransaction();
        } catch (RealmException e) {
            e.printStackTrace();
        }
        return count;
    }

    public boolean updateHomePageItems(Realm realm, Integer id) {
        boolean updatestatus = false;
        try {
            realm.beginTransaction();
            MyCarListItems carddetails = realm.where(MyCarListItems.class).equalTo("itemName", "Gallery").findFirst();
            carddetails.setIsDisplay(id);
            realm.commitTransaction();
            updatestatus = true;
        } catch (RealmException e) {
            e.printStackTrace();
            updatestatus = false;
        }
        return updatestatus;
    }

    public MyCarListItems getGalleryDetail(Realm realm) {
        MyCarListItems results = realm.where(MyCarListItems.class).equalTo("itemName", "Gallery").findFirst();
        try {
            realm.beginTransaction();
//            for (int i = 0; i < results.size(); i++) {
//                Log.d("1000 Realmdb--", "HomepageItems---" + results.get(i).getId() + "--" + results.get(i).getItemName()); // + "--" + results.get(i).getItemImage()
//            }
            realm.commitTransaction();
        } catch (RealmException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return results;
    }

    public List<MyCarListItems> getAllHomePageItems(Realm realm) {
        Integer count = 0;
        List<MyCarListItems> results = realm.where(MyCarListItems.class).equalTo("isDisplay", 1).findAll();
        try {
            realm.beginTransaction();
            for (int i = 0; i < results.size(); i++) {
                Log.d("1000 Realmdb--", "HomepageItems---" + results.get(i).getId() + "--" + results.get(i).getItemName()); // + "--" + results.get(i).getItemImage()
            }
            realm.commitTransaction();
        } catch (RealmException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return results;
    }

    public Integer deleteHomePageItems(Realm mrealm) {
        Integer value = 0;
        RealmResults<MyCarListItems> results = mrealm.where(MyCarListItems.class).findAll();
        try {
            mrealm.beginTransaction();
            // Delete all matches
            results.deleteAllFromRealm();
            mrealm.commitTransaction();
            value = 1;
        } catch (RealmException e) {
            e.printStackTrace();
            value = 0;
        }
        return value;
    }

    public Integer addLoginDetails(Realm realm, UserLoginDetails userDetails) {
        RealmList<UserLoginDetails> carList = new RealmList<>();
        Integer count = 0;
        try {
            realm.beginTransaction();
            UserLoginDetails details = realm.createObject(UserLoginDetails.class);
            details.setId(userDetails.getId());
            details.setUser_name(userDetails.getUser_name());
            details.setUser_pass(userDetails.getUser_pass());
            details.setRole_id(userDetails.getRole_id());
            details.setManufacturing_unit_id(userDetails.getManufacturing_unit_id());
            details.setShop_id(userDetails.getShop_id());
            details.setFirst_name(userDetails.getFirst_name());
            details.setLast_name(userDetails.getLast_name());
            details.setEmail(userDetails.getEmail());
            details.setPhone(userDetails.getPhone());
            details.setIs_active(userDetails.getIs_active());
            details.setStatus(userDetails.getStatus());
            details.setUser_action(userDetails.getUser_action());
            details.setCreated_at(userDetails.getCreated_at());
            details.setUpdated_at(userDetails.getUpdated_at());
            details.setFcm_id(userDetails.getFcm_id());
            details.setRole(userDetails.getRole());
            details.setAction(userDetails.getAction());
            carList.add(details);
            realm.commitTransaction();

            List<UserLoginDetails> results = realm.where(UserLoginDetails.class).findAll();
            realm.beginTransaction();
            for (int i = 0; i < results.size(); i++) {
                Log.d("1000", "Logindetails---" + results.get(i).getId() + "--" + results.get(i).getUser_name());
            }
            count = results.size();
            realm.commitTransaction();
        } catch (RealmException e) {
            e.printStackTrace();
            count = 0;
        }
        return count;
    }

    public List<UserLoginDetails> getUserLoginDetail(Realm realm) {
        Integer count = 0;
        List<UserLoginDetails> results = realm.where(UserLoginDetails.class).findAll();
        try {
            realm.beginTransaction();
            for (int i = 0; i < results.size(); i++) {
                UserLoginDetails logindetails = results.get(i);
                Log.d("1000 Realmdb--", "LoginDetails---" + logindetails.getId() + "--" + logindetails.getUser_name() + "--" + logindetails.getAction());
            }
            realm.commitTransaction();
        } catch (RealmException e) {
            e.printStackTrace();
            count = 0;
        }
        return results;
    }

    public List<JobTypeModel> getJobTypeList(Realm realm) {
        List<JobTypeModel> jobTypeResult = realm.where(JobTypeModel.class).findAll();
        try {
            realm.beginTransaction();
            for (int i = 0; i < jobTypeResult.size(); i++) {
                JobTypeModel jobType = jobTypeResult.get(i);
                Utils.showLogD("1000 Realmdb--jobtypelist---" + jobType.getName() + jobType.getPrice());
            }
            realm.commitTransaction();
        } catch (RealmException e) {
            e.printStackTrace();
        }
        return jobTypeResult;
    }

    public ReturnRealmModel addCarPartsItems(Realm realm, final List<CarPartsModel> carPartsList) {
        final ReturnRealmModel realmModel = new ReturnRealmModel();
        final List<CarPartsModel> partsList = new ArrayList<>();
        try {
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    CarPartsModel carPart;
                    for (int c = 0; c < carPartsList.size(); c++) {
                        carPart = realm.createObject(CarPartsModel.class);
                        carPart.setId(carPartsList.get(c).getId());
                        carPart.setCar_parts(carPartsList.get(c).getCar_parts());
                        partsList.add(carPart);
                    }
                    realmModel.setAddedCarPartsStatus(1);
                    List<CarPartsModel> results = realm.where(CarPartsModel.class).findAll();
                    Log.d("carParts--", "" + results.size());
                }
            });
        } catch (RealmException e) {
            e.printStackTrace();
            realmModel.setAddedCarPartsStatus(0);
        }
        return realmModel;
    }

    public List<CarPartsModel> getCartPartsList(Realm realm) {
        List<CarPartsModel> jobCardResult = realm.where(CarPartsModel.class).findAll();
        return jobCardResult;
    }


    public Integer addJobTypeDetails(Realm realm, List<JobTypeModel> jobType) {
        RealmList<JobTypeModel> carList = new RealmList<>();
        Integer count = 0;
        try {
            realm.beginTransaction();

            JobTypeModel details;
            for (JobTypeModel model : jobType) {

                details = realm.createObject(JobTypeModel.class);
                details.setId(model.getId());
                details.setName(model.getName());
                details.setOther_name(model.getOther_name());
                details.setPrice(model.getPrice());
                details.setActive(model.getActive());
                details.setDate_added(model.getDate_added());
                details.setDate_updated(model.getDate_updated());
                carList.add(details);
            }
            realm.commitTransaction();

            List<JobTypeModel> results = realm.where(JobTypeModel.class).findAll();
            realm.beginTransaction();
            for (JobTypeModel res : results) {
          /*  Log.d("","-------------------------------------------------------Start");
            Log.d("10001","getId--"+res.getId());
            Log.d("10001","getName--"+res.getName());
            Log.d("10001","getOther_name--"+res.getOther_name());
            Log.d("10001","getPrice--"+res.getPrice());
            Log.d("10001","getActive--"+res.getActive());
            Log.d("10001","getDate_added--"+res.getDate_added());
            Log.d("10001","getDate_updated--"+res.getDate_updated());
            Log.d("","-------------------------------------------------------End");*/
            }
            count = results.size();
            realm.commitTransaction();
        } catch (RealmException e) {
            e.printStackTrace();
        }
        return count;
    }

    public ReturnRealmModel getAllJobCardDetail(Realm realm) {
        ReturnRealmModel returnmodel = new ReturnRealmModel();
        try {
            List<JobCardDetailsRealmDBModel> jobCardResult = realm.where(JobCardDetailsRealmDBModel.class)
//                .equalTo("local_insert", "1")
//                .equalTo("sync_status", "false")
                    .equalTo("row_deleted", 0)
                    .findAll();
            List<JobCardItemsDetailsModel> jobCardItemsResult = realm.where(JobCardItemsDetailsModel.class)
                    .equalTo("row_deleted", 0)
                    .findAll();
            int dbJobCardSize = jobCardResult.size();
            int dbJobCardItemSize = jobCardItemsResult.size();
            realm.beginTransaction();
            Utils.showLogD("1007 Realmdb--jobCardResult---size--" + jobCardResult.size() + "--------jobCardItemResult---size--" + jobCardItemsResult.size());
            Utils.showLogD("1007 Realmdb--jobCardResult---id--------row_deleted---------name----------local_insert----------sync_status");
            for (int i = 0; i < jobCardResult.size(); i++) {
                JobCardDetailsRealmDBModel jobType = jobCardResult.get(i);
                Utils.showLogD("1007 Realmdb--jobCardResult---" + jobType.getId() + "--" + jobType.getRow_deleted() + "--" + jobType.getName()
                        + "--" + jobType.isLocal_insert() + "--" + jobType.isSync_status() + "--" + jobType.getCreated_date() + "--" + jobType.getDate_added() + "--" + jobType.getAdvisor_date());
            }

            Utils.showLogD("1007 ------------------------------------------------------------------------------------------------------------");
            Utils.showLogD("1007 Realmdb--jobCardItemsResult---------jobcardid--------id--------serviceid--------row_deleted---------name----------othername----------price-----------");
            for (int i = 0; i < jobCardItemsResult.size(); i++) {
                JobCardItemsDetailsModel jobType = jobCardItemsResult.get(i);
                Utils.showLogD("1007 Realmdb--jobCardItemsResult---" + jobType.getJob_card_id() + "--" + jobType.getId() + "--" + jobType.getService_id() + "--" + jobType.getRow_deleted() + "--" + jobType.getName() + "--" + jobType.getOther_name() + "--" + jobType.getPrice());
            }
            Utils.showLogD("1007 ------------------------------------------------------------------------------------------------------------");
            realm.commitTransaction();
            returnmodel.setJobCardDetailSize(dbJobCardSize);
            returnmodel.setJobCardItemDetailSize(dbJobCardItemSize);

        } catch (RealmException e) {
            e.printStackTrace();
        }
        return returnmodel;
    }

    public List<JobCardDetailsRealmDBModel> getJobCardDetail(Realm realm, Integer id) {

        List<JobCardDetailsRealmDBModel> jobCardResult = realm.where(JobCardDetailsRealmDBModel.class).equalTo("id", id).equalTo("row_deleted", 0).findAll();
        try {
            realm.beginTransaction();
            for (int i = 0; i < jobCardResult.size(); i++) {
                JobCardDetailsRealmDBModel jobType = jobCardResult.get(i);
                Utils.showLogD("1003 Realmdb--jobCardResult---" + jobType.getName());
            }
            realm.commitTransaction();

        } catch (RealmException e) {
            e.printStackTrace();
        }
        return jobCardResult;
    }

    public List<JobCardItemsDetailsModel> getCardItemsDetail(Realm realm, Integer job_card_id) { //, String uniqueId
        List<JobCardItemsDetailsModel> jobCardItemsResult = realm.where(JobCardItemsDetailsModel.class).equalTo("job_card_id", job_card_id).equalTo("row_deleted", 0).findAll(); //.equalTo("id", uniqueId)
        try {
            realm.beginTransaction();
            for (int i = 0; i < jobCardItemsResult.size(); i++) {
                JobCardItemsDetailsModel jobType = jobCardItemsResult.get(i);
                Utils.showLogD("1003 Realmdb--jobCardResult---" + jobType.getName() + "--" + jobType.getOther_name() + "--" + jobType.getPrice() + "--" + jobType.getDate_added());
            }
            realm.commitTransaction();

        } catch (RealmException e) {
            e.printStackTrace();
        }
        return jobCardItemsResult;
    }

    public List<JobCardCarViewModelDB> getJobCardCarViewDetails(Realm realm, Integer job_card_id) { //, String uniqueId
        List<JobCardCarViewModelDB> jobCardItemsResult = realm.where(JobCardCarViewModelDB.class).equalTo("job_card_id", job_card_id).findAll(); //.equalTo("id", uniqueId)
        try {
            realm.beginTransaction();
            for (int i = 0; i < jobCardItemsResult.size(); i++) {
                JobCardCarViewModelDB jobType = jobCardItemsResult.get(i);
                Utils.showLogD("1006 Realmdb--carview---" + jobType.getJob_card_id() + "--" + jobType.getImg_back_bumper() + "--" + jobType.getImgBonnet() + "--" + jobType.getImgFrontWindScreen());
            }
            realm.commitTransaction();

        } catch (RealmException e) {
            e.printStackTrace();
        }
        return jobCardItemsResult;
    }

    public ReturnRealmModel addJobCardCarViewDetails(Realm realm, final JobCardCarViewModelDB carview) {
        RealmList<JobCardCarViewModelDB> jobCardCarView = new RealmList<>();
        ReturnRealmModel returnmodel = new ReturnRealmModel();
        realm.beginTransaction();
        try {
            JobCardCarViewModelDB carimg = realm.createObject(JobCardCarViewModelDB.class);
/*       carImg = carview;
         jobCardCarView.add(carImg);*/
            jobCardAdded = true;
            carimg.setLocaldbid(carview.getLocaldbid());
            carimg.setJob_card_id(carview.getJob_card_id());
            carimg.setImg_back_bumper(carview.getImg_back_bumper());
            carimg.setImgRightTailLight(carview.getImgRightTailLight());
            carimg.setImgLeftTailLight(carview.getImgLeftTailLight());
            carimg.setImgTailGate(carview.getImgTailGate());
            carimg.setImgRightBackWheel(carview.getImgRightBackWheel());
            carimg.setImgLeftBackWheel(carview.getImgLeftBackWheel());
            carimg.setImgRightBackDoor(carview.getImgRightBackDoor());
            carimg.setImgLeftBackDoor(carview.getImgLeftBackDoor());
            carimg.setImgRightFrontDoor(carview.getImgRightFrontDoor());
            carimg.setImgLeftFrontDoor(carview.getImgLeftFrontDoor());
            carimg.setImgSteering(carview.getImgSteering());
            carimg.setImgFrontWindScreen(carview.getImgFrontWindScreen());
            carimg.setImgRightFrontWheel(carview.getImgRightFrontWheel());
            carimg.setImgLeftFrontWheel(carview.getImgLeftFrontWheel());
            carimg.setImgBonnet(carview.getImgBonnet());
            carimg.setImgRightFrontLight(carview.getImgRightFrontLight());
            carimg.setImgLeftFrontLight(carview.getImgLeftFrontLight());
            carimg.setImgRightFrontFender(carview.getImgRightFrontFender());
            carimg.setImgLeftFrontFender(carview.getImgLeftFrontFender());
            carimg.setImgFrontBumper(carview.getImgFrontBumper());
            carimg.setRow_deleted(0);
            jobCardCarView.add(carimg);
        } catch (RealmException e) {
            e.printStackTrace();
            jobCardAdded = false;
        }
        realm.commitTransaction();
        returnmodel.setStatus(jobCardAdded);
        returnmodel.setNextId(carview.getJob_card_id());
        return returnmodel;
    }

    public boolean updateDBJobCardStatus(Realm realm, JobCardDetailsRealmDBModel jobcard) {
        boolean updatestatus = false;
        Integer jobcardid = jobcard.getId();
        String jobcardstatus = jobcard.getStatus();
        try {
            realm.beginTransaction();
            JobCardDetailsRealmDBModel carddetails = realm.where(JobCardDetailsRealmDBModel.class).equalTo("id", jobcardid).equalTo("row_deleted", 0).findFirst();
            carddetails.setStatus(jobcardstatus);
            realm.commitTransaction();
            updatestatus = true;
        } catch (RealmException e) {
            e.printStackTrace();
        }
        return updatestatus;
    }

    public ReturnRealmModel updatePushedJobCardStatus(Realm realm, final List<JobCardDetailsRealmDBModel> pushedCardDetails) {
        ReturnRealmModel returnmodel = new ReturnRealmModel();
        try {
            realm.beginTransaction();
            for (int i = 0; i < pushedCardDetails.size(); i++) {
                JobCardDetailsRealmDBModel pushCard = pushedCardDetails.get(i);
                Integer oldId = Integer.parseInt(pushCard.getOld_id());
                JobCardDetailsRealmDBModel carddetails = realm.where(JobCardDetailsRealmDBModel.class).equalTo("id", oldId).equalTo("row_deleted", 0).findFirst();
                if (carddetails != null) {
                    if (pushCard.getLocal_insert().equals("1")) {
                        carddetails.setId(Integer.parseInt(pushCard.getNew_id()));
                        carddetails.setLocal_insert("0");
                        carddetails.setSync_status("true");
                        RealmResults<JobCardItemsDetailsModel> jobCardItemsResult = realm.where(JobCardItemsDetailsModel.class).equalTo("job_card_id", oldId).equalTo("row_deleted", 0).findAll();
                        for (int j = 0; j < jobCardItemsResult.size(); j++) {
                            JobCardItemsDetailsModel itemdetails = jobCardItemsResult.get(j);
                            if (itemdetails.getJob_card_id() == oldId) {
                                itemdetails.setJob_card_id(Integer.parseInt(pushCard.getNew_id()));
                            }
                        }
                        RealmResults<ImageUploadModel> jobImageItemsResult = realm.where(ImageUploadModel.class).equalTo("id", oldId).equalTo("row_deleted", 0).findAll();
                        if (jobImageItemsResult != null && jobImageItemsResult.size() > 0) {
                            for (int j = 0; j < jobImageItemsResult.size(); j++) {
                                ImageUploadModel imgItemdetails = jobImageItemsResult.get(j);
                                if (imgItemdetails.getId() == oldId) {
                                    imgItemdetails.setId(Integer.parseInt(pushCard.getNew_id()));
                                    imgItemdetails.setLocal_insert("0");
                                    imgItemdetails.setSync_status("true");
                                }
                            }
                        }
                        RealmResults<AudioUploadModel> jobAudioItemsResult = realm.where(AudioUploadModel.class).equalTo("id", oldId).equalTo("row_deleted", 0).findAll();
                        if (jobAudioItemsResult != null && jobAudioItemsResult.size() > 0) {
                            for (int j = 0; j < jobAudioItemsResult.size(); j++) {
                                AudioUploadModel audItemdetails = jobAudioItemsResult.get(j);
                                if (audItemdetails.getId() == oldId) {
                                    audItemdetails.setId(Integer.parseInt(pushCard.getNew_id()));
                                    audItemdetails.setLocal_insert("0");
                                    audItemdetails.setSync_status("true");
                                }
                            }
                        }

                    } else {
                        carddetails.setSync_status("true");
                    }
                }
            }
            returnmodel.setStatus(true);
            realm.commitTransaction();
        } catch (RealmException e) {
            e.printStackTrace();
            returnmodel.setStatus(false);
        } catch (Exception e) {
            e.printStackTrace();
            returnmodel.setStatus(false);
        }
        return returnmodel;
    }

    public Integer deleteAllDBItems(Realm mrealm) {
        Integer value = 0;
        RealmResults<JobCardDetailsRealmDBModel> cards = mrealm.where(JobCardDetailsRealmDBModel.class).findAll();
        RealmResults<JobCardCarViewModelDB> jobCardCar = mrealm.where(JobCardCarViewModelDB.class).findAll();
        RealmResults<JobCardItemsDetailsModel> jobCardItems = mrealm.where(JobCardItemsDetailsModel.class).findAll();
//        RealmResults<JobTypeModel> jobType = mrealm.where(JobTypeModel.class).findAll();
        RealmResults<AudioUploadModel> audio = mrealm.where(AudioUploadModel.class).findAll();
        RealmResults<ImageUploadModel> image = mrealm.where(ImageUploadModel.class).findAll();
        RealmResults<MyCarListItems> results = mrealm.where(MyCarListItems.class).findAll();
        try {
            mrealm.beginTransaction();
            // Delete all matches
            for (int i = 0; i < cards.size(); i++) {
                cards.get(i).deleteFromRealm();
            }
            for (int j = 0; j < jobCardCar.size(); j++) {
                jobCardCar.get(j).deleteFromRealm();
            }
            for (int k = 0; k < jobCardItems.size(); k++) {
                jobCardItems.get(k).deleteFromRealm();
            }
//            for (int l = 0; l < jobCardCar.size(); l++) {
//                jobType.get(l).deleteFromRealm();
//            }
            for (int m = 0; m < audio.size(); m++) {
                audio.get(m).deleteFromRealm();
            }
            for (int n = 0; n < image.size(); n++) {
                image.get(n).deleteFromRealm();
            }
            for (int o = 0; o < results.size(); o++) {
                results.get(o).deleteFromRealm();
            }
            mrealm.commitTransaction();
            value = 1;
        } catch (RealmException e) {
            e.printStackTrace();
            value = 0;
        }
        return value;
    }

    public ReturnRealmModel updateJobCard(Realm realm, final Integer jobcardid, final JobCardDetailsModel cards, final List<JobTypeModel> carditems) {
        if (realm.isClosed()) {
            realm.getDefaultInstance();
        }
        ReturnRealmModel returnmodel = new ReturnRealmModel();
        realm.beginTransaction();
        JobCardDetailsRealmDBModel carddetails = realm.where(JobCardDetailsRealmDBModel.class).equalTo("id", jobcardid).equalTo("row_deleted", 0).findFirst();
        carddetails.setCustomer_code(cards.getCustomer_code());
        carddetails.setVat_reg_no(cards.getVat_reg_no());
        carddetails.setName(cards.getName());
        carddetails.setPo_box(cards.getPo_box());
        carddetails.setCity(cards.getCity());
        carddetails.setTel_no(cards.getTel_no());
        carddetails.setMobile_no(cards.getMobile_no());
        carddetails.setFax_no(cards.getFax_no());
        carddetails.setEmail(cards.getEmail());
        carddetails.setAdvisor_date(cards.getAdvisor_date());
        carddetails.setAdvisor_time(cards.getAdvisor_time());
        carddetails.setReg_no(cards.getReg_no());
        carddetails.setModel(cards.getModel());
        carddetails.setChassis_no(cards.getChassis_no());
        carddetails.setColours(cards.getColours());
        carddetails.setFuel(cards.getFuel());
        carddetails.setOil(cards.getOil());
        carddetails.setKilometer(cards.getKilometer());
        carddetails.setDelivery_date(cards.getDelivery_date());
        carddetails.setJob_type(cards.getJob_type());
        carddetails.setTotal_amount(cards.getTotal_amount());
        carddetails.setDate_added(cards.getDate_added());
        carddetails.setCreated_date(cards.getCreated_date());
        carddetails.setCreated_date_str(cards.getCreated_date_str());
        carddetails.setAction(cards.getAction());
        carddetails.setStatus(cards.getStatus());
        carddetails.setLocal_insert(cards.isLocal_insert());
        carddetails.setSync_status(cards.isSync_status());
        carddetails.setRow_deleted(0);
        realm.commitTransaction();

        deleteExistJobCardItems(realm, jobcardid, carditems, carddetails);
        returnmodel.setNextId(jobcardid);
        returnmodel.setStatus(true);
        return returnmodel;
    }

    private void deleteExistJobCardItems(Realm realm, Integer jobcardid, final List<JobTypeModel> carditems, JobCardDetailsRealmDBModel carddetails) {
        try {
            realm.beginTransaction();
            RealmResults<JobCardItemsDetailsModel> jobCardItemsResult = realm.where(JobCardItemsDetailsModel.class).equalTo("job_card_id", jobcardid).equalTo("row_deleted", 0).findAll(); //.equalTo("id", uniqueId)
            for (int i = 0; i < jobCardItemsResult.size(); i++) {
                JobCardItemsDetailsModel jobType = jobCardItemsResult.get(i);
                Utils.showLogD("2001--Realmdb--jobCardResult---" + jobType.getName() + "--" + jobType.getOther_name() + "--" + jobType.getPrice() + "--" + jobType.getDate_added());
                JobCardItemsDetailsModel items = jobCardItemsResult.get(i);
                items.deleteFromRealm();
            }
            jobCardItemsResult.deleteAllFromRealm();
            RealmResults<JobCardItemsDetailsModel> jobCardItemsResult1 = realm.where(JobCardItemsDetailsModel.class).equalTo("job_card_id", jobcardid).equalTo("row_deleted", 0).findAll(); //.equalTo("id", uniqueId)
            Utils.showLogD("2001--Realmdb--itemsize--" + jobCardItemsResult1);
            realm.commitTransaction();

            final RealmList<JobCardItemsDetailsModel> jobCardItem = new RealmList<>();
            for (JobTypeModel model : carditems) {
                realm.beginTransaction();
                JobCardItemsDetailsModel itemdetails = realm.createObject(JobCardItemsDetailsModel.class);
                itemdetails.setId(carddetails.getLocaldbid());
                itemdetails.setJob_card_id(jobcardid);
                itemdetails.setService_id(model.getId());
                itemdetails.setName(model.getName());
                itemdetails.setOther_name(model.getOther_name());
                itemdetails.setPrice(Double.parseDouble(model.getPrice()));
                itemdetails.setDate_added(carddetails.getCreated_date());
                itemdetails.setRow_deleted(0);
                jobCardItem.add(itemdetails);
                realm.commitTransaction();
                Log.d("2001--", "itemdetails--" + model.getId() + "--" + model.getName());
            }
            List<JobCardItemsDetailsModel> carditemsresults = new ArrayList(realm.where(JobCardItemsDetailsModel.class).equalTo("job_card_id", jobcardid).findAll());
            Log.d("2001--", "" + carditemsresults.size());
            realm.beginTransaction();
            for (int i = 0; i < carditemsresults.size(); i++) {
                JobCardItemsDetailsModel itemdbdata = carditemsresults.get(i);
                Log.d("2001--", "Addedjobcarditems---" + itemdbdata.getId() + "--" + itemdbdata.getName() + "--" + itemdbdata.getOther_name()
                        + "--" + itemdbdata.getPrice() + "--" + itemdbdata.getService_id()
                );
                Log.d("", "----------------------------------------------------------------------------------------------------------------------");
            }
            realm.commitTransaction();
        } catch (RealmException e) {
            e.printStackTrace();
        }
    }

    public ReturnRealmModel addAudios(Realm realm, final AudioUploadModel imgObj) {
        final ReturnRealmModel returnmodel = new ReturnRealmModel();
        final RealmList<AudioUploadModel> audList = new RealmList<>();
        try {
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    AudioUploadModel audUpload = realm.createObject(AudioUploadModel.class);
                    audUpload.setLocaldbid(imgObj.getLocaldbid());
                    audUpload.setId(imgObj.getId());
                    audUpload.setUser_id(imgObj.getUser_id());
                    audUpload.setShop_id(imgObj.getShop_id());
                    audUpload.setAudio_encode_str(imgObj.getAudio_encode_str());
                    audUpload.setAudio_filename(imgObj.getAudio_filename());
                    audUpload.setAudio_filepath(imgObj.getAudio_filepath());
                    audUpload.setImage_uniqueid(imgObj.getImage_uniqueid());
                    audUpload.setLocal_insert(imgObj.getLocal_insert());
                    audUpload.setSync_status(imgObj.getSync_status());
                    audUpload.setRow_deleted(imgObj.getRow_deleted());
                    audList.add(audUpload);
                    returnmodel.setAudioUploadStatus(1);
                }
            });
        } catch (RealmException e) {
            e.printStackTrace();
            returnmodel.setAudioUploadStatus(0);
        }
        return returnmodel;
    }

    public ReturnRealmModel addGalleryImages(Realm realm, final GalleryUploadModel imgObj) {
        final ReturnRealmModel returnmodel = new ReturnRealmModel();
        final RealmList<GalleryUploadModel> imgList = new RealmList<>();
        try {
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    GalleryUploadModel imgUpload = realm.createObject(GalleryUploadModel.class);
                    imgUpload.setLocaldbid(imgObj.getLocaldbid());
                    imgUpload.setId(imgObj.getId());
                    imgUpload.setUser_id(imgObj.getUser_id());
                    imgUpload.setShop_id(imgObj.getShop_id());
                    imgUpload.setImage_encode_str(imgObj.getImage_encode_str());
                    imgUpload.setImage_filename(imgObj.getImage_filename());
                    imgUpload.setImage_uniqueid(imgObj.getImage_uniqueid());
                    imgUpload.setLocal_insert(imgObj.getLocal_insert());
                    imgUpload.setSync_status(imgObj.getSync_status());
                    imgUpload.setRow_deleted(imgObj.getRow_deleted());
                    imgList.add(imgUpload);
                    returnmodel.setImageUploadStatus(1);
                }
            });
        } catch (RealmException e) {
            e.printStackTrace();
            returnmodel.setImageUploadStatus(0);
        }
        return returnmodel;
    }

    public ReturnRealmModel addImages(Realm realm, final ImageUploadModel imgObj) {
        final ReturnRealmModel returnmodel = new ReturnRealmModel();
        final RealmList<ImageUploadModel> imgList = new RealmList<>();
        try {
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    ImageUploadModel imgUpload = realm.createObject(ImageUploadModel.class);
                    imgUpload.setLocaldbid(imgObj.getLocaldbid());
                    imgUpload.setId(imgObj.getId());
                    imgUpload.setUser_id(imgObj.getUser_id());
                    imgUpload.setShop_id(imgObj.getShop_id());
                    imgUpload.setImage_encode_str(imgObj.getImage_encode_str());
                    imgUpload.setImage_filename(imgObj.getImage_filename());
                    imgUpload.setImage_uniqueid(imgObj.getImage_uniqueid());
                    imgUpload.setLocal_insert(imgObj.getLocal_insert());
                    imgUpload.setSync_status(imgObj.getSync_status());
                    imgUpload.setRow_deleted(imgObj.getRow_deleted());
                    imgList.add(imgUpload);
                    returnmodel.setImageUploadStatus(1);
                }
            });
        } catch (RealmException e) {
            e.printStackTrace();
            returnmodel.setImageUploadStatus(0);
        }
        return returnmodel;
    }

    public ReturnRealmModel UpdateCarPartImages(Realm realm, final String imgUniqueId, final String carPartName) {
        final ReturnRealmModel returnmodel = new ReturnRealmModel();
        try {
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    ImageUploadModel imgUpload = realm.where(ImageUploadModel.class).equalTo("image_uniqueid", imgUniqueId).findFirst();
                    imgUpload.setLocaldbid(imgUpload.getLocaldbid());
                    imgUpload.setId(imgUpload.getId());
                    imgUpload.setUser_id(imgUpload.getUser_id());
                    imgUpload.setShop_id(imgUpload.getShop_id());
                    imgUpload.setImage_encode_str(imgUpload.getImage_encode_str());
                    imgUpload.setImage_filename(imgUpload.getImage_filename());
                    imgUpload.setImage_uniqueid(imgUpload.getImage_uniqueid());
                    imgUpload.setImage_carpartname(carPartName);
                    imgUpload.setLocal_insert(imgUpload.getLocal_insert());
                    imgUpload.setSync_status(imgUpload.getSync_status());
                    imgUpload.setRow_deleted(imgUpload.getRow_deleted());
                    returnmodel.setImageCarPartUpdateStatus(1);
                }
            });
        } catch (RealmException e) {
            e.printStackTrace();
            returnmodel.setImageCarPartUpdateStatus(0);
        }
        return returnmodel;
    }

    public ReturnRealmModel deleteImageFromId(Realm realm, final Integer jobCardId, final Integer position) {
        final ReturnRealmModel returnmodel = new ReturnRealmModel();
        try {
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    List<ImageUploadModel> selectedCardImgList = realm.where(ImageUploadModel.class).equalTo("id", jobCardId).findAll();
                    if (selectedCardImgList != null && selectedCardImgList.size() > 0) {
                        for (int i = 0; i < selectedCardImgList.size(); i++) {
                            if (i == position) {
                                ImageUploadModel selectedImg = selectedCardImgList.get(i);
                                selectedImg.deleteFromRealm();

                                List<ImageUploadModel> afterDeleteImg = realm.where(ImageUploadModel.class).equalTo("id", jobCardId).findAll();
                                returnmodel.setAfterDeleteImgSize(afterDeleteImg.size());
                                returnmodel.setImagedeletedStatus(1);
                            }
                        }
                    }

                }
            });
        } catch (RealmException e) {
            e.printStackTrace();
            List<ImageUploadModel> afterDeleteImg = realm.where(ImageUploadModel.class).equalTo("id", jobCardId).findAll();
            returnmodel.setAfterDeleteImgSize(afterDeleteImg.size());
            returnmodel.setImagedeletedStatus(0);
        }
        return returnmodel;
    }

    public ReturnRealmModel deleteGalleryImage(Realm realm, final String id, final Integer position) {
        final ReturnRealmModel returnmodel = new ReturnRealmModel();
        try {
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    List<GalleryUploadModel> selectedCardImgList = realm.where(GalleryUploadModel.class).equalTo("localdbid", id).findAll();
                    if (selectedCardImgList != null && selectedCardImgList.size() > 0) {
                        for (int i = 0; i < selectedCardImgList.size(); i++) {
//                            if (i == position) {
                                GalleryUploadModel selectedImg = selectedCardImgList.get(i);
                                selectedImg.deleteFromRealm();

                                List<GalleryUploadModel> afterDeleteImg = realm.where(GalleryUploadModel.class).findAll();
                                returnmodel.setAfterDeleteImgSize(afterDeleteImg.size());
                                returnmodel.setImagedeletedStatus(1);
//                            }
                        }
                    }
                }
            });
        } catch (RealmException e) {
            e.printStackTrace();
            List<GalleryUploadModel> afterDeleteImg = realm.where(GalleryUploadModel.class).findAll();
            returnmodel.setAfterDeleteImgSize(afterDeleteImg.size());
            returnmodel.setImagedeletedStatus(0);
        }
        return returnmodel;
    }

    public ReturnRealmModel deleteAudioFromId(Realm realm, final Integer jobCardId, final Integer position) {
        final ReturnRealmModel returnmodel = new ReturnRealmModel();
        try {
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    List<AudioUploadModel> selectedCardAudList = realm.where(AudioUploadModel.class).equalTo("id", jobCardId).findAll();
                    if (selectedCardAudList != null && selectedCardAudList.size() > 0) {
                        for (int i = 0; i < selectedCardAudList.size(); i++) {
                            if (i == position) {
                                AudioUploadModel selectedImg = selectedCardAudList.get(i);
                                selectedImg.deleteFromRealm();

                                List<AudioUploadModel> afterDeleteAud = realm.where(AudioUploadModel.class).equalTo("id", jobCardId).findAll();
                                returnmodel.setAfterDeleteAudSize(afterDeleteAud.size());
                                returnmodel.setAudiodeletedStatus(1);
                            }
                        }
                    }

                }
            });
        } catch (RealmException e) {
            e.printStackTrace();
            List<AudioUploadModel> afterDeleteAud = realm.where(AudioUploadModel.class).equalTo("id", jobCardId).findAll();
            returnmodel.setAfterDeleteAudSize(afterDeleteAud.size());
            returnmodel.setAudiodeletedStatus(0);
        }
        return returnmodel;
    }

    public ReturnRealmModel deleteAudioFromSelectedIdAndImgUniqueId(Realm realm, final Integer jobCardId, final String imgUniqueId, final Integer position) {
        final ReturnRealmModel returnmodel = new ReturnRealmModel();
        try {
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    List<AudioUploadModel> selectedCardAudList = realm.where(AudioUploadModel.class).equalTo("id", jobCardId).equalTo("image_uniqueid", imgUniqueId).findAll();
                    if (selectedCardAudList != null && selectedCardAudList.size() > 0) {
                        for (int i = 0; i < selectedCardAudList.size(); i++) {
                            if (i == position) {
                                AudioUploadModel selectedImg = selectedCardAudList.get(i);
                                selectedImg.deleteFromRealm();

                                List<AudioUploadModel> afterDeleteAud = realm.where(AudioUploadModel.class).equalTo("id", jobCardId).equalTo("image_uniqueid", imgUniqueId).findAll();
                                returnmodel.setAfterDeleteImgAudSize(afterDeleteAud.size());
                                returnmodel.setAudiodeletedStatus(1);
                            }
                        }
                    }

                }
            });
        } catch (RealmException e) {
            e.printStackTrace();
            List<AudioUploadModel> afterDeleteAud = realm.where(AudioUploadModel.class).equalTo("id", jobCardId).findAll();
            returnmodel.setAfterDeleteImgAudSize(afterDeleteAud.size());
            returnmodel.setAudiodeletedStatus(0);
        }
        return returnmodel;
    }

    public List<AudioUploadModel> getAudiosFromId(Realm realm, Integer jobUniqueId) {
        List<AudioUploadModel> selectedAudList = realm.where(AudioUploadModel.class).equalTo("id", jobUniqueId).equalTo("image_uniqueid", "0").findAll();
        return selectedAudList;
    }

    public List<AudioUploadModel> getAudiosFromSelectedIdAndImgUniqueId(Realm realm, Integer jobId, String imgUniqueId) {
        List<AudioUploadModel> selectedAudList = realm.where(AudioUploadModel.class).equalTo("id", jobId).equalTo("image_uniqueid", imgUniqueId).findAll();
        return selectedAudList;
    }

    public List<ImageUploadModel> getImagesFromId(Realm realm, Integer jobUniqueId) {
        List<ImageUploadModel> selectedImgList = realm.where(ImageUploadModel.class).equalTo("id", jobUniqueId).findAll();
        return selectedImgList;
    }

    public ReturnRealmModel getGalleryImages(Realm realm, final int index, final int length) {
        ReturnRealmModel returnmodel = new ReturnRealmModel();
        List<GalleryUploadModel> listGallery = new ArrayList<>();
        List<GalleryUploadModel> selectedImgList = realm.where(GalleryUploadModel.class).findAll();

        int maxLength = index + length;

        if (selectedImgList != null && selectedImgList.size() > 0) {
            for (int i = index; i < maxLength; i++) {
                if (selectedImgList.size() - 1 >= i) {
                    listGallery.add(selectedImgList.get(i));
                }
            }
        }
        returnmodel.setGalleryList(listGallery);
        returnmodel.setGalleryListSize(selectedImgList.size());
        return returnmodel;
    }

    public List<JobCardDetailsRealmDBModel> isExistingJobCard(Realm realm, final Integer uniqueId) {
        List<JobCardDetailsRealmDBModel> jobCard = realm.where(JobCardDetailsRealmDBModel.class).equalTo("id", uniqueId).findAll();
        return jobCard;
    }

    public List<JobCardDetailsRealmDBModel> isJobCardInLocal(Realm realm, final Integer uniqueId) {
        List<JobCardDetailsRealmDBModel> jobCard = realm.where(JobCardDetailsRealmDBModel.class).equalTo("id", uniqueId).findAll();
        return jobCard;
    }

    public ReturnRealmModel addExistingJobCardFromServer(Realm realm, final List<SyncJobCardModel.JobCardDetailsRealmDBModel> serverCard) {
        final RealmList<JobCardDetailsRealmDBModel> jobCard = new RealmList<>();
        final RealmList<JobCardItemsDetailsModel> jobCardItem = new RealmList<>();
//        realm.beginTransaction();
        ReturnRealmModel returnmodel = new ReturnRealmModel();
        try {
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {

                    for (int i = 0; i < serverCard.size(); i++) {
                        SyncJobCardModel.JobCardDetailsRealmDBModel cards = serverCard.get(i);
                        String jobCardUniqueId = UUID.randomUUID().toString();
                        JobCardDetailsRealmDBModel carddetails = realm.createObject(JobCardDetailsRealmDBModel.class);
                        carddetails.setLocaldbid(jobCardUniqueId);
                        carddetails.setId(Integer.parseInt(cards.getId()));
                        carddetails.setUser_id(Integer.parseInt(cards.getUser_id()));
                        carddetails.setShop_id(Integer.parseInt(cards.getShop_id()));
                        carddetails.setInvoice_no(cards.getInvoice_no());
                        carddetails.setCustomer_code(cards.getCustomer_code());
                        carddetails.setVat_reg_no(cards.getVat_reg_no());
                        carddetails.setName(cards.getName());
                        carddetails.setPo_box(cards.getPo_box());
                        carddetails.setCity(cards.getCity());
                        carddetails.setTel_no(cards.getTel_no());
                        carddetails.setMobile_no(cards.getMobile_no());
                        carddetails.setFax_no(cards.getFax_no());
                        carddetails.setEmail(cards.getEmail());
                        carddetails.setAdvisor_date(cards.getAdvisor_date());
                        carddetails.setAdvisor_time(cards.getAdvisor_time());
                        carddetails.setReg_no(cards.getReg_no());
                        carddetails.setModel(cards.getModel());
                        carddetails.setChassis_no(cards.getChassis_no());
                        carddetails.setColours(cards.getColours());
                        carddetails.setAdvisor_id(Integer.parseInt(cards.getAdvisor_id()));
                        carddetails.setAdvisor_name(cards.getAdvisor_name());
                        carddetails.setDelivery_date(cards.getDelivery_date());
                        carddetails.setJob_type(cards.getJob_type());
                        carddetails.setTotal_amount(cards.getTotal_amount());
                        carddetails.setDate_added(cards.getDate_added());
                        carddetails.setDate_update(cards.getDate_updated());
                        String createdDateStr = cards.getCreated_date_str();
                        String[] dateStr = createdDateStr.split(" ");
                        carddetails.setCreated_date(stringToDate(dateStr[0]));
                        carddetails.setCreated_date_str(cards.getCreated_date_str());
                        carddetails.setAction(Constant.SETJOBCARD);
                        carddetails.setStatus(cards.getStatus());
                        carddetails.setLocal_insert("0");
                        carddetails.setSync_status("true");
                        carddetails.setRow_deleted(0);
                        jobCard.add(carddetails);
//                realm.commitTransaction();

                        jobCardAdded = true;
                        for (SyncJobCardModel.JobCardDetailsRealmDBModel.JobCardItemsDetailsModel model : cards.getItems()) {
//                    realm.beginTransaction();
                            JobCardItemsDetailsModel itemdetails = realm.createObject(JobCardItemsDetailsModel.class);
                            itemdetails.setId(jobCardUniqueId);
                            itemdetails.setJob_card_id(Integer.parseInt(cards.getId()));
                            itemdetails.setService_id(model.getId());
                            itemdetails.setName(model.getName());
                            itemdetails.setOther_name(model.getOther_name());
                            itemdetails.setPrice(Double.parseDouble(model.getPrice()));
                            String createdDateStr1 = cards.getCreated_date_str();
                            String[] dateStr1 = createdDateStr1.split(" ");
                            itemdetails.setDate_added(stringToDate(dateStr1[0]));
                            itemdetails.setRow_deleted(0);
                            jobCardItem.add(itemdetails);
//                    realm.commitTransaction();
                            Log.d("1004", "itemdetails--" + model.getId() + "--" + model.getName());
                        }

                        final RealmList<ImageUploadModel> imgList = new RealmList<>();
                        for (int j = 0; j < cards.getImages().size(); j++) {
                            ImageUploadModel imgObj = cards.getImages().get(j);
                            ImageUploadModel imgUpload = realm.createObject(ImageUploadModel.class);
                            imgUpload.setLocaldbid(imgObj.getLocaldbid());
                            imgUpload.setId(imgObj.getId());
                            imgUpload.setUser_id(imgObj.getUser_id());
                            imgUpload.setShop_id(imgObj.getShop_id());
                            imgUpload.setImage_encode_str(imgObj.getImage_encode_str());
                            imgUpload.setImage_filename(imgObj.getImage_filename());
                            imgUpload.setImage_uniqueid(imgObj.getImage_uniqueid());
                            imgUpload.setLocal_insert(imgObj.getLocal_insert());
                            imgUpload.setSync_status(imgObj.getSync_status());
                            imgUpload.setRow_deleted(imgObj.getRow_deleted());
                            imgList.add(imgUpload);
                        }

                        final RealmList<AudioUploadModel> audList = new RealmList<>();
                        for (int j = 0; j < cards.getAudios().size(); j++) {
                            AudioUploadModel audObj = cards.getAudios().get(j);
                            AudioUploadModel audUpload = realm.createObject(AudioUploadModel.class);
                            audUpload.setLocaldbid(audObj.getLocaldbid());
                            audUpload.setId(audObj.getId());
                            audUpload.setUser_id(audObj.getUser_id());
                            audUpload.setShop_id(audObj.getShop_id());
                            audUpload.setAudio_encode_str(audObj.getAudio_encode_str());
                            audUpload.setAudio_filename(audObj.getAudio_filename());
                            audUpload.setAudio_filepath(audObj.getAudio_filepath());
                            audUpload.setImage_uniqueid(audObj.getImage_uniqueid());
                            audUpload.setLocal_insert(audObj.getLocal_insert());
                            audUpload.setSync_status(audObj.getSync_status());
                            audUpload.setRow_deleted(audObj.getRow_deleted());
                            audList.add(audUpload);
                        }
                    }
                }
            });

            RealmResults<JobCardDetailsRealmDBModel> jobcardresults = realm.where(JobCardDetailsRealmDBModel.class).findAll();
            realm.beginTransaction();
            for (int i = 0; i < jobcardresults.size(); i++) {
                JobCardDetailsRealmDBModel carddb = jobcardresults.get(i);
                Log.d("100002", "ServerAddedjobcard---" + carddb.getId() + "--" + carddb.getLocaldbid() + "--" + carddb.getUser_id()
                        + "--" + carddb.getShop_id() + "--" + carddb.getCustomer_code() + "--" + carddb.getVat_reg_no() + "--" + carddb.getName()
                );
                Log.d("", "----------------------------------------------------------------------------------------------------------------------");
            }
            List<JobCardItemsDetailsModel> carditemsresults = new ArrayList(realm.where(JobCardItemsDetailsModel.class).equalTo("job_card_id", nextId).findAll());
            Log.d("1005", "" + carditemsresults.size());
            for (int i = 0; i < carditemsresults.size(); i++) {
                JobCardItemsDetailsModel itemdbdata = carditemsresults.get(i);
                Log.d("100005", "ServerAddedjobcarditems---" + itemdbdata.getId() + "--" + itemdbdata.getName() + "--" + itemdbdata.getOther_name()
                        + "--" + itemdbdata.getPrice() + "--" + itemdbdata.getService_id()
                );
                Log.d("", "----------------------------------------------------------------------------------------------------------------------");
            }
            Integer count = jobcardresults.size();
            realm.commitTransaction();
            returnmodel.setAddedExistingJobCardFromServerStatus(1);
        } catch (RealmException e) {
            e.printStackTrace();
            returnmodel.setAddedExistingJobCardFromServerStatus(0);
        }
        return returnmodel;
    }

    public Date stringToDate(String aDate) {

        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "dd-MM-yyyy");
        Date myDate = null;
        try {
            myDate = dateFormat.parse(aDate);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat timeFormat = new SimpleDateFormat("dd-MM-yyyy");
        String finalDate = timeFormat.format(myDate);
        System.out.println(finalDate);
        return myDate;
    }

    public List<JobCardItemsDetailsModel> updateJobCardItems(Realm realm, final List<JobTypeModel> carditems, Integer nextId, String localId, Date createdDate){
        final List<JobCardItemsDetailsModel> jobCardItem = new RealmList<>();
        for (JobTypeModel model : carditems) {
            realm.beginTransaction();
            JobCardItemsDetailsModel itemdetails = realm.createObject(JobCardItemsDetailsModel.class);
            itemdetails.setId(localId);
            itemdetails.setJob_card_id(nextId);
            itemdetails.setService_id(model.getId());
            itemdetails.setName(model.getName());
            itemdetails.setOther_name(model.getOther_name());
            itemdetails.setPrice(Double.parseDouble(model.getPrice()));
            itemdetails.setDate_added(createdDate);
            itemdetails.setRow_deleted(0);
            jobCardItem.add(itemdetails);
            realm.commitTransaction();
            Log.d("1004", "itemdetails--" + model.getId() + "--" + model.getName());
        }
        return jobCardItem;
    }

    public ReturnRealmModel addJobCard(Realm realm, final JobCardDetailsModel cards, final List<JobTypeModel> carditems) {
        final RealmList<JobCardDetailsRealmDBModel> jobCard = new RealmList<>();
        final RealmList<JobCardItemsDetailsModel> jobCardItem = new RealmList<>();
        realm.beginTransaction();
        ReturnRealmModel returnmodel = new ReturnRealmModel();
        try {
            // increment index
            Number currentIdNum = realm.where(JobCardDetailsRealmDBModel.class).max("id");
            if (currentIdNum == null) {
                nextId = 1;
            } else {
                nextId = currentIdNum.intValue() + 1;
            }

            JobCardDetailsRealmDBModel carddetails = realm.createObject(JobCardDetailsRealmDBModel.class);
            carddetails.setLocaldbid(cards.getLocaldbid());
            carddetails.setId(nextId);
            carddetails.setUser_id(cards.getUser_id());
            carddetails.setShop_id(cards.getShop_id());
            carddetails.setInvoice_no("");
            carddetails.setCustomer_code(cards.getCustomer_code());
            carddetails.setVat_reg_no(cards.getVat_reg_no());
            carddetails.setName(cards.getName());
            carddetails.setPo_box(cards.getPo_box());
            carddetails.setCity(cards.getCity());
            carddetails.setTel_no(cards.getTel_no());
            carddetails.setMobile_no(cards.getMobile_no());
            carddetails.setFax_no(cards.getFax_no());
            carddetails.setEmail(cards.getEmail());
            carddetails.setAdvisor_date(cards.getAdvisor_date());
            carddetails.setAdvisor_time(cards.getAdvisor_time());
            carddetails.setReg_no(cards.getReg_no());
            carddetails.setModel(cards.getModel());
            carddetails.setChassis_no(cards.getChassis_no());
            carddetails.setColours(cards.getColours());
            carddetails.setFuel(cards.getFuel());
            carddetails.setOil(cards.getOil());
            carddetails.setKilometer(cards.getKilometer());
            carddetails.setAdvisor_id(cards.getAdvisor_id());
            carddetails.setAdvisor_name(cards.getAdvisor_name());
            carddetails.setDelivery_date(cards.getDelivery_date());
            carddetails.setJob_type(cards.getJob_type());
            carddetails.setTotal_amount(cards.getTotal_amount());
            carddetails.setDate_added(cards.getDate_added());
            carddetails.setDate_update(cards.getDate_update());
            carddetails.setCreated_date(cards.getCreated_date());
            carddetails.setCreated_date_str(cards.getCreated_date_str());
            carddetails.setAction(cards.getAction());
            carddetails.setStatus(cards.getStatus());
            carddetails.setLocal_insert(cards.isLocal_insert());
            carddetails.setSync_status(cards.isSync_status());
            carddetails.setRow_deleted(cards.getRow_deleted());
            jobCard.add(carddetails);
            realm.commitTransaction();

            jobCardAdded = true;
            for (JobTypeModel model : carditems) {
                realm.beginTransaction();
                JobCardItemsDetailsModel itemdetails = realm.createObject(JobCardItemsDetailsModel.class);
                itemdetails.setId(cards.getLocaldbid());
                itemdetails.setJob_card_id(nextId);
                itemdetails.setService_id(model.getId());
                itemdetails.setName(model.getName());
                itemdetails.setOther_name(model.getOther_name());
                itemdetails.setPrice(Double.parseDouble(model.getPrice()));
                itemdetails.setDate_added(cards.getCreated_date());
                itemdetails.setRow_deleted(0);
                jobCardItem.add(itemdetails);
                realm.commitTransaction();
                Log.d("1004", "itemdetails--" + model.getId() + "--" + model.getName());
            }

            RealmResults<JobCardDetailsRealmDBModel> jobcardresults = realm.where(JobCardDetailsRealmDBModel.class).findAll();
            realm.beginTransaction();
            for (int i = 0; i < jobcardresults.size(); i++) {
                JobCardDetailsRealmDBModel carddb = jobcardresults.get(i);
                Log.d("1002", "Addedjobcard---" + carddb.getId() + "--" + carddb.getLocaldbid() + "--" + carddb.getUser_id()
                        + "--" + carddb.getShop_id() + "--" + carddb.getCustomer_code() + "--" + carddb.getVat_reg_no() + "--" + carddb.getName()
                );
                Log.d("", "----------------------------------------------------------------------------------------------------------------------");
            }
            List<JobCardItemsDetailsModel> carditemsresults = new ArrayList(realm.where(JobCardItemsDetailsModel.class).equalTo("job_card_id", nextId).findAll());
            Log.d("1005", "" + carditemsresults.size());
            for (int i = 0; i < carditemsresults.size(); i++) {
                JobCardItemsDetailsModel itemdbdata = carditemsresults.get(i);
                Log.d("1005", "Addedjobcarditems---" + itemdbdata.getId() + "--" + itemdbdata.getName() + "--" + itemdbdata.getOther_name()
                        + "--" + itemdbdata.getPrice() + "--" + itemdbdata.getService_id()
                );
                Log.d("", "----------------------------------------------------------------------------------------------------------------------");
            }
            Integer count = jobcardresults.size();
            realm.commitTransaction();
            returnmodel.setStatus(jobCardAdded);
            returnmodel.setNextId(nextId);

        } catch (RealmException e) {
            e.printStackTrace();
            returnmodel.setStatus(false);
            returnmodel.setNextId(nextId);
        }
        return returnmodel;
    }

    public ReturnRealmModel getPushAllJobCardDetail(Realm realm) {
        String listStr = null;
        ReturnRealmModel returnmodel = new ReturnRealmModel();
        List<JobCardDetailsRealmDBModel> list = new ArrayList<>();
        List<JobCardItemsDetailsModel> itemsDetailsList;
        JobCardDetailsRealmDBModel jobCard;
        JobCardItemsDetailsModel carditems;
        List<JobCardDetailsRealmDBModel> jobCardResult = realm.where(JobCardDetailsRealmDBModel.class)
//                .equalTo("local_insert", "1")
                .equalTo("sync_status", "false")
                .equalTo("row_deleted", 0)
                .findAll();
        List<JobCardItemsDetailsModel> jobCardItemsResult = realm.where(JobCardItemsDetailsModel.class)
                .equalTo("row_deleted", 0)
                .findAll();
        List<ImageUploadModel> imgResult = realm.where(ImageUploadModel.class)
//                .equalTo("local_insert", "1")
                .equalTo("sync_status", "false")
                .equalTo("row_deleted", 0)
                .findAll();
        List<AudioUploadModel> audResult = realm.where(AudioUploadModel.class)
//                .equalTo("local_insert", "1")
                .equalTo("sync_status", "false")
                .equalTo("row_deleted", 0)
                .findAll();
        returnmodel.setJobCard(jobCardResult);
        returnmodel.setJobCardItems(jobCardItemsResult);
        returnmodel.setImageItems(imgResult);
        returnmodel.setAudioItems(audResult);

        return returnmodel;
    }

    public ReturnRealmModel getListingAllJobCard(Realm realm) {
        String listStr = null;
        ReturnRealmModel returnmodel = new ReturnRealmModel();
        List<JobCardDetailsRealmDBModel> list = new ArrayList<>();
        List<JobCardDetailsRealmDBModel> list1 = new ArrayList<>();
        List<JobCardItemsDetailsModel> itemsDetailsList;
        JobCardDetailsRealmDBModel jobCard;
        JobCardItemsDetailsModel carditems;
        List<JobCardDetailsRealmDBModel> jobCardResult = realm.where(JobCardDetailsRealmDBModel.class)
                .equalTo("row_deleted", 0)
                .findAll();
        List<JobCardItemsDetailsModel> jobCardItemsResult = realm.where(JobCardItemsDetailsModel.class)
                .equalTo("row_deleted", 0)
                .findAll();
        List<ImageUploadModel> imgResult = realm.where(ImageUploadModel.class)
                .equalTo("row_deleted", 0)
                .findAll();
        List<AudioUploadModel> audResult = realm.where(AudioUploadModel.class)
                .equalTo("row_deleted", 0)
                .findAll();
        returnmodel.setJobCard(jobCardResult);
        returnmodel.setJobCardItems(jobCardItemsResult);
        returnmodel.setImageItems(imgResult);
        returnmodel.setAudioItems(audResult);

        return returnmodel;
    }


    public ReturnRealmModel getJobCardPending(Realm realm, final String status, final int index, final int length) {
        String listStr = null;
        ReturnRealmModel returnmodel = new ReturnRealmModel();
        List<JobCardDetailsRealmDBModel> list1 = new ArrayList<>();
        List<JobCardItemsDetailsModel> itemsDetailsList1 = new ArrayList<>();
        List<ImageUploadModel> imgList1 = new ArrayList<>();

        JobCardDetailsRealmDBModel jobCard;
        JobCardItemsDetailsModel carditems;
        List<JobCardDetailsRealmDBModel> jobCardResult = realm.where(JobCardDetailsRealmDBModel.class)
                .equalTo("row_deleted", 0)
                .equalTo("local_insert", "1")
                .equalTo("sync_status", "false")
                .equalTo("status", status)
                .sort("id", Sort.DESCENDING)
                .findAll();
        List<JobCardItemsDetailsModel> jobCardItemsResult = realm.where(JobCardItemsDetailsModel.class)
                .equalTo("row_deleted", 0)
                .sort("job_card_id", Sort.DESCENDING)
                .findAll();
        List<ImageUploadModel> imgResult = realm.where(ImageUploadModel.class)
                .equalTo("row_deleted", 0)
                .sort("id", Sort.DESCENDING)
                .findAll();
        List<AudioUploadModel> audResult = realm.where(AudioUploadModel.class)
                .equalTo("row_deleted", 0)
                .sort("id", Sort.DESCENDING)
                .findAll();
//        int index = 40;
//        int length = 20;
        int maxLength = index + length;

        if (jobCardResult != null && jobCardResult.size() > 0) {
            for (int i = index; i < maxLength; i++) {
                if (jobCardResult.size() - 1 >= i) {
                    list1.add(jobCardResult.get(i));
                }
            }
        }
        returnmodel.setJobCard(list1);
        returnmodel.setJobCardItems(jobCardItemsResult);
        returnmodel.setImageItems(imgResult);
        returnmodel.setAudioItems(audResult);

        return returnmodel;
    }

    public ReturnRealmModel getJobCard(Realm realm, final String status, final int index, final int length) {
        String listStr = null;
        ReturnRealmModel returnmodel = new ReturnRealmModel();
        List<JobCardDetailsRealmDBModel> list1 = new ArrayList<>();
        List<JobCardItemsDetailsModel> itemsDetailsList1 = new ArrayList<>();
        List<ImageUploadModel> imgList1 = new ArrayList<>();

        JobCardDetailsRealmDBModel jobCard;
        JobCardItemsDetailsModel carditems;
        List<JobCardDetailsRealmDBModel> jobCardResult = realm.where(JobCardDetailsRealmDBModel.class)
                .equalTo("row_deleted", 0)
                .equalTo("status", status)
                .sort("id", Sort.DESCENDING)
                .findAll();
        List<JobCardItemsDetailsModel> jobCardItemsResult = realm.where(JobCardItemsDetailsModel.class)
                .equalTo("row_deleted", 0)
                .sort("job_card_id", Sort.DESCENDING)
                .findAll();
        List<ImageUploadModel> imgResult = realm.where(ImageUploadModel.class)
                .equalTo("row_deleted", 0)
                .sort("id", Sort.DESCENDING)
                .findAll();
        List<AudioUploadModel> audResult = realm.where(AudioUploadModel.class)
                .equalTo("row_deleted", 0)
                .sort("id", Sort.DESCENDING)
                .findAll();
//        int index = 40;
//        int length = 20;
        int maxLength = index + length;

        if (jobCardResult != null && jobCardResult.size() > 0) {
            for (int i = index; i < maxLength; i++) {
                if (jobCardResult.size() - 1 >= i) {
                    list1.add(jobCardResult.get(i));
                }
            }
        }
        returnmodel.setJobCard(list1);
        returnmodel.setJobCardItems(jobCardItemsResult);
        returnmodel.setImageItems(imgResult);
        returnmodel.setAudioItems(audResult);

        return returnmodel;
    }

    public ReturnRealmModel getTodayDeliveryJobCard(Realm realm, String mCurrentDate) {
        ReturnRealmModel returnmodel = new ReturnRealmModel();
        List<JobCardDetailsRealmDBModel> jobCardResult = realm.where(JobCardDetailsRealmDBModel.class)
                .equalTo("delivery_date", mCurrentDate)
                .equalTo("row_deleted", 0)
                .findAll();
        List<JobCardItemsDetailsModel> jobCardItemsResult = realm.where(JobCardItemsDetailsModel.class)
                .equalTo("row_deleted", 0)
                .findAll();
        returnmodel.setJobCard(jobCardResult);
        returnmodel.setJobCardItems(jobCardItemsResult);

        return returnmodel;
    }

    public ReturnRealmModel getJobCardReport(Realm realm, Date fromDate, Date toDate) {
        ReturnRealmModel returnmodel = new ReturnRealmModel();
        List<JobCardDetailsRealmDBModel> reportResults = realm.where(JobCardDetailsRealmDBModel.class)
                .between("created_date", fromDate, toDate).findAll();
        List<JobCardItemsDetailsModel> jobCardItemsResult = realm.where(JobCardItemsDetailsModel.class)
                .between("date_added", fromDate, toDate)
                .equalTo("row_deleted", 0)
                .findAll();
        for (int i = 0; i < reportResults.size(); i++) {
            Log.d("2002--", "RealmReports--" + reportResults.size() + "--" + reportResults.get(i).getId() + "--" + reportResults.get(i).getCreated_date() + "--" + reportResults.get(i).getName());
        }
        for (int j = 0; j < jobCardItemsResult.size(); j++) {
            Log.d("2002--", "RealmReports--" + jobCardItemsResult.size() + "--" + jobCardItemsResult.get(j).getId() + "--" + jobCardItemsResult.get(j).getJob_card_id() + "--" + jobCardItemsResult.get(j).getName() + "--" + jobCardItemsResult.get(j).getDate_added());
        }

        returnmodel.setJobCard(reportResults);
        returnmodel.setJobCardItems(jobCardItemsResult);
        return returnmodel;
    }
}