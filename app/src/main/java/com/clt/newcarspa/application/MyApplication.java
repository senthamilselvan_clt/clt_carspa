package com.clt.newcarspa.application;

import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.clt.newcarspa.R;
import com.clt.newcarspa.cltinterfaces.RestAdapter;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by Karthik Thumathy on 23-Jul-18.
 */
public class MyApplication extends MultiDexApplication {

    public static final String TAG = MyApplication.class.getSimpleName();

    private static MyApplication mInstance;
    private static boolean activityVisible;

    public static boolean isActivityVisible() {
        return activityVisible;
    }

    public static void activityResumed() {
        activityVisible = true;
    }

    public static void activityPaused() {
        activityVisible = false;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        MultiDex.install(this);
        mInstance = this;
        Realm.init(this);
        RestAdapter.init(this);
        try {

            RealmConfiguration realmConfiguration = new RealmConfiguration.Builder()
                    .name(getString(R.string.database_name))
                    .deleteRealmIfMigrationNeeded()
                    .schemaVersion(8)
                    .build();
            Realm.setDefaultConfiguration(realmConfiguration);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static synchronized MyApplication getInstance() {
        return mInstance;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}
