
package com.clt.newcarspa.Helper;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.clt.newcarspa.R;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Random;

import okhttp3.MediaType;

/**
 * Created by mohammed on 9/3/16.
 */
public class FileUtils {

    public static final String CAR_DIRECTORY_PATH = "/CARSPA/";
    public static final String SPA_DIRECTORY_PATH = "/CARSPA/";
    public static final String AUDIO_DIR_PATH = "/msg_aud/";
    private static final String TAG = FileUtils.class.getCanonicalName();
    public static final String COUNTRYJSON_DIR_PATH = "/countries_json/";
    public static final String CARSPA_DIRECTORY_PATH = "/CARSPA/";
    public static final MediaType MEDIA_TYPE_IMAGE = MediaType.parse("image/*");
    public static final MediaType MEDIA_TYPE_PDF = MediaType.parse("application/pdf");

    public static File initAndGetSthatAndAudioDirectory(Context context) {
        try {
            File sthatDir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + CAR_DIRECTORY_PATH);
//            Log.d(TAG, "CARSPA_DIRECTORY_PATH : sthatDir.mkdir() = " + sthatDir.mkdir()
//                    + " sthatDir = " + sthatDir + " sthatDir.getAbsolutePath = " + sthatDir.getAbsolutePath());
            sthatDir = new File(sthatDir.getAbsolutePath() + AUDIO_DIR_PATH);
//            Log.d(TAG, "AUDIO_DIR_PATH : sthatDir.mkdir() = " + sthatDir.mkdir()
//                    + "sthatDir = " + sthatDir + " sthatDir.getAbsolutePath = " + sthatDir.getAbsolutePath());
            return sthatDir;
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(context, R.string.err_init_directory, Toast.LENGTH_LONG).show();
            return null;
        }
    }

    public static File getRcvdAudioFile(Context context, String orderid, String fileKey) {
        if (orderid == null)
            orderid = "";
        /*String fileName = Chat.RCVD_AUDIO_FILE_PREFIX + "_" + orderid +"_"+
                new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()) + Chat.AUDIO_FILE_SUFFIX;*/
        String fileName = fileKey;
        try {
            File sthatDir = initAndGetSthatAndAudioDirectory(context);
            File file = new File(sthatDir.getAbsolutePath(), fileName);
            if (!file.exists())
                Log.d(TAG, "file.createNewFile() = " + file.createNewFile());
            return file;
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(context, R.string.err_init_audio_file, Toast.LENGTH_LONG).show();
            return null;
        }
    }

    /*public static final String RCVD_PIC_FILE_PREFIX = "CARTOWPIC_";*/
    public static final String RCVD_PIC_FILE_PREFIX = "DELIVERY_PIC_";
    public static final String PIC_FILE_SUFFIX = ".jpeg";

    public static File getRcvdPictureFile(Context applContext, String orderId, int keyNo) {

        String fileName = RCVD_PIC_FILE_PREFIX + keyNo + "_" + orderId + PIC_FILE_SUFFIX;
        try {
            File sthatDir = initSthatAndPictureDirectory(applContext);
            File file = new File(sthatDir.getAbsolutePath(), fileName);
            if (!file.exists())
                Log.d(TAG, "file.createNewFile() = " + file.createNewFile());
            return file;
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(applContext, R.string.err_init_audio_file, Toast.LENGTH_LONG).show();
            return null;
        }
    }

    public static final String RCVD_PIC_FILE_PREFIX_PRODUCT = "PRODUCT_PIC_";
    public static final String RCVD_PIC_FILE_PREFIX_STORE = "STORE_PIC_";

    public static File getProductOrStorePictureFile(Context applContext, String code, String imageType,
                                                    String fileNamePrefix, String folderName) {

        String fileName = imageType + fileNamePrefix + "_" + code + PIC_FILE_SUFFIX;
        try {
            File sthatDir = initTmmmTAndProductDirectory(applContext, folderName);
            File file = new File(sthatDir.getAbsolutePath(), fileName);
            if (!file.exists())
                Log.d(TAG, "file.createNewFile() = " + file.createNewFile());
            return file;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static File findPictureFile(Context applContext, String orderId, int keyNo) {
        String fileName = RCVD_PIC_FILE_PREFIX + keyNo + "_" + orderId + PIC_FILE_SUFFIX;
        try {
            File sthatDir = initSthatAndPictureDirectory(applContext);
            File file = new File(sthatDir.getAbsolutePath(), fileName);
            if (file.exists()) {
                Log.d(TAG, "file exists, name = " + file.getAbsolutePath());
                return file;
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(applContext, R.string.err_init_audio_file, Toast.LENGTH_LONG).show();
            return null;
        }
    }

    public static final String PIC_DIR_PATH = "/msg_pic/";

    public static File initSthatAndPictureDirectory(Context context) {
        try {
            File sthatDir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + CAR_DIRECTORY_PATH);
            sthatDir.mkdir();
            Log.d(TAG, "CARSPA_DIRECTORY_PATH : sthatDir = " + sthatDir + " sthatDir.getAbsolutePath = " + sthatDir.getAbsolutePath());
            sthatDir = new File(sthatDir.getAbsolutePath() + PIC_DIR_PATH);
            sthatDir.mkdir();
            Log.d(TAG, "AUDIO_DIR_PATH : mSthatDir = " + sthatDir + " mSthatDir.getAbsolutePath = " + sthatDir.getAbsolutePath());
            return sthatDir;
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(context, R.string.err_init_directory, Toast.LENGTH_LONG).show();
            return null;
        }
    }

    public static final String PRODUCT_IMAGE_PATH = "/Product Images/";
    public static final String STORE_IMAGE_PATH = "/Store Images/";

    public static File initTmmmTAndProductDirectory(Context context, String productImagePath) {
        try {
            File tmmmtDir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + SPA_DIRECTORY_PATH);
            tmmmtDir.mkdir();
            Log.d(TAG, "SPA_DIRECTORY_PATH : tmmmtDir = " + tmmmtDir + " sthatDir.getAbsolutePath = " + tmmmtDir.getAbsolutePath());
            tmmmtDir = new File(tmmmtDir.getAbsolutePath() + productImagePath);
            tmmmtDir.mkdir();
            Log.d(TAG, "AUDIO_DIR_PATH : mSthatDir = " + tmmmtDir + " mSthatDir.getAbsolutePath = " + tmmmtDir.getAbsolutePath());
            return tmmmtDir;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getProductImageFilePath() {
        try {
            File tmmmtDir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + SPA_DIRECTORY_PATH + PRODUCT_IMAGE_PATH);
            return tmmmtDir.getAbsolutePath();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getStoreImageFilePath() {
        try {
            File tmmmtDir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + SPA_DIRECTORY_PATH + STORE_IMAGE_PATH);
            return tmmmtDir.getAbsolutePath();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static File initCountriesJsonFileDirectory(Context context) {
        try {
            /*File sthatDir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + CARSPA_DIRECTORY_PATH);
            Log.d(TAG, "CARSPA_DIRECTORY_PATH : sthatDir.mkdir() = " + sthatDir.mkdir()
                    + " sthatDir = " + sthatDir + " sthatDir.getAbsolutePath = " + sthatDir.getAbsolutePath());
            sthatDir = new File(sthatDir.getAbsolutePath() + COUNTRYJSON_DIR_PATH);
            Log.d(TAG, "AUDIO_DIR_PATH : sthatDir.mkdir() = " + sthatDir.mkdir()
                    + "sthatDir = " + sthatDir + " sthatDir.getAbsolutePath = " + sthatDir.getAbsolutePath());*/

            File file_PackageInside = new File(context.getFilesDir().getAbsolutePath());

            /*return sthatDir;*/
            return file_PackageInside;
        } catch (Exception e) {
            e.printStackTrace();
            /*Toast.makeText(context, R.string.err_init_directory, Toast.LENGTH_LONG).show();*/
            return null;
        }
    }

    public static File getRcvdCountriesJsonFile(Context context, String fileKey) {
        String fileName = fileKey;
        try {
            File sthatDir = initCountriesJsonFileDirectory(context);
            File file = new File(sthatDir.getAbsolutePath(), fileName);
            Log.d(TAG, "file.createNewFile() EveryTime = " + file.createNewFile());
            return file;
        } catch (IOException e) {
            e.printStackTrace();
            /*Toast.makeText(context, R.string.err_init_countryjson_file, Toast.LENGTH_LONG).show();*/
            return null;
        }
    }


    public static String readFromFile(Context context, String filename) {
        /*File sthatDir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + CARSPA_DIRECTORY_PATH);
        sthatDir = new File(sthatDir.getAbsolutePath() + COUNTRYJSON_DIR_PATH);*/
        File file_PackageInside = new File(context.getFilesDir().getAbsolutePath());
        String path = file_PackageInside.getAbsolutePath() + "/" + filename;
        StringBuffer stringBuffer = new StringBuffer();
        String aDataRow = "";
        String aBuffer = "";
        try {
            File myFile = new File(path);
            FileInputStream fIn = new FileInputStream(myFile);
            BufferedReader myReader = new BufferedReader(
                    new InputStreamReader(fIn));

            while ((aDataRow = myReader.readLine()) != null) {
                aBuffer += aDataRow + "\n";
            }
            myReader.close();
        } catch (Exception e) {
            e.printStackTrace();
            return aBuffer;
        }
        return aBuffer;
    }


    public static String getPath(Context context, Uri uri) throws URISyntaxException {
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = { "_data" };
            Cursor cursor = null;
            try {
                cursor = context.getContentResolver().query(uri, projection, null, null, null);
                int column_index = cursor.getColumnIndexOrThrow("_data");
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
                e.printStackTrace();    // Eat it
            }
        }
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;
    }

    public static boolean isJpegImageFile(File file) {
        final String[] okFileExtensions =  new String[] {"jpg", "jpeg", "gif", "png", "bmp", "webp"};
        for (String extension : okFileExtensions)
        {
            if (file.getName().toLowerCase().endsWith(extension))
            {
                return true;
            }
        }
        return false;
    }

    public static String getPictureUploadingFileKey(Context context) {
        String fileName = "BCAST_AU_"+
                new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH).format(new Date()) + "_" + new Random().nextInt(1000) + ".jpeg";
        return fileName;
    }

    public static File initSthatAndAudioDirectory(Context context) {
        try {
            File sthatDir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + CAR_DIRECTORY_PATH);
            sthatDir.mkdir();
            Log.d(TAG, "CARSPA_DIRECTORY_PATH : sthatDir = " + sthatDir + " sthatDir.getAbsolutePath = " + sthatDir.getAbsolutePath());
            sthatDir = new File(sthatDir.getAbsolutePath() + AUDIO_DIR_PATH);
            sthatDir.mkdir();
            Log.d(TAG, "AUDIO_DIR_PATH : sthatDir = " + sthatDir + " sthatDir.getAbsolutePath = " + sthatDir.getAbsolutePath());
            return sthatDir;
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(context, R.string.err_init_directory, Toast.LENGTH_LONG).show();
            return null;
        }
    }

    public static String getAcceptableJpegFilePath(Intent intentData, Context context) {
        Uri uri = intentData.getData();
        Log.d(TAG, "File Uri: " + uri.toString());
        try {
            //String path = FileUtils.getPath(context, uri);
            String path = FileUtilsChooser.getPath(context, uri);
            Log.d(TAG, "File Path: " + path);
            File file = new File(path);
            if (FileUtils.isJpegImageFile(file)) {
                Log.d(TAG, "File is jpeg or jpg image");
                return path;
            } else {
                Log.d(TAG, "Invalid File..");
                //  Toast.makeText(context, R.string.err_jpg_file_selection, Toast.LENGTH_LONG).show();
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            //  Toast.makeText(context, R.string.err_jpg_file_selection, Toast.LENGTH_LONG).show();
            return null;
        }
    }

    public static Intent getIntentWithMime4ImagePdf() {
        Intent intent;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.setType("*/*");
            String[] mimetypes = {"image/*", "application/pdf"};
            intent.putExtra(Intent.EXTRA_MIME_TYPES, mimetypes);
        } else {
            intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("image/*|application/pdf");
            intent.addCategory(Intent.CATEGORY_OPENABLE);
        }
        return intent;
    }


    public static String getAcceptableFilePath(Context context, Intent intentData) {
        Uri uri = intentData.getData();
        Log.d(TAG, "File Uri: " + uri.toString());
        try {
            String path = FileUtilsChooser.getPath(context, uri);
            Log.d(TAG, "File Path: " + path);
            File file = new File(path);
            if (FileUtils.isImageFile(file)) {
                Log.d(TAG, "File is image");
                return path;
            } else if (FileUtils.isPdfFile(file)) {
                Log.d(TAG, "File is pdf");
                return path;
            } else {
                Log.d(TAG, "Invalid File..");
                Toast.makeText(context, R.string.err_file_selection, Toast.LENGTH_LONG).show();
                http:
//stackoverflow.com/questions/8716289/is-it-possible-to-rotate-a-drawable-in-the-xml-description?lq=1
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getFileExt(String fileName) {
        return fileName.substring((fileName.lastIndexOf(".") + 1), fileName.length());
    }

    public static boolean isPdfFile(File aFile) {
        if (aFile.isFile()) {
            String name = aFile.getName();
            if ("pdf".equalsIgnoreCase(getFileExt(name)))
                return true;
        }
        return false;
    }

    public static boolean isImageFile(File file) {
        final String[] okFileExtensions = new String[]{"jpg", "jpeg", "gif", "png", "bmp", "webp"};
        for (String extension : okFileExtensions) {
            if (file.getName().toLowerCase().endsWith(extension)) {
                return true;
            }
        }
        return false;
    }
}