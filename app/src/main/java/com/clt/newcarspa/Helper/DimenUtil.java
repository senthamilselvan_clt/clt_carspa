package com.clt.newcarspa.Helper;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;

import com.clt.newcarspa.R;

/**
 * Created by mohammed on 7/11/15.
 */
public class DimenUtil {

    public static final int DEFAULT_ACTION_BAR_HEIGHT_DP = 56;

    public static float SpToPixels(Context context, float sp) {
        float scaledDensity = context.getResources().getDisplayMetrics().scaledDensity;
        return sp*scaledDensity;
    }

    public static float convertDpToPixel(float dp, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return px;
    }

    /***
     * Returns the drawer width and drawer header height.
     * @param context
     * @return 1st is drawer width in pixels, 2nd drawer header height
     */
    public static int[] drawerWidthAndHeaderHt(Context context)  {
        int screenWidth = context.getResources().getDisplayMetrics().widthPixels;
        // Calculate ActionBar height
        TypedValue tv = new TypedValue();
        int actionBarHeight, defaultABHt;
        defaultABHt = actionBarHeight = (int) DimenUtil.convertDpToPixel( (float)DimenUtil.DEFAULT_ACTION_BAR_HEIGHT_DP, context);
        if (context.getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true))
        {
            actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data, context.getResources().getDisplayMetrics());
        }
        int navDrawerMaxWidth = (int) convertDpToPixel(320,context);
        boolean tabletSize = context.getResources().getBoolean(R.bool.isTablet);
        if (tabletSize) {
            // do something
            navDrawerMaxWidth = (int) convertDpToPixel(400,context);
        } else {
            // do something else
        }
        Log.d("Drawer","pixels - actionBarHeight : "+actionBarHeight+" defaultABHt : "+defaultABHt + " navDrawerMaxWidth = "+navDrawerMaxWidth);
        int actionBarHeightMax = Math.max(actionBarHeight, defaultABHt);

        int drawerWidth = screenWidth - actionBarHeightMax;
        drawerWidth = Math.min(drawerWidth, navDrawerMaxWidth);
        int drawerHeaderHeight = drawerWidth * 9/16;
        int ret [] = { drawerWidth, drawerHeaderHeight};
        Log.d("Drawer","pixels - drawerWidth = "+drawerWidth+" drawerHeaderHeight = "+drawerHeaderHeight +" actionBarHeightMax = "+actionBarHeightMax);
        return ret;
    }

    public static String getDeviceDimen(Activity activity) {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;
        int width = displaymetrics.widthPixels;
        return width+"x"+height;
    }

    public static float getScreenDensity(Context context) {
        return context.getResources().getDisplayMetrics().density;
    }
}
